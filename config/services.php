<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'techgrowth.com.co',
        'secret' => 'key-441abc145b16a4b04f447bc6a4704444',
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    //Checho Hernandez, Support al4cdeveloper@gmail.com
    'facebook' => [
    'client_id' => '365576190515221',
    'client_secret' => '965a59c301c11f867ddce4f72776a6a0',
    'redirect' => 'https://localhost:8000/auth/facebook/callback'
    ],
    //desarrollo@rutascolombia.com
    'google' => [
    'client_id' => '482362095160-uevacukemut8jdivl3am4v819l5o5gsh.apps.googleusercontent.com',
    'client_secret' => 'M3vtjoUf74JWSxnBs1wRVvor',
    'redirect' => 'http://localhost:8000/auth/google/callback'
    ]

];
