<?php

use Illuminate\Database\Seeder;
use App\Models\EcosystemCategory;
use App\Models\ServiceCategory;
use App\Models\Service;

class ResourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	EcosystemCategory::create(['ecosystem_category'=>'Agua']);
    	EcosystemCategory::create(['ecosystem_category'=>'Tierra']);
    	EcosystemCategory::create(['ecosystem_category'=>'Aire']);

    	ServiceCategory::create(['service_category'=>'Aventura']);

		Service::create(['service'=>'Torrentismo', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=>'Rafting', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=>'Buceo ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=>'Pesca Deportiva ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=>'Balsaje ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=>'Bote ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=>'Kayaking ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=> 'Jet Sky ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		Service::create(['service'=> 'Surf ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 1
		]);
		
		Service::create(['service'=> 'Parapente ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Ala Delta ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Paracaidismo', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Globo', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Ultraliviano', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Canopy', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Tirolesa', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Tirolina ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 3
		]);
		Service::create(['service'=> 'Tirolina ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Rappel ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Trekking ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Alta montaña ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Espeleología ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Bungee Jumping  ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Escalada  ', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Espeleísmo', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Excursiones 4x4', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
		Service::create(['service'=> 'Cabalgata', 
			'type_service'=>'Alto impacto',
			'description'=>'',
			'fk_service_category'=> 1,
			'fk_ecosystem_category'=> 2
		]);
    }
}
