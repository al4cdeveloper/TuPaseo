<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalityImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipality_images', function (Blueprint $table) {
            $table->increments('id_municipality_image');
            $table->mediumText('link_image');
            $table->integer('fk_municipality')->unsigned();
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipality_images');
    }
}
