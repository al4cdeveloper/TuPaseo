<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id_order_product');
            $table->integer('fk_order')->unsigned();
            $table->foreign('fk_order')->references('id_order')->on('orders');
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service_operator')->on('service_operators');
            $table->integer('units');
            $table->double('unit_price', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
