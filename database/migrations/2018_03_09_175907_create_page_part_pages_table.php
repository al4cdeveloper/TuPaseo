<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagePartPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_part_pages', function (Blueprint $table) {
            $table->increments('id_part_page');
            $table->integer('fk_page')->unsigned();
            $table->foreign('fk_page')->references('id_page')->on('pages'); 
            $table->integer('fk_part')->unsigned();
            $table->foreign('fk_part')->references('id_part')->on('page_parts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_part_pages');
    }
}
