<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id_schedule');
            
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service_operator')->on('service_operators');
            $table->string('day');
            $table->time('hora_inicio')->nullable();
            $table->time('hora_receso')->nullable();
            $table->integer('tiempo_receso')->nullable();
            $table->time('hora_final')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
