<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_operators', function (Blueprint $table) {
            $table->increments('id_service_operator');
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service')->on('services'); 
            $table->integer('fk_operator')->unsigned();
            $table->foreign('fk_operator')->references('id_operator')->on('operators');
            $table->string('service_name')->nullable();
            $table->string('cost')->nullable();
            $table->string('address')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->integer('capacity')->nullable();
            $table->integer('duration')->nullable();
            $table->string('video')->nullable();
            $table->text('requisites')->nullable();
            $table->text('description')->nullable();
            $table->text('policies')->nullable();
            $table->integer('fk_municipality');
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->string('fk_interest_site')->nullable();
            $table->string('state')->default('Inhabilitado');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_operators');
    }
}
