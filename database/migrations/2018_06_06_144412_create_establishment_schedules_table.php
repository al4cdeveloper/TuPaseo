<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment_schedules', function (Blueprint $table) {
            $table->increments('id_establishment_schedule');
            $table->string('name');
            $table->string('start',15)->nullable();
            $table->string('end',15)->nullable();
            $table->boolean('inactive')->default(false);
            $table->integer('fk_establishment')->unsigned();
            $table->foreign('fk_establishment')->references('id_establishment')->on('establishments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishment_schedules');
    }
}
