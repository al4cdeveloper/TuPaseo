<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operators', function (Blueprint $table) {
            $table->increments('id_operator');
            $table->string('name');
            $table->string('national_register');
            $table->string('contact_personal');
            $table->string('contact_phone');
            $table->string('email')->unique();
            $table->string('service_category');
            $table->string('web');
            $table->string('nit')->nullable();
            $table->string('address')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->integer('days_for_reservation')->default(2);
            $table->date('terms');
            $table->date('data_protection');
            $table->date('sustainability');
            $table->boolean('promotionals_mails')->default(0);
            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('type_account')->nullable();
            $table->string('avatar')->nullable();
            $table->string('role')->default("operator");
            $table->string('password');
            $table->string('status')->default('active');
            $table->string('confirmation_code')->nullable();
            $table->boolean('hotel_panel')->default(0);
            $table->boolean('restaurant_panel')->default(0);
            $table->boolean('service_panel')->default(0);
            $table->integer('commission');
            $table->string('option_iva');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('operators');
    }
}
