<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_orders', function (Blueprint $table) {

            $table->integer('orders_id')->unsigned()->index();
            $table->string('email');
            $table->string('type_dni');
            $table->string('dni');
            $table->string('first_name');
            $table->string('last_name');
            $table->mediumText('direccion');
            $table->string('country');
            $table->string('region');
            $table->string('city');
            $table->string('phone');
            $table->string('phone_2')->nullable();
            $table->timestamps();

            $table->foreign('orders_id')->references('id_order')->on('orders');
            $table->primary(['orders_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_orders');
    }
}
