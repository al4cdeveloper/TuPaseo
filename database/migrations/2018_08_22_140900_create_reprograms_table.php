<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReprogramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reprograms', function (Blueprint $table) {
            $table->increments('id_reprogram');
            $table->integer('fk_reservation')->unsigned();
            $table->foreign('fk_reservation')->references('id_reservation')->on('reservations');
            $table->date('previous_date');
            $table->time('previous_time');
            $table->date('new_date');
            $table->time('new_time');
            $table->text('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reprograms');
    }
}
