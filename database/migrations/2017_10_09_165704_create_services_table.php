<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id_service');
            $table->string('service');
            $table->string('type_service');
            $table->text('description');
            $table->string('image');
            $table->string('map_ico');
            $table->string('icon_class');
            $table->string('video_url')->nullable();
            $table->integer('fk_service_category')->unsigned();
            $table->foreign('fk_service_category')->references('id_category')->on('service_categories');
            $table->integer('fk_ecosystem_category')->unsigned();
            $table->foreign('fk_ecosystem_category')->references('id_ecosystem_category')->on('ecosystem_categories');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
