<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_pictures', function (Blueprint $table) {
            $table->increments('id_picture');
            $table->mediumText('link_image');
            $table->text('description')->nullable();
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service_operator')->on('service_operators');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_pictures');
    }
}
