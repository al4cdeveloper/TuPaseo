<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_items', function (Blueprint $table) {
            $table->increments('id_product_item');
            $table->integer('fk_order_product')->unsigned();
            $table->foreign('fk_order_product')->references('id_order_product')->on('order_products');
            $table->integer('fk_service_item')->unsigned();
            $table->foreign('fk_service_item')->references('id_service_item')->on('service_items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_items');
    }
}
