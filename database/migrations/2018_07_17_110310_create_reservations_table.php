<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id_reservation');
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service_operator')->on('service_operators');
            $table->integer('fk_user')->unsigned();
            $table->foreign('fk_user')->references('id')->on('users');
            $table->date('date');
            $table->time('time');
            $table->string('state');
            $table->integer('cuantity');
            $table->integer('fk_order_product')->unsigned();

            $table->foreign('fk_order_product')->references('id_order_product')->on('order_products');

            $table->text('comment')->nullable();
            $table->float('calification')->nullable();
            $table->tinyInteger('paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
