<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id_room');
            $table->string('room');
            $table->integer('cuantity');
            $table->string('capacity');
            $table->string('bed');
            $table->boolean('suite');
            $table->integer('fk_establishment')->unsigned();
            $table->foreign('fk_establishment')->references('id_establishment')->on('establishments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
