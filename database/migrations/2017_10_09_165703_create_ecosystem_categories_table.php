<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcosystemCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecosystem_categories', function (Blueprint $table) {
            $table->increments('id_ecosystem_category');
            $table->string('ecosystem_category');
            $table->string('subtitle')->nullable();
            $table->string('slug');
            $table->string('link_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecosystem_categories');
    }
}
