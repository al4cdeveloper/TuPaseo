<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishments', function (Blueprint $table) {
            $table->increments('id_establishment');
            $table->string('type_establishment');
            $table->string('establishment_name');
            $table->integer('phone');
            $table->string('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('website')->nullable();
            $table->string('video')->nullable();
            $table->string('kitchen_type')->nullable();
            $table->string('slug');
            $table->integer('fk_operator')->unsigned();
            $table->foreign('fk_operator')->references('id_operator')->on('operators');
            $table->text('description');
            $table->string('state')->default('activo');
            $table->integer('fk_municipality')->unsigned();
            $table->foreign('fk_municipality')->references('id_municipality')->on('municipalities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishments');
    }
}
