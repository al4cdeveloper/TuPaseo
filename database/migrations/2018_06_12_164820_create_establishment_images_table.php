<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment_images', function (Blueprint $table) {
            $table->increments('id_establishment_image');
            $table->mediumText('link_image');            
            $table->text('description')->nullable();
            $table->integer('fk_establishment')->unsigned();
            $table->foreign('fk_establishment')->references('id_establishment')->on('establishments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishment_images');
    }
}
