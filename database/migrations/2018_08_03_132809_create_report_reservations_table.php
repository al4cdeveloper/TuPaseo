<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_reservations', function (Blueprint $table) {
            $table->increments('id_report_reservation');
            $table->integer('fk_report')->unsigned();
            $table->foreign('fk_report')->references('id_report')->on('pay_reports');
            $table->integer('fk_reservation')->unsigned();
            $table->foreign('fk_reservation')->references('id_reservation')->on('reservations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_reservations');
    }
}
