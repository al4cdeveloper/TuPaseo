<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region_images', function (Blueprint $table) {
            $table->increments('id_region_image');
            $table->mediumText('link_image');
            $table->integer('fk_region')->unsigned();
            $table->foreign('fk_region')->references('id_region')->on('regions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region_images');
    }
}
