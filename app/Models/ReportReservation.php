<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportReservation extends Model
{
	protected $primaryKey='id_report_reservation';

	public function Report()
	{
		return $this->belongsTo(PayReport::class,'fk_report');
	}
}
