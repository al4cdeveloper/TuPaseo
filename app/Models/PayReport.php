<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayReport extends Model
{
	protected $primaryKey = 'id_report';
	protected $fillable = ['state','date_start','date_finish'];

	public function reservations()
	{
		return $this->belongsToMany(Reservation::class,'report_reservations','fk_report','fk_reservation');
	}
}
