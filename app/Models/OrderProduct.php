<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
	protected $primaryKey = "id_order_product";
	protected $fillable = ['fk_order','fk_service','units','unit_price'];

	public function service()
	{
		return $this->belongsTo(ServiceOperator::class,'fk_service','id_service_operator');
	}
	public function items()
	{
		return $this->belongsToMany(ServiceItem::class,'order_product_items','fk_order_product','fk_service_item');
	}

	public function reservation()
	{
		return $this->hasOne(Reservation::class, 'fk_order_product');
	}

	public function order()
	{
		return $this->belongsTo(Order::class,'fk_order');
	}

}
