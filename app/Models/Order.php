<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $primaryKey="id_order";
	protected $fillable = ['fk_user','estado','tipo_pago','descripcion','id_payu','costo'];

	public function shipping_order(){
		return $this->hasOne(ShippingOrder::class, 'orders_id');
	}
	public function directProducts()
	{
		return $this->belongsToMany(ServiceOperator::class, 'order_products','fk_order','fk_service')->withPivot('units', 'unit_price');
	}
	public function services()
	{
		return $this->hasMany(OrderProduct::class,'fk_order')->with('reservation');
	}

	public function customer()
	{
		return $this->belongsTo('App\User','fk_user');
	}

}
