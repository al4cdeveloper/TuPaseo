<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstablishmentSchedule extends Model
{
	protected $primaryKey = "id_establishment_schedule";
}
