<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class InterestSiteCategory extends Model
{

    use Sluggable;
	
	protected $primaryKey = "id_category";
	protected $fillable = ['category_name','default_image','default_icon','state','slug'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'category_name'
            ]
        ];
    }	
}
