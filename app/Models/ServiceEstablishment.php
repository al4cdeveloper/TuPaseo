<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceEstablishment extends Model
{
	protected $primaryKey = 'id_service_establishment';

	protected $fillable = ['id_service_establishment','service_name'];
}
