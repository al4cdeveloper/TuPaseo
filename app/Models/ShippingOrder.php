<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingOrder extends Model
{
	protected $primaryKey = 'orders_id';
	protected $fillable = ['email','dni','first_name','last_name','direccion','country','province','phone','phone_2'];
}
