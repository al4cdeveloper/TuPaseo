<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Department extends Model
{
	use Sluggable;
	
	protected $primaryKey = "id_department";
	protected $fillable = ['department_name','fk_region','short_description','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'department_name'
            ]
        ];
    }

    public function Region()
    {
        return $this->hasOne(Region::class,'id_region','fk_region');
    }

    public function Municipalities()
    {
        return $this->hasMany(Municipality::class,'fk_department','id_department')->where('state','activo');
    }

    public function TreeMunicipalitiesFront()
    {
        return $this->hasMany(Municipality::class,'fk_department','id_department')->where('state','activo')->orWhere('front_state_hotel','activo')->orWhere('front_state_restaurant','activo');
    }
}
