<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PagePart extends Model
{
	use Sluggable;

	protected $primaryKey = "id_part";
	protected $fillable = ['part_name','type','slug'];
	
	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'part_name'
            ]
        ];
    }

    public function Page()
    {
        return $this->belongsTomany('App\Models\Page','page_part_pages','fk_part','fk_page');
    }
}
