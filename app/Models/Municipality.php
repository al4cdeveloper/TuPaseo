<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Municipality extends Model
{
    use Sluggable;


	protected $primaryKey = 'id_municipality';

	protected $fillable = ['id_municipality','municipality_name','description','multimedia_type','latitude','longitude','slug','link_image','type_last_user','fk_last_edition','fk_department','state','front_state','front_state_restaurant','front_state_hotel'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'municipality_name'
            ]
        ];
    }	

    public function Department()
    {
    	return $this->hasOne('App\Models\Department','id_department','fk_department');
    }

    public function getPertinentRegionAttribute($region)
    {
        $department = $this->department;
        $region = $department->Region;
        return $region;
    }

    public function images()
    {
        return $this->hasMany('App\Models\MunicipalityImage', 'fk_municipality');
    }

    public function InterestSite()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality');
    }
    public function Video()
    {
        return $this->hasMany('App\Models\Video','fk_relation','id_municipality')->where('type_relation','municipality');
    }

    public function Plans()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_municipality','id_municipality')->where('state','activo');
    }

    public function Hotels()
    {
        return $this->hasMany(Establishment::class,'fk_municipality','id_municipality')->where('type_establishment','hotel')->where('state','activo');
    }

    public function twoRandomHotels()
    {
        return $this->hasMany(Establishment::class,'fk_municipality','id_municipality')->where('type_establishment','hotel')->where('state','activo')->inRandomOrder()->take(2);
    }

    public function Restaurants()
    {
        return $this->hasMany(Establishment::class,'fk_municipality','id_municipality')->where('type_establishment','restaurant')->where('state','activo');
    }

    public function twoRandomRestaurants()
    {
        return $this->hasMany(Establishment::class,'fk_municipality','id_municipality')->where('type_establishment','restaurant')->where('state','activo')->inRandomOrder()->take(2);
    }

    public function SixPlans()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_municipality','id_municipality')->where('state','activo')->inRandomOrder()->take(6);
    }

    public function KeyData()
    {
        return $this->hasMany(Keydata::class,'fk_municipality','id_municipality');
    }

}
