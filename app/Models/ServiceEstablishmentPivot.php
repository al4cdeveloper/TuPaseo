<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceEstablishmentPivot extends Model
{
	protected $primaryKey= 'id_service_establishment_pivot';
}
