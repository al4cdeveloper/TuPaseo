<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Service extends Model
{
    use Sluggable;
    
	protected $primaryKey = "id_service";
	protected $fillable = ['id_service','service','type_service','description','image','map_ico','icon_class','slug','video_url','fk_service_category','fk_ecosystem_category'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'service'
            ]
        ];
    }

	public function users()
    {
        return $this->belongsToMany('App\Operator', 'service_operators', 'fk_service','fk_operator');
    }

    public function ecosystem()
    {
    	return $this->belongsTo(EcosystemCategory::class,'fk_ecosystem_category');
    }

    public function Category()
    {
        return $this->belongsTo(ServiceCategory::class,'fk_service_category');
    }

    public function ActiveServices()
    {
        return $this->hasMany(ServiceOperator::class,'fk_service')->where('state','activo');
    }
}
