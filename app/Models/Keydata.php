<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keydata extends Model
{
	protected $primaryKey = 'id_keydata';
}
