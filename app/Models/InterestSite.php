<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class InterestSite extends Model
{
	use Sluggable;

	protected $primaryKey = "id_site";
	protected $fillable = ['site_name','fk_category','fk_municipality','address','phone','web','multimedia_type','latitude','longitude','slug','link_image','link_icon','keywords','description','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'site_name'
            ]
        ];
    }

    public function Category()
    {
    	return $this->hasOne('App\Models\InterestSiteCategory','id_category','fk_category');
    }

    public function Municipality()
    {
    	return $this->hasOne('App\Models\Municipality','id_municipality','fk_municipality');
    }

    public function images()
    {
        return $this->hasMany('App\Models\InterestSiteImage', 'fk_site');
    }

    public function Operators()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_location','id_municipality')->where('type_relation','InterestSite');
    }
}
