<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagePartPage extends Model
{
	protected $primaryKey = 'id_part_page';

	public function Pattern()
	{
		return $this->belongsToMany(Pattern::class,'pattern_parts','fk_pagepart','fk_pattern')->withPivot('id_pattern_part','fk_pattern','fk_pagepart','state');
	}

	public function Page()
	{
		return $this->belongsTo(Page::class,'fk_page','id_page');
	}

	public function Part()
	{
		return $this->belongsTo(PagePart::class,'fk_part','id_part');
	}
}
