<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plate extends Model
{
	protected $primaryKey = "id_plate";
	protected $fillable = ['id_plate','plate','cost','category','fk_establishment'];
}
