<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ServiceOperator extends Model
{

	use Sluggable;

	protected $primaryKey = "id_service_operator";
	protected $fillable = ['fk_service','fk_operator','service_name','cost','video','address','latitude','longitude','capacity','duration','requisites','description','policies','fk_municipality','fk_interest_site','state','slug'];

	public function PrincipalService()
	{
		return $this->hasOne('App\Models\Service','id_service','fk_service');
	}

	public function images(){
		return $this->hasMany('App\Models\ServicePicture', 'fk_service');
	}
	public function operador()
	{
		return $this->hasOne('App\Operator','id_operator','fk_operator');
	}

	public function Schedule()
	{
		return $this->hasMany('App\Models\Schedule','fk_service');
	}

	public function reservations()
	{
		return $this->hasMany('App\Models\Reservation','fk_service','id_service_operator');
	}

	public function items()
	{
		return $this->hasMany(ServiceItem::class,'fk_service');
	}

	public function Municipality()
	{
		return $this->belongsTo('App\Models\Municipality','fk_municipality','id_municipality');
	}

	public function interestSite()
	{
		return $this->belongsTo('App\Models\InterestSite','fk_interest_site','id_site');
	}


	public function sluggable()
    {
        return array(
            'slug' => [
                'source' => ['service_name'],
            ]
        );
    }

}
