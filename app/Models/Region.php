<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Region extends Model
{
	use Sluggable;


	protected $primaryKey="id_region";
	protected $fillable=['id_region','region_name','sentence','description','link_image','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'region_name'
            ]
        ];
    }
    public function images()
    {
        return $this->hasMany('App\Models\RegionImage', 'fk_region');
    }

    public function departments()
    {
        return $this->hasMany(Department::class,'fk_region');
    }

    public function FiveDepartmentsFront()
    {
        return $this->hasMany(Department::class,'fk_region')->inRandomOrder()->take(5);
    }
}
