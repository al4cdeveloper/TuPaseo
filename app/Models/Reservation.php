<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
	protected $primaryKey="id_reservation";

	public function Service()
	{
		return $this->belongsTo('App\Models\ServiceOperator','fk_service','id_service_operator');
	}

	public function user()
	{
		return $this->hasOne('App\User','id','fk_user');
	}

	public function orderProduct()
	{
		return $this->belongsTo(OrderProduct::class,'fk_order_product');
	}

	public function reportReservation()
	{
		return $this->belongsTo(ReportReservation::class,'id_reservation','fk_reservation');
	}

	public function Reprograms()
	{
		return $this->hasMany(Reprogram::class,'fk_reservation')->orderBy('id_reprogram','desc');
	}
}