<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class EcosystemCategory extends Model
{
	use Sluggable;
	protected $primaryKey="id_ecosystem_category";

	protected $fillable = ['subtitle','ecosystem_category', 'slug','link_image'];
	
	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'ecosystem_category'
            ]
        ];
    }

    public function Activities()
    {
        return $this->hasMany(Service::class,'fk_ecosystem_category');
    }
}
