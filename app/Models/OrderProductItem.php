<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProductItem extends Model
{
	protected $primaryKey='id_product_item';
	protected $fillable = ['fk_order_product','fk_service_item'];
}
