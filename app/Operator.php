<?php

namespace App;

use App\Notifications\OperatorResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Operator extends Authenticatable
{
    use Notifiable;
    protected $primaryKey='id_operator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'national_register','contact_personal','contact_phone','email','service_category','web','terms','data_protection','sustainability','promotionals_mails','role' ,'password','status','confirmation_code','hotel_panel','restaurant_panel','service_panel'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new OperatorResetPassword($token));
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'service_operators','fk_operator', 'fk_service')->withPivot('id_service_operator','address','cost','capacity','duration','requisites','description','fk_municipality','state','fk_interest_site');
    }
//https://laravel.com/docs/5.5/eloquent-relationships#has-many-through
    public function directReservations()
    {
        return $this->hasManyThrough('App\Models\Reservation','App\Models\ServiceOperator','fk_operator','fk_service');
    }

    public function ReservationsToDo()
    {
        return $this->hasManyThrough('App\Models\Reservation','App\Models\ServiceOperator','fk_operator','fk_service')->where('reservations.state','pagado');
    }

    public function ReservationsMade()
    {
        return $this->hasManyThrough('App\Models\Reservation','App\Models\ServiceOperator','fk_operator','fk_service')->where('reservations.state','Realizada')->orWhere('reservations.state','Calificada');
    }

    public function directPayReservations()
    {
        return $this->hasManyThrough('App\Models\Reservation','App\Models\ServiceOperator','fk_operator','fk_service')->where('reservations.paid',1)->with('reportReservation');
    }

    public function restaurants()
    {
        return $this->hasMany(Models\Establishment::class,'fk_operator')->where('type_establishment','restaurant');
    }

    public function hotels()
    {
        return $this->hasMany(Models\Establishment::class,'fk_operator')->where('type_establishment','hotel');
    }

    public function directOption()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_operator','id_operator')->with('reservations');
    }

    public function directService()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_operator');
    }
}
