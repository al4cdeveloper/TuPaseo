<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\Rule;
use App\Models\Reservation;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Contact;
use Session;
use Cart;
use Mail;
use App\Mail\ContactUser;
use App\User;
use App\Models\Service;
use App\Models\Poll;


class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginCart(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) 
        {
            $user = Auth::user();
            
            return redirect('viewcart');
        }
        else
        {
            Session::flash('message-error','Hubo un error en los datos ingresados, por favor intentelo nuevamente');
            return redirect("/login")->with('cart', 'sinde cart');
        }
    }

    public function registerCart(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'email'      => 'required|email|max:255|unique:users|unique:operators',
            ]);

        if ($validator->fails()) 
        {
            Session::flash('message-error','Al parecer tu correo ya está registrado, por favor intentalo nuevamente');
            return redirect("/login")->with('cart', 'sinde cart');
        } 
        else 
        {
            $user = new User;
            $user->first_name  =$request->first_name;
            $user->last_name  =$request->last_name;
            $user->email  =$request->email;
            $user->address  =$request->address;
            $user->phone  =$request->phone;
            $user->password = bcrypt($request->password);
            $user->save();

            $user = Auth::attempt(['email' => $request->email, 'password' => $request->password]);

            return redirect('viewcart');
        }
    }

    public function profile()
    {
        $user = Auth::user();
        $regions = [];
        $cities = [];
        $url ='https://battuta.medunes.net/api/country/all/?key=00000000000000000000000000000000'; 
        $ch = curl_init($url); 
        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
        CURLOPT_HTTPHEADER => array('Accept: application/json'), 
        CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        curl_setopt_array( $ch, $options );     
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        // Cerrar el recurso cURL y liberar recursos del sistema 
        curl_close($ch);   

        $countries = json_decode($response, true); 
        if($user->country)
        {
            $url ="https://battuta.medunes.net/api/region/$user->country/all/?key=00000000000000000000000000000000"; 
            $ch = curl_init($url); 
            // Configuring curl options 
            $options = array( 
                CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
            ); 
            // Setting curl options 
            curl_setopt_array( $ch, $options );     
            // Getting results 
            $response = curl_exec($ch); // Getting jSON result string   
            // Cerrar el recurso cURL y liberar recursos del sistema 
            curl_close($ch);   
            $regions = json_decode($response, true); 
        }
        else
        {
            $url ='https://battuta.medunes.net/api/region/co/all/?key=00000000000000000000000000000000'; 
            $ch = curl_init($url); 
            // Configuring curl options 
            $options = array( 
                CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
            ); 
            // Setting curl options 
            curl_setopt_array( $ch, $options );     
            // Getting results 
            $response = curl_exec($ch); // Getting jSON result string   
            // Cerrar el recurso cURL y liberar recursos del sistema 
            curl_close($ch);   
            $regions = json_decode($response, true); 
        }

        if($user->region)
        {
            $region = str_replace(' ','%20',$user->region);
            $url ="https://battuta.medunes.net/api/city/$user->country/search/?region=$region&key=00000000000000000000000000000000"; 
            $ch = curl_init($url); 
            // Configuring curl options 
            $options = array( 
                CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
            ); 
            // Setting curl options 
            curl_setopt_array( $ch, $options );     
            // Getting results 
            $response = curl_exec($ch); // Getting jSON result string   
            // Cerrar el recurso cURL y liberar recursos del sistema 
            curl_close($ch);   
            $cities = json_decode($response, true); 
        }

        return view('User.profile',compact('user','countries','regions','cities'));
    }

    public function updateprofile(Request $request)
    {
        $user     = User::find(Auth::user()->id);
        if($request->password)
        {
            $validator = \Validator::make($request->all(), [
             'first_name' => 'required|max:255',
             'last_name' => 'required|max:255',
             'phone'  => 'required|max:255',
             'email'      => ['required', 'email', Rule::unique('users')->ignore($user->id),Rule::unique('operators')],
             'password'   => 'required|min:6|confirmed',
            ]); 
        }
        else
        {
            $validator = \Validator::make($request->all(), [
             'first_name' => 'required|max:255',
             'last_name' => 'required|max:255',
             'phone'  => 'required|max:255',
             'email'      => ['required', 'email', Rule::unique('users')->ignore($user->id),Rule::unique('operators')]
            ]); 
        }


        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $files = $request->file('file');

            $user->first_name  =$request->first_name;
            $user->last_name  =$request->last_name;
            $user->email  =$request->email;
            $user->address  =$request->address;
            $user->email  =$request->email;
            $user->phone  =$request->phone;
            $user->country = $request->country;
            $user->region = $request->region;
            $user->city = $request->city;
            $user->promotionals_mails = $request->promotionals_mails;
            if($request->password)              $user->password = bcrypt($request->password);
            if($files)
            {
                if($user->avatar!="")
                {
                    chmod(public_path().'/images/profile',0777);
                    fopen(public_path().$user->avatar, 'wb');
                    unlink(public_path().$user->avatar);
                }
                $path = public_path().'/images/profile';

                $nombre ="";


                $filename = 'Profile_'.date('YmdHis', time()).rand().'.'.$files->getClientOriginalName();
                $nombre = '/images/profile/'.$filename;
                $files->move($path, $filename);

                $user->avatar=$nombre;
            }
            $user->save();

            Session::flash('message', 'Se ha realizado la actualización de información.');
            return redirect('userprofile');

        }
    }

    public function publicTerms()
    {
        return view('legalFiles.generalTerms');
    }

    public function operatorTerms()
    {
        return view('legalFiles.operatorTerms');
    }

    public function policyDataProtection()
    {
        return view('legalFiles.policyDataProtection');
    }
    
    public function policySostenibility()
    {
        return view('legalFiles.policySostenibility');
    }

    public function about()
    {
        return view('legalFiles.about');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $regions = [];
        $cities = [];
        $url ='https://battuta.medunes.net/api/country/all/?key=00000000000000000000000000000000'; 
        $ch = curl_init($url); 
        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
        CURLOPT_HTTPHEADER => array('Accept: application/json'), 
        CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        curl_setopt_array( $ch, $options );     
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        // Cerrar el recurso cURL y liberar recursos del sistema 
        curl_close($ch);   

        $countries = json_decode($response, true); 

        $url ='https://battuta.medunes.net/api/region/co/all/?key=00000000000000000000000000000000'; 
        $ch = curl_init($url); 
        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
        CURLOPT_HTTPHEADER => array('Accept: application/json'), 
        CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        curl_setopt_array( $ch, $options );     
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        // Cerrar el recurso cURL y liberar recursos del sistema 
        curl_close($ch);   
        $regions = json_decode($response, true); 

        return view('User.contact',compact('countries','regions'));
    }

    public function sendQuestions(Request $request)
    {
        $contact = new Poll;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->city = $request->city;
        if($request->observations!=null)
        $contact->observations = $request->observations;
        $cadena = "";
        foreach($request->services as $service)
        {
            $servicio = Service::find($service);
            $cadena.=$servicio->service.", ";
        }
        $contact->activities = $cadena;
        $contact->state = "new";
        $contact->save();
        $message  = "Hemos recibido tu mensaje, dentro de poco nos pondremos en contacto contigo.";
        return view('notifation',compact('message'));
    }

    public function saveEmail(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email'      => 'required|email',
            'phone'      => 'required',
            'country'      => 'required',
            'region'      => 'required',
            'city'      => 'required',
            'message_contact'      => 'required',
            'g-recaptcha-response'      => 'required',
            ]);

        if ($validator->fails())
        {
            Session::flash('message-error', 'Debes llenar en su totalidad el formulario');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->message = $request->message_contact;
        $contact->state = "new";
        $contact->country = $request->country;
        $contact->region = $request->region;
        $contact->city = $request->city;
        $contact->save();

        Mail::to('info@tupaseo.travel')
                ->send(new ContactUser($contact));

        return redirect('/contact?notify=ok');
    }

    public function mailchimpRegister(Request $request)
    {
        $exists = \Mailchimp::checkStatus('902fc26e33', $request->email);
        if ($exists == 'not found') {
            $register = \Mailchimp::subscribe('902fc26e33', $request->email, ['FNAME' => 'Sin Nombre', 'LNAME' => 'Sin Apellido'], true);

            \Session::flash('message', 'Su cuenta ha sido inscrita con nosotros, gracias por preferirnos');
        } else {
            \Session::flash('message-error', 'Su cuenta ya se encuentra inscrita con nosotros');
        }

        return redirect('/');
    }
}
