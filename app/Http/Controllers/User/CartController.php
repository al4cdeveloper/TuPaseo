<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceOperator;
use App\Models\ShippingOrder;
use App\Models\ServiceItem;
use App\Models\OrderProduct;
use App\Models\OrderProductItem;
use App\Models\Order;
use App\Models\Reservation;
use App\Mail\NotifyPay;
use App\Mail\Rejected;
use Cart;
use Auth;
use Session;
use Mail;
use Cookie;
use Carbon\Carbon;



//documentacion de paises 
//https://battuta.medunes.net/
//https://codepen.io/medunes/pen/GWoojz
class CartController extends Controller
{

    private function startPayU()
    {
        /*

         \Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi"); 
         // URL de Consultas
         \Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
         // URL de Suscripciones para Pagos Recurrentes
         \Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.3/"); 
        */

        // URL DE PAGOS (NO PRUEBAS)
        \Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi"); 
        \Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi");
        \Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.3/");


        /*

        \PayU::$apiKey = "C1XW5Gm23d9MCLiH1Rfn9y01Ks"; //Ingrese aquí su propio apiKey.
        \PayU::$apiLogin = "D8Fs01bjipCW8d5"; //Ingrese aquí su propio apiLogin.
        \PayU::$merchantId = "667200"; //Ingrese aquí su Id de Comercio.
        \PayU::$language = \SupportedLanguages::ES; //Seleccione el idioma.
        \PayU::$isTest = true; //Dejarlo True cuando sean pruebas.
        

        */



        \PayU::$apiKey = env('PAYU_API_KEY'); //Ingrese aquí su propio apiKey.
        \PayU::$apiLogin = env('PAYU_API_LOGIN'); //Ingrese aquí su propio apiLogin.
        \PayU::$merchantId = env('PAYU_MERCHANT_ID'); //Ingrese aquí su Id de Comercio.
        \PayU::$language = \SupportedLanguages::ES; //Seleccione el idioma.
        \PayU::$isTest = env('PAYU_ON_TESTING'); //Dejarlo True cuando sean pruebas.

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_cart($id,Request $request)
    {
        $service = ServiceOperator::find($id);
        $image = '';
        if(count($service->images)>0)
        {
            $image = $service->images->first()->link_image;
        }
        else
           $image = 'front/img/slides/rafting.jpg';

        $wishlist = Cart::instance('shopping')->add($id, $request->text, $request->cuantity, $request->priceforperson,['date'=>$request->date,'time'=>$request->time,'aditionalItems'=>explode(",", substr($request->aditionalItems, 0,-1)), 'image_link'=>$image,'totalAllPersons'=>$request->total])->associate('ServiceOperator');
        return "ok";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allcart()
    {
        $cart = Cart::instance('shopping')->content();
        return ['cart'=>$cart,'count'=>count($cart)];
    }

    public function viewCart()
    {
        $cart = Cart::instance('shopping')->content();
        $user = Auth::user();

        if($cart->count() >0)
        {
            $subtotal = Cart::instance('shopping')->subtotal(0,'.', '');
            $regions = [];
            $cities = [];


            $this->startPayU();
   
            $parameters = array(
               //Ingrese aquí el identificador de la cuenta.
               \PayUParameters::PAYMENT_METHOD => "PSE",
               //Ingrese aquí el nombre del pais.
               \PayUParameters::COUNTRY => \PayUCountries::CO,
            );
            try
            {
                $banks=\PayUPayments::getPSEBanks($parameters);
            }
            catch(\PayUApiServiceUtil $e)
            {
                dd($e);
            }
            $metodosPago = [null => 'Selecciona un Método de Pago','tarjeta_credito'=>'Tarjeta de Crédito','pse'=>'PSE','efectivo'=>'Efectivo'];

            //countries

            $url ='https://battuta.medunes.net/api/country/all/?key=00000000000000000000000000000000'; 
            $ch = curl_init($url); 
            // Configuring curl options 
            $options = array( 
                CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
            ); 
            // Setting curl options 
            curl_setopt_array( $ch, $options );     
            // Getting results 
            $response = curl_exec($ch); // Getting jSON result string   
            // Cerrar el recurso cURL y liberar recursos del sistema 
            curl_close($ch);   

            $countries = json_decode($response, true); 

            if($user && $user->country)
            {
                $url ="https://battuta.medunes.net/api/region/$user->country/all/?key=00000000000000000000000000000000"; 
                $ch = curl_init($url); 
                // Configuring curl options 
                $options = array( 
                    CURLOPT_RETURNTRANSFER => true,     
                CURLOPT_HTTPHEADER => array('Accept: application/json'), 
                CURLOPT_SSL_VERIFYPEER => false,     
                ); 
                // Setting curl options 
                curl_setopt_array( $ch, $options );     
                // Getting results 
                $response = curl_exec($ch); // Getting jSON result string   
                // Cerrar el recurso cURL y liberar recursos del sistema 
                curl_close($ch);   
                $regions = json_decode($response, true); 
            }
            else
            {
                $url ='https://battuta.medunes.net/api/region/co/all/?key=00000000000000000000000000000000'; 
                $ch = curl_init($url); 
                // Configuring curl options 
                $options = array( 
                    CURLOPT_RETURNTRANSFER => true,     
                CURLOPT_HTTPHEADER => array('Accept: application/json'), 
                CURLOPT_SSL_VERIFYPEER => false,     
                ); 
                // Setting curl options 
                curl_setopt_array( $ch, $options );     
                // Getting results 
                $response = curl_exec($ch); // Getting jSON result string   
                // Cerrar el recurso cURL y liberar recursos del sistema 
                curl_close($ch);   
                $regions = json_decode($response, true); 
            }

            if($user &&  $user->region)
            {
                $region = str_replace(' ','%20',$user->region);
                $url ="https://battuta.medunes.net/api/city/$user->country/search/?region=$region&key=00000000000000000000000000000000"; 
                $ch = curl_init($url); 
                // Configuring curl options 
                $options = array( 
                    CURLOPT_RETURNTRANSFER => true,     
                CURLOPT_HTTPHEADER => array('Accept: application/json'), 
                CURLOPT_SSL_VERIFYPEER => false,     
                ); 
                // Setting curl options 
                curl_setopt_array( $ch, $options );     
                // Getting results 
                $response = curl_exec($ch); // Getting jSON result string   
                // Cerrar el recurso cURL y liberar recursos del sistema 
                curl_close($ch);   
                $cities = json_decode($response, true); 
            }

            $type_documents = [ ''=>'Seleccione tipo documento',
                                'CC'=>'Cédula de ciudadanía',
                                'CE'=>'Cédula de extranjería',
                                'PP'=>'Pasaporte',
                                'NIT'=>'NIT'];
            $type_person = ['N'=>'Persona Natural',
                            'J'=>'Persona Juridica'];
            
            return view('user.cart',compact('cart','subtotal','metodosPago','banks','cities','regions','countries','type_documents','type_person','user'));
        }
        else
            return redirect('/home');

    }

    public function payCart(Request $request)
    {
        if($request->hascity)
        {
            $validator = \Validator::make($request->all(), [
                    'dni'          => 'required',
                    'type_dni'     => 'required',
                    'first_name'      => 'required|max:150',
                    'last_name'    => 'required|max:150',
                    'country'    => 'required',
                    'region'    => 'required',
                    'city'      => 'required',
                    'email'        => 'required|email',
                    'address'    => 'required|max:40',
                    'phone'     => 'required|numeric',
                    'payment_type' => 'required',
                    'terms'        => 'required'
                    ]);
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                    'dni'          => 'required',
                    'type_dni'     => 'required',
                    'first_name'      => 'required|max:150',
                    'last_name'    => 'required|max:150',
                    'country'    => 'required',
                    'region'    => 'required',
                    'email'        => 'required|email',
                    'address'    => 'required|max:40',
                    'phone'     => 'required|numeric',
                    'payment_type' => 'required',
                    'terms'        => 'required'
                    ]);
        }


        if ($validator->fails())
        {
            Session::flash('message-error', 'Debes llenar en su totalidad el formulario');
            return redirect('viewcart#confirmpay')->withErrors($validator)->withInput();
        }

        if ($request->payment_type == 'tarjeta_credito')
        {
            $validatorCreditCard = \Validator::make($request->all(), [
                'type_card'          => 'required',
                'cardholderName'      => 'required|max:150',
                'cardNumber'    => 'required|digits:16',
                'month_exp'    => 'required',
                'cardExpirationYear'    => 'required|digits:4',
                'securityCode'        => 'required|digits:3,4',
                'docType'    => 'required',
                'docNumber'     => 'required',
                'numero_coutas' => 'required|digits:1,2',
                ]);

            if ($validatorCreditCard->fails())
            {

                Session::flash('message-error', 'Debes llenar en su totalidad el formulario');
                return redirect('viewcart#confirmpay')->withErrors($validatorCreditCard)->withInput();
            }

        }
        elseif($request->payment_type == 'pse')
        {
            $validatorPSE = \Validator::make($request->all(), [
                'identification_type' => 'required',
                'identification_number'      => 'required|max:150',
                'all_name'    => 'required|max:150',
                'bank'    => 'required',
                'type_person'    => 'required',
                ]);
            if($request->bank== "0")
            {
                $validatorPSE->errors()->add('bank', 'Debe elegir un banco');
            }
            if ($validatorPSE->errors()->count()>0)
            {

                Session::flash('message-error', 'Debes llenar en su totalidad el formulario');
                return redirect('viewcart#confirmpay')->withErrors($validatorPSE)->withInput();
            }
            else if ($validatorPSE->fails())
            {
                Session::flash('message-error', 'Debes llenar en su totalidad el formulario');
                return redirect('viewcart#confirmpay')->withErrors($validatorPSE)->withInput();
            }
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'identification_type' => 'required',
                'identification_number'      => 'required|max:150',
                'all_name'    => 'required|max:150',
                ]);

            if ($validator->fails())
            {
                Session::flash('message-error', 'Debes llenar en su totalidad el formulario');
                return redirect('viewcart#confirmpay')->withErrors($validator)->withInput();
            }
        }

            $user = Auth::user();
            $cart = Cart::instance('shopping')->content();
            $subtotal = Cart::instance('shopping')->subtotal(0,'.', '');


            $orden = Order::create([
                    'fk_user'  => $user->id,
                    'estado'    => 'pendiente',
                    'tipo_pago' => $request->payment_type,
                    'costo'     => $subtotal,
                    ]);


            $shippingOrder = new ShippingOrder;
            $shippingOrder->email = $request->email;
            $shippingOrder->type_dni = $request->type_dni;
            $shippingOrder->dni = $request->dni;
            $shippingOrder->first_name = $request->first_name;
            $shippingOrder->last_name = $request->last_name;
            $shippingOrder->direccion = $request->address;
            $shippingOrder->country = $request->country; 
            $shippingOrder->region = $request->region;
            $shippingOrder->city = $request->city;
            $shippingOrder->phone = $request->phone;
            $shippingOrder->orders_id = $orden->id_order;
            if($request->phone_2!='')
            {
                $shippingOrder->phone = $request->phone;
            }
            $shippingOrder->save();
            //Agregar reporte a lista de productos y realizar reservación
            foreach ($cart as $service)
            {
                $order_product = OrderProduct::create([
                            'fk_order'   => $orden->id_order,
                            'fk_service' => $service->id,
                            'units'      => $service->qty,
                            'unit_price' => $service->price,
                            ]);
                if(count($service->options->aditionalItems)>0)
                {
                    foreach($service->options->aditionalItems as $item)
                    { 
                        if($item!="")
                        {
                            $order_product_item = OrderProductItem::create([
                                'fk_order_product'   => $order_product->id_order_product,
                                'fk_service_item' => $item,
                                ]);
                        }
                    }
                }
                $date = date_create($service->options->date);
                $reservation = new Reservation;
                $reservation->fk_service = $service->id;
                $reservation->fk_user = $user->id;
                $reservation->date =$date =date('Y-m-d', strtotime(str_replace('/','-', $service->options->date))); // => 2013-02-16,;
                $reservation->fk_order_product = $order_product->id_order_product;
                $reservation->cuantity = $service->qty;
                $reservation->time = $service->options->time;
                $reservation->state = "iniciado";
                $reservation->save();
            }

            //Pago con tajeta de credito
            if($request->payment_type == 'tarjeta_credito')
            {
                $parameters = array(
                    //Ingrese aquí el identificador de la cuenta.
                    \PayUParameters::ACCOUNT_ID => env('PAYU_ACCOUNT_ID'),
                    //Ingrese aquí el código de referencia.
                    \PayUParameters::REFERENCE_CODE => env('PAYU_REFERENCE_CODE').$orden->id_order,
                    //Ingrese aquí la descripción.
                    \PayUParameters::DESCRIPTION => "Compra TuPaseo ".$orden->id_order,

                        // -- Valores --
                        //Ingrese aquí el valor de la transacción.
                        \PayUParameters::VALUE => $subtotal,//
                        //Ingrese aquí el valor del IVA (Impuesto al Valor Agregado solo valido para Colombia) de la transacción,
                        //si se envía el IVA nulo el sistema aplicará el 19% automáticamente. Puede contener dos dígitos decimales.
                        //Ej: 19000.00. En caso de no tener IVA debe enviarse en 0.
                        \PayUParameters::TAX_VALUE => "0",
                        //Ingrese aquí el valor base sobre el cual se calcula el IVA (solo valido para Colombia).
                        //En caso de que no tenga IVA debe enviarse en 0.
                        \PayUParameters::TAX_RETURN_BASE => "0",
                    //Ingrese aquí la moneda.
                    \PayUParameters::CURRENCY => "COP",

                    // -- Comprador
                    //Ingrese aquí el nombre del comprador.
                    \PayUParameters::BUYER_NAME => $request->first_name . " ". $request->last_name,
                    //Ingrese aquí el email del comprador.
                    \PayUParameters::BUYER_EMAIL => $request->email,
                    //Ingrese aquí el teléfono de contacto del comprador.
                    \PayUParameters::BUYER_CONTACT_PHONE => $request->phone,
                    //Ingrese aquí el documento de contacto del comprador.
                    \PayUParameters::BUYER_DNI => $request->dni,
                    //Ingrese aquí la dirección del comprador.
                    \PayUParameters::BUYER_STREET => $request->address,
                    \PayUParameters::BUYER_STREET_2 => $request->address,
                    \PayUParameters::BUYER_CITY => $request->city!=''? $request->city : substr($request->region, 0,40),
                    \PayUParameters::BUYER_STATE =>substr($request->region, 0,40),
                    \PayUParameters::BUYER_COUNTRY => strtoupper($request->country),
                    \PayUParameters::BUYER_POSTAL_CODE => "000000",
                    \PayUParameters::BUYER_PHONE => $request->phone,

                    // -- pagador --
                    //Ingrese aquí el nombre del pagador.
                    \PayUParameters::PAYER_NAME => $request->cardholderName,
                    //Ingrese aquí el email del pagador.
                    \PayUParameters::PAYER_EMAIL => $request->email,
                    //Ingrese aquí el teléfono de contacto del pagador.
                    \PayUParameters::PAYER_CONTACT_PHONE => $request->phone,
                    //Ingrese aquí el documento de contacto del pagador.
                    \PayUParameters::PAYER_DNI => $request->docNumber,
                    //Ingrese aquí la dirección del pagador.
                    \PayUParameters::PAYER_STREET => $request->address,
                    \PayUParameters::PAYER_STREET_2 => $request->address,
                    \PayUParameters::PAYER_CITY => $request->city,
                    \PayUParameters::PAYER_STATE => $request->city!='' ? $request->city : substr($request->region, 0,40),
                    \PayUParameters::PAYER_COUNTRY => strtoupper($request->country),
                    \PayUParameters::PAYER_POSTAL_CODE => "000000",
                    \PayUParameters::PAYER_PHONE => $request->phone,

                    // -- Datos de la tarjeta de crédito --
                    //Ingrese aquí el número de la tarjeta de crédito
                    \PayUParameters::CREDIT_CARD_NUMBER => $request->cardNumber,
                    //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
                    \PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $request->cardExpirationYear."/".$request->month_exp,
                    //Ingrese aquí el código de seguridad de la tarjeta de crédito
                    \PayUParameters::CREDIT_CARD_SECURITY_CODE=> $request->securityCode,
                    //Ingrese aquí el nombre de la tarjeta de crédito
                    //VISA||MASTERCARD||AMEX||DINERS
                    \PayUParameters::PAYMENT_METHOD => $request->type_card,

                    //Ingrese aquí el número de cuotas.
                    \PayUParameters::INSTALLMENTS_NUMBER => $request->numero_coutas,
                    //Ingrese aquí el nombre del pais.
                    \PayUParameters::COUNTRY => \PayUCountries::CO,

                    //Session id del device.
                    \PayUParameters::DEVICE_SESSION_ID => Cookie::get()['laravel_session'],
                    //IP del pagadador
                    \PayUParameters::IP_ADDRESS => \Request::ip(),
                    //Cookie de la sesión actual.
                    \PayUParameters::PAYER_COOKIE=>Cookie::get()['XSRF-TOKEN'],
                    //Cookie de la sesión actual.
                    \PayUParameters::USER_AGENT=>$request->header('User-Agent'),
                    \PayUParameters::RESPONSE_URL=>"localhost:8000/finapasfiasdasd"
                );
                try
                {
                    $this->startPayU();

                    $response = \PayUPayments::doAuthorizationAndCapture($parameters);


                    if ($response->transactionResponse->state=="APPROVED") 
                    {
                        $orden->estado = 'pagado';
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();


                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->state = 'pagado';
                            $reservation->save();
                        }
                        
                        //SEND MAIL
                        Mail::to($orden->shipping_order->email)
                        ->bcc(env('MAIL_FROM'), '')
                        ->send(new NotifyPay($orden, Cart::instance('shopping')->subtotal(0,'.', '')));


                        $cart = Cart::instance('shopping')->destroy();

                        Session::flash('message','Se ha realizado correctamente la compra.');
                        return redirect('/user/home');
                    }
                    else if($response->transactionResponse->state=="REJECTED")
                    {
                        $message = "";
                        if(isset($response->transactionResponse->responseMessage))
                        {
                            $message = $response->transactionResponse->responseMessage;
                        }
                        else
                        {
                            $message = $response->transactionResponse->responseCode;
                        }

                        $orden->estado = 'rechazado';
                        $orden->descripcion = $message;
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();

                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->delete();
                        }

                        Mail::to($orden->shipping_order->email)
                        ->bcc(env('MAIL_FROM'), '')
                        ->send(new Rejected($orden,Cart::instance('shopping')->subtotal(0,'.', '')));

                        $error = strval("TRANSACCIÓN RECHAZADA - ".$message);
                        Session::flash('message-error', $error);
                        return redirect()->back()->withInput();
                    }
                    else if($response->transactionResponse->state=="PENDING")
                    {
                        $orden->estado = 'pendiente';
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();


                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->state = 'pendiente';
                            $reservation->save();
                        }
                        

                        $cart = Cart::instance('shopping')->destroy();


                        //SEND MAIL
                         Mail::to($orden->shipping_order->email)
                        ->bcc(env('MAIL_FROM'), '')
                        ->send(new NotifyPay($orden, Cart::instance('shopping')->subtotal(0,'.', '')));


                        Session::flash('message-warning','La compra ha quedado en un estado de pendiente.');
                        return redirect('/user/home');
                    }
                    else if($response->transactionResponse->state=="DECLINED")
                    {
                        $message = "";
                        if(isset($response->transactionResponse->responseMessage))
                        {
                            $message = $response->transactionResponse->responseMessage;
                        }
                        else
                        {
                            $message = $response->transactionResponse->responseCode;
                        }
                        $orden->estado = 'declinada';
                        $orden->descripcion = $message;
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();

                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->delete();
                        }

                        //SEND MAIL
                        Mail::to($orden->shipping_order->email)
                        ->bcc(env('MAIL_FROM'), '')
                        ->send(new Rejected($orden,Cart::instance('shopping')->subtotal(0,'.', '')));


                        $error = strval("TRANSACCIÓN DECLINADA - ".$message);
                        Session::flash('message-error', $error);
                        return redirect()->back()->withInput();
                    }
                    else
                    {
                        $orden->estado = 'sin respuesta';
                        $orden->descripcion = "No hay respuesta de pasarela";
                        $orden->id_payu = "000000000";
                        $orden->save();

                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->delete();
                        }
                        $error = strval("ERROR DESCONOCIDO, INTENTELO NUEVAMENTE ");
                        Session::flash('message-error', $error);
                        return redirect()->back()->withInput();
                    }

                }
                catch (\PayUException $e)
                {
                    $orden->estado = 'PayUException';
                    $orden->descripcion = substr($e->messageError,0,-1);
                    $orden->id_payu = "000000000";
                    $orden->save();

                    foreach($orden->services as $service)
                    {
                        $reservation = $service->reservation;
                        $reservation->delete();
                    }
                    $error = strval("ERROR AL PROCESAR EL PAGO - ".substr($e->messageError,0,-1));
                    Session::flash('message-error', $error);
                    return redirect()->back()->withInput();
                }

            }
            //FIN Pago con tajeta de credito
            else if($request->payment_type == 'pse')
            {
                $parameters = array(
                    //Ingrese aquí el identificador de la cuenta.
                    \PayUParameters::ACCOUNT_ID => env('PAYU_ACCOUNT_ID'),
                    //Ingrese aquí el código de referencia.
                    \PayUParameters::REFERENCE_CODE => env('PAYU_REFERENCE_CODE').$orden->id_order,
                    //Ingrese aquí la descripción.
                    \PayUParameters::DESCRIPTION => "Compra TuPaseo ".$orden->id_order,

                    // -- Valores --
                        //Ingrese aquí el valor de la transacción.
                        \PayUParameters::VALUE => $subtotal,
                        //Ingrese aquí el valor del IVA (Impuesto al Valor Agregado solo valido para Colombia) de la transacción,
                        //si se envía el IVA nulo el sistema aplicará el 19% automáticamente. Puede contener dos dígitos decimales.
                        //Ej: 19000.00. En caso de no tener IVA debe enviarse en 0.
                        \PayUParameters::TAX_VALUE => "0",
                        //Ingrese aquí el valor base sobre el cual se calcula el IVA (solo valido para Colombia).
                        //En caso de que no tenga IVA debe enviarse en 0.
                        \PayUParameters::TAX_RETURN_BASE => "0",
                    //Ingrese aquí la moneda.
                    \PayUParameters::CURRENCY => "COP",

                    //Ingrese aquí el email del comprador.
                    \PayUParameters::BUYER_EMAIL => $request->email,
                    //Ingrese aquí el nombre del pagador.
                    \PayUParameters::PAYER_NAME => $request->all_name,
                    //Ingrese aquí el email del pagador.
                    \PayUParameters::PAYER_EMAIL => $request->email,
                    //Ingrese aquí el teléfono de contacto del pagador.
                    \PayUParameters::PAYER_CONTACT_PHONE=> $request->phone,

                    // -- infarmación obligatoria para PSE --
                    //Ingrese aquí el código pse del banco.
                    \PayUParameters::PSE_FINANCIAL_INSTITUTION_CODE =>$request->bank,
                    //Ingrese aquí el tipo de persona (N natural o J jurídica)
                    \PayUParameters::PAYER_PERSON_TYPE => $request->type_person,
                    //Ingrese aquí el documento de contacto del pagador.
                    \PayUParameters::PAYER_DNI => $request->identification_number,
                    //Ingrese aquí el tipo de documento del pagador: CC, CE, NIT, TI, PP,IDC, CEL, RC, DE.
                    \PayUParameters::PAYER_DOCUMENT_TYPE => $request->identification_type,

                    //Ingrese aquí el nombre del método de pago
                    \PayUParameters::PAYMENT_METHOD => "PSE",

                    //Ingrese aquí el nombre del pais.
                    \PayUParameters::COUNTRY => \PayUCountries::CO,

                    //IP del pagadador
                    \PayUParameters::IP_ADDRESS => \Request::ip(),
                    //Cookie de la sesión actual.
                    \PayUParameters::PAYER_COOKIE=>Cookie::get()['XSRF-TOKEN'],
                    //Cookie de la sesión actual.
                    \PayUParameters::USER_AGENT=>$request->header('User-Agent'),

                    //Página de respuesta a la cual será redirigido el pagador.
                    \PayUParameters::RESPONSE_URL=> url('/callbackpayu/'.$orden->id_order),

                );

                try
                {
                    $this->startPayU();

                    $response = \PayUPayments::doAuthorizationAndCapture($parameters);

                    if($response->transactionResponse->state=="PENDING")
                    {
                        $orden->estado = 'pendiente';
                        $orden->descripcion = $response->transactionResponse->responseCode;
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();


                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->state = 'pendiente';
                            $reservation->save();
                        }

                        $cart = Cart::instance('shopping')->destroy();

                        
                        return redirect($response->transactionResponse->extraParameters->BANK_URL);
                    }
                    else if($response->transactionResponse->state=="DECLINED")
                    {
                        $message = "";
                        if(isset($response->transactionResponse->responseMessage))
                        {
                            $message = $response->transactionResponse->responseMessage;
                        }
                        else
                        {
                            $message = $response->transactionResponse->responseCode;
                        }
                        $orden->estado = 'declinada';
                        $orden->descripcion = $message;
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();

                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->delete();
                        }

                        //SEND MAIL
                        Mail::to($orden->shipping_order->email)
                        ->bcc(env('MAIL_FROM'), '')
                        ->send(new Rejected($orden,Cart::instance('shopping')->subtotal(0,'.', '')));


                        $error = strval("TRANSACCIÓN DECLINADA - ".$message);
                        Session::flash('message-error', $error);
                        return redirect()->back()->withInput();
                    }
                    else
                    {
                        $orden->estado = 'sin respuesta';
                        $orden->descripcion = "No hay respuesta de pasarela";
                        $orden->id_payu = "000000000";
                        $orden->save();

                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->delete();
                        }
                        $error = strval("ERROR DESCONOCIDO, INTENTELO NUEVAMENTE ");
                        Session::flash('message-error', $error);
                        return redirect()->back()->withInput();
                    }
                }
                catch (\PayUException $e)
                {
                    $orden->estado = 'PayUException';
                    $orden->descripcion = substr($e->messageError,0,-1);
                    $orden->id_payu = "000000000";
                    $orden->save();

                    foreach($orden->services as $service)
                    {
                        $reservation = $service->reservation;
                        $reservation->delete();
                    }
                    $error = strval("ERROR AL PROCESAR EL PAGO - ".substr($e->messageError,0,-1));
                    Session::flash('message-error', $error);
                    return redirect()->back()->withInput();
                }
            }
            //FIN Pago PSE
            else if($request->payment_type == 'efectivo')
            {
                $date = Carbon::now();
                $limit = $date->addDay();
                $limit = $limit->addHours(5)->format('Y-m-d\TH:i:s');

                $parameters = array(
                    //Ingrese aquí el identificador de la cuenta.
                    \PayUParameters::ACCOUNT_ID => env('PAYU_ACCOUNT_ID'),
                    //Ingrese aquí el código de referencia.
                    \PayUParameters::REFERENCE_CODE => env('PAYU_REFERENCE_CODE').$orden->id_order,
                    //Ingrese aquí la descripción.
                    \PayUParameters::DESCRIPTION => "Compra TuPaseo ".$orden->id_order,

                    // -- Valores --
                        //Ingrese aquí el valor de la transacción.
                        \PayUParameters::VALUE => $subtotal,
                        //Ingrese aquí el valor del IVA (Impuesto al Valor Agregado solo valido para Colombia) de la transacción,
                        //si se envía el IVA nulo el sistema aplicará el 19% automáticamente. Puede contener dos dígitos decimales.
                        //Ej: 19000.00. En caso de no tener IVA debe enviarse en 0.
                        \PayUParameters::TAX_VALUE => "0",
                        //Ingrese aquí el valor base sobre el cual se calcula el IVA (solo valido para Colombia).
                        //En caso de que no tenga IVA debe enviarse en 0.
                        \PayUParameters::TAX_RETURN_BASE => "0",
                    //Ingrese aquí la moneda.
                    \PayUParameters::CURRENCY => "COP",

                    //Ingrese aquí el email del comprador.
                    \PayUParameters::BUYER_EMAIL => $request->email,
                    //Ingrese aquí el nombre del pagador.
                    \PayUParameters::PAYER_NAME => $request->all_name,
                    //Ingrese aquí el documento de contacto del pagador.
                    \PayUParameters::PAYER_DNI=> $request->identification_number,

                    //Ingrese aquí el nombre del método de pago
                    \PayUParameters::PAYMENT_METHOD => $request->entity_cash,

                    //Ingrese aquí el nombre del pais.
                    \PayUParameters::COUNTRY => \PayUCountries::CO,

                    //Ingrese aquí la fecha de expiración.
                    \PayUParameters::EXPIRATION_DATE => $limit,
                    //IP del pagadador
                    \PayUParameters::IP_ADDRESS => \Request::ip(),

                    //\PayUParameters::RESPONSE_URL=> url('/callbackpayu/'.$orden->id_order),

                );

                try
                {
                    $this->startPayU();

                    $response = \PayUPayments::doAuthorizationAndCapture($parameters);

                    if($response->transactionResponse->state=="PENDING")
                    {
                        $orden->estado = 'pendiente';
                        $orden->descripcion = $response->transactionResponse->responseCode;
                        $orden->id_payu = $response->transactionResponse->orderId;
                        $orden->save();


                        foreach($orden->services as $service)
                        {
                            $reservation = $service->reservation;
                            $reservation->state = 'pendiente';
                            $reservation->save();
                        }

                        $cart = Cart::instance('shopping')->destroy();
                        
                        
                        return redirect($response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML);
                    }
                    else
                    {
                        dd($response);
                    }
                }
                catch(\PayUException $e)
                {
                    $orden->estado = 'PayUException';
                    $orden->descripcion = substr($e->messageError,0,-1);
                    $orden->id_payu = "000000000";
                    $orden->save();

                    foreach($orden->services as $service)
                    {
                        $reservation = $service->reservation;
                        $reservation->delete();
                    }
                    $error = strval("ERROR AL PROCESAR EL PAGO - ".substr($e->messageError,0,-1));
                    Session::flash('message-error', $error);
                    return redirect()->back()->withInput();
                }
                
            }
            //Fin pago EFECTIVO

    }

    public function callbackpayuWithId($id,Request $request)
    {
        $longitud =strlen(env('PAYU_REFERENCE_CODE'));

        $codigo =  substr($request->referenceCode,$longitud);
        $orden = Order::find($codigo);
        if($request->lapTransactionState == "APPROVED")
        {
            $orden->estado = 'pagado';
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->state = 'pagado';
                $reservation->save();
            }
            
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new NotifyPay($orden, (int)$request->TX_VALUE));


            $cart = Cart::instance('shopping')->destroy();

            Session::flash('message','Se ha realizado correctamente la compra.');
            return redirect('/user/home');
        }
        else if($request->lapTransactionState == "DECLINED")
        {
            $orden->estado = 'declinada';
            $orden->descripcion = $request->lapResponseCode;
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->delete();
            }
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new Rejected($orden,(int)$request->TX_VALUE));


            $error = strval("TRANSACCIÓN DECLINADA".$request->lapResponseCode);
            Session::flash('message-error', $error);
            $cart = Cart::instance('shopping')->content();
            if($cart->count() >0)
            {
                return redirect('/viewcart');
            }
            else
                return redirect('/home');
        }

        else if($request->lapTransactionState == "ERROR")
        {
            $orden->estado = 'ERROR';
            $orden->descripcion = $request->lapResponseCode;
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->delete();
            }
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new Rejected($orden,(int)$request->TX_VALUE));


            $error = strval("ERROR EN TRANSACCIÓN".$request->lapResponseCode);
            Session::flash('message-error', $error);
            $cart = Cart::instance('shopping')->content();
            if($cart->count() >0)
            {
                return redirect('/viewcart');
            }
            else
                return redirect('/home');
        }
        else if($request->lapTransactionState == "EXPIRED")
        {
            $orden->estado = 'expirada';
            $orden->descripcion = $request->lapResponseCode;
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->delete();
            }
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new Rejected($orden,(int)$request->TX_VALUE));


            $error = strval("TRANSACCIÓN EXPIRADA".$request->lapResponseCode);
            Session::flash('message-error', $error);
            $cart = Cart::instance('shopping')->content();
            if($cart->count() >0)
            {
                return redirect('/viewcart');
            }
            else
                return redirect('/home');
        }

        else if($request->lapTransactionState == "PENDING")
        {
            $orden->estado = 'pendiente';
            $orden->save();


            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->state = 'pendiente';
                $reservation->save();
            }
            

            $cart = Cart::instance('shopping')->destroy();


            //SEND MAIL
             Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new NotifyPay($orden, Cart::instance('shopping')->subtotal(0,'.', '')));


            Session::flash('message-warning','La compra ha quedado en un estado de pendiente.');
            return redirect('/user/home');
        }
    }

    public function getProvince(Request $request)
    {
        $country = Country::getByCode($request->country);
        $provinces = $country->children();
        return response()->json($provinces);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subtotal()
    {
         // $cart = Cart::instance('shopping')->destroy();
        return Cart::instance('shopping')->subtotal();
    }

    public function removeCart($rowId)
    {
        $cart = Cart::instance('shopping')->get($rowId);
        if ($cart) 
        {
            $newcart = Cart::instance('shopping')->remove($rowId);
        }
    }

    public function removeCartPay($rowId)
    {   
        try
        {
            $cart = Cart::instance('shopping')->get($rowId);
            if ($cart) 
            {
                $newcart = Cart::instance('shopping')->remove($rowId);
            }
            return redirect()->back();
        }
        catch(\Exception $e)
        {
            return redirect()->back();

        }
    }
    public function deleteCart()
    {
        $cart = Cart::instance('shopping')->destroy();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
