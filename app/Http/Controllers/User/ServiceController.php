<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceOperator;
use App\Models\Schedule;
use App\Models\Reservation;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $service = ServiceOperator::where('slug',$slug)->first();
        if($service)
        {
            $alldays =['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

            $horario = Schedule::where('fk_service',$service->id_service_operator)->where('state','active')->get();
            foreach($horario as $day)
            {
                if(in_array($day->day,$alldays))
                {
                    $posicion = (array_search($day->day, $alldays)); 
                    unset($alldays[$posicion]);
                }
            }

            $comments = Reservation::where('fk_service',$service->id_service_operator)->where('state','Calificada')->get();

            if(count($comments)>1)
            {
                $acumulador=0;
                foreach ($comments as $comment) 
                {
                    $acumulador+= $comment->calification;
                }
    
                $average = round($acumulador/count($comments), 1);
            }
            else
            {
                $average=5;
            }

            array_add($service,'average',$average);
            array_add($service,'days',$alldays);
            // dd($service);
            $contador =1;
            $sites = ServiceOperator::where('state','Activo')->get();
            return view('User.service',compact('service','contador','comments','sites'));
        }
        else
        {
            return redirect('/');
        }
    }

    public function search(Request $request)
    {
        $services = ServiceOperator::where('location',$request->location)->where('state','Activo')->get();
        if(count($services)>0)
        {
            $sites = ServiceOperator::where('state','Activo')->get();
            return view('reservation',compact('services','sites'));
        }
        else
        {
            return redirect('/reservation');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
