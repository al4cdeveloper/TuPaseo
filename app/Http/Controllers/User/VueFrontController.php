<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallpaper;
use App\Models\ServiceOperator;
use App\Models\Municipality;
use App\Models\Department;
use App\Models\Pattern;
use App\Models\PatternPart;
use App\Models\PagePartPage;
use App\Models\ServicePicture;
use App\Models\EstablishmentImage;
use App\Models\Reservation;
use App\Models\PatternClick;
use App\Models\Region;
use App\Models\Establishment;
use App\Models\EcosystemCategory;
use App\Models\Page;
use App\Models\Service;
Use DB;
use Auth;
class VueFrontController extends Controller
{
	public function home()
	{
		$wallpapers = Wallpaper::all()->where('state','activo');
		$municipalities = Municipality::all()->where('front_state','activo');
		$sixMunicipalities = Municipality::where('front_state','activo')->inRandomOrder()->take(6)->get();
		//Municipios donde hay hoteles
		$hotelsMunicipalities = Municipality::with('hotels')->whereHas('Hotels',function ($q1)
		{
			$q1->where('state','activo');
		})->get();

		//Municipios donde hay restaurantes
		$restaurantsMunicipalities = Municipality::with('restaurants')->whereHas('Restaurants',function ($q1)
				{
					$q1->where('state','activo');
				})->get();
		//Actividades que tienen servicios activos
		$activities = Service::whereHas('ActiveServices', function ($query) 
			{
				$query->where('state','activo');

			})->get();
		//Municipios que tienen servicios activos
		$activitiesMunicipalities = Municipality::whereHas('Plans',function($q2)
			{
				$q2->where('state','Activo');

			})->get();


		$serviceOperator = ServiceOperator::where('state','activo')->inRandomOrder()->take(6)->get();
		$allplans = ServiceOperator::where('state','activo')->count();
		foreach ($serviceOperator as $service) 
		{
			if(count($service->images)>0)
			{
				array_add($service,'image_link',$service->images->first()->link_image);
			}
			else
				array_add($service,'image_link','front/img/slides/rafting.jpg');

			$average=0;
			$cuantity=0;
			foreach($service->reservations as $reservation)
			{
				if(($reservation->state)=="Calificada")
				{
					$average += $reservation->calification;
					$cuantity++;
				}
			}
			if($cuantity>0)
			{
				$average = $average/$cuantity;
			}

			array_add($service,'average',floor($average));
			array_add($service,'cuantity',$cuantity);
			array_add($service,'operator',$service->operador->name);
			array_add($service,'municipality',$service->Municipality->municipality_name);
			array_add($service,'icon_class',$service->PrincipalService->icon_class);
		}
		
		$page = Page::where('url','/')->first();
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}


		return ['wallpapers'=>$wallpapers,
				'municipalities'=>$municipalities,
				'sixMunicipalities'=>$sixMunicipalities,
				'services'=>$serviceOperator,
				'patterns'=>$finalPattern,
				'allPlans'=>$allplans,
				'hotelsMunicipalities'=>$hotelsMunicipalities,
				'restaurantsMunicipalities'=>$restaurantsMunicipalities,
				'activities'=>$activities,
				'activitiesMunicipalities'=>$activitiesMunicipalities,
				'page'=>$page,
			];
	}

	public function getTypesKitchen(Request $request)
	{
		$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->pluck('kitchen_type');

		return response()->json(['typeskitchen'=>$typeskitchen]);
	}

	public function sumClick($id)
	{
		$pattern = PatternPart::findOrfail($id);
		if($pattern->clicks =="" || $pattern->clicks==null)
		{
			$pattern->clicks=0;
			$pattern->save();
		}
		$pattern->clicks++;
		$pattern->save();
	}

	public function navComponent()
	{
		$regions = Region::where('state','activo')->orderBy('region_name')->get();

		$ecosystems = EcosystemCategory::all();
		$token = csrf_token();
		$user = [];

		if(Auth::guard('operator')->user())
		{
			$user = Auth::guard('operator')->user();
			array_add($user,'type_user','operator');
			array_add($user,'loginState','active');
		}
		elseif(Auth::user())
		{
			$user = Auth::user();
			array_add($user,'type_user','customer');
			array_add($user,'loginState','active');
		}
		else
		{
			$user = ['loginState'=>'inactive'];
		}




		//dd(Auth::guard('operator')->user());


		return response()->json(['regions'=>$regions,
								'ecosystems'=>$ecosystems,
								'token'=>$token,
								'user'=>$user,
								]);
	}

	public function region($slug)
	{
		$region = Region::with('images')->where('slug',$slug)->first();
		$page = Page::where('url',"/region/$slug")->first();
		$allplans = ServiceOperator::where('state','activo')->count();
		$allRestaurants = Establishment::where('state','activo')->where('type_establishment','restaurant')->count();
		$allHotels = Establishment::where('state','activo')->where('type_establishment','hotel')->count();
		$operators = [];
		$planes=[]; 

		//Planes
		foreach($region->departments as $department)
		{
			foreach($department->municipalities as $municipality)
			{
				if(count($municipality->Plans)>0)
				{
					foreach ($municipality->Plans as $op) 
					{
						$operators []=$op;
					}
				}


			}

			$planes[] = Municipality::where('fk_department',$department->id_department)->whereHas('Plans', function ($query)
			{
				$query->where('service_operators.state','activo');
			})->get();
		}

		
		shuffle($operators);
		
		//Patterns
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$departments =[];
		foreach($region->FiveDepartmentsFront as $department)
		{
			$departments[$department->department_name] = ['name'=>$department->department_name,
														'short_description'=>$department->short_description];
			foreach($department->TreeMunicipalitiesFront as $municipality)
			{
				$departments[$department->department_name]['municipalities'] = $municipality;
			}
		}

		$municipalities = DB::table('municipalities')
							->join('departments','municipalities.fk_department','=','departments.id_department')
							->join('regions','departments.fk_region','=','regions.id_region')
							->select('municipalities.*')
							->where('municipalities.front_state','activo')
							->where('regions.id_region',$region->id_region)
							->inRandomOrder()->take(6)->get();

		$plans = DB::table('service_operators')
							->join('municipalities','service_operators.fk_municipality','=','municipalities.id_municipality')
							->join('departments','municipalities.fk_department','=','departments.id_department')
							->join('regions','departments.fk_region','=','regions.id_region')
							->join('operators','service_operators.fk_operator','=','operators.id_operator')
							->select('service_operators.*','municipalities.municipality_name','operators.name')
							->where('service_operators.state','activo')
							->where('municipalities.front_state','activo')
							->where('regions.id_region',$region->id_region)
							->inRandomOrder()->take(6)->sharedLock()->get();
		$plans = json_decode($plans, true); //Eliminando un formato que se le había dado 
		foreach ($plans as $position => $service) 
		{
			if(ServicePicture::where('fk_service',$service['id_service_operator'])->count()>0)
			{
				$plans[$position] = array_merge($plans[$position],['image_link'=>ServicePicture::where('fk_service',$service['id_service_operator'])->first()->link_image]);

			}
			else
				$plans[$position] = array_merge($plans[$position],['image_link'=>'front/img/slides/rafting.jpg']);

			$average=0;
			$cuantity=0;
			foreach(Reservation::where('fk_service',$service['id_service_operator'])->get() as $reservation)
			{
				if(($reservation->state)=="Calificada")
				{
					$average += $reservation->calification;
					$cuantity++;
				}
			}
			if($cuantity>0)
			{
				$average = $average/$cuantity;
			}
			
			$plans[$position] = array_merge($plans[$position],['average'=>floor($average)]);
			$plans[$position] = array_merge($plans[$position],['cuantity'=>$cuantity]);

		}

		//Restaurantes de la región activos
		$restaurants = DB::table('establishments')	
							->join('municipalities','establishments.fk_municipality','=','municipalities.id_municipality')
							->join('departments','municipalities.fk_department','=','departments.id_department')
							->join('regions','departments.fk_region','=','regions.id_region')
							->select('establishments.*','municipalities.municipality_name')
							->where('establishments.state','activo')
							->where('establishments.type_establishment','restaurant')
							->where('municipalities.front_state_restaurant','activo')
							->where('regions.id_region',$region->id_region)
							->inRandomOrder()->take(3)->sharedLock()->get();
		$restaurants = json_decode($restaurants, true); //Eliminando un formato que se le había dado 

		foreach ($restaurants as $position => $restaurant) 
		{
			$images = EstablishmentImage::where('fk_establishment',$restaurant['id_establishment'])->get();
			if($images->count()>0)
			{
				$restaurants[$position] = array_merge($restaurants[$position],['image_link'=>$images->first()->link_image]);

			}
			else
				$restaurants[$position] = array_merge($restaurants[$position],['image_link'=>'front/img/restaurant1.jpg']);
		}

		//Restaurantes de la región activos
		$hotels = DB::table('establishments')	
							->join('municipalities','establishments.fk_municipality','=','municipalities.id_municipality')
							->join('departments','municipalities.fk_department','=','departments.id_department')
							->join('regions','departments.fk_region','=','regions.id_region')
							->select('establishments.*','municipalities.municipality_name')
							->where('establishments.state','activo')
							->where('establishments.type_establishment','hotel')
							->where('municipalities.front_state_restaurant','activo')
							->where('regions.id_region',$region->id_region)
							->inRandomOrder()->take(3)->sharedLock()->get();
		$hotels = json_decode($hotels, true); //Eliminando un formato que se le había dado 

		foreach ($hotels as $position => $hotel) 
		{
			$images = EstablishmentImage::where('fk_establishment',$hotel['id_establishment'])->get();
			if($images->count()>0)
			{
				$hotels[$position] = array_merge($hotels[$position],['image_link'=>$images->first()->link_image]);

			}
			else
				$hotels[$position] = array_merge($hotels[$position],['image_link'=>'front/img/hotel1.jpg']);
		}




		return response()->json(['region'=>$region,
								'patterns'=>$finalPattern,
								'municipalities'=>$municipalities,
								'departments'=>$departments,
								'plans'=>$plans,
								'allPlans'=>$allplans,
								'page'=>$page,
								'restaurants'=>$restaurants,
								'hotels'=>$hotels,
								'allRestaurants'=>$allRestaurants,
								'allHotels'=>$allHotels,
								]);
	}
	public function municipality($slug)
	{	
		$municipality = Municipality::with('images','Department','Video')->where('slug',$slug)->first();
		array_add($municipality,'region',$municipality->PertinentRegion);
		$page = Page::where('url',"/municipality/$slug")->first();
		$plans = $municipality->SixPlans;
		$allplans = ServiceOperator::where('state','activo')->count();
		$finalPattern=[];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$servicesNames = [];
		foreach($municipality->Plans->groupBy('fk_service') as $service => $services)
		{
			$serviceFind = Service::find($service);
			$servicesNames[] = ['name'=>$serviceFind->service, 'icon_class'=>$serviceFind->icon_class];
		}
		foreach ($plans as $service) 
		{
			if(count($service->images)>0)
			{
				array_add($service,'image_link',$service->images->first()->link_image);
			}
			else
				array_add($service,'image_link','front/img/slides/rafting.jpg');

			$average=0;
			$cuantity=0;
			foreach($service->reservations as $reservation)
			{
				if(($reservation->state)=="Calificada")
				{
					$average += $reservation->calification;
					$cuantity++;
				}
			}
			if($cuantity>0)
			{
				$average = $average/$cuantity;
			}

			array_add($service,'average',floor($average));
			array_add($service,'cuantity',$cuantity);
			array_add($service,'operator',$service->operador->name);
			array_add($service,'icon_class',$service->PrincipalService->icon_class);
		}

		$hotels = $municipality->twoRandomHotels;
		foreach($hotels as $hotel)
		{
			$images = $hotel->images;
			if($hotel->images->count()>0)
			{
				array_add($hotel,'link_image',$images->first()->link_image);
			}
			else
			{
				array_add($hotel,'link_image','front/img/hotel1.jpg');
			}
		}
		$cantHotels = $municipality->Hotels->count();

		$restaurants = $municipality->twoRandomRestaurants;
		foreach($restaurants as $restaurant)
		{
			$images = $restaurant->images;
			if($restaurant->images->count()>0)
			{
				array_add($restaurant,'link_image',$images->first()->link_image);
			}
			else
			{
				array_add($restaurant,'link_image','front/img/restaurant1.jpg');
			}
		}
		$cantRestaurants = $municipality->Restaurants->count();

		return response()->json(['municipality'=>$municipality,
								'page'=>$page,
								'plans'=>$plans,
								'allPlans'=>$allplans,
								'patterns'=>$finalPattern,
								'keydata'=>$municipality->KeyData->groupBy('category'),
								'services'=>$servicesNames,
								'hotels'=>$hotels,
								'cantHotels'=>$cantHotels,
								'restaurants'=>$restaurants,
								'cantRestaurants'=>$cantRestaurants,
								]);
	}

	public function restaurant($slug)
	{
		$restaurant = Establishment::with('municipality','images','services','schedule')->where('slug',$slug)->first();
		$images = $restaurant->images;
		$allplans = ServiceOperator::where('state','activo')->count();

		if($images->count()>0)
		{
				array_add($restaurant,'link_image',$images->first()->link_image);
		}
		else
				array_add($restaurant,'link_image','front/img/restaurant1.jpg');

		$plates = $restaurant->plates->groupBy('category');
		$socialNetworks = false;

		if($restaurant->facebook!='' || $restaurant->twitter!='' || $restaurant->instagram!='' || $restaurant->website!='')
		{
			$socialNetworks = true;
		}	

		$page = Page::where('url',"/restaurant/".$restaurant->Municipality->slug)->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		$municipality = $restaurant->municipality;
		$plans = $municipality->SixPlans;
		foreach ($plans as $service) 
		{
			if(count($service->images)>0)
			{
				array_add($service,'image_link',$service->images->first()->link_image);
			}
			else
				array_add($service,'image_link','front/img/slides/rafting.jpg');

			$average=0;
			$cuantity=0;
			foreach($service->reservations as $reservation)
			{
				if(($reservation->state)=="Calificada")
				{
					$average += $reservation->calification;
					$cuantity++;
				}
			}
			if($cuantity>0)
			{
				$average = $average/$cuantity;
			}

			array_add($service,'average',floor($average));
			array_add($service,'cuantity',$cuantity);
			array_add($service,'operator',$service->operador->name);
			array_add($service,'icon_class',$service->PrincipalService->icon_class);
		}

		return response()->json(['restaurant'=>$restaurant,
								'page'=>$page,
								'plates'=>$plates,
								'socialNetworks'=>$socialNetworks,
								'patterns'=>$finalPattern,
								'plans'=>$plans,
								'allPlans'=>$allplans,
								]);
	}

	public function hotel($slug)
	{
		$hotel = Establishment::with('municipality','images','services','schedule')->where('slug',$slug)->first();
		$images = $hotel->images;
		$allplans = ServiceOperator::where('state','activo')->count();

		if($images->count()>0)
		{
				array_add($hotel,'link_image',$images->first()->link_image);
		}
		else
				array_add($hotel,'link_image','front/img/hotel1.jpg');

		$rooms = $hotel->Rooms->groupBy('suite');
		$socialNetworks = false;

		if($hotel->facebook!='' || $hotel->twitter!='' || $hotel->instagram!='' || $hotel->website!='')
		{
			$socialNetworks = true;
		}	
		//publicidad
		$page = Page::where('url',"/hotel/".$hotel->Municipality->slug)->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		$municipality = $hotel->municipality;
		$plans = $municipality->SixPlans;
		foreach ($plans as $service) 
		{
			if(count($service->images)>0)
			{
				array_add($service,'image_link',$service->images->first()->link_image);
			}
			else
				array_add($service,'image_link','front/img/slides/rafting.jpg');

			$average=0;
			$cuantity=0;
			foreach($service->reservations as $reservation)
			{
				if(($reservation->state)=="Calificada")
				{
					$average += $reservation->calification;
					$cuantity++;
				}
			}
			if($cuantity>0)
			{
				$average = $average/$cuantity;
			}

			array_add($service,'average',floor($average));
			array_add($service,'cuantity',$cuantity);
			array_add($service,'operator',$service->operador->name);
			array_add($service,'icon_class',$service->PrincipalService->icon_class);
		}

		return response()->json(['hotel'=>$hotel,
								'rooms'=>$rooms,
								'socialNetworks'=>$socialNetworks,
								'patterns'=>$finalPattern,
								'plans'=>$plans,
								'allPlans'=>$allplans,
								]);
	}

	public function service($slug)
	{
		$service = ServiceOperator::with('images','reservations','municipality')->where('slug',$slug)->first();
		if(count($service->images)>0)
		{
			array_add($service,'image_link',$service->images->first()->link_image);
		}
		else
			array_add($service,'image_link','front/img/slides/rafting.jpg');

		$average=0;
		$cuantity=0;
		foreach($service->reservations as $reservation)
		{
			if(($reservation->state)=="Calificada")
			{
				array_add($service,'user',$reservation->user);
				$average += $reservation->calification;
				$cuantity++;
			}
		}
		if($cuantity>0)
		{
			$average = $average/$cuantity;
		}

		array_add($service,'average',floor($average));
		array_add($service,'cuantity',$cuantity);
		array_add($service,'operator',$service->operador->name);
		array_add($service,'icon_class',$service->PrincipalService->icon_class);
		array_add($service,'name_principal_service',$service->PrincipalService->service);
		array_add($service,'impact',$service->PrincipalService->type_service);
		array_add($service,'ecosystem',$service->PrincipalService->ecosystem->ecosystem_category);
		$items = $service->items->where('state','activo');

		$municipality = Municipality::find($service->fk_municipality);
		array_add($municipality,'region',$municipality->PertinentRegion);
		$finalSchedule = [];

        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
        $disabledDays = '';
        // Inhabilitación de días que no se trabaja
        foreach($days as $position=>$day)
        {
        	$state = false;
        	$temp = [];
        	foreach($service->schedule as $scheduleOperator)
        	{
        		if($scheduleOperator->day == $day)
        		{
        			$state = true;
        			$temp = $scheduleOperator;
        		}

        	}
        	if($state)
    		{
    			array_add($temp,'frontState','active');
    			$finalSchedule[] = $temp;
    		}
    		else
    		{
    			$finalSchedule[] = ['day'=>$day,
    								'frontState'=>'inactive'];
    			if(($position+1)==7)
    			{
    				$disabledDays.= '0,';

    			}
    			else
    				$disabledDays.=($position+1).',';
    		}
        }
        $disabledDays = substr($disabledDays, 0,-1);
        //publicidad
		$page = Page::where('url',"/service/".$service->Municipality->slug)->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}


		return response()->json(['service'=>$service,
								'items'=>$items,
								'municipality'=>$municipality,
								'schedule'=>$finalSchedule,
								'patterns'=>$finalPattern,
								'disabledDays'=>$disabledDays,
									]);
	}

	public function ecosystem($slug)
	{
		$ecosystem = EcosystemCategory::where('slug',$slug)->first();
		$page = Page::where('url',"/ecosystem/".$ecosystem->slug)->first();
		$activities = $ecosystem->Activities;
		$finalActivities= [];
		foreach ($activities as $activity) 
		{
			if($activity->ActiveServices->count()!=0)
			{
				$finalActivities[] = $activity;
			}
		}
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		return response()->json(['ecosystem'=>$ecosystem,
								'activities'=>$finalActivities,
								'patterns'=>$finalPattern,
								]);
	}

	public function services(Request $request)
	{
		$regions = Region::all()->where('state','activo');
		$services = [];
		$ecosystems = [];
		$departments = [];
		$municipalities = [];
		//publicidad
		$page = Page::where('url',"/services")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}
		//Ecosistemas
		if($request->type)
		{
			$types= explode(',', substr($request->type, 0,-1));
			for ($i=0; $i <count($types) ; $i++) 
			{ 
				$types[$i] = $types[$i]. " impacto";
			}

			$ecosystems =  Service::with('ecosystem')->whereHas('ActiveServices', function ($query) use ($types)
			{
				$query->whereIn('type_service',$types);

			})->get()->groupBy('ecosystem.ecosystem_category');
		}
		else
		{
			$ecosystems = EcosystemCategory::all();
			$finalEcosystems = [];
			foreach($ecosystems as $ecosystem)
			{
				$finalEcosystems[$ecosystem->ecosystem_category]=[];
				foreach($ecosystem->Activities as $activity)
				{
					if($activity->ActiveServices->count())
					{
						array_push($finalEcosystems[$ecosystem->ecosystem_category] , $activity);
					}
				}
			}
			$ecosystems = $finalEcosystems;
		}

		if($request->region)
		{	
			$departments = Department::with('Region')->whereHas('Region',function($query1) use($request)
						{
							$regionsSearch = explode(',', substr($request->region, 0,-1));
							$query1->whereIn('slug',$regionsSearch);
						})->get()->groupBy('region.region_name');
		}
		if($request->department)
		{
			$municipalities = Municipality::whereHas('Department',function($q1) use ($request)
				{
					$q1->where('slug',$request->department);
				})->whereHas('Plans',function($q2)
				{
					$q2->where('state','Activo');

				})->get();

		}
			// dd($ecosystems);
			// //$servicesss = Service::whereIn('type_service','like', '%'.$types.'%')->get();
			// //dd($servicesss);

			// $ecosystems = EcosystemCategory::whereHas('Activities',function ($query)
			// {
			// 	$types = ['Alto impacto','Bajo impacto'];
				
			// 	$query->whereHas('ActiveServices',function ($query2) use ($types)
			// 	{
			// 		$query2->whereIn('type_service',$types);

			// 	});
			// })->get();

			// dd($ecosystems);
		//SE FILTRA POR MUNICIPIOS
		if($request->municipality && $request->type)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$types= explode(',', substr($request->type, 0,-1));
				for ($i=0; $i <count($types) ; $i++) 
				{ 
					$types[$i] = $types[$i]. " impacto";
				}
				$q4->whereIn('type_service',$types);
			})->where('state','Activo')->paginate(3);
		}
		else if($request->municipality && $request->activity)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->whereIn('slug',explode(',',substr($request->activity, 0,-1)));
			})->where('state','Activo')->paginate(3);
		}

		//SE FILTRA POR DEPARTAMENTOS
		else if($request->department && $request->type)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{

				$departmentSearch = $request->department;
				$q1->whereHas('Department',function($q2)  use ($departmentSearch)
					{
						$q2->where('slug',$departmentSearch);
					});
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$types= explode(',', substr($request->type, 0,-1));
				for ($i=0; $i <count($types) ; $i++) 
				{ 
					$types[$i] = $types[$i]. " impacto";
				}
				$q4->whereIn('type_service',$types);
			})->where('state','Activo')->paginate(3);
		}
		else if($request->department && $request->activity)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$departmentSearch = $request->department;
				$q1->whereHas('Department',function($q2)  use ($departmentSearch)
					{
						$q2->where('slug',$departmentSearch);
					});
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->whereIn('slug',explode(',',substr($request->activity, 0,-1)));
			})->where('state','Activo')->paginate(3);
		}


		//SI SE FILTRA POR ACTIVIDAD Y REGIÓN && $request->type && $request->activity
		else if($request->region && $request->type)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{

				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$types= explode(',', substr($request->type, 0,-1));
				for ($i=0; $i <count($types) ; $i++) 
				{ 
					$types[$i] = $types[$i]. " impacto";
				}
				$q4->whereIn('type_service',$types);
			})->where('state','Activo')->paginate(3);

		}
		else if($request->region && $request->activity)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->whereIn('slug',explode(',',substr($request->activity, 0,-1)));
			})->where('state','Activo')->paginate(3);

		}
		else if($request->municipality)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','Activo')->paginate(3);
		}
		else if($request->department)
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$departmentSearch = $request->department;
				$q1->whereHas('Department',function($q2)  use ($departmentSearch)
					{
						$q2->where('slug',$departmentSearch);
					});
			})->where('state','Activo')->paginate(3);
		}
		else if($request->region )
		{
			$services = ServiceOperator::whereHas('Municipality',function($q1) use($request)
			{
				$regionsSearch = explode(',', substr($request->region, 0,-1));
				$q1->whereHas('Department',function($q2)  use ($regionsSearch)
					{
						$aux = $regionsSearch;
						$q2->whereHas('Region',function($q3)  use ($aux)
						{
							$q3->whereIn('slug',$aux);
						});
					});
			})->where('state','Activo')->paginate(3);
		}
		else if($request->type)
		{
			$services = ServiceOperator::whereHas('PrincipalService',function($q4) use ($request)
			{
				$types= explode(',', substr($request->type, 0,-1));
				for ($i=0; $i <count($types) ; $i++) 
				{ 
					$types[$i] = $types[$i]. " impacto";
				}
				$q4->whereIn('type_service',$types);
			})->where('state','Activo')->paginate(3);
		}
		else if($request->activity)
		{
			$services = ServiceOperator::whereHas('PrincipalService',function($q4) use ($request)
			{
				$q4->whereIn('slug',explode(',',substr($request->activity, 0,-1)));
			})->where('state','Activo')->paginate(3);
		}
		else
		{
			$services = ServiceOperator::whereHas('PrincipalService',function($q4) 
			{
				$q4->where('state','Activo');
			})->paginate(3);
		}


		//ADAPTACIÓN FINAL A CUALQUIERA QUE SEAN LOS SERVICIOS FINALES
		foreach ($services as $service) 
		{
			if(count($service->images)>0)
			{
				array_add($service,'image_link',$service->images->first()->link_image);
			}
			else
				array_add($service,'image_link','front/img/slides/rafting.jpg');

			$average=0;
			$cuantity=0;
			foreach($service->reservations as $reservation)
			{
				if(($reservation->state)=="Calificada")
				{
					array_add($service,'user',$reservation->user);
					$average += $reservation->calification;
					$cuantity++;
				}
			}
			if($cuantity>0)
			{
				$average = $average/$cuantity;
			}

			array_add($service,'average',floor($average));
			array_add($service,'cuantity',$cuantity);
			array_add($service,'operator',$service->operador->name);
			array_add($service,'icon_class',$service->PrincipalService->icon_class);
			array_add($service,'name_principal_service',$service->PrincipalService->service);
			array_add($service,'municipality',$service->Municipality->municipality_name);
			array_add($service,'region',$service->municipality->PertinentRegion);
		}

		if($request->price)
		{
			if($request->price == "lower")
			{

				$services = collect($services)->sortBy('cost');
			}
			else
			{
				$services = collect($services)->sortByDesc('cost');
			}
		}
		else
		{
			$services = collect($services);
		}

        $cant = (int)round($services['total']/3);

		return response()->json(['regions'=>$regions,
								'services'=>$services,
								'ecosystems'=>$ecosystems,
								'departments'=>$departments,
								'municipalities'=>$municipalities,
								'patterns'=>$finalPattern,
								'cuantity'=>$cant,
								'pagination'=>[
									'total'			=>$services['total'],
									'current_page'	=>$services['current_page'],
									'per_page'		=>$services['per_page'],
									'from'			=>$services['from'],
									'last_page'		=>$services['last_page'],
									'to'			=>$services['to'],
									],
								]);
	}

	public function hotels(Request $request)
	{
		$cantHotels = Establishment::where('state','activo')->where('type_establishment','hotel')->count();
		//publicidad
		$hotels = [];
		$page = Page::where('url',"/hotels")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if($request->name)
		{
			$hotels = Establishment::with('municipality')
									->where('type_establishment','hotel')
									->where('state','activo')	
									->where('establishment_name','like',"%$request->name%")
									->paginate(10);
		}
		else if($request->municipality)
		{
			$hotels = Establishment::with('municipality')->where('type_establishment','hotel')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->paginate(10);
		}
		else
		{
			$hotels = Establishment::with('municipality')->where('state','activo')->where('type_establishment','hotel')->paginate(10);
		}
		
		$municipalities = Municipality::with('hotels')->whereHas('Hotels',function ($q1)
		{
			$q1->where('state','activo');
		})->get();

		foreach ($municipalities as $municipality) 
		{
			array_add($municipality,'cant_hotels',$municipality->hotels->count());
		}


		foreach ($hotels as $position => $hotel) 
		{
			$images = EstablishmentImage::where('fk_establishment',$hotel->id_establishment)->get();
			if($images->count()>0)
			{
				array_add($hotel,'image_link',$images->first()->link_image);
			}
			else
				array_add($hotel,'image_link','front/img/hotel1.jpg');
		}

		$hotels = collect($hotels);


		return response()->json(['cantHotels'=>$cantHotels,
								'municipalities'=>$municipalities,
								'patterns'=>$finalPattern,
								'hotels'=>$hotels,
								'pagination'=>[
									'total'			=>$hotels['total'],
									'current_page'	=>$hotels['current_page'],
									'per_page'		=>$hotels['per_page'],
									'from'			=>$hotels['from'],
									'last_page'		=>$hotels['last_page'],
									'to'			=>$hotels['to'],
									],
								]);
	}

	public function restaurants(Request $request)
	{
		$cantRestaurants = Establishment::where('state','activo')->where('type_establishment','restaurant')->count();
		//publicidad
		$restaurants = [];
		$typeskitchen = [];
		$page = Page::where('url',"/restaurants")->first();
		$finalPattern = [];
		if(isset($page))
		{	
			$finalPattern = $this->pattern($page);
		}

		if($request->name)
		{
			$restaurants = Establishment::with('municipality')
									->where('type_establishment','restaurant')
									->where('state','activo')	
									->where('establishment_name','like',"%$request->name%")
									->paginate(10);
		}
		else if($request->municipality && $request->type_kitchen)
		{
			$restaurants = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->where('kitchen_type',$request->type_kitchen)->paginate(10);

			$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->pluck('kitchen_type');

		}
		else if($request->type_kitchen)
		{
			$restaurants = Establishment::with('municipality')->where('type_establishment','restaurant')->where('state','activo')->where('kitchen_type',$request->type_kitchen)->paginate(10);

			$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->where('state','activo')->pluck('kitchen_type');

		}
		else if($request->municipality)
		{
			$restaurants = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->paginate(10);

			$typeskitchen = Establishment::with('municipality')->where('type_establishment','restaurant')->whereHas('municipality',function ($q1) use($request)
			{
				$q1->where('slug',$request->municipality);
			})->where('state','activo')->pluck('kitchen_type');

		}
		else
		{
			$restaurants = Establishment::with('municipality')->where('state','activo')->where('type_establishment','restaurant')->paginate(10);
			$typeskitchen = Establishment::with('municipality')->where('state','activo')->where('type_establishment','restaurant')->pluck('kitchen_type');
		}
		
		$municipalities = Municipality::with('restaurants')->whereHas('Restaurants',function ($q1)
		{
			$q1->where('state','activo');
		})->get();

		foreach ($municipalities as $municipality) 
		{
			array_add($municipality,'cant_restaurants',$municipality->restaurants->count());
		}


		foreach ($restaurants as $position => $restaurant) 
		{
			$images = EstablishmentImage::where('fk_establishment',$restaurant->id_establishment)->get();
			if($images->count()>0)
			{
				array_add($restaurant,'image_link',$images->first()->link_image);
			}
			else
				array_add($restaurant,'image_link','front/img/restaurant1.jpg');
		}

		$restaurants = collect($restaurants);

		return response()->json(['cantRestaurants'=>$cantRestaurants,
								'municipalities'=>$municipalities,
								'patterns'=>$finalPattern,
								'restaurants'=>$restaurants,
								'typeskitchen'=>$typeskitchen,
								'pagination'=>[
									'total'			=>$restaurants['total'],
									'current_page'	=>$restaurants['current_page'],
									'per_page'		=>$restaurants['per_page'],
									'from'			=>$restaurants['from'],
									'last_page'		=>$restaurants['last_page'],
									'to'			=>$restaurants['to'],
									],
								]);
	}

	private function pattern(Page $page)
	{
		$parts = $page->Parts;
		$finalPattern = [];
		foreach($parts as $part)
		{
			$partPage  = PagePartPage::find($part->pivot->id_part_page);
			if(count($partPage->Pattern->where('state','activo'))>0)
			{	
				foreach($partPage->Pattern->where('state','activo') as $pattern)
				{
					if($pattern->activeByPart($partPage->id_part_page)->count()>0)
					{
						$finalPattern[$part->slug][] = $pattern;
					}
				}
			}
		}
		return $finalPattern;
	}




}
