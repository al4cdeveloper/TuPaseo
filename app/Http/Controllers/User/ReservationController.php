<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceOperator;
use App\Models\Reservation;
use App\Models\Schedule;
use App\User;
use Carbon\Carbon;
use Auth;
use Session;
use Cart;
use App\Models\Service;
use App\Models\Order;
use App\Models\OrderProduct;



class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('operator')->user()!=null)
        {
            Auth::guard('operator')->logout();
        }


        $services = ServiceOperator::where('state','Activo')->get();
        $sites = ServiceOperator::where('state','Activo')->get();

        foreach ($services as $service) 
        {
            $alldays =['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

            $horario = Schedule::where('fk_service',$service->id_service_operator)->where('state','active')->get();
            foreach($horario as $day)
            {
                if(in_array($day->day,$alldays))
                {
                    $posicion = (array_search($day->day, $alldays)); 
                    unset($alldays[$posicion]);
                }
            }
            array_add($service,'days',$alldays);
        }

        return view('reservation',compact('services','sites'));
    }

    public function returnServices()
    {
        $services = ServiceOperator::where('state','Activo')->get();
        foreach($services as $service)
        {
            $principalService = $service->principalService;
            $images = $service->images;
            $operator = $service->operador;
            $date = Carbon::now()->addDays($operator->days_for_reservation);
            $date = $date->format('d-m-Y');
            $comments = Reservation::where('fk_service',$service->id_service_operator)->where('state','Calificada')->get();

            if(count($comments)>1)
            {
                $acumulador=0;
                foreach ($comments as $comment) 
                {
                    $acumulador+= $comment->calification;
                }
    
                $average = round($acumulador/count($comments), 1);
            }
            else
            {
                $average=5;
            }
            array_add($service,'average',$average);

            array_add($service,"since_date",$date);
        }
        
        return ['services'=>$services,'mindate'=>$date];
    }

    public function prereservation(Request $request)
    {
        // $user = new User;
        // $user->first_name  =$request->nombres;
        // $user->last_name  =$request->apellidos;
        // $user->email  =$request->email;
        // $user->address  =$request->direccion;
        // $user->phone  =$request->telefono;
        // $user->password = bcrypt($request->password);
        // $user->save();

        // $reservation = new Reservation;
        // $reservation->fk_service = $request->service;
        // $reservation->fk_user = $user->id;
        // $reservation->date = $request->date;
        // $reservation->time = $request->time;
        // $reservation->state = "Por realizar";
        // $reservation->save();

        dd(Auth::User());

        return $user;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function freeHours($id,$date,$cuantity)
    {
        $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
        $dia = $dias[date('N', strtotime($date))] ;
        $serviceOperator = ServiceOperator::find($id);
        $schedule  = Schedule::where('fk_service',$id)->where('day',$dia)->first();

        if($schedule->hora_inicio!="")
        {
            $tiempo = [];
            if($schedule->tiempo_receso!=0)
            {// separo las partes de cada fecha
                            $iniHora = $schedule->hora_inicio;
                            list($hora, $min, $seg) = explode(":", $iniHora);
                            $tiempoIni = mktime($hora + 0, $min + 0, $seg + 0);
                    
                            $finHora = $schedule->hora_receso;
                            // hago lo mismo para obtener el $tiempoFin
                            list($hora, $min, $seg) = explode(":", $finHora);
                            $tiempoFin = mktime($hora + 0, $min + 0, $seg + 0);
                    
                            // al restar los valores, obtenemos los SEGUNDOS de diferencia
                            $cantidadHoras = ($tiempoFin - $tiempoIni)/3600;
                            $cantidadServiciosEnIntervaloDeTiempo = $cantidadHoras/ $serviceOperator->duration;
                    
                    
                            $horaInicial = $schedule->hora_inicio;
                            $ultimaHora;
                            for ($i=1; $i <=$cantidadServiciosEnIntervaloDeTiempo ; $i++) 
                            {
                                if($i ==1)
                                {
                                    array_push($tiempo,$horaInicial);
                                    $ultimaHora = date('H:s:i', strtotime($horaInicial. '+ '.$serviceOperator->duration.' hours'));
                                }
                                else
                                {
                                    array_push($tiempo,$ultimaHora);
                                    $adicionar = date('H:s:i', strtotime($ultimaHora. '+ '.$serviceOperator->duration.' hours'));
                                    $ultimaHora = $adicionar;
                                }
                            }
                    
                            //EN EL ARRAY TIEMPO TENEMOS LAS HORAS DISPONIBLES HASTA LA HORA ANTES DEL RECESO
                    
                            $horaDespuesReceso = date('H:s:i', strtotime($schedule->hora_receso. '+ '.$schedule->tiempo_receso.' hours'));
                            // separo las partes de cada fecha
                    
                            list($hora, $min, $seg) = explode(":", $horaDespuesReceso);
                            $tiempoIni = mktime($hora + 0, $min + 0, $seg + 0);
                    
                            $finHora = $schedule->hora_final;
                            // hago lo mismo para obtener el $tiempoFin
                            list($hora, $min, $seg) = explode(":", $finHora);
                            $tiempoFin = mktime($hora + 0, $min + 0, $seg + 0);
                    
                            // al restar los valores, obtenemos los SEGUNDOS de diferencia
                            $cantidadHorasDespuesReceso = ($tiempoFin - $tiempoIni)/3600;
                            $cantidadServiciosEnIntervaloDeTiempo = $cantidadHorasDespuesReceso/ $serviceOperator->duration;
                    
                            $horaInicial = $horaDespuesReceso;
                            $ultimaHora;
                            for ($i=1; $i <=$cantidadServiciosEnIntervaloDeTiempo ; $i++) 
                            {
                                if($i ==1)
                                {
                                    array_push($tiempo,$horaInicial);
                                    $ultimaHora = date('H:s:i', strtotime($horaInicial. '+ '.$serviceOperator->duration.' hours'));
                                }
                                else
                                {
                                    array_push($tiempo,$ultimaHora);
                                    $adicionar = date('H:s:i', strtotime($ultimaHora. '+ '.$serviceOperator->duration.' hours'));
                                    $ultimaHora = $adicionar;
                                }
                            }
            }
            //Se realiza la separación del algoritmo segun el tiempo de receso, debido a que si se separan al tener una jornada continua la cantidad de servicios no sería la misma por que de cierta manera se realiza una partición de horas
            else
            {
                $iniHora = $schedule->hora_inicio;
                list($hora, $min, $seg) = explode(":", $iniHora);
                $tiempoIni = mktime($hora + 0, $min + 0, $seg + 0);
        
                $finHora = $schedule->hora_final;
                // hago lo mismo para obtener el $tiempoFin
                list($hora, $min, $seg) = explode(":", $finHora);
                $tiempoFin = mktime($hora + 0, $min + 0, $seg + 0);
        
                // al restar los valores, obtenemos los SEGUNDOS de diferencia
                $cantidadHoras = ($tiempoFin - $tiempoIni)/3600;
                $cantidadServiciosEnIntervaloDeTiempo = $cantidadHoras/ $serviceOperator->duration;
        
        
                $horaInicial = $schedule->hora_inicio;
                $ultimaHora;
                for ($i=1; $i <=$cantidadServiciosEnIntervaloDeTiempo ; $i++) 
                {
                    if($i ==1)
                    {
                        array_push($tiempo,$horaInicial);
                        $ultimaHora = date('H:s:i', strtotime($horaInicial. '+ '.$serviceOperator->duration.' hours'));
                    }
                    else
                    {
                        array_push($tiempo,$ultimaHora);
                        $adicionar = date('H:s:i', strtotime($ultimaHora. '+ '.$serviceOperator->duration.' hours'));
                        $ultimaHora = $adicionar;
                    }
                }

            }
                //ESTA HABIENDO ERROR EN LA LECTURA DE HORAS, SE ESTÁ TOMANDO DOS HORAS DESPUES DE LA INICIAL Y DOS HORAS DESPUÉS DE LA FINAL, RECORDAR QUE ANTES DE HACER EL PRIMER PUSH HAY QUE HAY QUE HACER EL PUSH DE LA PRIMERA HORA (QUE NO TIENE MODIFICACIONES). 
                //  LISTO!! :3
                //La variable tiempo obtiene las horas en las cuales se puede prestar el serivicio
                $reservations  = Reservation::where('date',date('Y-m-d', strtotime(str_replace('/','-', $date))))->where('fk_service',$id)->where('state','!=','Cancelado')->get();
                if(count($reservations)>0)
                {
                    foreach ($reservations as $reservation) 
                    {
                        $cuposDisponibles = $serviceOperator->capacity -  $reservation->cuantity;
                        if($cuantity<=$cuposDisponibles)
                        {
                            //dd("cabe");

                        }
                        else
                        {
                            //dd("nocabe");
                            if(in_array($reservation->time,$tiempo))
                            {
                                $posicion = (array_search($reservation->time, $tiempo)); 
                                unset($tiempo[$posicion]);
                            }
                            
                        }

                    }
                }
                
        
                if(count($tiempo)>1)        
                {
                    return ['tiempo'=>$tiempo,'cantidad'=>count($tiempo)];
                }
                else      
                {
                    return ['tiempo'=>$tiempo,'cantidad'=>'-1'];
                }
            }
            else
            {
                $tiempo = "";
                return ['tiempo'=>$tiempo,'cantidad'=>'-1'];

            }
        
    }

    public function saveComment(Request $request)
    {
        $reservation = Reservation::find($request->id_service);
        $reservation->comment = $request->comment;
        $reservation->calification = $request->cantstars;
        $reservation->state="Calificada";
        $reservation->save();
        Session::flash('message','Se ha realizado la calificacion exitosamente.');
        return redirect()->back();

    }

    public function reservationLogerUser()
    {
        $cart = Cart::instance('shopping')->content();
        $user = Auth::user();
        if(count($cart)>0)
        {
            foreach ($cart as $service) 
            {
                $date = date_create($service->options->date);
                $reservation = new Reservation;
                $reservation->fk_service = $service->id;
                $reservation->fk_user = $user->id;
                $reservation->date =$date =date('Y-m-d', strtotime(str_replace('/','-', $service->options->date))); // => 2013-02-16,;
                $reservation->cuantity = $service->qty;
                $reservation->time = $service->options->time;
                $reservation->state = "Por realizar";
                $reservation->save();
            }

            $orden = Order::create([
                    'fk_user'  => $user->id,
                    'estado'    => 'pendiente',
                    'tipo_pago' => 'en sitio',
                    ]);
            foreach ($cart as $service)
            {
                $order_product = OrderProduct::create([
                            'fk_order'   => $orden->id_order,
                            'fk_service' => $service->id,
                            'units'      => $service->qty,
                            'unit_price' => $service->price
                            ]);
            } 

            $cart = Cart::instance('shopping')->destroy();
        }
        Session::flash('message','Se ha realizado la reservacion.');

        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
