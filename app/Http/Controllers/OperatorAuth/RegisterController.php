<?php

namespace App\Http\Controllers\OperatorAuth;

use App\Operator;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\ServiceOperator;
use Carbon\Carbon;
use Mail;
use App\Mail\ConfirmationAccount;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/operator/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('operator.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'national_register' => 'required|max:255',
            'contact_personal' => 'required|max:255',
            'contact_phone' => 'required|max:255',
            'email' => 'required|email|max:255|unique:operators|unique:users',
            'web' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
            'val-terms'=>'required',
            'data-protection'=>'required',
            'sustainability'=>'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Operator
     */
    protected function create(array $data)
    {
        $data['confirmation_code'] = str_random(25);


        $date = Carbon::now();
        $date = $date->format('Y-m-d');
        $operator = new Operator;
        $operator->name  =$data['name'];
        $operator->national_register  =$data['national_register'];
        $operator->contact_personal  =$data['contact_personal'];
        $operator->contact_phone  =$data['contact_phone'];
        $operator->email  =$data['email'];
        $operator->web  =$data['web'];
        $operator->terms= $date;
        $operator->status  = 'pending';
        $operator->data_protection= $date;
        $operator->sustainability= $date;
        $operator->confirmation_code = $data['confirmation_code'];
        if(isset($data['promotionals_mails']))
        {
            $operator->promotionals_mails= 1;
        }
        $operator->service_category  =1;
        $operator->commission = env('COMMISION_DEFECT');
        $operator->option_iva = 'mas';
        $operator->password = bcrypt($data['password']);
        $operator->save();

        Mail::to($operator->email,$operator->name)
            ->send(new ConfirmationAccount('operator',$data['confirmation_code'],$operator->name));


        return $operator;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('operator.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('operator');
    }

}
