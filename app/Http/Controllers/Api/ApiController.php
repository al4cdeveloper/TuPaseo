<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PagePart;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\Page;
use App\Models\PatternPart;
use App\Models\Order;
use App\Models\Reservation;
use App\Mail\ResendOrder;
use App\Mail\orderResend;
use Mail;
use Cache;
class ApiController extends Controller
{
	public function getSectionPart(Request $request)
	{	
		$pages = Page::all();
		$response = [];
		foreach ($pages as $page) 
		{
			if(count($page->Parts->where('type',$request->typePublicity))>0)
			{
				if($request->typePublicity=="Estatica")
				{
					foreach($page->Parts->where('type',$request->typePublicity) as $part)
					{
						$pattern = PatternPart::where('fk_pagepart',$part->pivot->id_part_page)->count();
						if($pattern == 0)
						{
							$response[$page->page_name][]=$part;
						}
					}
				}
				else
				$response[$page->page_name]=$page->Parts->where('type',$request->typePublicity);
			}
		}
		return response()->json($response);
	}

	public function getMunicipalities(Request $request)
	{
		$municipalities = Municipality::all()->where('state','activo')->where('fk_department',$request->department)->pluck('municipality_name','id_municipality');
		return response()->json($municipalities);
	}

	public function getInterestSites(Request $request)
	{
		$interestSites = InterestSite::all()->where('state','activo')->where('fk_municipality',$request->municipality)->pluck('site_name','id_site');
		return response()->json(['number'=>count($interestSites),
			'sites'=>$interestSites]);
	}

	public function resendOrder(Request $request)
	{
		if (!Cache::has("resendorder-$request->order")) 
		{
			$order = Order::find($request->order);
			//SEND MAIL
	        Mail::to($order->shipping_order->email)
	        ->bcc(env('MAIL_FROM'), '')
	        ->send(new ResendOrder($order));

 			Cache::put("resendorder-$request->order", $request->order, 20);

	        return $order->shipping_order->email;
	    }
	    else
	    {
	    	return "fail";
	    }
	}

	public function resendReservation(Request $request)
	{
		if (!Cache::has("resendreservation-$request->reservation")) 
		{
			$reservation = Reservation::find($request->reservation);
			//SEND MAIL
			$order = $reservation->orderProduct->order;
            $directService = $reservation->Service->PrincipalService;
            $operator = $reservation->Service->operador;
            $items =$reservation->orderProduct->items;

            Mail::send('emails.notifyOperator', ['orden'=> $order, 'service'=>$directService,'operator'=>$operator,'reservation'=>$reservation,'items'=>$items], function ($message) use ($operator,$reservation)
            {
                    $message->from(env('MAIL_FROM'), 'TuPaseo');
                    $message->to($operator->email)->subject('Copia de reservación #RSVT-'.str_pad($reservation->id_reservation, 6, "0", STR_PAD_LEFT));
            });

 			Cache::put("resendreservation-$request->reservation", $request->reservation, 20);

	        return $operator->email;
	    }
	    else
	    {
	    	return "fail";
	    }
	}

	public function responsePayU(Request $request)
	{
		$longitud =strlen(env('PAYU_REFERENCE_CODE'));

        $codigo =  substr($request->reference_sale,$longitud);
        $orden = Order::find($codigo);
        if($request->state_pol == 4)
        {
            $orden->estado = 'pagado';
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->state = 'pagado';
                $reservation->save();
            }
            
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new NotifyPay($orden, (int)$request->TX_VALUE));

        }
        else if($request->state_pol == 6)
        {
            $orden->estado = 'declinada';
            $orden->descripcion = $request->response_message_pol;
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->delete();
            }
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new Rejected($orden,(int)$request->TX_VALUE));
        }

        else if($request->state_pol == 5)
        {
            $orden->estado = 'expirada';
            $orden->descripcion = $request->response_message_pol;
            $orden->save();

            foreach($orden->services as $service)
            {
                $reservation = $service->reservation;
                $reservation->delete();
            }
            //SEND MAIL
            Mail::to($orden->shipping_order->email)
            ->bcc(env('MAIL_FROM'), '')
            ->send(new Rejected($orden,(int)$request->TX_VALUE));
        }
	}
}
