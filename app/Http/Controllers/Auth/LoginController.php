<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Session;
use Auth;
use App\User;
use App\Operator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public $redirectTo = '/';

    public function showLoginForm()
    {
        return view('auth.login');
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->back();
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function login(Request $request)
    {
        $email = $request->email;
        $user = User::where('email',$email)->first();
        if($user)
        {
            //VALIDACIÓN DE USER
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) 
            {
                if($user->status == 'pending')
                {
                    Session::flash('message-warning','Para poder disfrutar de nuestros servicios es necesario que confirmes tu cuenta.');
                    Auth::logout();
                    return redirect('/login')->with('pending', 'Cuenta pendiente de activar');
                }
                if($user->status == 'inactive')
                {
                    Session::flash('message-error','Su cuenta ha sido suspendida, para más información comuníquese con soporte.');
                    Auth::logout();
                    return redirect('/login');
                }
                return redirect()->back();
            }
            else
            {
                // If the class is using the ThrottlesLogins trait, we can automatically throttle
                // the login attempts for this application. We'll key this by the username and
                // the IP address of the client making these requests into this application.
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);

                    return $this->sendLockoutResponse($request);
                }

                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }

                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);

                
                return redirect('login')
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => Lang::get('auth.failed'),
                ]);
            }
        }
        else
        {
            $operator = Operator::where('email',$email)->first();
            if($operator)
            {
                if (Auth::guard('operator')->attempt(['email' => $request->email, 'password' => $request->password])) 
                {
                     if($operator->status == 'pending')
                    {
                        Session::flash('message-warning','Para poder disfrutar de nuestros servicios es necesario que confirmes tu cuenta.');
                        Auth::logout();
                        return redirect('/login')->with('pending', 'Cuenta pendiente de activar');
                    }
                    if($operator->status == 'inactive')
                    {
                        Session::flash('message-error','Su cuenta ha sido suspendida, para más información comuníquese con soporte.');
                        Auth::logout();
                        return redirect('/login');
                    }
                    return redirect()->intended('/operator/home');
                }
                else
                {
                    // If the class is using the ThrottlesLogins trait, we can automatically throttle
                    // the login attempts for this application. We'll key this by the username and
                    // the IP address of the client making these requests into this application.
                    if ($this->hasTooManyLoginAttempts($request)) {
                        $this->fireLockoutEvent($request);

                        return $this->sendLockoutResponse($request);
                    }

                    if ($this->attemptLogin($request)) {
                        return $this->sendLoginResponse($request);
                    }

                    // If the login attempt was unsuccessful we will increment the number of attempts
                    // to login and redirect the user back to the login form. Of course, when this
                    // user surpasses their maximum number of attempts they will get locked out.
                    $this->incrementLoginAttempts($request);

                    
                    return redirect('login')
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors([
                        $this->username() => Lang::get('auth.failed'),
                    ]);
                }
            }
            else
            {

                // If the class is using the ThrottlesLogins trait, we can automatically throttle
                // the login attempts for this application. We'll key this by the username and
                // the IP address of the client making these requests into this application.
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);

                    return $this->sendLockoutResponse($request);
                }

                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }

                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);

                
                return redirect('login')
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => Lang::get('auth.failed'),
                ]);
            }
        }
        // $this->validateLogin($request);


        // return $this->sendFailedLoginResponse($request);
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

}
