<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Operator;
use Session;
use Mail;
use App\Mail\ConfirmationAccount;
use Auth;
class ActivateAccountController extends Controller
{

	public function VerifyCodeOperator($code)
    {
        $operator = Operator::where('confirmation_code', $code)->first();

        if (! $operator)
            return redirect('/');

        $operator->status = 'active';
        $operator->confirmation_code = null;
        $operator->save();
        Session::flash('message','Haz activado correctamente tu cuenta');
		Auth::guard('operator')->login($operator);

        return redirect('/operator/home');
    }

    public function VerifyCodeUser($code)
    {
        $user = User::where('confirmation_code', $code)->first();

        if (! $user)
            return redirect('/');

        $user->status = 'active';
        $user->confirmation_code = null;
        $user->save();
        Session::flash('message','Haz activado correctamente tu cuenta');
		Auth::login($user);
        
        return redirect('/home');
    }


	public function showResendForm()
    {
    	return view('auth.emailVerifyCode');
    }

    public function validateEmail(Request $request)
	{
		$user = User::where('email',$request->email)->first();
		if($user)
		{
			if($user->status == 'pending')
			{
				$user->confirmation_code = str_random(25);
	    		$user->save();

	    		Mail::to($user->email,$user->name)
	        	->send(new ConfirmationAccount('user',$user->confirmation_code,$user->first_name. " ".$user->last_name));

	        	Session::flash('message','Se ha enviado nuevamente el link de confirmación de tu cuenta.');
				return redirect('login');
			}
			else
			{
				Session::flash('message-error','Tu cuenta no requiere ser activada');
				return redirect('/login');
			}
			
		}
		else
		{
			$operator = Operator::where('email',$request->email)->first();
			if($operator)
			{
				if($operator->status == 'pending')
				{
	        		$operator->confirmation_code = str_random(25);
	        		$operator->save();

	        		Mail::to($operator->email,$operator->name)
	            	->send(new ConfirmationAccount('operator',$operator->confirmation_code,$operator->name));

	            	Session::flash('message','Se ha enviado nuevamente el link de confirmación de tu cuenta.');
					return redirect('login');
				}
				else
				{
					Session::flash('message-error','Tu cuenta no requiere ser activada');
					return redirect('/login');
				}
			}
			else
			{
				Session::flash('message-error','No se ha encontrado su dirección de correo electrónico, por favor verifique e intente nuevamente');
				return redirect()->back();
			}
		}
	}
}
