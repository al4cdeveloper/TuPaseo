<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Reservation;
use Cart;
use App\Models\Order;
use Mail;
use App\Mail\ConfirmationAccount;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email'      => 'required|email|max:255|unique:users|unique:operators',
            'password'   => 'required|min:6|confirmed',
            'phone'   => 'required|numeric',
            'terms'      => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data['confirmation_code'] = str_random(25);

        $user = new User;
        $user->first_name  =$data['first_name'];
        $user->last_name  =$data['last_name'];
        $user->email  =$data['email'];
        $user->phone  =$data['phone'];
        $user->status  = 'pending';
        $user->confirmation_code = $data['confirmation_code'];
        if(isset($data['promotionals_mails']))
        {
            $user->promotionals_mails = 1;
        }
        $user->password = bcrypt($data['password']);
        $user->role="user";
        $user->save();

        Mail::to($user->email,$user->first_name)
            ->send(new ConfirmationAccount('user',$data['confirmation_code'],$user->first_name. " ".$user->last_name));

        return $user;
    }
}
