<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PayReport;
use Auth;
use Crypt;
class EconomyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operator = Auth::user();
        $reports = $operator->directPayReservations->groupBy('reportReservation.fk_report','desc');
        return view('operator.Economy.listPayReports',compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePayReport($id)
    {
        $id = Crypt::decrypt($id);
        $report = PayReport::find($id);
        $operators = $report->reservations->groupBy('fk_service');
        foreach($operators as $operator)
        {
            if($operator[0]->Service->operador->id_operator == Auth::user()->id_operator)
            {
                $reservations = $operator;
                $pdf = \PDF::loadView('reports.reportPayOperator',compact('report','reservations'));
                return $pdf->stream();

            }
            
        }
    }

   
}
