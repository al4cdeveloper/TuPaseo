<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\Establishment;
use App\Models\ServiceEstablishment;
use App\Models\Plate;
use App\Models\EstablishmentSchedule;
use App\Models\EstablishmentImage;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\Room;
use Session;
use File;
use Auth;
class establishmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        if($type=="restaurant")
        {
            $establishments = Auth::user()->restaurants;
        }
        else
        {
            $establishments = Auth::user()->hotels;
        }

        return view('operator.Establishment.listEstablishment',compact('establishments','type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $services = ServiceEstablishment::all();
        $cant = (int)round(count($services)/3);
        $departments = Department::where('state','activo')->get()->pluck('department_name','id_department')->prepend('','');
        if($type == "restaurant")
        {
            $kitchenType = [
                'Africana'=>'Africana',
                'Alemana'=>'Alemana',
                'Árabe'=>'Árabe',
                'Argentina'=>'Argentina',
                'Asiática'=>'Asiática',
                'Australiana'=>'Australiana',
                'Balti'=>'Balti',
                'Bar'=>'Bar',
                'Bar de vinos'=>'Bar de vinos',
                'Belga'=>'Belga',
                'Brasilera'=>'Brasilera',
                'Británica'=>'Británica',
                'Café'=>'Café',
                'Criolla'=>'Criolla',
                'Camboyana'=>'Camboyana',
                'Canadiense'=>'Canadiense',
                'Cantonés'=>'Cantonés',
                'Caribeña'=>'Caribeña',
                'Centroamericana'=>'Centroamericana',
                'Cervecería'=>'Cervecería',
                'Chilena'=>'Chilena',
                'China'=>'China',
                'China: Xinjiang'=>'China: Xinjiang',
                'Churrasquería'=>'Churrasquería',
                'Colombiana'=>'Colombiana',
                'Comida de calle'=>'Comida de calle',
                'Comida rápida'=>'Comida rápida',
                'Contemporánea'=>'Contemporánea',
                'Coreana'=>'Coreana',
                'Croata'=>'Croata',
                'Cubana'=>'Cubana',
                'Danesa'=>'Danesa',
                'De Hong Kong'=>'De Hong Kong',
                'De la India'=>'De la India',
                'Del Medio Oriente'=>'Del Medio Oriente',
                'Delicatessen'=>'Delicatessen',
                'Ecuatoriana'=>'Ecuatoriana',
                'Escandinava'=>'Escandinava',
                'Española'=>'Española',
                'Estadounidense'=>'Estadounidense',
                'Europea'=>'Europea',
                'Filipina'=>'Filipina',
                'Francesa'=>'Francesa',
                'Fusión'=>'Fusión',
                'Gastropub'=>'Gastropub',
                'Griega'=>'Griega',
                'Hawaiana'=>'Hawaiana',
                'Indonesia'=>'Indonesia',
                'Internacional'=>'Internacional',
                'Irlandesa'=>'Irlandesa',
                'Israelí'=>'Israelí',
                'Italiana'=>'Italiana',
                'Japonesa'=>'Japonesa',
                'Latina'=>'Latina',
                'Libanesa'=>'Libanesa',
                'Mariscos'=>'Mariscos',
                'Mediterránea'=>'Mediterránea',
                'Mexicana'=>'Mexicana',
                'Pakistaní'=>'Pakistaní',
                'Parrilla'=>'Parrilla',
                'Persa'=>'Persa',
                'Peruana'=>'Peruana',
                'Pizza'=>'Pizza',
                'Portuguesa'=>'Portuguesa',
                'Pub'=>'Pub',
                'Puertorriqueña'=>'Puertorriqueña',
                'Saludable'=>'Saludable',
                'Shanghai'=>'Shanghai',
                'Sichuan'=>'Sichuan',
                'Singapurense'=>'Singapurense',
                'Sopas'=>'Sopas',
                'Sudamericana'=>'Sudamericana',
                'Sueca'=>'Sueca',
                'Suiza'=>'Suiza',
                'Sushi'=>'Sushi',
                'Tailandesa'=>'Tailandesa',
                'Taiwanesa'=>'Taiwanesa',
                'Típica'=>'Típica',
                'Turca'=>'Turca',
                'Vasca'=>'Vasca',
                'Venezolana'=>'Venezolana',
                'Vietnamita'=>'Vietnamita',
            ];
        }
        if($type == 'hotel')
        {
            return view('operator.Establishment.createEditEstablishment',compact('services','type','cant','departments'));
        }
        else
        {   
            $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
            return view('operator.Establishment.createEditEstablishment',compact('services','type','cant','departments','kitchenType','days'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$type)
    {
        if($type == "restaurant")
        {
            $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'kitchen_type'  => 'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
             'category'  => 'required',
             'Lunes.inicio'=>'required',
             'Lunes.fin'=>'required',
             'Martes.inicio'=>'required',
             'Martes.fin'=>'required',
             'Miercoles.inicio'=>'required',
             'Miercoles.fin'=>'required',
             'Jueves.inicio'=>'required',
             'Jueves.fin'=>'required',
             'Viernes.inicio'=>'required',
             'Viernes.fin'=>'required',
             'Sabado.inicio'=>'required',
             'Sabado.inicio'=>'required',
             'Domingo.fin'=>'required',
             'Domingo.fin'=>'required',
             'category.1'=>'required',
             'plate.1.1'=>'required',
             'cost.1.1'=>'required',
            ]); 

            if ($validator->fails()) 
            {
                if(isset($validator->errors()->messages()['category.1']))
                {
                    $validator->getMessageBag()->add('category', 'Se debe agregar por lo menos una categoría de comida');
                }
                if(isset($validator->errors()->messages()['plate.1.1'])|| isset($validator->errors()->messages()['cost.1.1']))
                {
                    $validator->getMessageBag()->add('plates', 'Se debe agregar por lo menos un plato a la categoría');
                }
                if(isset($validator->errors()->messages()['latitude']) || isset($validator->errors()->messages()['latitude']))
                {
                    $validator->getMessageBag()->add('address', 'El campo dirección es requerido y debe ser una dirección válida.');
                }
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 

            if(Auth::user()->restaurants->count()<3)
            {
                $establishment = new Establishment;
                $establishment->type_establishment = $type;
                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }

                $establishment->kitchen_type = $request->kitchen_type;

                $establishment->fk_operator = Auth::user()->id_operator;
                $establishment->fk_municipality = $request->fk_municipality;
                $establishment->description = $request->description;
                $establishment->save();
                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }
                 //Creación de platos de establecimiento
                foreach($request->category as $id => $category)
                {
                    if($category!="")
                    {
                        foreach ($request->plate[$id] as $idPlate => $Rplate) 
                        {

                            if($Rplate)
                            {
                                $plate = new Plate;
                                $plate->plate = $Rplate;
                                $plate->cost = $request->cost[$id][$idPlate];
                                $plate->category = $category;
                                $plate->fk_establishment = $establishment->id_establishment;
                                $plate->save();
                               
                            }
                            
                        }
                    }
                }


                //Horarios de establecimiento
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];

                foreach($days as $day)
                {
                    $schedule = new EstablishmentSchedule;
                    $schedule->name = $day;
                    $schedule->start = $request->$day['inicio'];
                    $schedule->end = $request->$day['fin'];
                    if(isset($request->$day['noservice']))
                    {
                        $schedule->inactive = true;
                    }
                    $schedule->fk_establishment = $establishment->id_establishment;
                    $schedule->save();
                }

                if(Auth::user()->restaurant_panel==0)
                {
                    $operator = Auth::user();
                    $operator->restaurant_panel = 1;
                    $operator->save();
                }

                //Activación de municipios
                $municipality = Municipality::find($request->fk_municipality);
                if($municipality->front_state_restaurant!="activo")
                {
                    $municipality->front_state_restaurant = "activo";
                    $municipality->save();
                }


                Session::flash('message','Establecimiento registrado correctamente');
                return redirect('operator/establishment/'.$type);
            }
            else
            {
                Session::flash('message-error','No se permite crear más de 3 restaurantes');
                return redirect('operator/establishment/'.$type);
            }


        }//end if type
        elseif($type=="hotel")
        {
            $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
             'room.*'=>'required',
             'cuantity.*'=>'required',
             'capacity.*'=>'required',
             'bed.*'=>'required',
             'checkin.*'=>'required',
             'checkout.*'=>'required',
            ]); 

            if ($validator->fails()) 
            {
                foreach($validator->errors()->messages() as $errores => $mensajes)
                {
                    if((substr($errores,0,8) == "capacity") || (substr($errores,0,4) == "room") || (substr($errores,0,8) == "cuantity")|| (substr($errores,0,3) == "bed"))
                    {
                        $validator->getMessageBag()->add('rooms', 'Se debe ingresar toda la información de cada habitación.');
                    }
                }
                
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 
            if(Auth::user()->hotels->count()<3)
            {
                
                $establishment = new Establishment;
                $establishment->type_establishment = $type;
                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }

                $establishment->fk_operator = Auth::user()->id_operator;
                $establishment->fk_municipality = $request->fk_municipality;
                $establishment->description = $request->description;
                $establishment->save();
                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                 //Creación de cuartos de establecimiento
                foreach($request->room as $id => $room)
                {
                    $newRoom = new Room;
                    $newRoom->room = $room;
                    $newRoom->cuantity = $request->cuantity[$id];
                    $newRoom->capacity = $request->capacity[$id];
                    $newRoom->bed = $request->bed[$id];
                    if(isset($request->suite[$id]))
                    {
                        $newRoom->suite = true;
                    }
                    $newRoom->fk_establishment = $establishment->id_establishment;
                    $newRoom->save();
                }

                //Horarios de establecimiento

                $schedule = new EstablishmentSchedule;
                $schedule->name = 'checkin';
                $schedule->start = $request->checkin['inicio'];
                $schedule->end = $request->checkin['fin'];
                $schedule->fk_establishment = $establishment->id_establishment;
                $schedule->save();

                $schedule = new EstablishmentSchedule;
                $schedule->name = 'checkout';
                $schedule->start = $request->checkout['inicio'];
                $schedule->end = $request->checkout['fin'];
                $schedule->fk_establishment = $establishment->id_establishment;
                $schedule->save();

                if(Auth::user()->hotel_panel==0)
                {
                    $operator = Auth::user();
                    $operator->hotel_panel = 1;
                    $operator->save();
                }

                //Activación de municipios
                $municipality = Municipality::find($request->fk_municipality);
                if($municipality->front_state_hotel!="activo")
                {
                    $municipality->front_state_hotel = "activo";
                    $municipality->save();
                }


                Session::flash('message','Establecimiento registrado correctamente');
                return redirect('operator/establishment/'.$type);
            }
            else
            {
                Session::flash('message-error','No se permite crear más de 3 hoteles');
                return redirect('operator/establishment/'.$type);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            $services = ServiceEstablishment::all();
            $cant = (int)round(count($services)/3);
            $departments = Department::where('state','activo')->get()->pluck('department_name','id_department')->prepend('','');
            $type = $establishment->type_establishment;
            $department = Department::find($establishment->Municipality->fk_department)->id_department;
            $municipality = $establishment->Municipality;
            $municipalities = Department::find($department)->Municipalities->pluck('municipality_name','id_municipality');

            if($type == 'restaurant')
            {
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
                $kitchenType = [
                                'Africana'=>'Africana',
                                'Alemana'=>'Alemana',
                                'Árabe'=>'Árabe',
                                'Argentina'=>'Argentina',
                                'Asiática'=>'Asiática',
                                'Australiana'=>'Australiana',
                                'Balti'=>'Balti',
                                'Bar'=>'Bar',
                                'Bar de vinos'=>'Bar de vinos',
                                'Belga'=>'Belga',
                                'Brasilera'=>'Brasilera',
                                'Británica'=>'Británica',
                                'Café'=>'Café',
                                'Criolla'=>'Criolla',
                                'Camboyana'=>'Camboyana',
                                'Canadiense'=>'Canadiense',
                                'Cantonés'=>'Cantonés',
                                'Caribeña'=>'Caribeña',
                                'Centroamericana'=>'Centroamericana',
                                'Cervecería'=>'Cervecería',
                                'Chilena'=>'Chilena',
                                'China'=>'China',
                                'China: Xinjiang'=>'China: Xinjiang',
                                'Churrasquería'=>'Churrasquería',
                                'Colombiana'=>'Colombiana',
                                'Comida de calle'=>'Comida de calle',
                                'Comida rápida'=>'Comida rápida',
                                'Contemporánea'=>'Contemporánea',
                                'Coreana'=>'Coreana',
                                'Croata'=>'Croata',
                                'Cubana'=>'Cubana',
                                'Danesa'=>'Danesa',
                                'De Hong Kong'=>'De Hong Kong',
                                'De la India'=>'De la India',
                                'Del Medio Oriente'=>'Del Medio Oriente',
                                'Delicatessen'=>'Delicatessen',
                                'Ecuatoriana'=>'Ecuatoriana',
                                'Escandinava'=>'Escandinava',
                                'Española'=>'Española',
                                'Estadounidense'=>'Estadounidense',
                                'Europea'=>'Europea',
                                'Filipina'=>'Filipina',
                                'Francesa'=>'Francesa',
                                'Fusión'=>'Fusión',
                                'Gastropub'=>'Gastropub',
                                'Griega'=>'Griega',
                                'Hawaiana'=>'Hawaiana',
                                'Indonesia'=>'Indonesia',
                                'Internacional'=>'Internacional',
                                'Irlandesa'=>'Irlandesa',
                                'Israelí'=>'Israelí',
                                'Italiana'=>'Italiana',
                                'Japonesa'=>'Japonesa',
                                'Latina'=>'Latina',
                                'Libanesa'=>'Libanesa',
                                'Mariscos'=>'Mariscos',
                                'Mediterránea'=>'Mediterránea',
                                'Mexicana'=>'Mexicana',
                                'Pakistaní'=>'Pakistaní',
                                'Parrilla'=>'Parrilla',
                                'Persa'=>'Persa',
                                'Peruana'=>'Peruana',
                                'Pizza'=>'Pizza',
                                'Portuguesa'=>'Portuguesa',
                                'Pub'=>'Pub',
                                'Puertorriqueña'=>'Puertorriqueña',
                                'Saludable'=>'Saludable',
                                'Shanghai'=>'Shanghai',
                                'Sichuan'=>'Sichuan',
                                'Singapurense'=>'Singapurense',
                                'Sopas'=>'Sopas',
                                'Sudamericana'=>'Sudamericana',
                                'Sueca'=>'Sueca',
                                'Suiza'=>'Suiza',
                                'Sushi'=>'Sushi',
                                'Tailandesa'=>'Tailandesa',
                                'Taiwanesa'=>'Taiwanesa',
                                'Típica'=>'Típica',
                                'Turca'=>'Turca',
                                'Vasca'=>'Vasca',
                                'Venezolana'=>'Venezolana',
                                'Vietnamita'=>'Vietnamita',
                            ];
                return view('operator.Establishment.createEditEstablishment',compact('establishment','services','type','cant','departments','kitchenType','days','department','municipalities','municipality'));
            }
            else
            {
                return view('operator.Establishment.createEditEstablishment',compact('establishment','services','type','cant','departments','municipalities','municipality','department'));
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            if($establishment->type_establishment == "restaurant")
            {
                $validator = \Validator::make($request->all(), [
                 'establishment_name' => 'required|max:150',
                 'kitchen_type'  => 'required',
                 'address'  => 'required|max:150',
                 'latitude'  => 'required|max:150',
                 'longitude'  => 'required|max:150',
                 'phone'  => 'required|max:150',
                 'description'  => 'required',
                 'category'  => 'required',
                 'Lunes.inicio'=>'required',
                 'Lunes.fin'=>'required',
                 'Martes.inicio'=>'required',
                 'Martes.fin'=>'required',
                 'Miercoles.inicio'=>'required',
                 'Miercoles.fin'=>'required',
                 'Jueves.inicio'=>'required',
                 'Jueves.fin'=>'required',
                 'Viernes.inicio'=>'required',
                 'Viernes.fin'=>'required',
                 'Sabado.inicio'=>'required',
                 'Sabado.fin'=>'required',
                 'Domingo.inicio'=>'required',
                 'Domingo.fin'=>'required',
                 'category.1'=>'required',
                 'plate.1.1'=>'required',
                 'cost.1.1'=>'required',
                ]); 

                if ($validator->fails()) 
                {
                    if(isset($validator->errors()->messages()['category.1']))
                    {
                        $validator->getMessageBag()->add('category', 'Se debe agregar por lo menos una categoría de comida');
                    }
                    if(isset($validator->errors()->messages()['plate.1.1'])|| isset($validator->errors()->messages()['cost.1.1']))
                    {
                        $validator->getMessageBag()->add('plates', 'Se debe agregar por lo menos un plato a la categoría');
                    }
                    if(isset($validator->errors()->messages()['latitude']) || isset($validator->errors()->messages()['latitude']))
                    {
                        $validator->getMessageBag()->add('address', 'El campo dirección es requerido y debe ser una dirección válida.');
                    }
                    foreach ($validator->errors()->all() as $error)
                    {
                        Session::flash('message-error', $error);
                    }

                    return redirect()->back()->withErrors($validator)->withInput();
                } 

                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }

                $establishment->kitchen_type = $request->kitchen_type;

                $oldId = $establishment->fk_municipality;

                $establishment->fk_municipality = $request->fk_municipality;

                $establishment->description = $request->description;
                $establishment->save();

                //DESACTIVACION DE MUNICIPIO EN CASO DE SER DIFERENTE
                if($oldId != $request->fk_municipality)
                {
                    $oldMunicipality = Municipality::find($oldId);
                     //Desativación del anterior
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_restaurant = "inactivo";
                        $oldMunicipality->save();
                    }

                    $municipality = Municipality::find($request->fk_municipality);
                    if($municipality->front_state_restaurant!="activo")
                    {
                        $municipality->front_state_restaurant = "activo";
                        $municipality->save();
                    }
                    
                }   

                //FIN DESACTIVACIÓN MUNICIPIO

                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                foreach($establishment->Plates as $oldPlate)
                {
                    $oldPlate->delete();
                }

                 //Creación de platos de establecimiento
                foreach($request->category as $id => $category)
                {
                    if($category!="")
                    {
                        foreach ($request->plate[$id] as $idPlate => $Rplate) 
                        {

                            if($Rplate)
                            {
                                $plate = new Plate;
                                $plate->plate = $Rplate;
                                $plate->cost = $request->cost[$id][$idPlate];
                                $plate->category = $category;
                                $plate->fk_establishment = $establishment->id_establishment;
                                $plate->save();
                            }
                            
                        }
                    }
                }
                //Horarios de establecimiento
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];

                foreach($days as $day)
                {
                    $oldDay = EstablishmentSchedule::where('name',$day)->where('fk_establishment',$id)->first();
                    $oldDay->start = $request->$day['inicio'];
                    $oldDay->end = $request->$day['fin'];
                    if(isset($request->$day['noservice']))
                    {
                        $oldDay->inactive = true;
                    }
                    else
                    {
                        $oldDay->inactive = false;
                    }
                    $oldDay->save();
                }


                
                Session::flash('message','Establecimiento actualizado correctamente');
                return redirect('operator/establishment/'.$establishment->type_establishment);

            }//end if type
            elseif($establishment->type_establishment == "hotel")
            {
                $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
             'room.*'=>'required',
             'cuantity.*'=>'required',
             'capacity.*'=>'required',
             'bed.*'=>'required',
             'checkin.*'=>'required',
             'checkout.*'=>'required',
            ]); 

            if ($validator->fails()) 
            {
                foreach($validator->errors()->messages() as $errores => $mensajes)
                {
                    if((substr($errores,0,8) == "capacity") || (substr($errores,0,4) == "room") || (substr($errores,0,8) == "cuantity")|| (substr($errores,0,3) == "bed"))
                    {
                        $validator->getMessageBag()->add('rooms', 'Se debe ingresar toda la información de cada habitación.');
                    }
                }
                
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 

                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }

                $oldId = $establishment->fk_municipality;

                $establishment->fk_municipality = $request->fk_municipality;

                $establishment->description = $request->description;
                $establishment->save();

                //DESACTIVACION DE MUNICIPIO EN CASO DE SER DIFERENTE
                if($oldId != $request->fk_municipality)
                {
                    $oldMunicipality = Municipality::find($oldId);
                     //Desativación del anterior
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_hotel  = "inactivo";
                        $oldMunicipality->save();
                    }

                    $municipality = Municipality::find($request->fk_municipality);
                    if($municipality->front_state_hotel !="activo")
                    {
                        $municipality->front_state_hotel  = "activo";
                        $municipality->save();
                    }
                    
                }   

                //FIN DESACTIVACIÓN MUNICIPIO

                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                foreach($establishment->Rooms as $room)
                {
                    $room->delete();
                }  

                 //Creación de cuartos de establecimiento
                foreach($request->room as $id => $room)
                {
                    $newRoom = new Room;
                    $newRoom->room = $room;
                    $newRoom->cuantity = $request->cuantity[$id];
                    $newRoom->capacity = $request->capacity[$id];
                    $newRoom->bed = $request->bed[$id];
                    if(isset($request->suite[$id]))
                    {
                        $newRoom->suite = true;
                    }
                    $newRoom->fk_establishment = $establishment->id_establishment;
                    $newRoom->save();
                }

                //Horarios de establecimiento

                $schedule = EstablishmentSchedule::where('name','checkin')->where('fk_establishment',$establishment->id_establishment)->first();
                $schedule->start = $request->checkin['inicio'];
                $schedule->end = $request->checkin['fin'];
                $schedule->save();

                $schedule = EstablishmentSchedule::where('name','checkout')->where('fk_establishment',$establishment->id_establishment)->first();
                $schedule->start = $request->checkout['inicio'];
                $schedule->end = $request->checkout['fin'];
                $schedule->save();

                if(Auth::user()->hotel_panel==0)
                {
                    $operator = Auth::user();
                    $operator->hotel_panel = 1;
                    $operator->save();
                }

                Session::flash('message','Establecimiento actualizado correctamente');
                return redirect('operator/establishment/'.$establishment->type_establishment);
            
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function upload_images($id,ImageRequest $request)
    {
        $files = $request->file('file');
        $establishment = Establishment::find($id);
        $new_service = [];
        foreach ($files as $file) {
            $imageName = cargar_imagen($file,$establishment->type_establishment);
            if ($imageName)  
            {
                $new_image = ['link_image' => $imageName, 'fk_establishment' => $id];
                $imageEstablishment = EstablishmentImage::create($new_image);
                $new_service[] = $imageEstablishment->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = EstablishmentImage::find($id);

        $exists = File::exists(public_path($image->link_imagen));
        if ($exists) {
            File::delete(public_path($image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function desactivate($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            if($establishment->fk_operator == Auth::user()->id_operator)
            {
                $establishment->state = 'inactivo';
                $establishment->save();
                //Desativación del municipio en caso de no terner mas restaurantes
                $oldId = $establishment->fk_municipality;
                $oldMunicipality = Municipality::find($oldId);
                
                if($establishment->type_establishment == "restaurant")
                {
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_restaurant = "inactivo";
                        $oldMunicipality->save();
                    }
                }
                else
                {
                    $operatorsOldMunicipality = count($oldMunicipality->Hotels);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_hotel = "inactivo";
                        $oldMunicipality->save();
                    }
                }
            }
            else
            {
                 Session::flash('message-error','Accion no permitida.');
                return redirect()->back();
            }

            Session::flash('message','Establecimiento desactivado correctamente');
            return redirect('operator/establishment/'.$establishment->type_establishment);
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    } 

    public function images($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            if($establishment->fk_operator == Auth::user()->id_operator)
            {
                return view('operator.Establishment.imageEstablishment', compact('establishment'));
            }
            else
            {
                 Session::flash('message-error','Accion no permitida.');
                return redirect()->back();
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function update_images(Request $request)
    {
        foreach ($request->description as $id => $value) 
        {
            $image = EstablishmentImage::find($id);
            $image->description = $value;
            $image->save();
        }
        Session::flash('message','Se ha realizado la actualizacíón correctamente');
        return redirect()->back();
    }

    public function activate($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            if($establishment->fk_operator == Auth::user()->id_operator)
            {
                $establishment->state = 'activo';
                $establishment->save();
                 //Activación de municipios
                $municipality = Municipality::find($establishment->fk_municipality);
                if($establishment->type_establishment == "restaurant")
                {
                    if($municipality->front_state_restaurant!="activo")
                    {
                        $municipality->front_state_restaurant = "activo";
                        $municipality->save();
                    }
                }
                else
                {
                    if($municipality->front_state_hotel!="activo")
                    {
                        $municipality->front_state_hotel = "activo";
                        $municipality->save();
                    }
                }
                

            }
            else
            {
                 Session::flash('message-error','Accion no permitida.');
                return redirect()->back();
            }


            Session::flash('message','Establecimiento activado correctamente');
            return redirect('operator/establishment/'.$establishment->type_establishment);
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
