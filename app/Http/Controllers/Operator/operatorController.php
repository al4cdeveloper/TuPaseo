<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use App\Models\Service;
use App\Operator;
use App\Models\ServiceOperator;
use App\Models\Contact;
use App\User;
use Illuminate\Validation\Rule;
use Auth;
use Session;

class operatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $categories = ServiceCategory::all();
        // $services = Service::all();

        if(Auth::guard('operator')->user())
        {
            return redirect('/operator/home');
        }
        else
        {
            return redirect('/login');
        }
        
        //return view('infoOperator',compact('categories','services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ServiceCategory::all();
        $services = Service::all();
        return view('registerOperator',compact('categories','services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operator = new Operator;
        $operator->name  =$request->name;
        $operator->national_register  =$request->national_register;
        $operator->contact_personal  =$request->contact_personal;
        $operator->contact_phone  =$request->contact_phone;
        $operator->email  =$request->email;
        $operator->web  =$request->web;
        $operator->service_category  =$request->service_category;
        $operator->save();
        foreach($request->services  as $serviceO)
        {
            $service = new ServiceOperator;
            $service->fk_service = $serviceO;
            $service->fk_operator  =$operator->id;
            $service->save();
        } 
        $message  = "Se ha realizado tu registro correctamente!";
        return view('notifation',compact('message'));

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user(); 
        $cities = [];

        $url ='https://battuta.medunes.net/api/region/co/all/?key=00000000000000000000000000000000'; 
        $ch = curl_init($url); 
        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
        CURLOPT_HTTPHEADER => array('Accept: application/json'), 
        CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        curl_setopt_array( $ch, $options );     
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        // Cerrar el recurso cURL y liberar recursos del sistema 
        curl_close($ch);   
        $regions = json_decode($response, true); 

        if($user->region)
        {
            $region = str_replace(' ','%20',$user->region);
            $url ="https://battuta.medunes.net/api/city/co/search/?region=$region&key=00000000000000000000000000000000"; 
            $ch = curl_init($url); 
            // Configuring curl options 
            $options = array( 
                CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
            ); 
            // Setting curl options 
            curl_setopt_array( $ch, $options );     
            // Getting results 
            $response = curl_exec($ch); // Getting jSON result string   
            // Cerrar el recurso cURL y liberar recursos del sistema 
            curl_close($ch);   
            $cities = json_decode($response, true); 
        }


        return view('operator.profile',compact('user','regions','cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $operator     = Operator::find(Auth::user()->id_operator);
        if($request->password)
        {
            $validator = \Validator::make($request->all(), [
             'name' => 'required|max:255',
             'national_register'  => 'required|max:255',
             'nit'  => 'required|max:255',
             'contact_personal'  => 'required|max:255',
             'contact_phone'  => 'required|numeric',
             'address'  => 'required|max:255',
             'region'  => 'required|max:255',
             'city'  => 'required|max:255',
             'email'      => ['required','email',Rule::unique('operators')->ignore($operator->id_operator, 'id_operator'),Rule::unique('users')],
             'password'   => 'required|min:6|confirmed',
            ]); 
        }
        else
        {
            $validator = \Validator::make($request->all(), [
             'name' => 'required|max:255',
             'national_register'  => 'required|max:255',
             'nit'  => 'required|max:255',
             'contact_personal'  => 'required|max:255',
             'contact_phone'  => 'required|numeric',
             'address'  => 'required|max:255',
             'region'  => 'required|max:255',
             'city'  => 'required|max:255',
             'email'      => ['required','email',Rule::unique('operators')->ignore($operator->id_operator, 'id_operator'),Rule::unique('users')],
            ]); 
        }

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $files = $request->file('file');

            $operator->name  =$request->name;
            $operator->national_register  =$request->national_register;
            $operator->contact_personal  =$request->contact_personal;
            $operator->contact_phone  =$request->contact_phone;
            $operator->email  =$request->email;
            $operator->web  =$request->web;
            $operator->nit = $request->nit;
            $operator->address = $request->address;
            $operator->region = $request->region;
            $operator->city = $request->city;

            if($request->password)              $operator->password = bcrypt($request->password);
            if($files)
            {
                if($operator->avatar!="")
                {
                    chmod(public_path().'/images/profile',0777);
                    fopen(public_path().$operator->avatar, 'wb');
                    unlink(public_path().$operator->avatar);
                }
                $path = public_path().'/images/profile';

                $nombre ="";


                $filename = 'Profile_'.date('YmdHis', time()).rand().'.'.$files->getClientOriginalName();
                $nombre = '/images/profile/'.$filename;
                $files->move($path, $filename);

                $operator->avatar=$nombre;
            }
            $operator->save();

            Session::flash('message', 'Se ha realizado la actualización de información.');
            return redirect('operator/profile');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function preferences()
    {
        $user = Auth::User();
        return view('operator.preferences',compact('user'));
    }
    public function updatepreferences(Request $request)
    {
        $user = Auth::User();
        $user->days_for_reservation = $request->days_for_reservation;
        $user->save();
        Session::flash('message','Se ha realizado la actualización correctamente.');
        return redirect('operator/preferences');
    }

    public function bankInformation()
    {
        $user = Auth::user();
        $options = ['Cuenta ahorros'=>'Cuenta ahorros','Cuenta corriente'=>'Cuenta corriente'];
        return view('operator.bankInformation',compact('user','options'));
    }

    public function updateBankInformation(Request $request)
    {
         $validator = \Validator::make($request->all(), [
             'bank_name' => 'required|max:200',
             'account_number'  => 'required|max:50',
             'type_account'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 

        $user = Auth::user();
        $user->bank_name = $request->bank_name;
        $user->account_number = $request->account_number;
        $user->type_account = $request->type_account;
        $user->save();
        Session::flash('message','Se ha realizado la actualización correctamente.');
        return redirect('operator/bankinformation');
    }
}
