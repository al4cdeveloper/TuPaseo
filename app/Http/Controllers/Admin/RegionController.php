<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\URL;
//Este archivo se requiere crear manualmente php artisan make:request ImageRequest
use App\Http\Requests\ImageRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Region;
use App\Models\RegionImage;
use App\Admin;
use Auth;
use Session;
use File;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::orderBy('region_name')->get();
        return view('admin.Region.listRegion',compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Region.createEditRegion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'region_name' => 'required|max:50',
             'description'  => 'required',
             'link_image'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $files = $request->file('link_image');
            $link_image = $this->cargar_imagen($files);

            $region = Region::create([
                'region_name'=>$request->region_name,
                'sentence'=>$request->sentence,
                'description'  => $request->description,
                'link_image' => "images/regions/".$link_image,
                ]);

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/regions');
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $region = Region::where('slug',$slug)->first();
        if($region)
        {
            return view('admin.Region.createEditRegion',compact('region'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la región.");
            return redirect('admin/regions');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'region_name' => 'required|max:50',
             'description'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $region = Region::find($id);
            if($request->link_image)
            {
                $exists = File::exists(public_path($region->link_image));
                if ($exists) 
                {
                    File::delete(public_path($region->link_image));
                }
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen($files);

                $region->link_image = "images/regions/".$link_image;
                $region->save();

            }
            $region->fill(['region_name' => $request->region_name,
                            'sentence'=> $request->sentence,
                            'description'=> $request->description])->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/regions');
        }
    }


    public function desactivate($id)
    {
        $region = Region::find($id);
        if($region)
        {
            $region->state = "inactivo";
            $region->save();
            Session::flash('message','Se ha desactivado la región correctamente');
            return redirect('admin/regions');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la región.");
            return redirect('admin/regions');
        }
    }

    public function activate($id)
    {
        $region = Region::find($id);
        if($region)
        {
            $region->state = "activo";
            $region->save();
            Session::flash('message','Se ha activado la región correctamente');
            return redirect('admin/regions');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la región.");
            return redirect('admin/regions');
        }
    }

    public function images($slug)
    {
        $region = Region::where('slug',$slug)->first();
        return view('admin.Region.imagesRegion', compact('region'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/regions/'.$imageName, 'fk_region' => $id];
                $service = RegionImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = RegionImage::find($id);

        $exists = File::exists(public_path("images/regions/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/regions/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }
    
    public function updateImage(Request $request)
    {
        foreach ($request->description as $id => $value) 
        {
            $image = RegionImage::find($id);
            $image->description = $value;
            $image->save(); 
        }
        Session::flash('message','Se ha realizado la actualización correctamente');
        return redirect()->back();
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/regions/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/regions/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Region_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/regions'), $imageName);

        $exists = File::exists(public_path("images/regions/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
