<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\EcosystemCategory;
use Session;
use File;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('admin/Service/listService',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ServiceCategory::all()->pluck('service_category','id_category');
        $ecosystems = EcosystemCategory::all()->pluck('ecosystem_category','id_ecosystem_category');
        $typeService = [
                        'Alto impacto'=>'Alto impacto',
                        'Medio impacto'=>'Medio impacto',
                        'Bajo impacto'=>'Bajo impacto',
                        ];
        $icons = [
                    'icon_set_2_icon-102' => 'icon_set_2_icon-102',
                    'icon_set_2_icon-103' => 'icon_set_2_icon-103',
                    'icon_set_2_icon-105' => 'icon_set_2_icon-105',
                    'icon_set_2_icon-106' => 'icon_set_2_icon-106',
                    'icon_set_2_icon-107' => 'icon_set_2_icon-107',
                    'icon_set_2_icon-108' => 'icon_set_2_icon-108',
                    'icon_set_2_icon-109' => 'icon_set_2_icon-109',
                    'icon_set_2_icon-110' => 'icon_set_2_icon-110',
                    'icon_set_2_icon-111' => 'icon_set_2_icon-111',
                    'icon_set_2_icon-112' => 'icon_set_2_icon-112',
                    'icon_set_2_icon-104' => 'icon_set_2_icon-104',
                    'icon_set_2_icon-114' => 'icon_set_2_icon-114',
                    'icon_set_2_icon-115' => 'icon_set_2_icon-115',
                    'icon_set_2_icon-116' => 'icon_set_2_icon-116',
                    'icon_set_2_icon-117' => 'icon_set_2_icon-117',
                    'icon_set_2_icon-118' => 'icon_set_2_icon-118',
                    ];
        return view('admin/Service/createEditService',compact('categories','ecosystems','typeService','icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'service' => 'required|max:50',
             'description'  => 'required',
             'icon_class'  => 'required',
             'link_image'  => 'required',
             'map_ico'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            //imagen
            $imagen = $request->file('link_image');
            $link_image = cargar_imagen($imagen,"Service");
            //icon map
            $image = $request->file('map_ico');
            $map_ico = cargar_imagen($image,"Service");
            $video = "";
            if($request->video_url)
            {
                $video = youtube_match($request->video_url);
            }

            $service = Service::create([
                'service'=>$request->service,
                'type_service'=>$request->type_service,
                'description'  => $request->description,
                'image' => $link_image,
                'map_ico' => $map_ico,
                'video_url' => $video,
                'icon_class'=>$request->icon_class,
                'fk_service_category'  => $request->fk_service_category,
                'fk_ecosystem_category'  => $request->fk_ecosystem_category,
                ]);

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/activities/service');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $categories = ServiceCategory::all()->pluck('service_category','id_category');
        $ecosystems = EcosystemCategory::all()->pluck('ecosystem_category','id_ecosystem_category');
        $typeService = [
                        'Alto impacto'=>'Alto impacto',
                        'Medio impacto'=>'Medio impacto',
                        'Bajo impacto'=>'Bajo impacto',
                        ];
        $icons = [
                    'icon_set_2_icon-102' => 'icon_set_2_icon-102',
                    'icon_set_2_icon-103' => 'icon_set_2_icon-103',
                    'icon_set_2_icon-105' => 'icon_set_2_icon-105',
                    'icon_set_2_icon-106' => 'icon_set_2_icon-106',
                    'icon_set_2_icon-107' => 'icon_set_2_icon-107',
                    'icon_set_2_icon-108' => 'icon_set_2_icon-108',
                    'icon_set_2_icon-109' => 'icon_set_2_icon-109',
                    'icon_set_2_icon-110' => 'icon_set_2_icon-110',
                    'icon_set_2_icon-111' => 'icon_set_2_icon-111',
                    'icon_set_2_icon-112' => 'icon_set_2_icon-112',
                    'icon_set_2_icon-104' => 'icon_set_2_icon-104',
                    'icon_set_2_icon-114' => 'icon_set_2_icon-114',
                    'icon_set_2_icon-115' => 'icon_set_2_icon-115',
                    'icon_set_2_icon-116' => 'icon_set_2_icon-116',
                    'icon_set_2_icon-117' => 'icon_set_2_icon-117',
                    'icon_set_2_icon-118' => 'icon_set_2_icon-118',
                    ];
        $service = Service::where('slug',$slug)->first();
        if(!$service)
        {
            Session::flash('message-error','No se ha encontrado el servicio a actualizar');
            return redirect('admin/activities/service');
        }
        return view('admin/Service/createEditService',compact('categories','ecosystems','typeService','icons','service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'service' => 'required|max:50',
             'description'  => 'required',
             'icon_class'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $service = Service::find($id);
            if($request->link_image)
            {
                $exists = File::exists(public_path($service->link_image));
                if ($exists) 
                {
                    File::delete(public_path($service->link_image));
                }
                $image = $request->file('link_image');
                $link_image = cargar_imagen($image,"Service");

                $service->image = $link_image;
                $service->save();
            }
            if($request->map_ico)
            {
                $exists = File::exists(public_path($service->map_ico));
                if ($exists) 
                {
                    File::delete(public_path($service->map_ico));
                }
                $image = $request->file('map_ico');
                $map_ico = cargar_imagen($image,"Service");

                $service->map_ico = $map_ico;
                $service->save();
            }
            $video = "";

            if($request->video_url)
            {
                $video = youtube_match($request->video_url);
            }

            $service->fill([ 'service'=>$request->service,
                            'type_service'=>$request->type_service,
                            'description'  => $request->description,
                            'video_url' => $video,
                            'icon_class'=>$request->icon_class,
                            'fk_service_category'  => $request->fk_service_category,
                            'fk_ecosystem_category'  => $request->fk_ecosystem_category,])->save();
            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/activities/service');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
