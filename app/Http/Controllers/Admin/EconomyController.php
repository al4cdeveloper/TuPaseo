<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\PayReport;
use App\Mail\NotifyPayOperator;
use Mail;


//Documentación de plugin
//https://aprendible.com/series/laravel-tips/lecciones/laravel-pdf


class EconomyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd("perro a trabajar con todaaa este modulo, unos reportes en pdf, los arreglitos de taiga y los cambios de PAYU y sale");
        if($request->button)
        {
            $validator = \Validator::make($request->all(), [
            'initialDate'     => 'required|date|before:today',
            'finishDate'     => 'after:initialDate',
            ]);

            if ($validator->fails()) 
            {
                return redirect()->back()->withErrors($validator)->withInput();
            } 
        }
        $inital = $request->initialDate;
        $finish = $request->finishDate;
        $state = $request->state;

        if(!isset($request->paid))
            $paid = 0;
        else
        $paid = $request->paid;

        $options = ["realizadas"=>'Realizadas',"pagado"=>'Pagadas pero no realizadas',"pendiente"=>'Pendientes por pago'];
        if($paid)
        {
            if($request->initialDate != '' && $request->finishDate != '' && $request->state != '')
            {
                if($request->state == "realizadas")
                {
                    $reservations = Reservation::whereBetween('date',array($request->initialDate,$request->finishDate))
                                            ->where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })
                                            ->where('paid',0)
                                            ->get();
                }
                else
                {
                    $reservations = Reservation::where('state',$request->state)
                                            ->where('paid',0)
                                            ->whereBetween('date',[$request->initialDate,$request->finishDate])
                                            ->get();
                }
            }
            else if($request->initialDate != '' && $request->finishDate != '')
            {
                $reservations = Reservation::where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })
                                            ->where('paid',0)
                                            ->whereBetween('date',[$request->initialDate,$request->finishDate])
                                            ->get();
            }
            else if($request->state != '')
            {
                if($request->state == "realizadas")
                {
                    $reservations = Reservation::where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })
                                            ->where('paid',0)
                                            ->get();
                }
                else
                {
                    $reservations = Reservation::where('state',$request->state)
                                            ->where('paid',0)
                                            ->get();
                }
            }
            else
            {
                //$reservations = Reservation::where('state','Realizada')->orWhere('state','Calificada')->where('paid',0)->get();
                $reservations = [];
            }
        }
        else
        {
            if($request->initialDate != '' && $request->finishDate != '' && $request->state != '')
            {
                if($request->state == "realizadas")
                {
                    $reservations = Reservation::where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })
                                            ->where('paid',1)
                                            ->whereBetween('date',[$request->initialDate,$request->finishDate])
                                            ->get();
                }
                else
                {
                    $reservations = Reservation::where('state',$request->state)
                                            ->where('paid',1)
                                            ->whereBetween('date',[$request->initialDate,$request->finishDate])
                                            ->get();
                }
            }
            else if($request->initialDate != '' && $request->finishDate != '')
            {
                $reservations = Reservation::where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })
                                            ->where('paid',1)
                                            ->whereBetween('date',[$request->initialDate,$request->finishDate])
                                            ->get();
            }
            else if($request->state != '')
            {
                if($request->state == "realizadas")
                {
                    $reservations = Reservation::where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })
                                            ->where('paid',1)
                                            ->get();
                }
                else
                {
                    $reservations = Reservation::where('state',$request->state)
                                            ->where('paid',1)
                                            ->get();
                }
            }
            else
            {
                $reservations = Reservation::where(function ($query) {
                                                $query->where('state','Realizada')
                                                      ->orWhere('state','Calificada');
                                            })->where('paid',1)->get();
            }
        }


        return view('admin.Economy.consultReservations',compact('reservations','options','inital','finish','state','paid'));
    }

    public function generateReport(Request $request)
    {
        $reservations = Reservation::where(function ($query) {
                                        $query->where('state','Realizada')
                                              ->orWhere('state','Calificada');
                                    })
                                    ->where('paid',0)
                                    ->whereBetween('date',[$request->initial,$request->finish])
                                    ->get();
        $report = new PayReport;
        $report->date_start = $request->initial;
        $report->date_finish = $request->finish;
        $report->state = 'generado';
        $report->save();

        $report->reservations()->sync($reservations->pluck('id_reservation'));

        $create = $report->id_report;

        $pays = PayReport::where('state','generado')->orderBy('id_report','desc')->get();

        return view('admin.Economy.paysList',compact('pays','create'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historyPays(Request $request)
    {
        if(!$request->state)
        {
            $pays = PayReport::where('state','generado')->orderBy('id_report','desc')->get();
        }
        else
        {
            $pays = PayReport::where('state',$request->state)->orderBy('id_report','desc')->get();
        }
        return view('admin.Economy.paysList',compact('pays'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generatePDFReservation($id)
    {
        $report = PayReport::find($id);
        if($report)
        {
            $reservationsByOperator = $report->reservations->groupBy('fk_service');
            $pdf = \PDF::loadView('admin.Reports.ReservationReport',compact('reservationsByOperator','report'));
            return $pdf->stream();
        }
        else
        {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkPay($id)
    {
        $report = PayReport::find($id);
        if($report)
        {
            $report->state = 'pagado';
            $report->save();

            foreach($report->reservations as $reservation)
            {
                $reservation->paid=1;
                $reservation->save();
            }

            $operators = $report->reservations->groupBy('fk_service');

            foreach($operators as $operator)
            {
                Mail::to($operator[0]->Service->operador->email)
                ->bcc(env('MAIL_FROM'), '')
                ->send(new NotifyPayOperator($operator,$report));
            }
            return redirect('/admin/economy/history');
        }
        else
        {
            abort(403, 'Unauthorized action.');
        }
    }

    public function restartPay($id)
    {
        $report = PayReport::find($id);
        if($report)
        {
            $report->state = 'generado';
            $report->save();

            foreach($report->reservations as $reservation)
            {
                $reservation->paid=0;
                $reservation->save();
            }

            return redirect('/admin/economy/history');
        }
        else
        {
            abort(403, 'Unauthorized action.');
        }
    }

    public function cancelPay($id)
    {
        $report = PayReport::find($id);
        if($report)
        {
            $report->state = 'cancelado';
            $report->save();

            foreach($report->reservations as $reservation)
            {
                $reservation->paid=0;
                $reservation->save();
            }

            return redirect('/admin/economy/history');
        }
        else
        {
            abort(403, 'Unauthorized action.');
        }
    }

    public function reactivatePay($id)
    {
        $report = PayReport::find($id);
        if($report)
        {
            $report->state = 'generado';
            $report->save();

            return redirect('/admin/economy/history');
        }
        else
        {
            abort(403, 'Unauthorized action.');
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
