<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin; 
use Crypt;
use Session;
use Validator;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Admin::all();
        $role = ['superadmin'=>'superadmin',
                'generaladmin'=>'generaladmin',
                'contable'=>'contable',
                'periodismo'=>'periodismo'];
        return view('admin.User.listUser',compact('users','role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = ['superadmin'=>'superadmin',
                'generaladmin'=>'generaladmin',
                'contable'=>'contable',
                'periodismo'=>'periodismo'];
        return view('admin.User.createUser',compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required|max:100',
            'email'=>'required|email|unique:admins',
            'role' => 'required'
        ]);
        if($validator->fails())
        {
            foreach($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }

        $admin  = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->role = $request->role;
        $admin->password = bcrypt(env('DEFAULT_PASSWORD'));
        $admin->save();

        Session::flash('message','Se ha creado correctamente el usuario.');
        return redirect('admin/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restartPassword($id)
    {
        $user = Admin::find(Crypt::decrypt($id));
        $user->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user->save();

        Session::flash('message','Se ha restaurado la contraseña de la cuenta.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $user = Admin::find($id);

        return view('admin.User.createEditUser',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->save();

        Session::flash('message','Actualización realizada correctamente');
        return redirect('admin/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function desactivate($id)
    {
        $admin = Admin::find(Crypt::decrypt($id));
        if($admin)
        {
            $admin->state = 'inactive';
            $admin->save();
            Session::flash('message','Se ha desactivado al usuario');
        }
        else
            Session::flash('message-error','No se ha encotrado al usuario');
        return redirect('admin/admin');
    }

    public function activate($id)
    {
        $admin = Admin::find(Crypt::decrypt($id));
        if($admin)
        {
            $admin->state = 'active';
            $admin->save();
            Session::flash('message','Se ha activado al usuario');
        }
        else
            Session::flash('message-error','No se ha encotrado al usuario');
        return redirect('admin/admin');
    }
}
