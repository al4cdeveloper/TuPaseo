<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EcosystemCategory;
use File;
use Session;
class EcosystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ecosystems = EcosystemCategory::all();
        return view('admin/Ecosystem/listEcosystem',compact('ecosystems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/Ecosystem/createEditEcosystem');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'ecosystem_category' => 'required|max:50',
             'link_image'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $files = $request->file('link_image');
            $link_image = $this->cargar_imagen($files);
            $region = EcosystemCategory::create([
                'ecosystem_category'=>$request->ecosystem_category,
                'subtitle'=>$request->subtitle,
                'link_image' => "images/ecosystem/".$link_image,
                ]);

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/activities/ecosystem');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ecosystem = EcosystemCategory::find($id);
        return view('admin/Ecosystem/createEditEcosystem',compact('ecosystem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'ecosystem_category' => 'required|max:50',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $ecosystem = EcosystemCategory::find($id);
            if($request->link_image)
            {
                $exists = File::exists(public_path($ecosystem->link_image));
                if ($exists) 
                {
                    File::delete(public_path($ecosystem->link_image));
                }
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen($files);

                $ecosystem->link_image = "images/ecosystem/".$link_image;
                $ecosystem->save();

            }
            $ecosystem->fill(['ecosystem_category' => $request->ecosystem_category,
                            'subtitle'=> $request->subtitle
                                ])->save();


            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/activities/ecosystem');
        }
    }


    private function cargar_imagen($file, $imageName = false)
            {
                if ($imageName) 
                {
                    $exists = File::exists(public_path("images/ecosystem/".$imageName));
                    if ($exists) 
                    {
                        File::delete(public_path("images/ecosystem/".$imageName));
                    }

                    $image = explode('.', $imageName);
                    $imageName = $image[0].'.'.$file->getClientOriginalExtension();
                } 
                else 
                {
                    $imageName = 'Ecosystem_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
                }

                $file->move(public_path('images/ecosystem'), $imageName);

                $exists = File::exists(public_path("images/ecosystem/".$imageName));

                if ($exists) 
                {
                    return $imageName;
                } 
                else 
                {
                    return false;
                }
            }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
