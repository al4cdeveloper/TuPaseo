<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Region;
use App\Models\DepartmentImage;
use App\Models\DepartmentCircuit;
use App\Models\InterestSite;
use App\Models\Municipality;
use App\Admin;
use Session;
use Auth;
use File;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        return view('admin.Department.listDepartment',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::where('state','activo')->pluck('region_name', 'id_region');
        if(count($regions)==0)
        {
            Session::flash('message-error','No se encuentran regiones creadas. Para crear un departamento se requiere una región.');
            return redirect()->back();
        }
        else
        {
            return view('admin.Department.createEditDepartment',compact('regions'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'fk_region'=>'required',
             'department_name' => 'required|max:50',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $department = new Department;
            $department->department_name = $request->department_name;
            $department->fk_region = $request->fk_region;
            $department->short_description = $request->short_description;
            $department->save();

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/departments');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $department = Department::where('slug',$slug)->first();
        if($department)
        {
            $regions = Region::where('state','activo')->pluck('region_name', 'id_region');
            return view('admin.Department.createEditDepartment',compact('department','regions'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el departamento.");
            return redirect('admin/departments');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'fk_region'=>'required',
             'department_name' => 'required|max:50',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $department = Department::find($id);
            $department->department_name = $request->department_name;
            $department->fk_region = $request->fk_region;
            $department->short_description = $request->short_description;
            $department->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/departments');
        }
    }



    public function desactivate($id)
    {
        $department = Department::find($id);
        if($department)
        {
            $department->state = "inactivo";
            $department->save();
            Session::flash('message','Se ha desactivado el departamento correctamente');
            return redirect('admin/departments');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el departamento.");
            return redirect('admin/departments');
        }
    }

    public function activate($id)
    {
        $department = Department::find($id);
        if($department)
        {
            $department->state = "activo";
            $department->save();
            Session::flash('message','Se ha activado el departamento correctamente');
            return redirect('admin/departments');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el departamento.");
            return redirect('admin/departments');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
