<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pattern;
use App\Models\Customer;
use App\Models\PatternPart;
use App\Models\Page;
use Session;
use File;
use Carbon\Carbon;

class PatternController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patterns = Pattern::all();
        $now = Carbon::now();
        $now = $now->format('Y-m-d');
        foreach ($patterns as $pattern) 
        {
            if($pattern->publication_finish<$now)
            {
                $pattern->state = "inactivo";
                $pattern->save();
            }
            if($pattern->state=="inactivo" && $now>=$pattern->publication_day && $now<$pattern->publication_finish)
            {
                $pattern->state = "activo";
                $pattern->save();
            }
        }
        return view('admin.Pattern.listPattern',compact('patterns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all()->where('state','activo')->pluck('name','id_customer');
        $type = ['Estatica'=>'Estática','Carrusel'=>'Carrusel'];
        $multimediaType = ['','Imagen'=>'Imágen','Video'=>'Vídeo','Codigo'=>'Código'];
        return view('admin.Pattern.createEditPattern',compact('customers','type','multimediaType'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $validator = \Validator::make($request->all(), [
             'fk_customer'=>'required',
             'multimedia_type'=>'required',
             'type'=>'required',
             'publication_day'=>'required|date',
             'publication_finish'=>'required|date|after:publication_day',
             'pattern'=>'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        $pattern = new Pattern;
        //dd($request->all());
        $pattern->fk_customer = $request->fk_customer;
        $pattern->type = $request->type;
        $pattern->multimedia_type = $request->multimedia_type;
        if($request->redirection!=null) $pattern->redirection = $request->redirection;
        $pattern->publication_day = $request->publication_day;
        $pattern->publication_finish = $request->publication_finish;
        $now = Carbon::now();
        $now = $now->format('Y-m-d');

        $state = 'activo';
        if($now<$request->publication_day)
        {
            $state = 'inactivo';
        }
        $pattern->state = $state;

        if($request->multimedia_type=='Imagen')
        {
            $files = $request->file('pattern');

            $link_image = $this->cargar_imagen($files);

            $pattern->pattern = "images/patterns/".$link_image;
        }
        if($request->multimedia_type=='Video')
        {
            $pattern->pattern = youtube_match($request->pattern);
        }
        if($request->multimedia_type=='Codigo')
        {
            $pattern->pattern = $request->pattern;
        }

        $pattern->save();


        $pattern->Parts()->sync($request->page_parts,['pivot_state' => $state]);

        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/patterns/edit/'.$pattern->id_pattern);
        }
        else
        return redirect('admin/patterns');

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pattern = Pattern::findOrFail($id);
        $customers = Customer::all()->where('state','activo')->pluck('name','id_customer');
        $type = ['Estatica'=>'Estática','Carrusel'=>'Carrusel'];
        $multimediaType = ['','Imagen'=>'Imágen','Video'=>'Vídeo','Codigo'=>'Código'];
        $pages = Page::all();
        return view('admin.Pattern.createEditPattern',compact('pattern','customers','type','multimediaType','pages'));  
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pattern = Pattern::findOrFail($id);
        if($pattern->multimedia_type!=$request->multimedia_type)
        {
            $validacion = [
             'fk_customer'=>'required',
             'multimedia_type'=>'required',
             'type'=>'required',
             'publication_day'=>'required',
             'publication_finish'=>'required',
             'pattern'=>'required'
            ];
        }
        else
        {
            $validacion = [
             'fk_customer'=>'required',
             'multimedia_type'=>'required',
             'type'=>'required',
             'publication_day'=>'required',
             'publication_finish'=>'required',
            ];
        }
        $validator = \Validator::make($request->all(),$validacion ); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        }

        if($pattern->multimedia_type!=$request->multimedia_type && $pattern->multimedia_type=="Imagen")
        {
            $exists = File::exists(public_path($pattern->link_image));
            if ($exists) 
            {
                File::delete(public_path($pattern->link_image));
            }
        }

        $pattern->fk_customer = $request->fk_customer;
        $pattern->type = $request->type;
        $pattern->multimedia_type = $request->multimedia_type;
        $pattern->redirection = $request->redirection;
        $pattern->publication_day = $request->publication_day;
        $pattern->publication_finish = $request->publication_finish;
        $now = Carbon::now();

        $now = $now->format('Y-m-d');

        $state = 'activo';
        if($now<$request->publication_day)
        {
            $state = 'inactivo';
        }

        $pattern->state = $state;
        if($request->multimedia_type=='Imagen')
        {
            if($request->pattern)
            {
                $files = $request->file('pattern');

                $link_image = $this->cargar_imagen($files);

                $pattern->pattern = "images/patterns/".$link_image;
            }
            
        }
        if($request->multimedia_type=='Video')
        {
            $pattern->pattern = youtube_match($request->pattern);
        }
        if($request->multimedia_type=='Codigo')
        {
            $pattern->pattern = $request->pattern;
        }

        $pattern->save();
        foreach($pattern->Parts as $part)
        {
            $OldpatternPart = PatternPart::find($part->pivot->id_pattern_part);
            $OldpatternPart->state = 'inactivo';
            $OldpatternPart->save();
        }

        foreach ($request->page_parts as $newParts) 
        {
            $patternPart = PatternPart::where('fk_pattern',$pattern->id_pattern)->where('fk_pagepart',$newParts)->first();
            if($patternPart)
            {
                $patternPart->state = 'activo';
                $patternPart->save();
            }
            else
            {
                $pattern->Parts()->attach($newParts,['state' => 'activo','clicks'=>0]);
            }
        }

        //foreach($pattern->Parts as $parts)
        //{
        //    echo($parts->pivot->fk_pagepart." - ");
        //    echo($parts->pivot->state . "<br>");
        //}
        //dd("ok");
        Session::flash('message', 'Se ha realizado correctamente el registro de información.');
        if($request->typesubmit=="guardartodo")
        {
            return redirect('admin/patterns/edit/'.$pattern->id_pattern);
        }
        else
        return redirect('admin/patterns'); 
    }

    public function desactivate($id)
    {
        $pattern = Pattern::find($id);
        if($pattern)
        {
            $pattern->state = "inactivo";
            $pattern->publication_finish = "";
            $pattern->save();
            Session::flash('message','Se ha desactivado la publicidad correctamente');
            return redirect('admin/patterns');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la publicidad.");
            return redirect('admin/patterns');
        }
    }

    public function activate($id)
    {
        $pattern = Pattern::find($id);
        if($pattern)
        {
            $pattern->state = "activo";
            $pattern->save();
            Session::flash('message','Se ha activado la publicidad correctamente');
            return redirect('admin/patterns');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la publicidad.");
            return redirect('admin/patterns');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/patterns/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/patterns/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'pattern'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/patterns'), $imageName);

        $exists = File::exists(public_path("images/patterns/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
