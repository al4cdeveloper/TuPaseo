<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\InterestSiteCategory;
use App\Models\InterestSite;
use App\Models\InterestSiteImage;
use App\Models\Municipality;
use Session;
use File;
class InterestSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = InterestSite::all();
        return view('admin.InterestSite.listInterestSite',compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = InterestSiteCategory::where('state','activo')->pluck('category_name','id_category')->prepend(['Selecciona'=>null]);


        $municipalities = Municipality::where('state','activo')->pluck('municipality_name','id_municipality');

        return view('admin.InterestSite.createEditInterestSite',compact('categories','municipalities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'fk_category'=>'required',
             'fk_municipality'  => 'required',
             'site_name'  => 'required|max:40',
             'address'  => 'required|max:100',
             'description'  => 'required',
             'latitude'  => 'required',
             'longitude'  => 'required',
             'keywords'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $site = new InterestSite;
            $site->site_name = $request->site_name;
            $site->fk_category = $request->fk_category;
            $site->fk_municipality = $request->fk_municipality;
            $site->address = $request->address;
            $site->description = $request->description;
            $site->multimedia_type = "images";
            $site->latitude = $request->latitude;
            $site->longitude = $request->longitude;

            if(isset($request->link_image))
            {
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen_site($files);

                $site->link_image = "images/interestsites/".$link_image;
            }
            if(isset($request->link_icon))
            {
                $files = $request->file('link_icon');

                $link_icon = $this->cargar_imagen_site($files);

                $site->link_icon = "images/interestsites/".$link_icon;
            }
            $site->keywords = $request->keywords;
            $site->save();

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            if($request->typesubmit=="guardartodo")
            {
                return redirect('admin/interestsites/edit/'.$site->slug);
            }
            else
            return redirect('admin/interestsites');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $site = InterestSite::where('slug',$slug)->first();
        if($site)
        {
            $categories = InterestSiteCategory::where('state','activo')->pluck('category_name','id_category')->prepend(['Selecciona'=>null]);


            $municipalities = Municipality::where('state','activo')->pluck('municipality_name','id_municipality');
            return view('admin.InterestSite.createEditInterestSite',compact('categories','municipalities','site'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el sitio de interés.");
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'fk_category'=>'required',
             'fk_municipality'  => 'required',
             'site_name'  => 'required|max:40',
             'address'  => 'required|max:100',
             'description'  => 'required',
             'latitude'  => 'required',
             'longitude'  => 'required',
             'keywords'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $site = InterestSite::find($id);
            $site->site_name = $request->site_name;
            $site->fk_category = $request->fk_category;
            $site->fk_municipality = $request->fk_municipality;
            $site->address = $request->address;
            $site->latitude = $request->latitude;
            $site->longitude = $request->longitude;

            if(isset($request->link_image))
            {
                if($site->link_image)
                {
                    $exists = File::exists(public_path($site->link_image));
                    if ($exists) 
                    {
                        File::delete(public_path($site->link_image));
                    }
                }
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen_site($files);

                $site->link_image = "images/interestsites/".$link_image;
            }
            if(isset($request->link_icon))
            {
                if($site->link_icon)
                {
                    $exists = File::exists(public_path($site->link_icon));
                    if ($exists) 
                    {
                        File::delete(public_path($site->link_icon));
                    }
                }
                $files = $request->file('link_icon');

                $link_icon = $this->cargar_imagen_site($files);

                $site->link_icon = "images/interestsites/".$link_icon;
            }
            $site->description = $request->description;
            $site->keywords = $request->keywords;
            $site->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            if($request->typesubmit=="guardartodo")
            {
                return redirect('admin/interestsites/edit/'.$site->slug);
            }
            else
            return redirect('admin/interestsites');
        }
    }

    public function desactivate($id)
    {
        $site = InterestSite::find($id);
        if($site)
        {
            $site->state = "inactivo";
            $site->save();
            Session::flash('message','Se ha desactivado el sitio de interés correctamente');
            return redirect('admin/interestsites');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el sitio de interés.");
            return redirect('admin/interestsites');
        }
    }

    public function activate($id)
    {
        $site = InterestSite::find($id);
        if($site)
        {
            $site->state = "activo";
            $site->save();
            Session::flash('message','Se ha activado el sitio de interés correctamente');
            return redirect('admin/interestsites');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el sitio de interés.");
            return redirect('admin/interestsites');
        }
    }

    public function images($slug)
    {
        $site = InterestSite::where('slug',$slug)->first();
        return view('admin.InterestSite.imageInterestSite', compact('site'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen_site($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/interestsites/'.$imageName, 'fk_site' => $id];
                $service = InterestSiteImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = InterestSiteImage::find($id);

        $exists = File::exists(public_path("images/interestsites/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/interestsites/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function cargar_imagen_site($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/interestsites/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/interestsites/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'site'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/interestsites'), $imageName);

        $exists = File::exists(public_path("images/interestsites/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }


    //CATEGORY


    public function indexCategory()
    {
        // $categories = InterestSiteCategory::where('state','activo')->get();
        $categories = InterestSiteCategory::all();
        return view('admin.InterestSiteCategory.listInterestSiteCategory',compact('categories'));
    }

    public function createCategory()
    {
        return view('admin.InterestSiteCategory.createEditInterestSiteCategory');
    }

    public function storeCategory(Request $request)
    {

        $validator = \Validator::make($request->all(), [
             'category_name'=>'required',
             'default_avatar'  => 'required',
             'default_icon'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $files = $request->file('default_avatar');
            $default_avatar = $this->cargar_imagen($files);

            $files = $request->file('default_icon');
            $default_icon = $this->cargar_imagen($files);


            $category = new InterestSiteCategory;
            $category->category_name = $request->category_name;
            $category->default_image = "images/interestsitescategory/".$default_avatar;
            $category->default_icon = "images/interestsitescategory/".$default_icon;
            $category->save();

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/interestsites/category');
        }
    }

    public function editCategory($slug)
    {
        $category = InterestSiteCategory::where('slug',$slug)->first();
        if($category)
        {
            return view('admin.InterestSiteCategory.createEditInterestSiteCategory',compact('category'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la categoría.");
            return redirect('admin/interestsites/category');
        }
    }

    public function updateCategory(Request $request,$id)
    {
        $validator = \Validator::make($request->all(), [
             'category_name'=>'required',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $category = InterestSiteCategory::find($id);
            $category->category_name = $request->category_name;

            if($request->default_avatar)
            {
                $exists = File::exists(public_path($category->default_image));
                if ($exists) 
                {
                    File::delete(public_path($category->default_image));
                }
                $files = $request->file('default_avatar');

                $default_avatar = $this->cargar_imagen($files);

                $category->default_image = "images/interestsitescategory/".$default_avatar;

            }
            if($request->default_icon)
            {
                $exists = File::exists(public_path($category->default_icon));
                if ($exists) 
                {
                    File::delete(public_path($category->default_icon));
                }
                $files = $request->file('default_icon');

                $default_icon = $this->cargar_imagen($files);

                $category->default_icon = "images/interestsitescategory/".$default_icon;
            }


            $category->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/interestsites/category');
        }
    }

    public function desactivateCategory($id)
    {
        $category = InterestSiteCategory::find($id);
        if($category)
        {
            $category->state = "inactivo";
            $category->save();
            Session::flash('message','Se ha desactivado la categoria correctamente');
            return redirect('admin/interestsites/category');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la categoria.");
            return redirect('admin/interestsites/category');
        }
    }

    public function activateCategory($id)
    {
        $category = InterestSiteCategory::find($id);
        if($category)
        {
            $category->state = "activo";
            $category->save();
            Session::flash('message','Se ha activado la categoria correctamente');
            return redirect('admin/interestsites/category');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la categoria.");
            return redirect('admin/interestsites/category');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/interestsitescategory/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/interestsitescategory/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'category'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/interestsitescategory'), $imageName);

        $exists = File::exists(public_path("images/interestsitescategory/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }
}
