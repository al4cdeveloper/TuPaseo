<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Models\Reservation;
use App\Operator;
use App\User;
use App\Models\ServiceOperator;
use App\Models\Schedule;
use App\Models\Service;
use App\Models\ServicePicture;
use App\Models\ServiceItem;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\Establishment;
use App\Models\ServiceEstablishment;
use App\Models\EstablishmentSchedule;
use App\Models\Room;
use App\Models\EstablishmentImage;
use App\Models\Plate;
use File;
use Crypt;
use Session;
class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function operators()
    {
        $operators = Operator::all();
        foreach($operators as $operator)
        {
            $totalReservations = 0;
            $totalServices = 0;
            foreach($operator->directService as $service)
            {
                $totalReservations += count($service->reservations);
                
                foreach ($service->reservations as $thisservice) 
                {
                    if($thisservice->state == "Calificada" || $thisservice->state == "Realizada")
                    {
                        $totalServices ++;
                    }
                }
            }

            array_add($operator,'num_restaurants',count($operator->restaurants));
            array_add($operator,'num_hotels',count($operator->hotels));


            array_add($operator,'totalReservations',$totalReservations);
            array_add($operator,'totalServices',$totalServices);            
        }
        // $ch = curl_init();

        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_URL,"https://www.rues.org.co/RNT/ConsultaNumRNT_json");
        // curl_setopt($ch, CURLOPT_URL,"https://www.rues.org.co/RNT/ConsultaNITRNT_json");
        // curl_setopt($ch, CURLOPT_URL,"http://versionanterior.rues.org.co/RUES_Web/Consultas/ConsultaRNT_json");
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
        //             "txtRNT=404");
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
                    // "txtNIT=860029002");
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
                    // "strRNT=404");

        // in real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 
        //          http_build_query(array('postvar1' => 'value1')));

        // receive server response ...
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // $server_output = curl_exec ($ch);

        // curl_close ($ch);
        // dd(json_decode($server_output, true));
        // further processing ....

        return view('admin.Operator.listOperator',compact('operators'));
    }

    public function updateOperator($id,Request $request)
    {

        $operator = Operator::find($id);
        $operator->commission = $request->commission;
        $operator->option_iva = $request->option_iva;
        $operator->cuantity_hotels = $request->cuantity_hotels;
        $operator->cuantity_restaurants = $request->cuantity_restaurants;
        $operator->cuantity_services = $request->cuantity_services;
        $operator->save();

        Session::flash('message','Se ha actualizado la información correctamente');
        return redirect("admin/operators?operator=$id");
    }

    public function operatorServices($id)
    {
        $operator = Operator::find($id);
        $services = $operator->directOption;
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];

        foreach($services as $service)
        {
            $horario = Schedule::where('fk_service',$service->id_service_operator)->where('state','active')->get();

            if(count($horario)>0)
            {
                array_add($service,'days',$horario);
            }

        }

        // return ['services'=>$services,'days'=>$days];

        return view('admin.Operator.listService',compact('services','days','operator'));
    }

    public function editServiceOperator($id)
    {
        $service = ServiceOperator::find($id);
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
        $services = Service::all();
        $departments = Department::where('state','activo')->get()->pluck('department_name','id_department')->prepend('','');

        if($service->type_relation=="Municipality")
        {
            $department = Department::find($service->Municipality->fk_department)->id_department;
            $municipality = $service->Municipality;
        }
        else
        {
            $municipality = Municipality::find($service->interestSite->fk_municipality); 
            $department = Department::find($municipality->fk_department)->id_department;
        }

        $municipalities = Department::find($department)->Municipalities->pluck('municipality_name','id_municipality');
        return view('admin.Operator.editService',compact('services','days','service','departments','municipalities','department','municipality'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateServiceOperator($id,Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'service'     => 'required|max:150',
            'service_name'     => 'required|max:50',
            'cost' => 'required|numeric',
            'address'    => 'required|max:150',
            'capacity'    => 'required|numeric|max:50',
            'duration'    => 'required|numeric',
            'description'    => 'required',
            'requisites'    => 'required',
            'fk_municipality'    => 'required'
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else
        {
            $service = ServiceOperator::find($id);
            $oldId = $service->fk_municipality;

            $service->fk_municipality = $request->fk_municipality;

            if($request->fk_site)
                $service->fk_interest_site  = $request->fk_site;
            $service->service_name = $request->service_name;
            $service->cost = $request->cost;
            $service->address = $request->address;
            $service->capacity = $request->capacity;
            $service->duration = $request->duration;

            $service->requisites = $request->requisites;
            $service->description = $request->description;
            $service->save();

            if($oldId != $request->fk_municipality)
            {
                $oldMunicipality = Municipality::find($oldId);
                 //Desativación del anterior
                $operatorsOldMunicipality = count($oldMunicipality->Plans);
                if($operatorsOldMunicipality<1)
                {
                    $oldMunicipality->front_state = "inactivo";
                    $oldMunicipality->save();
                }

                $municipality = Municipality::find($request->fk_municipality);
                if($municipality->front_state!="activo")
                {
                    $municipality->front_state = "activo";
                    $municipality->save();
                }
                
            }
            
            
            \Session::flash('message', 'Se ha realizado la actualización correctamente');

            return redirect('admin/operators/services/'.$service->operador->id_operator);
        }
    }


    public function imagesServiceOperator($id)
    {
        $service = ServiceOperator::find($id);
        $images = ServicePicture::where('fk_service',$service->id_service_operator)->get();

        return view('admin.Operator.imageService', compact('images','service'));
    }

    public function itemsOperator($id)
    {
        $items = ServiceItem::all()->where('fk_service',$id);
        $service = ServiceOperator::find($id);
        return view('admin.Operator.listItem',compact('items','service'));
    }

    public function editItemOperator($id)
    {
        $item = ServiceItem::find($id);
        $service = ServiceOperator::where('id_service_operator',$item->fk_service)->first();
        return view('admin.Operator.editItem',compact('item','service'));
    }

    public function updateItemOperator($id,Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'item_name'     => 'required|max:150',
            'cost' => 'required',
            'description'    => 'required',
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else 
        {
            $item = ServiceItem::find($id);
            $item->item_name = $request->item_name;
            $item->cost = $request->cost;
            $item->description = $request->description;
            $item->fk_service = $request->fk_service;
            $item->save();

            Session::flash('message','Se ha actualizado la infomación correctamente');
            return redirect('admin/operators/items/'.$request->fk_service);
        }
    }

    public function desactivateItem($id)
    {
        $item = ServiceItem::find($id);
        if($item)
        {
            $item->state = "Desactivado por administrador";
            $item->save();
            Session::flash('message','Se ha desactivado el servicio adicional correctamente');
            return redirect('admin/operators/items/'.$item->fk_service);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio adicional.");
            return redirect('admin/operators');
        }
    }

    public function activateItem($id)
    {
        $item = ServiceItem::find($id);
        if($item)
        {
            $item->state = "activo";
            $item->save();
            Session::flash('message','Se ha activado el servicio adicional correctamente');
            return redirect('admin/operators/items/'.$item->fk_service);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio adicional.");
            return redirect('admin/operators');
        }
    }

    public function desactivate($id)
    {
        $service = ServiceOperator::find($id);
        if($service)
        {
            $service->state = "Desactivado por administrador";
            $service->save();
            $oldId = $service->fk_municipality;

            $oldMunicipality = Municipality::find($oldId);
             //Desativación del anterior
            $operatorsOldMunicipality = count($oldMunicipality->Plans);
            if($operatorsOldMunicipality<1)
            {
                $oldMunicipality->front_state = "inactivo";
                $oldMunicipality->save();
            }

                
            Session::flash('message','Se ha desactivado el servicio correctamente');
            return redirect('admin/operators/services/'.$service->operador->id_operator);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio.");
            return redirect('admin/operators');
        }
    }

    public function activate($id)
    {
        $service = ServiceOperator::find($id);
        if($service)
        {
            $service->state = "Activo";
            $service->save();
            $municipality = Municipality::find($service->fk_municipality);
            if($municipality->front_state!="activo")
            {
                $municipality->front_state = "activo";
                $municipality->save();
            }
            Session::flash('message','Se ha activado el servicio correctamente');
            return redirect('admin/operators/services/'.$service->operador->id_operator);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio.");
            return redirect('admin/operators');
        }
    }

    public function services()
    {
        $services = ServiceOperator::where('state','Activo')->get();
        return view('admin.listServices',compact('services'));
    }

    public function detailService($slug)
    {
        $service = ServiceOperator::where('slug',$slug)->first();
        // dd($service);
        $comments = Reservation::where('fk_service',$service->id_service_operator)->where('state','Calificada')->get();

        if(count($comments)>1)
        {
            $acumulador=0;
            foreach ($comments as $comment) 
            {
                $acumulador+= $comment->calification;
            }

            $average = round($acumulador/count($comments), 1);
        }
        else
        {
            $average=5;
        }

        array_add($service,'average',$average);
        return view('admin.detailService',compact('service','comments'));
    }

    public function completeReservationSpecific($nationalregister)
    {
        $operator = Operator::where('national_register',$nationalregister)->first();
        if($operator)
        {
            $completeService= [];
            foreach($operator->directService as $service)
            {            
                foreach ($service->reservations as $thisservice) 
                {
                    if($thisservice->state == "Calificada" || $thisservice->state =="Realizada")
                    {
                        $completeService[] = $thisservice;
                    }
                }
            }
            $completeServices  = ($completeService);
            if(count($completeServices)>0)
            {
                return view('admin.completeServices',compact('completeServices'));
            }
            else
            {
                \Session::flash('message-error', 'No se han encontrado servicios completados para este operador');
                return redirect('admin/complreservation');
                
            }
        }
        else
        {
            \Session::flash('message-error', 'No se han encontrado el operador');
            return redirect('admin/complreservation');
        }

    }
     public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/services/'.$imageName, 'fk_service' => $id];
                $service = ServicePicture::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = ServicePicture::find($id);

        $exists = File::exists(public_path("images/services/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/services/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function desactivateOperator($id)
    {
        $operator = Operator::find($id);
        $operator->status = 'inactive';
        $operator->save();
        foreach($operator->directService as $services)
        {
            $services->state = 'Desactivado por administrador';
            $services->save();
        }
        foreach($operator->restaurants as $restaurant)
        {
            $restaurant->state = 'Desactivado por administrador';
            $restaurant->save();
        }
        foreach($operator->hotels as $hotel)
        {
            $hotel->state = 'Desactivado por administrador';
            $hotel->save();
        }
        Session::flash('message','Se ha desactivado al operador, sus actividades, hoteles y restaurantes');
        return redirect('admin/operators');
    }

    public function activateOperator($id)
    {
        $operator = Operator::find($id);
        $operator->status = 'active';
        $operator->save();
        foreach($operator->directService as $services)
        {
            $services->state = 'inactivo';
            $services->save();
        }
        foreach($operator->restaurants as $restaurant)
        {
            $restaurant->state = 'inactivo';
            $restaurant->save();
        }
        foreach($operator->hotels as $hotel)
        {
            $hotel->state = 'inactivo';
            $hotel->save();
        }
        Session::flash('message','Se ha activado al operador y sus actividades han quedado inactivas, pero el operador puede activarlas');
        return redirect('admin/operators');
    }

    public function establishment($type,$id_operator)
    {
        $operator = Operator::find(Crypt::decrypt($id_operator));
        $establishments = [];
        if($type=="restaurant")
        {
            $establishments = $operator->restaurants;
        }
        else
        {
            $establishments = $operator->hotels;
        }

        return view('admin.Establishment.listEstablishment',compact('establishments','type','operator'));
    }


    public function editEstablishment($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            $services = ServiceEstablishment::all();
            $cant = (int)round(count($services)/3);
            $departments = Department::where('state','activo')->get()->pluck('department_name','id_department')->prepend('','');
            $type = $establishment->type_establishment;
            $department = Department::find($establishment->Municipality->fk_department)->id_department;
            $municipality = $establishment->Municipality;
            $municipalities = Department::find($department)->Municipalities->pluck('municipality_name','id_municipality');

            if($type == 'restaurant')
            {
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
                $kitchenType = [
                                'Africana'=>'Africana',
                                'Alemana'=>'Alemana',
                                'Árabe'=>'Árabe',
                                'Argentina'=>'Argentina',
                                'Asiática'=>'Asiática',
                                'Australiana'=>'Australiana',
                                'Balti'=>'Balti',
                                'Bar'=>'Bar',
                                'Bar de vinos'=>'Bar de vinos',
                                'Belga'=>'Belga',
                                'Brasilera'=>'Brasilera',
                                'Británica'=>'Británica',
                                'Café'=>'Café',
                                'Criolla'=>'Criolla',
                                'Camboyana'=>'Camboyana',
                                'Canadiense'=>'Canadiense',
                                'Cantonés'=>'Cantonés',
                                'Caribeña'=>'Caribeña',
                                'Centroamericana'=>'Centroamericana',
                                'Cervecería'=>'Cervecería',
                                'Chilena'=>'Chilena',
                                'China'=>'China',
                                'China: Xinjiang'=>'China: Xinjiang',
                                'Churrasquería'=>'Churrasquería',
                                'Colombiana'=>'Colombiana',
                                'Comida de calle'=>'Comida de calle',
                                'Comida rápida'=>'Comida rápida',
                                'Contemporánea'=>'Contemporánea',
                                'Coreana'=>'Coreana',
                                'Croata'=>'Croata',
                                'Cubana'=>'Cubana',
                                'Danesa'=>'Danesa',
                                'De Hong Kong'=>'De Hong Kong',
                                'De la India'=>'De la India',
                                'Del Medio Oriente'=>'Del Medio Oriente',
                                'Delicatessen'=>'Delicatessen',
                                'Ecuatoriana'=>'Ecuatoriana',
                                'Escandinava'=>'Escandinava',
                                'Española'=>'Española',
                                'Estadounidense'=>'Estadounidense',
                                'Europea'=>'Europea',
                                'Filipina'=>'Filipina',
                                'Francesa'=>'Francesa',
                                'Fusión'=>'Fusión',
                                'Gastropub'=>'Gastropub',
                                'Griega'=>'Griega',
                                'Hawaiana'=>'Hawaiana',
                                'Indonesia'=>'Indonesia',
                                'Internacional'=>'Internacional',
                                'Irlandesa'=>'Irlandesa',
                                'Israelí'=>'Israelí',
                                'Italiana'=>'Italiana',
                                'Japonesa'=>'Japonesa',
                                'Latina'=>'Latina',
                                'Libanesa'=>'Libanesa',
                                'Mariscos'=>'Mariscos',
                                'Mediterránea'=>'Mediterránea',
                                'Mexicana'=>'Mexicana',
                                'Pakistaní'=>'Pakistaní',
                                'Parrilla'=>'Parrilla',
                                'Persa'=>'Persa',
                                'Peruana'=>'Peruana',
                                'Pizza'=>'Pizza',
                                'Portuguesa'=>'Portuguesa',
                                'Pub'=>'Pub',
                                'Puertorriqueña'=>'Puertorriqueña',
                                'Saludable'=>'Saludable',
                                'Shanghai'=>'Shanghai',
                                'Sichuan'=>'Sichuan',
                                'Singapurense'=>'Singapurense',
                                'Sopas'=>'Sopas',
                                'Sudamericana'=>'Sudamericana',
                                'Sueca'=>'Sueca',
                                'Suiza'=>'Suiza',
                                'Sushi'=>'Sushi',
                                'Tailandesa'=>'Tailandesa',
                                'Taiwanesa'=>'Taiwanesa',
                                'Típica'=>'Típica',
                                'Turca'=>'Turca',
                                'Vasca'=>'Vasca',
                                'Venezolana'=>'Venezolana',
                                'Vietnamita'=>'Vietnamita',
                            ];
                return view('admin.Establishment.createEditEstablishment',compact('establishment','services','type','cant','departments','kitchenType','days','department','municipalities','municipality'));
            }
            else
            {
                return view('admin.Establishment.createEditEstablishment',compact('establishment','services','type','cant','departments','municipalities','municipality','department'));
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function updateEstablishment(Request $request, $slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            if($establishment->type_establishment == "restaurant")
            {
                $validator = \Validator::make($request->all(), [
                 'establishment_name' => 'required|max:150',
                 'kitchen_type'  => 'required',
                 'address'  => 'required|max:150',
                 'latitude'  => 'required|max:150',
                 'longitude'  => 'required|max:150',
                 'phone'  => 'required|max:150',
                 'description'  => 'required',
                 'category'  => 'required',
                 'Lunes.inicio'=>'required',
                 'Lunes.fin'=>'required',
                 'Martes.inicio'=>'required',
                 'Martes.fin'=>'required',
                 'Miercoles.inicio'=>'required',
                 'Miercoles.fin'=>'required',
                 'Jueves.inicio'=>'required',
                 'Jueves.fin'=>'required',
                 'Viernes.inicio'=>'required',
                 'Viernes.fin'=>'required',
                 'Sabado.inicio'=>'required',
                 'Sabado.fin'=>'required',
                 'Domingo.inicio'=>'required',
                 'Domingo.fin'=>'required',
                 'category.1'=>'required',
                 'plate.1.1'=>'required',
                 'cost.1.1'=>'required',
                ]); 

                if ($validator->fails()) 
                {
                    if(isset($validator->errors()->messages()['category.1']))
                    {
                        $validator->getMessageBag()->add('category', 'Se debe agregar por lo menos una categoría de comida');
                    }
                    if(isset($validator->errors()->messages()['plate.1.1'])|| isset($validator->errors()->messages()['cost.1.1']))
                    {
                        $validator->getMessageBag()->add('plates', 'Se debe agregar por lo menos un plato a la categoría');
                    }
                    if(isset($validator->errors()->messages()['latitude']) || isset($validator->errors()->messages()['latitude']))
                    {
                        $validator->getMessageBag()->add('address', 'El campo dirección es requerido y debe ser una dirección válida.');
                    }
                    foreach ($validator->errors()->all() as $error)
                    {
                        Session::flash('message-error', $error);
                    }

                    return redirect()->back()->withErrors($validator)->withInput();
                } 

                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }

                $establishment->kitchen_type = $request->kitchen_type;

                $oldId = $establishment->fk_municipality;

                $establishment->fk_municipality = $request->fk_municipality;

                $establishment->description = $request->description;
                $establishment->save();

                //DESACTIVACION DE MUNICIPIO EN CASO DE SER DIFERENTE
                if($oldId != $request->fk_municipality)
                {
                    $oldMunicipality = Municipality::find($oldId);
                     //Desativación del anterior
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_restaurant = "inactivo";
                        $oldMunicipality->save();
                    }

                    $municipality = Municipality::find($request->fk_municipality);
                    if($municipality->front_state_restaurant!="activo")
                    {
                        $municipality->front_state_restaurant = "activo";
                        $municipality->save();
                    }
                    
                }   

                //FIN DESACTIVACIÓN MUNICIPIO

                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                foreach($establishment->Plates as $oldPlate)
                {
                    $oldPlate->delete();
                }

                 //Creación de platos de establecimiento
                foreach($request->category as $id => $category)
                {
                    if($category!="")
                    {
                        foreach ($request->plate[$id] as $idPlate => $Rplate) 
                        {

                            if($Rplate)
                            {
                                $plate = new Plate;
                                $plate->plate = $Rplate;
                                $plate->cost = $request->cost[$id][$idPlate];
                                $plate->category = $category;
                                $plate->fk_establishment = $establishment->id_establishment;
                                $plate->save();
                            }
                            
                        }
                    }
                }
                //Horarios de establecimiento
                $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];

                foreach($days as $day)
                {
                    $oldDay = EstablishmentSchedule::where('name',$day)->where('fk_establishment',$id)->first();
                    $oldDay->start = $request->$day['inicio'];
                    $oldDay->end = $request->$day['fin'];
                    if(isset($request->$day['noservice']))
                    {
                        $oldDay->inactive = true;
                    }
                    else
                    {
                        $oldDay->inactive = false;
                    }
                    $oldDay->save();
                }
                
                Session::flash('message','Establecimiento actualizado correctamente');
                return redirect("admin/operators/establishment/$establishment->type_establishment/".Crypt::encrypt($establishment->Operator->id_operator));

            }//end if type
            elseif($establishment->type_establishment == "hotel")
            {
                $validator = \Validator::make($request->all(), [
             'establishment_name' => 'required|max:150',
             'fk_municipality'=>'required',
             'address'  => 'required|max:150',
             'latitude'  => 'required|max:150',
             'longitude'  => 'required|max:150',
             'phone'  => 'required|max:150',
             'description'  => 'required',
             'room.*'=>'required',
             'cuantity.*'=>'required',
             'capacity.*'=>'required',
             'bed.*'=>'required',
             'checkin.*'=>'required',
             'checkout.*'=>'required',
            ]); 

            if ($validator->fails()) 
            {
                foreach($validator->errors()->messages() as $errores => $mensajes)
                {
                    if((substr($errores,0,8) == "capacity") || (substr($errores,0,4) == "room") || (substr($errores,0,8) == "cuantity")|| (substr($errores,0,3) == "bed"))
                    {
                        $validator->getMessageBag()->add('rooms', 'Se debe ingresar toda la información de cada habitación.');
                    }
                }
                
                foreach ($validator->errors()->all() as $error)
                {
                    Session::flash('message-error', $error);
                }

                return redirect()->back()->withErrors($validator)->withInput();
            } 

                $establishment->establishment_name = $request->establishment_name;
                $establishment->phone = $request->phone;
                $establishment->address = $request->address;
                $establishment->latitude = $request->latitude;
                $establishment->longitude = $request->longitude;
                $establishment->facebook = $request->facebook;
                $establishment->twitter = $request->twitter;
                $establishment->instagram = $request->instagram;
                if($request->video)
                {
                    $establishment->video = youtube_match($request->video); 
                }

                $oldId = $establishment->fk_municipality;

                $establishment->fk_municipality = $request->fk_municipality;

                $establishment->description = $request->description;
                $establishment->save();

                //DESACTIVACION DE MUNICIPIO EN CASO DE SER DIFERENTE
                if($oldId != $request->fk_municipality)
                {
                    $oldMunicipality = Municipality::find($oldId);
                     //Desativación del anterior
                    $operatorsOldMunicipality = count($oldMunicipality->Restaurants);
                    if($operatorsOldMunicipality<1)
                    {
                        $oldMunicipality->front_state_hotel  = "inactivo";
                        $oldMunicipality->save();
                    }

                    $municipality = Municipality::find($request->fk_municipality);
                    if($municipality->front_state_hotel !="activo")
                    {
                        $municipality->front_state_hotel  = "activo";
                        $municipality->save();
                    }
                    
                }   

                //FIN DESACTIVACIÓN MUNICIPIO

                //servicios
                if(isset($request->service))
                {
                    $establishment->Services()->sync($request->service);
                }

                foreach($establishment->Rooms as $room)
                {
                    $room->delete();
                }  

                 //Creación de cuartos de establecimiento
                foreach($request->room as $id => $room)
                {
                    $newRoom = new Room;
                    $newRoom->room = $room;
                    $newRoom->cuantity = $request->cuantity[$id];
                    $newRoom->capacity = $request->capacity[$id];
                    $newRoom->bed = $request->bed[$id];
                    if(isset($request->suite[$id]))
                    {
                        $newRoom->suite = true;
                    }
                    $newRoom->fk_establishment = $establishment->id_establishment;
                    $newRoom->save();
                }

                //Horarios de establecimiento

                $schedule = EstablishmentSchedule::where('name','checkin')->where('fk_establishment',$establishment->id_establishment)->first();
                $schedule->start = $request->checkin['inicio'];
                $schedule->end = $request->checkin['fin'];
                $schedule->save();

                $schedule = EstablishmentSchedule::where('name','checkout')->where('fk_establishment',$establishment->id_establishment)->first();
                $schedule->start = $request->checkout['inicio'];
                $schedule->end = $request->checkout['fin'];
                $schedule->save();


                Session::flash('message','Establecimiento actualizado correctamente');
                return redirect("admin/operators/establishment/$establishment->type_establishment/".Crypt::encrypt($establishment->Operator->id_operator));
            
            }
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function imagesEstablishment($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        if($establishment)
        {
            return view('admin.Establishment.imageEstablishment', compact('establishment'));
        }
        else
        {
            Session::flash('message-error','No se ha encontrado el establecimiento.');
            return redirect()->back();
        }
    }

    public function update_imagesEstablishment(Request $request)
    {
        foreach ($request->description as $id => $value) 
        {
            $image = EstablishmentImage::find($id);
            $image->description = $value;
            $image->save();
        }
        Session::flash('message','Se ha realizado la actualizacíón correctamente');
        return redirect()->back();
    }
    public function delete_imageEstablishment($id)
    {
        $image = EstablishmentImage::find($id);

        $exists = File::exists(public_path($image->link_imagen));
        if ($exists) {
            File::delete(public_path($image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }
    public function upload_imagesEstablishment($id,ImageRequest $request)
    {
        $files = $request->file('file');
        $establishment = Establishment::find($id);
        $new_service = [];
        foreach ($files as $file) {
            $imageName = cargar_imagen($file,$establishment->type_establishment);
            if ($imageName)  
            {
                $new_image = ['link_image' => $imageName, 'fk_establishment' => $id];
                $imageEstablishment = EstablishmentImage::create($new_image);
                $new_service[] = $imageEstablishment->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function desactivateEstablishment($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        $establishment->state = "Desactivado por administrador";
        $establishment->save();
        Session::flash('message','Se ha desactivado el establecimiento adicional correctamente');
        return redirect()->back();
    }

    public function activateEstablishment($slug)
    {
        $establishment = Establishment::where('slug',$slug)->first();
        $establishment->state = "activo";
        $establishment->save();
        Session::flash('message','Se ha activado el establecimiento adicional correctamente');
        return redirect()->back();
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/services/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/services/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Service_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/services'), $imageName);

        $exists = File::exists(public_path("images/services/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        $users = User::all();
        return view('admin.User.listUser',compact('users'));
    }
    /**
    public function contact()
    {
        $contacts = Contact::all();
        return view('admin.Contact.listContact',compact('contacts'));
    }
    */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
