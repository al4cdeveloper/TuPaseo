<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Session;

class ContactController extends Controller
{
	public function index(Request $request)
	{
		$contacts = [];
		if(!$request->state)
        {
            $contacts = Contact::where('state','new')->orderBy('id_contact','desc')->get();
        }
        else
        {
            $contacts = Contact::where('state',$request->state)->orderBy('id_contact','desc')->get();
        }

        return view('admin.Contact.contactList',compact('contacts'));
	}

	public function archive($id)
	{
		$contact = Contact::find($id);
		if($contact)
		{	
			$contact->state = 'archivado';
			$contact->save();
			Session::flash('message','Se ha archivado el mensaje correctamente');
		}
		else
		{
			Session::flash('message-error','No se ha encontrado el mensaje');
		}
		return redirect()->back();
	}

	public function active($id)
	{
		$contact = Contact::find($id);
		if($contact)
		{	
			$contact->state = 'new';
			$contact->save();
			Session::flash('message','Se ha desarchivado el mensaje correctamente');
		}
		else
		{
			Session::flash('message-error','No se ha encontrado el mensaje');
		}
		return redirect()->back();
	}
}
