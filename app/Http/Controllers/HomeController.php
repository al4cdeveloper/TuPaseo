<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;
use Cart;
use App\Models\Order;
use App\Models\OrderProduct;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reservations = Reservation::where('fk_user',Auth::user()->id)->where('state','pagado')->orWhere('state','Realizada')->orWhere('state','Calificada')->orderBy('id_reservation','desc')->get();
        $orders =Order::where('fk_user',Auth::user()->id)->orderBy('id_order','desc')->limit(100)->get();

        if(count($reservations)>0)
        {
            return view('user.home',compact('reservations','orders'));
        }
        else
        {
            return redirect('/');
        }
    }
}
