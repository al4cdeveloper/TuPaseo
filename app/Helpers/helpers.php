<?php

use File as FileDirective;

function getTheDate($date){
    $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    $date = $dias[date('w', strtotime($date))]." ".date('d', strtotime($date))." de ".$meses[date('n', strtotime($date))-1]. " de ".date('Y', strtotime($date)). date(' h:i:s A', strtotime($date));

    return $date;
}


function youtube_match($texto) {
    $url = urldecode(rawurldecode($texto));
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

    if (is_array($matches) && isset($matches[1])) {
        return $matches[1];
    } else {
        return '';
    }
}
function cop_format($number){
    $cant = number_format((int)$number, 0, '', '.');

    return 'COP $'.$cant;
}
function cargar_imagen($file,$nombreSite ,$imageName = false)
    {
        if ($imageName) 
        {
            $exists = FileDirective::exists(public_path("images/$nombreSite/".$imageName));
            if ($exists) 
            {
                FileDirective::delete(public_path("images/$nombreSite/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = $nombreSite.'_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path("images/$nombreSite"), $imageName);

        $exists = FileDirective::exists(public_path("images/$nombreSite/".$imageName));

        if ($exists) 
        {
            $imageName = "images/$nombreSite/".$imageName;
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }


?>