<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Rejected extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $total_amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$total_amount)
    {
        $this->order = $order;
        $this->total_amount = $total_amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $startDate = Carbon::parse( $this->order->created_at);
        array_add($this->order,'start_date',$startDate->format('d-m-Y'));

        return $this->from(env('MAIL_FROM'), 'TuPaseo')
        ->subject('Su Orden # ' . str_pad($this->order->id_order, 6, "0", STR_PAD_LEFT) . ' ha sido rechazada')
        ->view('emails.reject')
        ->with([
            'orden'        => $this->order,
            'total_amount' => $this->total_amount
        ]);
    }
}
