<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmationAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $typeUser;
    public $confirmation_code;
    public $name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($typeUser,$code,$name)
    {
        $this->typeUser = $typeUser;
        $this->confirmation_code = $code;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('markdown.confirmationAccount')
            ->from(env('MAIL_FROM'), 'TuPaseo')
            ->subject('Por favor confirma tu correo');
    }
}
