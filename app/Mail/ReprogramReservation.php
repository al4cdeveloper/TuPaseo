<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReprogramReservation extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $reprogram;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reservation,$reprogram)
    {
        $this->reservation = $reservation;
        $this->reprogram  = $reprogram;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('markdown.reprogramReservation')
        ->from(env('MAIL_FROM'), 'TuPaseo')
        ->subject('⏰ Su reservación de '.$this->reservation->Service->service_name.", ha sido reprogramada");
    }
}
