<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReprogramReservationOperator extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $reprogram;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reservation,$reprogram)
    {
        $this->reservation = $reservation;
        $this->reprogram  = $reprogram;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('markdown.ReprogramReservationOperator')
        ->from(env('MAIL_FROM'), 'TuPaseo')
        ->subject('⏰ La reservación # RSVT-'.str_pad($this->reservation->id_reservation, 6, "0", STR_PAD_LEFT)." ha sido reprogramada");
    }
}
