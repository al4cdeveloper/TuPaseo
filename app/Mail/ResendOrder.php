<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class ResendOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $total_amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->total_amount = $order->costo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $startDate = Carbon::parse( $this->order->created_at);
        array_add($this->order,'start_date',$startDate->format('d-m-Y'));
        $endDate = $startDate->addMonths(3); 
        array_add($this->order,'end_date',$endDate->format('d-m-Y'));

        //Confirmación al usuario
        return $this->from(env('MAIL_FROM'), 'TuPaseo')
        ->subject('Reenvío recibo: ' . str_pad($this->order->id_order, 6, "0", STR_PAD_LEFT))
        ->view('emails.order')
        ->with([
            'orden'        => $this->order,
            'total_amount' => $this->total_amount
        ]);
    }
}
