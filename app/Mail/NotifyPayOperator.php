<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyPayOperator extends Mailable
{
    use Queueable, SerializesModels;
    public $payReport;
    public $report;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payReport,$report)
    {
        $this->payReport = $payReport;
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        $report = $this->report;
        $reservations = $this->payReport;
        $pdf = \PDF::loadView('reports.reportPayOperator',compact('report','reservations'));
        return $this->from(env('MAIL_FROM'), 'TuPaseo')
        ->subject('Se ha realizado un pago')
        ->view('emails.notifyPayOperator')
        ->attachData($pdf->output(), 'reporte_operador.pdf', [
                        'mime' => 'application/pdf'])
        ->with([
            'reservations'=> $this->payReport,
            'report'=> $this->report,
        ]);
    }
}
