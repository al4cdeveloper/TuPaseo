<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\Reservation;
class NotifyPay extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $total_amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$total_amount)
    {
        $this->order = $order;
        $this->total_amount = $total_amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $startDate = Carbon::parse( $this->order->created_at);
        array_add($this->order,'start_date',$startDate->format('d-m-Y'));
        $endDate = $startDate->addMonths(3); 
        array_add($this->order,'end_date',$endDate->format('d-m-Y'));

        //Aviso a los operadores 
        $services = $this->order->services;
        foreach($services as $service)
        {
            $reservation = $service->reservation;
            $directService = $service->Service;
            $operator = $reservation->Service->operador;
            $items =$service->items;
            Mail::send('emails.notifyOperator', ['orden'=> $this->order, 'service'=>$directService,'operator'=>$operator,'reservation'=>$reservation,'items'=>$items], function ($message) use ($operator)
            {
                    $message->from(env('MAIL_FROM'), 'TuPaseo');
                    $message->to($operator->email)->subject('Haz recibido una nueva reservación');
            });
        }

        //Confirmación al usuario
        return $this->from(env('MAIL_FROM'), 'TuPaseo')
        ->subject('Orden de Pedido: ' . str_pad($this->order->id_order, 6, "0", STR_PAD_LEFT))
        ->view('emails.order')
        ->with([
            'orden'        => $this->order,
            'total_amount' => $this->total_amount
        ]);
        //return $this->view('view.name');
    }
}
