import Vue from 'vue'
import VueRouter from 'vue-router';
import Home from './components/front/home.vue';
import Region from './components/front/region.vue';
import Municipality from './components/front/municipality.vue';
import Restaurant from './components/front/restaurant.vue';
import Hotel from './components/front/hotel.vue';
import Service from './components/front/service.vue';
import Ecosystem from './components/front/ecosystem.vue';
import listServices from './components/front/listServices.vue';
import listHotels from './components/front/listHotels.vue';
import listRestaurants from './components/front/listRestaurants.vue';
import Navbar from './components/front/navbar.vue';
import Meta from 'vue-meta';
import swal from 'sweetalert';

//https://medium.com/vuejobs/create-a-global-event-bus-in-vue-js-838a5d9ab03a
Vue.prototype.$eventHub = new Vue(); // Global event bus


Vue.use(VueRouter);
Vue.use(Meta);
//Vue.component('navbar',require('./components/front/navbar.vue'));

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',name: 'home',component: Home
        },
        {
            path: '/home/region/:slug',name: 'region', component: Region,
        },
        {
            path: '/home/municipality/:slug', name:'municipality', component: Municipality,
        },
        {
            path: '/home/restaurant/:slug', name:'restaurant', component: Restaurant,
        },
        {
            path: '/home/hotel/:slug', name:'hotel', component: Hotel,
        },
        {
            path: '/home/service/:slug', name:'service', component: Service,
        },
        {
            path: '/home/ecosystem/:slug', name:'ecosystem', component: Ecosystem,
        },
        {
            path: '/home/services', name:'services', component: listServices,
        },
        {
            path: '/home/hotels', name:'hotels', component: listHotels,
        },
        {
            path: '/home/restaurants', name:'restaurants', component: listRestaurants,
        },
    ],
});


let elements = document.getElementsByClassName('app') //Llamado a elementos con la clase APP
for(let el of elements){
	new Vue({
  	el:el,
  	components: { Navbar,swal },
    router,
  })
}



//DOCUMENTACIÓN DEL FOR https://forum.vuejs.org/t/one-component-applicable-to-multiple-elements/22441/3
/*new Vue({
  el: '#app',
  components: { Navbar },
    router,
});
*/
