new Vue({
	el: '#page-content',
	data(){
	  	return{
  			fillReservation:{'cuantity':'1','date':'','time':'','total':'','aditionalItems':'','text':'','priceforperson':''},
  			id_service:0,
			disabledDays:'',
			hours:[],
			countHours:0,
			slug:'',
	  	}
	},
	methods:
	{
		configModal:function(id_service,numPersonas,slug,id_reservation)
		{
			this.slug = slug;
			axios.get('/vue/service/'+this.slug).then(response=>{
				this.disabledDays = response.data.disabledDays;
				$('input.date-pick').datepicker({
                format: 'dd/mm/yyyy',
                startDate: '+0d',
                daysOfWeekDisabled: this.disabledDays,
	            });
	            $('input.time-pick').timepicker({
	                minuteStep: 15,
	                showInpunts: false
	            });
			});
			$('#numPersons').val(numPersonas);
			$('#id_reservation').val(id_reservation);
            $("#modal-reprogram").modal();
            this.id_service = id_service;
			
		},
		validateDate:function()
		{
			// console.log(this.fillReservation);
			//self variable auxiliar dentro de la funcion para indicar que es this :v
			var self = this;
			this.hours=[],
				this.countHours=0,
			setTimeout(function(){
				var fecha = $("#dateService").val();
				var res = fecha.replace("/", "-");
				res = res.replace("/", "-");
				var cantidad = $("#numPersons").val();
				var url = "/freehours/"+self.id_service+"/"+res+"/"+cantidad;
				self.fillReservation.date = $("#dateService").val();
				axios.get(url).then(response=>{
					self.hours=response.data.tiempo;
					self.countHours = response.data.cantidad;
				});
            }, 500);
		},
	}
});