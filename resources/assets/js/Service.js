import Vue from 'vue'
import VueRouter from 'vue-router'

import App from '../components/Servicio/App.vue'
import listService from '../components/Servicio/listService.vue'
import newService from '../components/Servicio/newService.vue'
import Schedule from '../components/Servicio/schedule.vue'


// var VueRouter = require('vue-router');
// import {VueRouter} = from ('vue-router');
// import { DatePicker } from 'vue-datepicker';
// var ServicioList = require('../components/Servicio/listService.vue');


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('home',App);

Vue.use(VueRouter); //Así cargamos la librería

const routes = [
	{path: '/operator/myservice',component: listService},
	{path: '/operator/new-service',component: newService},
	{path: '/operator/shedule/:id', name:'schedule', component: Schedule},
];

const router = new VueRouter({
	routes,
	mode: 'history'
})


const app = new Vue({
    el: '#service',
    router,
});

