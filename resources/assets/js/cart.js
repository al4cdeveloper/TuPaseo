


// const router = new VueRouter();
new Vue({
	el: '.vuejs',
	mounted() //Lo primero que se va a ejecutar
	{
		this.getCar();
		this.getServices();
	},
	data:{
		services:[],
		fillReservation:{'cuantity':'','date':'','time':''},
		cart:[],
		hours:[],
		countHours:0,
		itemsCart:null,
		total:null,
		mindate:null
	},
	methods:
	{
		getServices:function()
		{
			axios.get('/serviceslist').then((respuesta)=>{
				this.services= respuesta.data.services;
				this.mindate = respuesta.data.mindate;
			});
		},
		getCar:function()
		{
			axios.get('/allcart')
			.then((respuesta)=>{
				this.cart= respuesta.data.cart;
				this.itemsCart=respuesta.data.count;
			});
			axios.get('/subtotal')
			.then((respuesta)=>{
				this.total = respuesta.data;
			});
		},
		addcar:function(id,cuantity)
		{
			console.log(this.fillReservation);
			console.log(id);
			if(this.fillReservation.cuantity=="" || this.fillReservation.date=="" || this.fillReservation.time=="")
			{
				swal('Error','Para realizar una reservacion se requiere que todos los campos esten llenos','error');
			}
			else
			{
				if(this.fillReservation.cuantity>cuantity)
				{
					swal('Error','La cantidad de personas no puede superar la capacidad del operador','error');
				}
				else
				{
					var url = '/add_cart/'+id
					axios.put(url,this.fillReservation).then(response=>{
						this.getCar();
						this.fillReservation={'cuantity':'','date':'','time':''};
						this.hours=[],
						this.countHours=0,
						toastr.success('Se ha agregado el producto correctamente al carrito');
					});
					$("#detailService"+id).modal("hide");
					$(".modal-backdrop.in").css('display','none');

				}
			}			
		},
		launchModal:function(id)
		{
			$("#detailService"+id).modal('show');
		},
		removeCart:function(rowid)
		{
			var url = '/remove_cart/'+rowid;
			axios.get(url)
			.then((respuesta)=>
			{
				this.getCar();
			});
		},
		deleteCart:function()
		{
			url = '/delete_cart';
			axios.get(url).then((respuesta)=>
			{
				this.getCar();
			});
		},
		ValidateInput:function(id)
		{
			// patron = /^\d{2}\/\d{2}\/\d{2}$/
			// alert(patron.test(value));
			$('#'+id).datepicker({
            beforeShowDay: noExcursion,
            }).show();
		},
		validateDate:function(id)
		{
			// console.log(this.fillReservation);
			var self = this;
			this.hours=[],
				this.countHours=0,
			setTimeout(function(){
				fecha = $("#dateService"+id).val();
				res = fecha.replace("/", "-");
				res = res.replace("/", "-");
				url = "/freehours/"+id+"/"+res;
				self.fillReservation.date = $("#dateService"+id).val();
				axios.get(url).then(response=>{
					self.hours=response.data.tiempo;
					self.countHours = response.data.cantidad;
				});
            }, 500);
		},
		alerta:function()
		{
			alert("alerta");
			console.log($("#dateService1").val());
		}
	}
})