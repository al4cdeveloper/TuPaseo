<!DOCTYPE html>

<html lang="es">

<head>
    <!-- Basic Meta -->
    <meta name="encoding" charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
   

    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">

    <style scoped>
        .card {
    margin-bottom: 1.875rem;
    border: none;
    border-radius: 0;
    box-shadow: 0 10px 40px 0 rgba(62, 57, 107, 0.07), 0 2px 9px 0 rgba(62, 57, 107, 0.06);
    padding: 30px;
}

.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, 0.06);
    border-radius: 0.25rem;
}

#invoice-template {
    padding: 4rem;
}

.card-body {
    flex: 1 1 auto;
    padding: 1.5rem;
    border-left: 4px solid #4CAF50;
}

*, *::before, *::after {
    box-sizing: border-box;
}

.row {
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

.media {
    display: flex;
    align-items: flex-start;
}

img {
    vertical-align: middle;
    border-style: none;
}

.media-body {
    flex: 1;
}

.pl-0, .px-0 {
    padding-left: 0 !important;
}

.pr-0, .px-0 {
    padding-right: 0 !important;
}

.ml-2, .mx-2 {
    margin-left: 1.5rem !important;
}

.list-unstyled {
    padding-left: 0;
    list-style: none;
}

ol, ul, dl {
    margin-top: 0;
    margin-bottom: 1rem;
}

ol li, ul li, dl li {
    line-height: 1.8;
}

h2, .h2 {
    font-size: 1.74rem;
}

h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    margin-bottom: 0.5rem;
    font-family: "Montserrat", Georgia, "Times New Roman", Times, serif;
    font-weight: 400;
    line-height: 1.2;
    color: inherit;
}

h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: 0.5rem;
}

.pb-3, .py-3 {
    padding-bottom: 3rem !important;
}

p {
    margin-top: 0;
    margin-bottom: 1rem;
}

p {
    display: block;
    -webkit-margin-before: 1em;
    -webkit-margin-after: 1em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
}

.lead {
    font-size: 1.25rem;
    font-weight: 400;
}


.text-bold-800 {
  font-weight: 800; }

.pt-2, .py-2 {
    padding-top: 1.5rem !important;
}

.text-muted {
    color: #404E67 !important;
}

.text-bold-400 {
  font-weight: 400; }

.table-responsive {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
}

table {
    border-collapse: collapse;
    border-spacing: 2px;
    border-color: grey;
}

table {
    border-collapse: collapse;
}

tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}

thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}

tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}

.table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #e3ebf3;
    border-top: 1px solid #e3ebf3;
}

.table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #98A4B8;
}

.table th, .table td {
    padding: 0.75rem 2rem;
}

.table th, .table td {
    border-bottom: 1px solid #e3ebf3;
}

.table th, .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #98A4B8;
}

th {
    white-space: nowrap;
    font-weight: bold;
}

th {
    text-align: inherit;
}

td, th {
    display: table-cell;
    vertical-align: inherit;
}

.text-right {
    text-align: right !important;
}

.table.table-sm th, .table.table-sm td {
    padding: 0.6rem 2rem;
}

.table-borderless > tbody > tr > td, .table-borderless > tbody > tr > th {
    border: 0;
}

.table th, .table td {
    padding: 0.75rem 2rem;
}

.table th, .table td {
    border-bottom: 1px solid #e3ebf3;
}

.table-borderless td, .table-borderless th {
    border: none;
}

.table-sm th, .table-sm td {
    padding: 0.3rem;
}
 .text-md-right {
     text-align: right !important;
     }
.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
.col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
.col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
.col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
.col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
.col-xl-auto {
  position: relative;
  width: 100%;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px;
}
.img-responsive
 {
  display: block;
  max-width: 100%;
  height: auto;
}
 .col-md-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
  }
  .col-sm-12 {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
  }
  .pt-2,
.py-2 {
  padding-top: 0.5rem !important;
}
.text-right {
  text-align: right !important;
}
.lead {
  font-size: 1.25rem;
  font-weight: 300;
}
.row {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.text-muted {
  color: #6c757d !important;
}
@media (max-width: 575.98px) {
  .table-responsive-sm {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-sm > .table-bordered {
    border: 0;
  }
}

@media (max-width: 767.98px) {
  .table-responsive-md {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-md > .table-bordered {
    border: 0;
  }
}

@media (max-width: 991.98px) {
  .table-responsive-lg {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-lg > .table-bordered {
    border: 0;
  }
}

@media (max-width: 1199.98px) {
  .table-responsive-xl {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-xl > .table-bordered {
    border: 0;
  }
}

.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -webkit-overflow-scrolling: touch;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table-responsive > .table-bordered {
  border: 0;
}
  .table {
    border-collapse: collapse !important;
  }
  .table td,
  .table th {
    background-color: #fff !important;
  }
.col-md-7 {
    -ms-flex: 0 0 58.333333%;
    flex: 0 0 58.333333%;
    max-width: 58.333333%;
  }
  .col-md-5 {
    -ms-flex: 0 0 41.666667%;
    flex: 0 0 41.666667%;
    max-width: 41.666667%;
  }

  @media (min-width: 768px) {
  .text-md-left {
    text-align: left !important;
  }
  .text-md-right {
    text-align: right !important;
  }
  .text-md-center {
    text-align: center !important;
  }
}
    </style>
</head>
        
</head>
<body>
    <section class="card">
        <div id="invoice-template" class="card-body">

            <div id="invoice-company-details" class="row">
                <div class="col-md-6 col-sm-12 text-md-left">
                    <div class="media">
                        <img src="http://tupaseo.travel/images/mail_logo-80x80.png" style="width: 80px;height: 80px;" alt="TuPaseo" class="img-responsive">
                        <div class="media-body">
                            <ul class="ml-2 px-0 list-unstyled">
                                <li class="text-bold-800"><b>TuPaseo.Travel</b></li>
                                <li>NIT 901.193.027-4</li>
                                <li>Cra 9 # 72-81 Oficina 504</li>
                                <li>PBX (+0057) 1 3121540</li>
                                <li>Bogotá D.C., Colombia</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- End Logo -->

            
            <div id="invoice-items-details" class="pt-2">
              <p>Se ha realizado la transferencia económica a la cuenta suministrada en nuestra plataforma. <br>
              Los pagos se han realizado por las reservaciones completadas en los periodos comprendidos entre {{$report->date_start}} y {{$report->date_finish}}.</p>
                <div class="row">

                    <div class="col-md-12 col-sm-12">
                        <div class="text-center">
                            <p> </p>
                            <img src="http://tupaseo.travel/images/mail_logo.png" alt="signature" class="height-100">
                            <h6>Equipo</h6>
                            <p class="text-muted">TuPaseo.Travel</p>
                        </div>
                    </div> <!-- End Total -->
                </div>
            </div>

            <div id="invoice-footer">
              <div class="row">
                <div class="col-md-7 col-sm-12">
                  <h4>Para ver el detalle de los pagos recibidos visita <a href="{{url('/operator/pays')}}"> {{url('/operator/pays')}}</a>.</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-7 col-sm-12">
                  <h6>Términos y Condiciones</h6>
                  <p>Consulta nuestras políticas en <a href="{{url('/terminos_operador')}}">{{url('/terminos_operador')}}</a>.</p>
                </div>
              </div>
            </div>

        </div>
    </section>
</body>
</html>