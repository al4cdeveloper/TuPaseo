@extends('layouts.front')
@section('title','Tu Paseo')
@section('content')    
    <!-- ====== End Menu Section ====== -->
 
    <!-- ====== Header Section ====== -->
    <header id="top" style="background-image: url({{asset('img/escalada.jpg')}})">
      <div class="bg-color">
        <div class="top section-padding">
          <div class="container">
            <div class="row">

              <div class="col-sm-7 col-md-7">
                <div class="content">
                  <h1><strong></strong></h1>
                  <h1>Disfruta tus vacaciones</h1>
                  <h1>Disfruta tu libertad</h1>
                  <div class="button" id="download-app1">
                    <a href="{{url('reservation')}}" class="btn btn-default btn-lg" style="background-color: #f28e00;color:white"><i class="fa fa-car"></i>Reservar</a>
                  </div> <!-- end .button -->
                </div> <!-- end .content -->
              </div> <!-- end .top > .container> .row> .col-md-7 -->

              <div class="col-sm-5 col-md-5">
              </div> <!-- end .top > .container> .row> .col-md-5 -->

            </div> <!-- end .top> .container> .row -->
          </div> <!-- end .top> .container -->
        </div> <!-- end .top -->
      </div> <!-- end .bg-color -->
    </header>
    <!-- ====== End Header Section ====== -->

    <!-- ====== Screenshots Section ====== -->
    <section id="page-content">

      <div class="screenshots section-padding">
      <div class="container">
                
      <div class="header">
                <h1>¡Sal de la ciudad y experimenta nuevas sensaciones!</h1>
                <p></p>
      </div>
    
          <div class="owl-carousel owl-theme">
            <div class="item">
              <a href="{{asset('images/2.jpg')}}" data-rel="prettyPhoto"><img src="{{asset('images/2.jpg')}}" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="{{asset('images/3.jpg')}}" data-rel="prettyPhoto"><img src="{{asset('images/3.jpg')}}" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="{{asset('images/4.jpg')}}" data-rel="prettyPhoto"><img src="{{asset('images/4.jpg')}}" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="{{asset('images/5.jpg')}}" data-rel="prettyPhoto"><img src="{{asset('images/5.jpg')}}" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="{{asset('images/6.jpg')}}" data-rel="prettyPhoto"><img src="{{asset('images/6.jpg')}}" alt="item photo"></a>
            </div> <!-- end item -->
          </div> <!-- end owl carousel -->

          
          @foreach($servicesO as $serviceO)
          <div class="col-sm-4">
              <a href="{{url('services/'.$serviceO->slug)}}" class="widget">
                  {{-- <div class="widget-content themed-background text-light-op">
                      <i class="fa fa-fw fa-pencil"></i> <strong>Story</strong>
                  </div> --}}
                  <div class="widget-image widget-image-sm">
                      <img src="{{asset($serviceO->PrincipalService->image)}}" alt="image">
                      <div class="widget-image-content">
                          <h2 class="widget-heading text-light"><strong>{{$serviceO->PrincipalService->service}}</strong></h2>
                          <h3 class="widget-heading text-light-op h4">{{$serviceO->location}}</h3>
                      </div>
                      <i class="gi gi-airplane"></i>
                  </div>
                  <div class="widget-content text-dark">
                      <div class="row text-center">
                          <div class="col-xs-6">
                              <i class="fa fa-heart-o"></i> 1 - {{{$serviceO->capacity}}}
                          </div>
                          <div class="col-xs-6">
                              <i class="fa fa-eye"></i> ${{$serviceO->cost}}
                          </div>

                      </div>
                  </div>
              </a>
          </div>
          @endforeach
          
      </div>
    </div>
</section>

<section id="testimonial">
      <div class="bg-color bg-testimonial">
        <div class="testimonial section-padding">
          <div class="container">
            <div class="testimonial-slide">
                    <div class="content">
                      <h2>¿Que aventuras te gustaría tener aquí?</h2>
            <br><br>
            <form action="{{url('encuesta')}}" method="post" class=" form contact">
              {{csrf_field()}}
              <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="name" class="sr-only">Nombre</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required>
                </div> <!-- end .form-group -->

                <div class="form-group">
                  <label for="email" class="sr-only">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div> <!-- end .form-group -->
                
                 <div class="form-group">
                  <label for="phone" class="sr-only">Teléfono</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Teléfono" required>
                </div> <!-- end .form-group -->

                <div class="form-group">
                  <label for="city" class="sr-only">Ciudad</label>
                  <input type="text" class="form-control" id="city" name="city" placeholder="Ciudad" required>
                </div> <!-- end .form-group -->
              </div> <!-- end .form> .row> .col-md-4 -->
              
              <div class="col-xs-12 col-md-6">

               
                <div class="form-group">
                  <label for="message" class="sr-only">Actividades</label>
                  
                    <select id="service" name="services[]" class="select-chosen" data-placeholder="Seleccione las actividades de su interes" style="width: 250px;" required multiple="">
                          <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                          @foreach($services as $service)
                            <option value="{{$service->id_service}}">{{$service->service}}</option>
                          @endforeach
                  </select>

                </div> <!-- end .form-group -->
                <div class="form-group">
                  <label for="message" class="sr-only">Observaciones o comentarios</label>
                  <textarea name="message" id="message" name="message" placeholder="Observaciones o comentarios" style="width: 100%;"></textarea>
                </div> <!-- end .form-group -->
              </div>
              <button class="btn btn-primary large" style="background-color: #f28e00;color:white"> Enviar</button>
            </div> <!-- end .form> .row -->
            </form>
                    </div> <!-- end .content -->
            </div> <!-- end .testimonial-slide -->
          </div> <!-- end .container -->
        </div> <!-- end .testimonial -->
      </div> <!-- end .bg-testimonial -->
    </section>
    <!-- ====== End Testimonial Section ====== -->

    <!-- ====== End Screenshots Section ====== -->
 {{-- <section id="encuesta">
      <div class="subscribe section-padding">
        <div class="container">
          <div class="subscribe-header">
            <h2>¿Que aventuras te gustaría tener aquí?</h2>
            <br><br>
            <form action="{{url('encuesta')}}" method="post" class=" form contact">
              {{csrf_field()}}
              <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="name" class="sr-only">Nombre</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required>
                </div> <!-- end .form-group -->

                <div class="form-group">
                  <label for="email" class="sr-only">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div> <!-- end .form-group -->
                
                 <div class="form-group">
                  <label for="phone" class="sr-only">Teléfono</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Teléfono" required>
                </div> <!-- end .form-group -->

                <div class="form-group">
                  <label for="city" class="sr-only">Ciudad</label>
                  <input type="text" class="form-control" id="city" name="city" placeholder="Ciudad" required>
                </div> <!-- end .form-group -->
              </div> <!-- end .form> .row> .col-md-4 -->
              
              <div class="col-xs-12 col-md-6">

               
                <div class="form-group">
                  <label for="message" class="sr-only">Actividades</label>
                  <select class="selectpicker form-group"  title="Actividades" data-width="100%"  id="service" name="services[]" multiple data-live-search="true" required="">
                      @foreach($services as $service)
                        <option value="{{$service->id_service}}">{{$service->service}}</option>
                      @endforeach
                  </select>

                </div> <!-- end .form-group -->
                <div class="form-group">
                  <label for="message" class="sr-only">Observaciones o comentarios</label>
                  <textarea name="message" id="message" name="message" placeholder="Observaciones o comentarios" style="width: 100%;"></textarea>
                </div> <!-- end .form-group -->
              </div>
              <button class="btn btn-primary large"> Enviar</button>
            </div> <!-- end .form> .row -->
            </form>
          </div>
      </div>
    </div>
</section> --}}

    <!-- ====== Subscribe Section ====== -->
    <section id="subscribe">
      <div class="subscribe section-padding">
        <div class="container">
          <div class="subscribe-header">
            <h2>¿Quieres conocer nuevas experiencias de viaje?</h2>
            <p>Déjanos tu correo electrónico para recibir las ofertas más recientes.</p>
            <form action="mailchimp_register" class="form subscribe-form" method="post">
              {{csrf_field()}}
              <div class="form-group">
                <div class="input-group">
                  <label for="f-name" class="sr-only">Newsletter</label>
                  <input type="email" class="form-control" id="f-name" name="email" placeholder="Ingresar correo eletrónico" required>
                  <div class="input-group-addon" style="background-color: #f28e00;color:white">
                    <button type="submit">ENVIAR</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="social">
            <ul>
              <li><a class="facebook" href="https://www.facebook.com/tupaseo.co/" target="_blank"><i class="fa fa-facebook"></i></a></li> 
              <li><a class="flickr" href="https://www.instagram.com/tupaseo/" target="_blank"><i class="fa fa-instagram"></i></a></li>          
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- ====== End Subscribe Section ====== -->

@endsection