@extends('layouts.front')
@section('title','Contacta con TuPaseo')

@section('content')   

    <!-- ====== Header Section ====== -->
    <header id="top" style="background-image: url({{asset('images/bg4.jpg')}})">
      <div class="bg-color">
        <div class="top section-padding">
          <div class="container">
            <div class="row">

              <div class="col-sm-7 col-md-7">
                <div class="content">
                  <h1><strong></strong></h1>
                  <h1>Disfruta la aventura,</h1>
                  <h1>Disfruta tu libertad</h1>
                  <div class="button" id="download-app1">
                    <a href="{{url('reservation')}}" class="btn btn-default btn-lg" style="background-color: #f28e00;color:white"><i class="fa fa-car"></i>Reservar</a>
                  </div> <!-- end .button -->
                </div> <!-- end .content -->
              </div> <!-- end .top > .container> .row> .col-md-7 -->

              <div class="col-sm-5 col-md-5">
              </div> <!-- end .top > .container> .row> .col-md-5 -->

            </div> <!-- end .top> .container> .row -->
          </div> <!-- end .top> .container -->
        </div> <!-- end .top -->
      </div> <!-- end .bg-color -->
    </header>
    <!-- ====== End Header Section ====== -->


    <!-- ====== Contact Section ====== -->
    <footer id="contact">
      <div class="footer section-padding">
        <div class="container">
          <h1>Contáctanos</h1>
          <form action="{{url('storeemail')}}" method="post"  class="form contact">
          {{csrf_field()}}

            <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="name" class="sr-only">Nombre</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required>
                </div> <!-- end .form-group -->

                <div class="form-group">
                  <label for="email" class="sr-only">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div> <!-- end .form-group -->
                
                 <div class="form-group">
                  <label for="phone" class="sr-only">Teléfono</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Teléfono" required>
                </div> <!-- end .form-group -->

                <div class="form-group">
                  <label for="city" class="sr-only">Ciudad</label>
                  <input type="text" class="form-control" id="city" name="city" placeholder="Ciudad" required>
                </div> <!-- end .form-group -->
              </div> <!-- end .form> .row> .col-md-4 -->
              
              <div class="col-xs-12 col-md-6">
               
                 <div class="form-group">
                  <label for="subject" class="sr-only">Asunto</label>
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Asunto" required>
                </div> <!-- end .form-group -->
                               
                <div class="form-group">
                  <label for="message" class="sr-only">Mensaje</label>
                  <textarea name="message" id="message" name="message" placeholder="Mensaje" required></textarea>
                </div> <!-- end .form-group -->
              </div>
            </div> <!-- end .form> .row -->

            <button class="btn btn-default contact-submit " style="background-color: #f28e00;color:white" type="submit"><i class="fa fa-hand-o-right"></i>Enviar</button>
          </form> <!-- end .form -->   
        </div> <!-- end .container -->
      </div> <!-- end .footer -->
    </footer>
    <!-- ====== End Contact Section ====== -->

  @endsection
  
  @section('additionalScript')
    <script>
      jQuery(document).ready(function($) {
        "use strict";

        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools:false});
      });
    </script>
  @endsection