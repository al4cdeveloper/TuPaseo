@extends('layouts.front')
@section('title','Beneficios para operadores turísticos | Tu Paseo')

@section('content')   
    <!-- ====== Header Section ====== -->
    <header id="top" style="background-image: url({{asset('images/bg5.jpg')}})">
      <div class="bg-color">
        <div class="top section-padding">
          <div class="container">
            <div class="row">

              <div class="col-sm-7 col-md-7">
                <div class="content">
                  <h1><strong></strong></h1>
                  <h1>¿Quieres que manejemos tus reservas y pagos? </h1>
                  <div class="button" id="download-app1">
                    <a href="#" id="registrar" class="btn btn-default btn-lg" style="background-color: #f28e00;color:white"><i class="fa fa-send"></i>Crear perfil</a>
                  </div> <!-- end .button -->
                </div> <!-- end .content -->
              </div> <!-- end .top > .container> .row> .col-md-7 -->

              <div class="col-sm-5 col-md-5">
              </div> <!-- end .top > .container> .row> .col-md-5 -->

            </div> <!-- end .top> .container> .row -->
          </div> <!-- end .top> .container -->
        </div> <!-- end .top -->
      </div> <!-- end .bg-color -->
    </header>
    <!-- ====== End Header Section ====== -->
    <!-- ====== Description Section ====== -->
    <section >
      <div class="description">
        <div class="description-one section-padding">
          <div class="container" >
            <div class="row">
              <div class="col-sm-5 col-md-6">
              <div class=" col-md-11">
                  <h3 align="center">¿Eres operador turístico y quieres obtener más clientes mientras estás en operación?</h3>
                  <p class="text-w3l-services">Con nuestro sistema de reservas y pagos podrás organizar<br />
                  tu disponibilidad y capacidad de servicio para mantener la continuidad de su negocio.</p><br />
                  <h3 align="center">Características del asistente de TuPaseo</h3><br />
                  <h4>Disponibilidad de servicio</h4>
				<p>Indicar el tiempo de servicio del operador.</p>
                 <h4>Últimas ofertas</h4>
				<p>Ofertas de acuerdo a temporadas.</p>
                 <h4>Accesibilidad</h4>
				<p>Accesibilidad y usabilidad de la plataforma para mejorar experiencia de usuario.</p>
                 <h4>Reportes</h4>
				<p>Reportes de reservas e ingresos.</p>
                 <h4>Notificaciones</h4>
				<p>Notificaciones de reservas y pagos vía correo electrónico.</p>
                 <h4>Capacidad de servicio</h4>
				<p>Indicar la cantidad mínima y máxina de turistas que puede atender.</p>
                  </div>
              </div>
              
              <div class="col-sm-7 col-md-6" id="enlaceRegistro" name="enlaceRegistro">
                <div class="content">
                  <h2 align="center">CREA TU PERFIL</h2>
                  <span>¿Quieres que manejemos todas tus reservas y pagos mientras te encuentras en operación?.<br />¡Es tu oportunidad de hacer parte de nosotros!</span><br /><br />

                  <form class="form contact" method="post" action="{{url('operator/register')}}">
	                  {{csrf_field()}}
				            <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
				                            <label for="text" class="sr-only">Nombre</label>
				                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre o Razón social" required>
                                    <span class="label label-danger">{{$errors->first('name') }}</span>

				                        </div>
				        
				            <div class="form-group col-md-6 {{ $errors->has('national_register') ? ' has-error' : '' }}">
				                            <label for="text" class="sr-only">Registro nacional de turismo</label>
				                            <input type="text" class="form-control" id="national_register" name="national_register" placeholder="Registro Nacional de Turismo" required>
                                    <span class="label label-danger">{{$errors->first('national_register') }}</span>

				                        </div>
				                                
				            <div class="form-group col-md-12 {{ $errors->has('contact_personal') ? ' has-error' : '' }}">
				                            <label for="text" class="sr-only">Persona de contacto</label>
				                            <input type="text" class="form-control" id="contact_personal" name="contact_personal" placeholder="Persona de contacto" required>
                                    <span class="label label-danger">{{$errors->first('contact_personal') }}</span>

				                        </div>

				            <div class="form-group col-md-4 {{ $errors->has('contact_phone') ? ' has-error' : '' }}">
				                            <label for="text" class="sr-only">Teléfono de contacto</label>
				                            <input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Teléfono de contacto" required>
                                    <span class="label label-danger">{{$errors->first('contact_phone') }}</span>
				                        </div>

				            <div class="form-group col-md-8 {{ $errors->has('email') ? ' has-error' : '' }}">
				                            <label for="text" class="sr-only">Email</label>
				                            <input type="text" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
                                    <span class="label label-danger">{{$errors->first('email') }}</span>

				                        </div>
				            
				            <div class="form-group col-md-12 {{ $errors->has('web') ? ' has-error' : '' }}">
			                    <label for="text" class="sr-only">Página web</label>
			                    <input type="text" class="form-control" id="web" name="web" placeholder="Página web" required>
                          <span class="label label-danger">{{$errors->first('web') }}</span>

			                </div>          
{{-- 
				            <div class="form-group col-md-12 {{ $errors->has('service_category') ? ' has-error' : '' }}">
			                    <label for="text" class="sr-only">Categoría servicio</label>
			                    <select class="selectpicker form-group"  title="Categoria de servicios" data-width="100%"  id="service_category" name="service_category">
			                        @foreach($categories as $category)
			                          <option value="{{$category->id_category}}">{{$category->service_category}}</option>
			                        @endforeach
			                    </select>
			                </div>
				             
				            <div class="form-group col-md-12 {{ $errors->has('services') ? ' has-error' : '' }}">
			                    <label for="text" class="sr-only">Servicios</label>
			                    <select class="selectpicker form-group"  title="Servicios" data-width="100%"  id="service" name="services[]" multiple data-live-search="true">
			                        @foreach($services as $service)
			                          <option value="{{$service->id_service}}">{{$service->service}}</option>
			                        @endforeach
			                    </select>
			                </div> --}}

			                <div class="form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
			                    <label for="password" class="sr-only">Password</label>
			                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                          <span class="label label-danger">{{$errors->first('password') }}</span>

			                </div>
			                <div class="form-group col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
			                    <label for="password_confirmation" class="sr-only">Confirmar Password</label>
			                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirmar password" required>
			                </div>
                      <div class="form-group col-md-12">
                          <label class="col-md-8 "> <input type="checkbox" id="val-terms" name="val-terms" value="1" checked="" required><a href="#modal-terms" style="color: #6c9e2e" data-toggle="modal">Acepto terminos y condiciones</a> <span class="text-danger">*</span></label>
                      </div>
                      <!-- Terms Modal -->
                            <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title text-center"><strong>Terms and Conditions</strong></h3>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="page-header">1. <strong>General</strong></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                            <h4 class="page-header">2. <strong>Account</strong></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                            <h4 class="page-header">3. <strong>Service</strong></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                            <h4 class="page-header">4. <strong>Payments</strong></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="text-center">
                                                <button type="button" class="btn btn-effect-ripple btn-primary" data-dismiss="modal">I've read them!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                      <!-- END Terms Modal -->
			               <div class="col-sm-6"><button class="btn btn-default contact-submit custom-btn" type="submit"><i class="fa fa-hand-o-right"></i> Crear perfil</button></div>
  				            <br>
  				            <br>
  							<div class="col-lg-offset-8">¿Ya posees una cuenta?</div>
  				            <div class="col-lg-offset-9">
                                       <a style="color: #6c9e2e" href="{{url('/login')}}">Iniciar sesión</a>
                              </div> <!-- end .button -->
  				            
  				    </form>
                   
                  </div>
                </div>
              </div> <!-- .container> .row -->
            </div> <!-- .container -->
          </div> <!-- end .description-one -->

        </div> <!-- end .description -->
      </section>
  @endsection
     
  @section('additionalScript')
  <script>
    jQuery(document).ready(function($) {
      "use strict";

      jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools:false});
    });
  </script>
  <script type="text/javascript">
  $('.transform').click(function()
  {
        $('.card').toggleClass('flipped');
        $('.card').toggleClass('height');
    });
    $('#registrar').on('click', function(e) {
      e.preventDefault();
      $("html, body").animate({scrollTop: $('#enlaceRegistro').offset().top }, 1000);
  });
</script>
@endsection