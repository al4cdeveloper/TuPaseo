<!DOCTYPE html>
<html lang="es">
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia" />
           <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->
   <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
     </script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script> <!-- termina GTM -->
    
    <!-- Template Title -->
    <title>TuPaseo reserva</title>

    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon"/>
    
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">

    <!-- Bootstrap 3.2.0 stylesheet -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Font Awesome Icon stylesheet -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Custom stylesheet -->
    <link href="{{asset('style.css')}}" rel="stylesheet">
    
    <link href="{{asset('css/color/white.css')}}" rel="stylesheet">

    <!-- Custom Responsive stylesheet -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

    <!-- Modernizr (browser feature detection library) -->
    <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <style type="text/css">
        #page-container.sidebar-visible-lg-full header.navbar-fixed-top, #page-container.sidebar-visible-lg-full header.navbar-fixed-bottom{
            left: 0px!important;
        }
        .navbar.navbar-inverse.navbar-glass
        {
            background-color:#fd8c02!important
        }
    </style>   
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
         <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
   
    <!-- ====== Menu Section ====== -->
        <section id="menu">
              
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">TuPaseo</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/tupaseo.png')}}" /></a>
            </div> <!-- end .navbar-header -->              
            
            <div class="">
              <ul class="nav navbar-nav navbar-left">
        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
        <li><a href="{{url('/')}}">Home</a></li>
        <li class="active"><a class="" href="{{url('operator')}}">¿Quieres que vean tu negocio?</a></li>
        <li><a href="{{url('contact')}}">Contactános</a></li>
              </ul>

      <div class="social-icons">
        <a class="facebook" href="https://www.facebook.com/tupaseo.co/" target="_blank"><i class="fa fa-facebook"></i></a>
        <a class="twitter" href="https://www.instagram.com/tupaseo/" target="_blank"><i class="fa fa-instagram"></i></a>
        <a class="linkedin" href="{{url('contact')}}"><i class="fa fa-envelope"></i></a>
      </div>
    </div>
    </section>

    <!-- ====== End Menu Section ====== -->
     <!-- ====== Header Section ====== -->
    <header id="top" style="background-image: url({{asset('images/bg.jpg')}})">
      <div class="bg-color">
        <div class="top section-padding">
          <div class="container">
            <div class="row">

              <div class="col-sm-7 col-md-7">
                <div class="content">
                  <h1><strong></strong></h1>
                  <h1>¡Te invitamos a volver a sentir la emoción por la aventura!</h1>
                  <!-- <div class="button" id="download-app1">
                    <a href="#download" class="btn btn-default btn-lg custom-btn"><i class="fa fa-car"></i>Reservar</a>
                  </div> <!-- end .button -->
                </div> <!-- end .content -->
              </div> <!-- end .top > .container> .row> .col-md-7 -->

              <div class="col-sm-5 col-md-5">
              </div> <!-- end .top > .container> .row> .col-md-5 -->

            </div> <!-- end .top> .container> .row -->
          </div> <!-- end .top> .container -->
        </div> <!-- end .top -->
      </div> <!-- end .bg-color -->
    </header>
    <!-- ====== End Header Section ====== -->

 <!-- ====== Screenshots Section ====== -->
<section id="screenshots">
    <div class="screenshots section-padding">
      <div class="container">
            
        <div class="header">
                  <h1>{{$message}}</h1>
        </div>
      
      </div>
  </div>
</section>

    <!-- ====== End Screenshots Section ====== -->

    <!-- ====== Copyright Section ====== -->
    <section class="copyright dark-bg">
      <div class="container">
        <p>&copy; 2017 <a href="{{url('contact')}}">TuPaseo</a>, Todos los derechos reservados</p>
      </div> <!-- end .container -->
    </section>
    <!-- ====== End Copyright Section ====== -->

<!-- js-scripts -->     

       <script src="{{asset('js/jquery.js')}}"></script>
    
    <!-- Modernizr js -->
    <script src="{{asset('js/modernizr-latest.js')}}"></script>

    <!-- Bootstrap 3.2.0 js -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <!-- Owl Carousel plugin -->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>

    <!-- ScrollTo js -->
    <script src="{{asset('js/jquery.scrollto.min.js')}}"></script>

    <!-- LocalScroll js -->
    <script src="{{asset('js/jquery.localScroll.min.js')}}"></script>

    <!-- jQuery Parallax plugin -->
    <script src="{{asset('js/jquery.parallax-1.1.3.js')}}"></script>

    <!-- Skrollr js plugin -->
    <script src="{{asset('js/skrollr.min.js')}}"></script>

    <!-- jQuery One Page Nav Plugin -->
    <script src="{{asset('js/jquery.nav.js')}}"></script>
    
    <!-- jQuery Pretty Photo Plugin -->
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>


    <!-- Custom JS -->
    <script src="{{asset('js/main.js')}}"></script>

    <script>
      jQuery(document).ready(function($) {
        "use strict";

        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools:false});
      });
    </script>

    <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
    <script src="{{asset('auth-panel/js/app.js')}}"></script>
</body>
</html>