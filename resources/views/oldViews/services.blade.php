<!DOCTYPE html>
<html lang="es">
<head>
<title>De paseo con Tu Paseo</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia" />
        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->
  	<!-- Global Site Tag (gtag.js) - Google Analytics -->
  	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
  	</script>
  	<script>
  	  	window.dataLayer = window.dataLayer || [];
  	  	function gtag(){dataLayer.push(arguments);}
  	  	gtag('js', new Date());

  	  	gtag('config', 'UA-107504893-1');
  	 </script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--// Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}"> <!-- Bootstrap-Core-CSS -->
<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="all" /> <!-- Style-CSS --> 
<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}"> <!-- Font-Awesome-Icons-CSS -->
<!-- //css files -->
<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Coda:400,800&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
<!-- //online-fonts -->
</head>
<body> 
<div class="main-agile banner-2">
	<!-- header -->
	<div class="header-w3layouts"> 
	<!-- Navigation -->
	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">TuPaseo</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div> 
		<div class="logo-agile-1"> 
			<h1><a class="logo" href="{{url('/')}}"><img src="{{asset('images/tupaseo.png')}}" /></a></h1>
		</div> 
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
				<li><a href="{{url('/')}}">Home</a></li>
				<li class="active"><a class="" href="{{url('operator')}}">¿Quieres que vean tu negocio?</a></li>

        </ul>
			<div class="w3ls-social-icons-2">
				<a class="facebook" href="https://www.facebook.com/tupaseo.co/"><i class="fa fa-facebook"></i></a>
				<a class="twitter" href="https://www.instagram.com/tupaseo/"><i class="fa fa-instagram"></i></a>
				<a class="linkedin" href="{{url('contact')}}"><i class="fa fa-envelope"></i></a>
			</div>
		</div>
		<!-- //navbar-collapse -->
		</nav>  
	</div>	
	<!-- //header -->
</div>
    
<!-- services -->
<div class="banner-bottom">
	<div class="container">
		<h3>¡Te invitamos a volver a sentir la emoción por la aventura!</h3>
		<p class="text-w3l-services">Sal de la ciudad, experimenta nuevas sensaciones y despierta tu adrenalina</p>
		<div class='film_roll_container'>
			<div id='film_roll_2'>
				<div id='fr2_0'><img src="{{asset('images/2.jpg')}}" alt=" " class="img-responsive" /></div>
				<div id='fr2_1'><img src="{{asset('images/6.jpg')}}" alt=" " class="img-responsive" /></div>
				<div id='fr2_2'><img src="{{asset('images/3.jpg')}}" alt=" " class="img-responsive" /></div>
				<div id='fr2_3'><img src="{{asset('images/4.jpg')}}" alt=" " class="img-responsive" /></div>
				<div id='fr2_4'><img src="{{asset('images/5.jpg')}}" alt=" " class="img-responsive" /></div>
			</div>
			<a href='#' class='btn btn-primary w3l_btn' id='film_roll_2_left'><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a> 
			<a href='#' class='btn btn-primary w3l_btn' id='film_roll_2_right'><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> 
		</div>
		<div class="w3_banner_bottom_service">
			<div class="col-md-4 w3_banner_bottom_service1">
				<div class="col-xs-4 w3layouts_banner_bottom_servicel">
					<div class="agile-hvr-icon-pulse-shrink agileits_w3layouts_thumb">
					</div>
				</div>
				<div class="col-xs-8 w3layouts_banner_bottom_servicer">
					<h4>Hermosos destinos</h4>
					<p>En TuPaseo encontrarás los destinos de aventura más atractivos de Cundinamarca</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-4 w3_banner_bottom_service1">
				<div class="col-xs-4 w3layouts_banner_bottom_servicel">
					<div class="agile-hvr-icon-pulse-shrink agileits_w3layouts_gift">
					</div>
				</div>
				<div class="col-xs-8 w3layouts_banner_bottom_servicer">
					<h4>Pasión por la aventura</h4>
					<p>Vive los mejores planes en deporte de aventura como rafting, kayak, parapente, y mucho más.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-4 w3_banner_bottom_service1">
				<div class="col-xs-4 w3layouts_banner_bottom_servicel">
					<div class="agile-hvr-icon-pulse-shrink agileits_w3layouts_heart">
					</div>
				</div>
				<div class="col-xs-8 w3layouts_banner_bottom_servicer">
					<h4>Vive más con seguridad</h4>
					<p>Disfruta al máximo una aventura nueva cada vez con operadores certificados.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //services -->    
    
<!-- Popular -->
<div class="albums">
	<div class="container">
		<h3 class="agile-title">EXPERIMENTA</h3> 
		<div class="w3layouts_header">
			<p><span><i class="fa fa-car sub-w3l" aria-hidden="true"></i></span></p>
		</div>
		<div class="col-md-6 w3lsalbums-grid">
			<div class="albums-left"> 
				<div class="wthree-almub">  
				</div>
			</div>
			<div class="albums-right">
				<h4>Parapente</h4>
				<p>Pilotos profesionales experimentados y cdertificados cumplirán tu sueño de volar.</p>
				<a class="w3more" href="{{url('service')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Más info.</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-6 w3lsalbums-grid">
			<div class="albums-left"> 
				<div class="wthree-almub wthree-almub2"> 
				</div> 
			</div>
			<div class="albums-right">
				<h4>Escalada</h4>
				<p>¿Sientes que es hora de sentir más adrenalina que la que se experimenta subiendo al ascensor?</p>
				<a class="w3more" href="{{url('service')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Más info.</a>
			</div>
			<div class="clearfix"></div>
		</div>  
		<div class="col-md-6 w3lsalbums-grid">
			<div class="albums1-right"> 
				<div class="wthree-almub wthree-almub3">  
				</div>
			</div>
			<div class="albums1-left">
				<h4>Rafting</h4>
				<p>La adrenalina que proporciona este hermoso río de Cundinamarca es una experiencia inolvidable.</p>
				<a class="w3more" href="{{url('service')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Más info.</a>
			</div> 
			<div class="clearfix"></div>
		</div>
		<div class="col-md-6 w3lsalbums-grid">
			<div class="albums1-right"> 
				<div class="wthree-almub wthree-almub4">  
				</div>
			</div>
			<div class="albums1-left">
				<h4>Rappel</h4>
				<p>Eleva los niveles de adrenalina al máximo mientras exploras la riqueza natural de Colombia.</p>
				<a class="w3more" href="{{url('service')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Más info.</a>
			</div> 
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div> 
	</div>
</div>
<!-- //Popular --> 

<!-- footer -->
<footer>
	<div class="agileinfo-footer">
		<div class="container">

		</div>
		<div class="agileits-footer-bottom">
			<p class="footer-class">&copy; 2017 TuPaseo. Todos los derechos reservados</p>
		</div>
	</div>
</footer>
<!-- //footer -->
<!-- js-scripts -->			
	<!-- js -->
	<script type="text/javascript" src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!-- Numscroller -->
	<script type="text/javascript" src="{{asset('js/numscroller-1.0.js')}}"></script>
	<!-- //Numscroller -->
	<!-- smooth scrolling -->
	<script src="{{asset('js/SmoothScroll.min.js')}}"></script>
	<!-- //smooth scrolling -->
	<!-- carousal-plugin -->
	<script src="{{asset('js/jquery.film_roll.js')}}"></script> 
	<script src="{{asset('js/index.js')}}"></script>
	<!-- //carousal-plugin -->
<!-- //js-scripts -->
</body>
</html>