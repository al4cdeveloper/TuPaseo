@extends('layouts.front')
@section('title','Reserva | Tu Paseo')

@section('aditionalStyle')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <style type="text/css">
        #page-container.sidebar-visible-lg-full header.navbar-fixed-top, #page-container.sidebar-visible-lg-full header.navbar-fixed-bottom{
            left: 0px!important;
        }
        .navbar.navbar-inverse.navbar-glass
        {
            background-color:#fd8c02!important
        }
    </style>   

@endsection

@section('content')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

         <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
   

     <!-- ====== Header Section ====== -->
    <header id="top" style="background-image: url({{asset('img/bike2.jpg')}})">
      <div class="bg-color">
        <div class="top section-padding">
          <div class="container">
            <div class="row">
            </div> <!-- end .top> .container> .row -->
          </div> <!-- end .top> .container -->
        </div> <!-- end .top -->
      </div> <!-- end .bg-color -->
    </header>
    <!-- ====== End Header Section ====== -->


    <div id="page-wrapper" class="page-loading">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Cargando..</strong></h3>
                </div>
            </div>
    
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full vuejs">
                <!-- Main Container -->
                <div >

                    {{-- Barra lateral --}}
                    <div id="page-content" class="inner-sidebar-right ">
                       <!-- Inner Sidebar -->

                       

                       
                        <div id="page-content-sidebar" v-if="itemsCart>0">
                            <!-- Collapsible Shopping Cart -->
                            <a href="javascript:void(0)" class="btn btn-block btn-default visible-xs" data-toggle="collapse" data-target="#shopping-cart"><i class="fa fa-shopping-cart"></i> Ver carro (@{{itemsCart}})</a>
                            <div id="shopping-cart" class="collapse navbar-collapse remove-padding">
                                <h4 class="inner-sidebar-header">
                                    <a href="javascript:void(0)" @click="deleteCart()" class="btn btn-effect-ripple btn-xs btn-warning pull-right"><i class="fa fa-trash"></i></a>
                                    Carrito de compras (@{{itemsCart}})
                                </h4>
                                <table class="table table-striped table-borderless table-vcenter">
                                    <tbody >
                                        <tr v-for="service in cart">
                                            <td class="text-center">
                                                <button @click="removeCart(service.rowId)" class="btn btn-effect-ripple btn-xs btn-danger pull-right"><i class="fa fa-times"></i></button>
                                            </td>
                                            <td style="width: 80px;">
                                                <h5>@{{service.name}}</h5>
                                            </td>
                                            <td style="width: 60px;" class="text-center">
                                                <h5>@{{service.qty}}</h5>
                                            </td>
                                            <td class="text-right">
                                                <strong>$ @{{service.subtotal}}</strong>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td colspan="3">Total</td>
                                            <td class="text-right">
                                            <strong>$@{{total}}</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @if(Auth::user())
                                <a href="{{url('logeruser/reservation')}}" class="btn btn-effect-ripple btn-block btn-success push-bit-top-bottom" data-toggle="modal">
                                    <i class="fa fa-shopping-cart"></i> Reservar
                                </a>
                                @else
                                    <a href="#modal-checkout" class="btn btn-effect-ripple btn-block btn-success push-bit-top-bottom" data-toggle="modal">
                                        <i class="fa fa-shopping-cart"></i> Reservar
                                    </a>
                                @endif
                            </div>
                            <!-- END Collapsible Shopping Cart -->
                        </div>
                        <!-- END Inner Sidebar --> 
    
                <!-- eStore Header -->
                       <!-- eStore Header -->
                        <div class="content-header">
                            <div class="header-section">
                                <form action="{{url('search')}}" method="get" class="form-horizontal">
                                    <div class="form-group remove-margin-bottom">
                                        <div class="col-sm-6 push-bit">
                                            <select id="example-chosen" name="location" class="select-chosen" data-placeholder="Seleccione lugar para filtrar" style="width: 250px;" required>
                                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    @foreach($sites as $site)
                                                        <option value="{{$site->location}}">{{$site->location}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-4">
                                            <button type="submit" class="btn btn-effect-ripple btn-effect-ripple btn-primary" style="background-color: #fd8c02;border-color: #fd8c02;"><i class="fa fa-search"></i> Encuentra ahora!</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END eStore Header -->

                         <!-- eStore Content -->
                        <div class="row ">
                            {{-- <div class="col-sm-5">
                                <pre>
                                    @{{$data}}  Las variables que tienen arroba son de vue
                                </pre>
                            </div> --}}
                            @foreach($services as $service)

                            <div class="col-lg-6 " {{-- v-for="service in services" --}}>
                                <a href="{{url('/services/'.$service->slug)}}" class="widget animation-fadeInQuick"  {{-- v-on:click="launchModal(service.id_service_operator)" --}}>
                                    <div class="widget-image widget-image-xs">
                                        <img src="{{asset($service->PrincipalService->image)}}" alt="image">
                                        <div class="widget-image-content">
                                            {{-- <div class="pull-right text-light-op"><strong>OFERTA!</strong></div> --}}
                                            <h2 class="widget-heading text-light"><strong>{{$service->PrincipalService->service}}</strong></h2>
                                            <h3 class="widget-heading text-light-op">1-{{$service->capacity}} Personas     <span class="rating" data-default-rating="5" disabled style="color:rgb(255, 205, 5)"></span></h3>

                                        </div>
                                    </div>
                                    <div class="widget-content">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="text-muted"><em>Ubicación: {{$service->location}}</em>
                                                    <br><em> {{$service->PrincipalService->type_service}}</em></div>
                                                {{-- <div class="text-dark push-bit">10 Packages Available</div> --}}
                                                {{-- <div class="text-warning">
                                                    <i class="fa fa-2x fa-star"></i>
                                                    <i class="fa fa-2x fa-star"></i>
                                                    <i class="fa fa-2x fa-star"></i>
                                                    <i class="fa fa-2x fa-star"></i>
                                                    <i class="fa fa-2x fa-star-half-o"></i>

                                                
                                                </div> --}}
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                <h2><span class="text-muted">$</span> <strong class="costo">{{$service->cost}}</strong></h2>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            
                        </div>
                        
                            <div v-for="service in services" :id="'detailService'+service.id_service_operator" class="modal fade modalService" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                        <input type="hidden" name="id_service_operator" :value="service.id_service_operator">
                                        <div class="modal-header-tabs">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <ul class="nav nav-tabs" data-toggle="tabs">
                                                <li class="active">
                                                    <a href="#modal-tabs-package"><i class="gi gi-package"></i> Servicio</a>
                                                </li>
                                                <li>
                                                    <a href="#modal-tabs-text"><i class="gi gi-package"></i> Descripción y recomendaciones</a>
                                                </li>
                                                <li>
                                                    <a href="#modal-tabs-reviews"><i class="fa fa-pencil"></i> Comentarios</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="modal-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="modal-tabs-package">
                                                    <div class="row">
                                                        <h1 class="h2"><strong>@{{service.principal_service.service}}</strong> <small>(@{{service.capacity}} Personas)</small></h1>
                                                        <div class="row push">
                                                            <div class="col-xs-12">
                                                                <div class="block-content-full">
                                                                    <div id="animation-carousel" class="carousel slide remove-margin" data-ride="carousel" >
                                                                        <!-- Wrapper for slides -->
                                                                        <div class="carousel-inner" >
                                                                            <div v-for="(eachimage, index) in service.images" class="item" v-bind:class="{active: index == 1}">
                                                                                <img :src="'{{asset('')}}'+eachimage.link_image" alt="image">
                                                                            </div >
                                                                        </div>
                                                                        <!-- END Wrapper for slides -->

                                                                        <!-- Controls -->
                                                                        <a class="left carousel-control" href="#animation-carousel" data-slide="prev">
                                                                            <span><i class="fa fa-chevron-left"></i></span>
                                                                        </a>
                                                                        <a class="right carousel-control" href="#animation-carousel" data-slide="next">
                                                                            <span><i class="fa fa-chevron-right"></i></span>
                                                                        </a>
                                                                        <!-- END Controls -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- <div class="col-xs-6">
                                                                <img src="img/placeholders/photos/photo2.jpg" alt="Image" class="img-responsive">
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-horizontal">
                                                            <div class="form-group ">
                                                                <label for="service" class="col-md-4 control-label">Fecha</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" name="date" class="form-control input-datepicker datepicker" data-date-format="mm/dd/yy" placeholder="mm/dd/yy" v-model="fillReservation.date" :min="service.since_date" v-on:blur="validateDate(service.id_service_operator)" :id="'dateService'+service.id_service_operator" v-on:click="ValidateInput('dateService'+service.id_service_operator)" >
                                                                </div>
                                                                <button class="btn btn-primary" v-on:click="ValidateInput('dateService'+service.id_service_operator)"> Ver</button>
                                                                <div class="col-sm-8 col-sm-offset-2">
                                                                    
                                                                    <p>En caso de que no se despliegue el calendario por favor oprimir el boton de "Ver" e intentarlo nuevamente.</p>
                                                                </div>
                                                            </div>

                                                            <div class="form-group " v-if="countHours>0">

                                                                <label for="hour" class="col-md-4 control-label">Hora</label>
                                                                <div class="col-md-6">
                                                                    <select id="hour" class="form-control" name="time" required v-model="fillReservation.time">
                                                                        <option v-for="hour in hours">
                                                                            @{{hour}}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div v-else-if="countHours==-1" align="center">
                                                                No se encuentran horarios disponibles para este día.
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="service" class="col-md-4 control-label">Cantidad personas</label>
                                                                <div class="col-md-6">
                                                                    <input :id="'cantidad_personas'+service.id_service_operator" type="text" class="form-control" name="persons" :max="service.capacity" v-model="fillReservation.cuantity" required onchange=>
                                                                </div>
                                                            </div>

                                                            <div class="form-group ">
                                                                <label for="service" class="col-md-4 control-label">Tipo de pago:</label>
                                                                <div class="col-md-6">
                                                                    <input id="pay_type" type="text" class="form-control" name="pay_type" value="En sitio" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="tab-pane" id="modal-tabs-text">

                                                        <h1 class="h2"><strong>@{{service.principal_service.service}}</strong> <small>(@{{service.capacity}} Personas)</small></h1>
                                                        <div class="">
                                                            <div class="row col-md-6">
                                                                <h3><strong>Descripción</strong></h3>
                                                                <p>@{{service.description}}</p>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                     <h3><strong>Recomendaciones</strong></h3>
                                                                    <p>@{{service.requisites}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="tab-pane" id="modal-tabs-reviews">
                                                    <div class="text-center text-warning push">
                                                        <i class="fa fa-2x fa-star"></i>
                                                        <i class="fa fa-2x fa-star"></i>
                                                        <i class="fa fa-2x fa-star"></i>
                                                        <i class="fa fa-2x fa-star"></i>
                                                        <i class="fa fa-2x fa-star"></i>
                                                    </div>
                                                    <div class="text-center">
                                                        <strong>5.0</strong> /5.0 (3 Calificaciones)
                                                    </div>
                                                    <hr>
                                                    <ul class="media-list">
                                                        <li class="media">
                                                            {{-- <a href="javascript:void(0)" class="pull-left">
                                                                <img src="img/placeholders/avatars/avatar12.jpg" alt="Avatar" class="img-circle">
                                                            </a> --}}
                                                            <div class="media-body">
                                                                <span class="text-warning">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </span>
                                                                <a href="javascript:void(0)"><strong>Nicoline Andersen</strong></a>
                                                                <p class="push-bit"><span class="label label-success">Positivo</span> Una experiencia innolvidable</p>
                                                                <p><span class="label label-danger">Negativo</span> Ninguna.</p>
                                                            </div>
                                                        </li>
                                                        <li class="media">
                                                           {{--  <a href="javascript:void(0)" class="pull-left">
                                                                <img src="img/placeholders/avatars/avatar13.jpg" alt="Avatar" class="img-circle">
                                                            </a> --}}
                                                            <div class="media-body">
                                                                <span class="text-warning">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </span>
                                                                <a href="javascript:void(0)"><strong>Josefina Orozco</strong></a>
                                                                <p class="push-bit"><span class="label label-success">Positivo</span> Sin duda volveré a hacerlo</p>
                                                                <p><span class="label label-danger">Negativo</span> Ninguna.</p>
                                                            </div>
                                                        </li>
                                                        <li class="media">
                                                            <a href="javascript:void(0)" class="pull-left">
                                                                <img src="{{asset('img/placeholders/avatars/avatar7.jpg')}}" alt="Avatar" class="img-circle">
                                                            </a>
                                                            <div class="media-body">
                                                                <span class="text-warning">
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                    <i class="fa fa-star"></i>
                                                                </span>
                                                                <a href="javascript:void(0)"><strong>Donald Pierce</strong></a>
                                                                <p class="push-bit"><span class="label label-success">Positivo</span> Buen lugar, la mejor experiencia que he tenido.</p>
                                                                <p><span class="label label-danger">Negativo</span> Ninguna.</p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <h4 class="pull-left"><span class="text-muted">$</span> <strong class="text-primary">@{{service.cost}}</strong></h4>
                                            <button type="button" class="btn btn-effect-ripple btn-success" {{-- onclick="Enviar('@{{service->id_service_operator}}')" --}} v-on:click.prevent="addcar(service.id_service_operator,service.capacity)" hidden="hidden" id="Reservar"><i class="fa fa-shopping-cart" ></i> Reservar ahora</button>
                                            <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal" >Cerrar</button>
                                        </div>
                                </div>
                            </div>
                        </div>

                    
                    </div>
                        <!-- END eStore Content -->

                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Example Package Modal -->
        
        <!-- END Example Package Modal -->

        <!-- Login Modal -->
        <div id="iniciarsesion" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="push" action="{{ route('logincart') }}" method="post">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="modal-title"><strong>Finalizar reserva</strong></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="page-header text-center">{{-- <i class="fa fa-credit-card"></i>  --}}<strong>Iniciar sesión</strong></h4>
                                </div>

                                <input type="hidden" name="origen" value="reservation">
    

                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="checkout-payment-name">Email</label>
                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="checkout-payment-name">Password</label>
                                        <input type="password"  name="password" class="form-control" placeholder="Password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{-- <h4 class="pull-left">$ <strong class="text-primary-dark">@{{total}}</strong></h4> --}}
                            
                            <button type="submit" class="btn btn-effect-ripple btn-success"><i class="fa fa-check"></i> Completar compra</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Checkout Modal -->
        <div id="modal-checkout" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="push" action="{{ route('registercart') }}" method="post">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="modal-title"><strong>Finalizar reserva</strong></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="page-header text-center">{{-- <i class="fa fa-credit-card"></i>  --}}<strong>Ingresa tu información</strong></h4>
                                    <p>A continuación diligencia el formulario para realizar el registro de tu reserva y de tu información para proximas compras. Si ya te has registrado anteriormente, inicia sesión.</p>
                                </div>
                                <input type="hidden" name="time" id="timePay">
                                <input type="hidden" name="date" id="datePay">
                                <input type="hidden" name="service" id="servicePay">
                                <input type="hidden" name="persons_quantity" id="persons_quantity">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="checkout-payment-name">Nombres</label>
                                        <input type="text"  name="nombres" class="form-control" placeholder="Nombres" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-payment-name">Apellidos</label>
                                        <input type="text"  name="apellidos" class="form-control" placeholder="Apellidos" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-payment-name">Email</label>
                                        <input type="email"  name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-name">Dirección</label>
                                                <input type="text"  name="direccion" class="form-control" placeholder="Direccion" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-cvc">Telefono</label>
                                                <input type="number" id="checkout-payment-cvc" name="telefono" class="form-control" placeholder="Telefono">
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-name">Password</label>
                                                <input type="password"  name="password" class="form-control" placeholder="Password" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-cvc">Confirmar Password</label>
                                                <input type="password" name="password_confirmation" class="form-control" placeholder="Conrfimar password">
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{-- <h4 class="pull-left">$ <strong class="text-primary-dark">@{{total}}</strong></h4> --}}
                            <button type="button" class="btn btn-effect-ripple btn-info" onclick="iniciar()">Ya poseo una cuenta</button>
                            <button type="submit" class="btn btn-effect-ripple btn-success"><i class="fa fa-check"></i> Completar compra</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- END Checkout Modal -->

@endsection

@section('additionalScript')
<!-- jQuery Pretty Photo Plugin -->
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.0/sweetalert2.all.js"></script>

    

<script src="https://unpkg.com/vue-router"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
    <script src="{{asset('vue/js/Cart.js')}}"></script>

    <script>
      jQuery(document).ready(function($) {
        "use strict";

        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools:false});
      });
    </script>

    <script>$(function(){ FormsComponents.init(); });</script>
    
    {{-- <script type="text/javascript">
        function Enviar(id)
        {
            $("#timePay").val($("#timeForm").val());
            $("#datePay").val($("#dateForm").val());
            $("#servicePay").val(id);
            $("#persons_quantity").val($("#cantidad_personas").val())
            $("#btnpagar").removeAttr('hidden');
            // $("#modal-checkout").modal("show");
        }
        function iniciar()
        {
            $("#modal-checkout").modal("hide");
            $("#iniciarsesion").modal("show");
        }
        $(document).ready(function() {
            $(".datepicker").datepicker();
            @foreach($services as $service)
                $("#dateService{{$service->id_service_operator}}").datepicker().show();
            @endforeach
            $('.selectpicker').selectpicker()
        });
        @foreach($services as $service)

        function noExcursion(date){ 
            var day = date.getDay();
            // aqui indicamos el numero correspondiente a los dias que ha de bloquearse (el 0 es Domingo, 1 Lunes, etc...) en el ejemplo bloqueo todos menos los lunes y jueves.
            return [(
                day = day
                @foreach($service->days as $day)
                    @if($day =="Domingo")
                        && day != 0

                    @elseif($day =="Lunes")
                        && day != 1 
                    @elseif($day =="Martes")
                        && day != 2 
                    @elseif($day =="Miercoles")
                        && day != 3 
                    @elseif($day =="Jueves")
                        && day != 4 
                    @elseif($day =="Viernes")
                        && day != 5 
                    @elseif($day =="Sabado")
                        && day != 6 
                    @endif
                @endforeach
                ), ''];
            };

            //Crear el datepicker
            $('#dateService{{$service->id_service_operator}}').datepicker({
            beforeShowDay: noExcursion,
            });
            $("#dateService{{$service->id_service_operator}}").click(function(event) {
                $("#dateService{{$service->id_service_operator}}").datepicker().show();
            });

            $('#dateService{{$service->id_service_operator}}').change(function(event) 
            {
                alert('changeeee')
                var d = new Date();
                var strDate = d.getDate()+ "/" + (d.getMonth()+1) + "/" +  d.getFullYear();
                valuesStart=strDate.split("/");
                 valuesEnd=$(this).val().split("/");
                // Verificamos que la fecha no sea posterior a la actual
                var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
                var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
                if(dateStart<=dateEnd)
                {

                }
                else
                {
                    swal(
                      'Error!!!',
                      'La fecha de agendamiento debe ser superior a hoy.',
                      'error'
                    );
                    $(this).val("");
                }
            });

            $("#cantidad_personas{{$service->id_service_operator}}").change(function()
            {
                if($(this).val()>{{$service->capacity}})
                {
                    swal(
                      'Error!!!',
                      'No puede sobrepasar la capacidad.',
                      'error'
                    );
                    $(this).val("");
                }
                else{

                }
            });
            
            // var day = date.getDay();

            // .datepicker({
            //     beforeShowDay:[(day != 0 && day != 2 && day != 3 && day != 5 && day != 6), '']
            // })
        @endforeach
         $(function() {
    
         //Array para dar formato en español
          $.datepicker.regional['es'] = 
          {
          closeText: 'Cerrar', 
          prevText: 'Previo', 
          nextText: 'Próximo',
          
          monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
          'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
          monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
          'Jul','Ago','Sep','Oct','Nov','Dic'],
          monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
          dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
          dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
          dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
          dateFormat: 'dd/mm/yy', firstDay: 0, 
          initStatus: 'Selecciona la fecha', isRTL: false};
         $.datepicker.setDefaults($.datepicker.regional['es']);
         
         //miDate: fecha de comienzo D=días | M=mes | Y=año
         //maxDate: fecha tope D=días | M=mes | Y=año
            $( "#datepicker" ).datepicker({ minDate: "-1D", maxDate: "+1M +10D" });
          });
         
        // function noExcursion(date){ 
        //     var day = date.getDay();
        //     // aqui indicamos el numero correspondiente a los dias que ha de bloquearse (el 0 es Domingo, 1 Lunes, etc...) en el ejemplo bloqueo todos menos los lunes y jueves.
        //     return [(day != 0 && day != 2 && day != 3 && day != 5 && day != 6), ''];
        //     };

        //     //Crear el datepicker
        //     $("#fechaSur").datepicker({
        //     beforeShowDay: noExcursion,
            });
    </script> --}}
 @endsection