<!DOCTYPE html>
<html lang="en">
<head>
<title>Beneficios para operadores turísticos | Tu Paseo</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia" />
        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
     </script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--// Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}"> <!-- Bootstrap-Core-CSS -->
<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="all" /> <!-- Style-CSS --> 
<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}"> <!-- Font-Awesome-Icons-CSS -->
<link rel="stylesheet" href="{{asset('css/chocolat.css')}}" type="text/css" media="screen"><!-- chocolate css for gallery light box-->
<!-- //css files -->
<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Coda:400,800&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->

<!-- //online-fonts -->
</head>
<body> 
<div class="main-agile banner-2">
    
<!-- header -->
<div class="header-w3layouts"> 
  <!-- Navigation -->
  <nav class="navbar navbar-default">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">TuPaseo</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div> 
    <div class="logo-agile-1"> 
          <h1><a class="logo" href="{{url('/')}}"><img src="{{{asset('images/tupaseo.png')}}}" /></a></h1>
    </div> 
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav navbar-right">
        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
        <li><a href="{{url('url')}}">Home</a></li>
        <li class="active"><a class="" href="{{url('operator')}}">¿Quieres que vean tu negocio?</a></li>
      </ul>
      <div class="w3ls-social-icons-2">
        <a class="facebook" href="https://www.facebook.com/tupaseo.co/"><i class="fa fa-facebook"></i></a>
        <a class="twitter" href="https://www.instagram.com/tupaseo/"><i class="fa fa-instagram"></i></a>
        <a class="linkedin" href="{{url('contact')}}"><i class="fa fa-envelope"></i></a>
      </div>
    </div>
    <!-- //navbar-collapse -->
  </nav>  
  <div class="clearfix"> </div>
</div>  
<!-- //header -->
</div> 
    

<!-- Popular -->
<div class="albums">
  <div class="container">
    <h2 class="agile-title">CREA TU PERFIL  </h2> 
    <div class="w3layouts_header">
      <p><span><i class="fa fa-check sub-w3l" aria-hidden="true"></i></span></p>
    </div>
       <section id="description">
      <div class="description">
        <div class="description-one section-padding dark-bg1">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 col-md-12">
                <div class="app-image">
                  <img src="{{asset('images/duel-phone-big1.png')}}" alt="">
                </div>
              </div>
              <div class="col-sm-6 col-md-6 col-md-offset-3" align="center">
                <div class="content">
                   <form class="" method="post" action="{{route('operator.store')}}">
                  {{csrf_field()}}
            <div class="form-group">
                            <label for="text" class="sr-only">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre o Razón social" required>
                        </div>
        
            <div class="form-group">
                            <label for="text" class="sr-only">Registro nacional de turismo</label>
                            <input type="text" class="form-control" id="national_register" name="national_register" placeholder="Registro Nacional de Turismo" required>
                        </div>
                                
            <div class="form-group">
                            <label for="text" class="sr-only">Persona de contacto</label>
                            <input type="text" class="form-control" id="contact_personal" name="contact_personal" placeholder="Persona de contacto" required>
                        </div>

            <div class="form-group">
                            <label for="text" class="sr-only">Teléfono de contacto</label>
                            <input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Teléfono de contacto" required>
                        </div>

            <div class="form-group">
                            <label for="text" class="sr-only">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
                        </div>
            
            <div class="form-group">
                            <label for="text" class="sr-only">Página web</label>
                            <input type="text" class="form-control" id="web" name="web" placeholder="Página web" required>
                        </div>          

            <div class="form-group">
                            <label for="text" class="sr-only">Categoría servicio</label>
                            <select class="selectpicker form-group"  title="Categoria de servicios" data-width="100%"  id="service_category" name="service_category">
                                @foreach($categories as $category)
                                  <option value="{{$category->id_category}}">{{$category->service_category}}</option>
                                @endforeach
                            </select>
                        </div>
             
            <div class="form-group">
                            <label for="text" class="sr-only">Servicios</label>
                            <select class="selectpicker form-group"  title="Servicios" data-width="100%"  id="service" name="services[]" multiple data-live-search="true">
                                @foreach($services as $service)
                                  <option value="{{$service->id_service}}">{{$service->service}}</option>
                                @endforeach
                            </select>
                        </div>

            <button class="btn btn-default contact-submit custom-btn" type="submit"><i class="fa fa-hand-o-right"></i>Crear perfil</button>
            
          </form>
                </div>
              </div>
            </div> <!-- .container> .row -->
          </div> <!-- .container -->
        </div> <!-- end .description-one -->

      </div> <!-- end .description -->
    </section>
      
      <div class="clearfix"></div>
    </div>  
    <div class="clearfix"></div> 
  </div>
</div>
<!-- //Popular -->
    
<!-- footer -->
<footer>
  <div class="agileinfo-footer">
    <div class="container">
    </div>
    <div class="agileits-footer-bottom">
      <p class="footer-class">&copy; 2017 TuPaseo. Todos los derechos reservados</p>
    </div>
  </div>
</footer>
<!-- //footer -->
<!-- js-scripts -->     
  <!-- js -->
  <script type="text/javascript" src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
  <!-- //js -->
  <!-- start-smoth-scrolling -->
  <script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $(".scroll").click(function(event){   
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
      });
    });
  </script>
  <!-- start-smoth-scrolling -->
  <!-- smooth scrolling -->
  <script src="{{asset('js/SmoothScroll.min.js')}}"></script>
  <!-- //smooth scrolling -->
  <!-- fliter-JavaScript -->
    <script src="{{asset('js/jquery.filterizr.js')}}"></script>
    <script src="{{asset('js/controls.js')}}"></script>
    <script type="text/javascript">
      $(function() {
        $('.filtr-container').filterizr();
      });
    </script>
  <!-- //fliter-JavaScript -->
  <!-- PopUp-Box-JavaScript -->
    <script src="{{asset('js/jquery.chocolat.js')}}"></script>
    <script type="text/javascript">
      $(function() {
        $('.filtr-item a').Chocolat();
      });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- Input tags -->
    <script src="{{asset('plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
  <!-- //PopUp-Box-JavaScript --> 
<!-- //js-scripts -->
</body>
</html>
      