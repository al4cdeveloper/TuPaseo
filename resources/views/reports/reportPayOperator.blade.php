<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Reporte de pago a operador</title>
 <style >

.row {
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

.media {
    display: flex;
    align-items: flex-start;
}

img {
    vertical-align: middle;
    border-style: none;
}

.media-body {
    flex: 1;
}

.pl-0, .px-0 {
    padding-left: 0 !important;
}

.pr-0, .px-0 {
    padding-right: 0 !important;
}

.ml-2, .mx-2 {
    margin-left: 1.5rem !important;
}
table {
   width: 100%;
   border: 1px solid #000;
}
th, td {
   width: 25%;
   text-align: left;
   vertical-align: top;
   border: 1px solid #000;
   border-collapse: collapse;
   padding: 0.3em;
   caption-side: bottom;
}
caption {
   padding: 0.3em;
}

.list-unstyled {
    padding-left: 0;
    list-style: none;
}

ol, ul, dl {
    margin-top: 0;
    margin-bottom: 1rem;
}

ol li, ul li, dl li {
    line-height: 1.8;
}

h2, .h2 {
    font-size: 1.74rem;
}

h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    margin-bottom: 0.5rem;
    font-family: "Montserrat", Georgia, "Times New Roman", Times, serif;
    font-weight: 400;
    line-height: 1.2;
    color: inherit;
}

h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: 0.5rem;
}

.pb-3, .py-3 {
    padding-bottom: 3rem !important;
}

p {
    margin-top: 0;
    margin-bottom: 1rem;
}

p {
    display: block;
    -webkit-margin-before: 1em;
    -webkit-margin-after: 1em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
}

.lead {
    font-size: 1.25rem;
    font-weight: 400;
}


.text-bold-800 {
  font-weight: 800; }

.pt-2, .py-2 {
    padding-top: 1.5rem !important;
}

.text-muted {
    color: #404E67 !important;
}

.text-bold-400 {
  font-weight: 400; }



.text-right {
    text-align: right !important;
}

 .text-md-right {
     text-align: right !important;
     }
.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
.col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
.col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
.col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
.col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
.col-xl-auto {
  position: relative;
  width: 100%;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px;
}
.img-responsive
 {
  display: block;
  max-width: 100%;
  height: auto;
}
 .col-md-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
  }
  .col-md-12 {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
  }
  .pt-2,
.py-2 {
  padding-top: 0.5rem !important;
}
.text-right {
  text-align: right !important;
}
.lead {
  font-size: 1.25rem;
  font-weight: 300;
}
.row {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.text-muted {
  color: #6c757d !important;
}




.col-md-7 {
    -ms-flex: 0 0 58.333333%;
    flex: 0 0 58.333333%;
    max-width: 58.333333%;
  }
  .col-md-5 {
    -ms-flex: 0 0 41.666667%;
    flex: 0 0 41.666667%;
    max-width: 41.666667%;
  }
  .pull-right {
  float: right !important;
}
.pull-left {
  float: left !important;
}

.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #006699; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 15px; font-weight: bold; border-left: 1px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00557F; border-left: 1px solid #E1EEF4;font-size: 12px;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEf4; color: #00557F; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }.datagrid table tfoot td div { border-top: 1px solid #006699;background: #E1EEf4;} .datagrid table tfoot td { padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid table tfoot td ul { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid table tfoot  li { display: inline; }.datagrid table tfoot li a { text-decoration: none; display: inline-block;  padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #006699;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; }.datagrid table tfoot ul.active, .datagrid table tfoot ul a:hover { text-decoration: none;border-color: #00557F; color: #FFFFFF; background: none; background-color:#006699;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }
    </style>
</head>
<body>

            <div class="row">
                <div class="col-md-6 text-md-left pull-left">
                      <img src="http://tupaseo.travel/images/mail_logo-80x80.png" alt="TuPaseo">
                          <ul class="ml-2 px-0 list-unstyled">
                              <li class="text-bold-800"><b>TuPaseo.Travel</b></li>
                              <li>NIT 901.193.027-4</li>
                              <li>Cra 9 # 72-81 Oficina 504</li>
                              <li>PBX (+0057) 1 3121540</li>
                              <li>Bogotá D.C., Colombia</li>
                          </ul>
                </div>
                <div class="col-md-6 col-sm-12 text-center text-md-right pull-right">
                <h2 class="text-muted">Reporte de pago a operador</h2>
                <p># RPO-{{str_pad($report->id_report, 6, "0", STR_PAD_LEFT)}}</p>
                <div id="invoice-customer-details" class="row pt-2">
                  <div class="col-md-12 text-right text-md-right pull-right">
                    <p>
                      <span class="text-muted">Desde:</span> {{$report->date_start}} <br>   <span class="text-muted">Hasta:</span> {{$report->date_finish}} <br>
                      <span class="text-muted">Estado:</span> {{$report->state}}</p>
                  </div>
                </div> <!-- End Bill To -->
              </div>
            </div> <!-- End Logo -->
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>


            <hr>
            <div class="row">
                <div class="col-md-12">
                  <h3>Operador: {{$reservations[0]->Service->operador->name}}</h3>
                </div> 
            </div>
            <div class="row">

                  <div class="col-md-5 pull-left">
                    <ul class="ml-2 px-0 list-unstyled">
                      <li><strong>Cantidad reservaciones: </strong> {{count($reservations)}} </li>
                      <li><strong>Comisión acordada: </strong> {{$reservations[0]->Service->operador->commission}}%</li>
                      <li><strong>IVA: </strong> {{$reservations[0]->Service->operador->option_iva}} IVA</li>
                  </ul>
                  </div>
                  <div  class="col-md-5 pull-right">
                    <ul class="ml-2 px-0 list-unstyled">
                        <li><strong>Entidad bancaria: </strong> {{$reservations[0]->Service->operador->bank_name}}</li>
                        <li><strong>Numero de cuenta: </strong> {{$reservations[0]->Service->operador->account_number}}</li>
                        <li><strong>Tipo de cuenta: </strong> {{$reservations[0]->Service->operador->type_account}}  </li>
                    </ul>
                  </div>
            </div>
            <br> 
            <br> 
            <br> 
            <br> 
            <br> 
            <div class="row datagrid">
                    <div class=" col-sm-12">
                        <table >
                            <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Servicio</th>
                                    <th>Nombre pagador</th>
                                    <th>Identificación pagador</th>
                                    <th>Fecha de realización</th>
                                    <th>Cantidad</th>
                                    <th>Costo Unitario</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody {{$acumulador = 0}}>
            <!--INICIA FOREACH-->
                                @foreach($reservations as $reservation)
                                <tr>
                                    <th># RSVT-{{$reservation->id_reservation}}</th>
                                    <td>{{$reservation->Service->service_name}}
                                        @if(count($reservation->orderProduct->items)>0)
                                            @foreach($reservation->orderProduct->items as $item)
                                                + {{$item->item_name}}
                                            @endforeach
                                        @endif
                                    </td>
                                    <th>{{$reservation->orderProduct->order->shipping_order->first_name}} {{$reservation->orderProduct->order->shipping_order->last_name}}</th>
                                    <th>{{$reservation->orderProduct->order->shipping_order->type_dni}} {{$reservation->orderProduct->order->shipping_order->dni}}</th>
                                    <td>{{$reservation->date}}</td>
                                    <td>{{$reservation->cuantity}}</td>
                                    <td>{{cop_format($reservation->orderProduct->unit_price)}}</td>
                                    <td {{$acumulador += $reservation->cuantity*$reservation->orderProduct->unit_price}}>{{cop_format($reservation->cuantity*$reservation->orderProduct->unit_price)}}</td>
                                </tr>
                                @endforeach
            <!--TERMINA FOREACH-->
                                <tr >
                                    <td colspan="5"></td>
                                    <td >Sub Total</td>
                                    <td class="text-right " colspan="2">{{cop_format($acumulador)}}</td>
                                </tr>
                                <!--MÁS IVA-->
                                @if($reservations[0]->Service->operador->option_iva=='mas')
                                <tr >
                                    <td colspan="5"></td>
                                    <td >Comision ({{$reservations[0]->Service->operador->commission}}%)</td>
                                    <td class="text-right" colspan="2"> - {{cop_format(($acumulador*$reservations[0]->Service->operador->commission)/100)}}</td>
                                </tr>
                                <tr >
                                    <td colspan="5"></td>
                                    <td>IVA ({{env('IVA')}}%) </td>
                                    <td class="text-right" colspan="2"> - {{cop_format(((($acumulador*$reservations[0]->Service->operador->commission)/100)*env('IVA'))/100)}}</td>
                                </tr>
                                <tr >
                                    <td colspan="5"></td>
                                    <td  class="text-bold-800">Total</td>
                                    <td class="text-bold-800 text-right" colspan="2"> {{cop_format($acumulador - (($acumulador*$reservations[0]->Service->operador->commission)/100) - (((($acumulador*$reservations[0]->Service->operador->commission)/100)*env('IVA'))/100))}}</td>
                                </tr>
                                <!--INCLUIDO IVA-->
                                @else
                                <tr >
                                    <td colspan="5"></td>
                                    <td>Comisión({{$reservations[0]->Service->operador->commission}}%)</td>
                                    <td class="text-right" colspan="2"> - {{cop_format((($acumulador*$reservations[0]->Service->operador->commission)/100) - (((($acumulador*$reservations[0]->Service->operador->commission)/100)*env('IVA'))/100))}}</td>
                                </tr>
                                <tr >
                                    <td colspan="5"></td>
                                    <td>IVA ({{env('IVA')}}%)</td>
                                    <td class="text-right" colspan="2"> - {{cop_format(((($acumulador*$reservations[0]->Service->operador->commission)/100)*env('IVA'))/100)}}</td>
                                </tr>
                                <tr >
                                    <td colspan="5"></td>
                                    <td  class="text-bold-800">Total</td>
                                    <td class="text-bold-800 text-right" colspan="2"> {{cop_format($acumulador - (($acumulador*$reservations[0]->Service->operador->commission)/100))}}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
            </div> <!-- End Items -->

            <br><br><br>
            <div class="row" align="center">

                    <div class="col-md-12 col-sm-12">
                        <div class="text-center">
                            <p> </p>
                            <img src="http://tupaseo.travel/images/mail_logo.png" alt="signature" class="height-100">
                            <h6>Equipo</h6>
                            <p class="text-muted">TuPaseo.Travel</p>
                        </div>
                    </div> <!-- End Total -->
                </div>
</body>
</html>