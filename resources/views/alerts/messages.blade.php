@if (count($errors) > 0)
<!-- Messenger http://github.hubspot.com/messenger/ -->
<script src="{{asset('plugins/toastr/toastr.js')}}"></script>
<link href="{{asset('plugins/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />


<script type="text/javascript">
  jQuery(document).ready(function($) {
  toastr.options = {
    "closeButton": true,
    "newestOnTop": true,
    "positionClass": "toast-top-right"
  };

  @foreach($errors->all() as $error)
  toastr["error"]("{{ $error }}","{{ config('app.name') }}");
  @endforeach
    
  });
</script>
@endif


@if(Session::has('message'))
<!-- Messenger http://github.hubspot.com/messenger/ -->
<script src="{{asset('plugins/toastr/toastr.js')}}"></script>
<link href="{{asset('plugins/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />


<script type="text/javascript">
$(document).ready(function($) {

	toastr.options = {
		"closeButton": true,
		"newestOnTop": true,
		"positionClass": "toast-top-right"
	};

	toastr["success"]("{{ Session::get('message') }}","{{ config('app.name') }}");
});
</script>
@endif

@if(Session::has('message-error'))
<!-- Messenger http://github.hubspot.com/messenger/ -->
<script src="{{asset('plugins/toastr/toastr.js')}}"></script>
<link href="{{asset('plugins/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />


<script type="text/javascript">
$(document).ready(function($) {
	toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "5000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
	
	toastr["error"]("{{ Session::get('message-error') }}","TuPaseo");

});

</script>
@endif

@if(Session::has('message-warning'))
<!-- Messenger http://github.hubspot.com/messenger/ -->
<script src="{{asset('plugins/toastr/toastr.js')}}"></script>
<link href="{{asset('plugins/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />


<script type="text/javascript">
$(document).ready(function($) {
	toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "5000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
	
	toastr["warning"]("{{ Session::get('message-warning') }}","TuPaseo");

});

</script>
@endif