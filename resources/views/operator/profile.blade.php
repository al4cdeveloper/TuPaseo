@extends('operator.layout.auth')

@section('title', 'Perfil')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
      <div class="content-header">
          <div class="row">
              <form>
                  <div class="col-sm-12">
                      <div class="header-section">
                          <h1>Perfil</h1>
                      </div>
                      
                  </div>
              </form>
          </div>
      </div>
      <!-- END Page Header -->

      <!-- Example Block -->
      <div class="block" id="service">
          <div class="row">
            <!-- Form Validation Content -->
                    <div class="col-md-12">
                      <h2>Editar información</h2>
                      <small>A continuación por favor ingresar la información que desees cambiar y luego clickea en el botón "Guardar".</small>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 ">
                            <!-- Form Validation Form -->
                            <form id="form-validation" action="{{url('operator/update')}}" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                              {{csrf_field()}}
                                <div class="form-group">
                                  <div class="">
                                  <label class="col-md-3 control-label" for="name">Imagen de perfil <span class="text-danger">*</span></label>
                                    <div class="kv-avatar center-block text-center" style="width:200px">
                                        <input id="avatar-2" name="file" type="file" class="file-loading">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Nombre <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Nombre o Razón social" required value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="national_register">Registro nacional de turismo <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" id="national_register" name="national_register" class="form-control" placeholder="Registro Nacional de Turismo" required value="{{$user->national_register}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                      <label class="col-md-3 control-label" for="nit">NIT <span class="text-danger">*</span></label>
                                      <div class="col-md-6">
                                          <input type="text" id="nit" name="nit" class="form-control" placeholder="NIT de la empresa" required value="{{$user->nit}}">
                                      </div>
                                  </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="contact_personal">Persona de contacto<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" id="contact_personal" name="contact_personal" class="form-control" placeholder="Persona de contacto" required value="{{$user->contact_personal}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="contact_phone">Teléfono de contacto<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" id="contact_phone" name="contact_phone" class="form-control" placeholder="Teléfono de contacto" required value="{{$user->contact_phone}}">
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="col-md-3 control-label" for="email">Email <span class="text-danger">*</span></label>
                                      <div class="col-md-6">
                                          <input type="text" id="email" name="email" class="form-control" placeholder="Ingresa tu email" required value="{{$user->email}}">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-3 control-label" for="web">Página web <small>opcional</small></label>
                                      <div class="col-md-6">
                                          <input type="text" id="web" name="web" class="form-control" placeholder="Página web" value="{{$user->web}}">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-3 control-label" for="address">Dirección <span class="text-danger">*</span></label>
                                      <div class="col-md-6">
                                          <input type="text" id="address" name="address" class="form-control" placeholder="Dirección " required value="{{$user->address}}">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-3 control-label" for="region">Region <span class="text-danger">*</span></label>
                                      <div class="col-md-6">
                                          @if(!$user->region)
                                           <select name="region" id="region" class="select-chosen" data-live-search="true" data-size="10"title="Seleccione">
                                               @foreach($regions as $region)
                                                    <option value="{{$region['region']}}" >{{$region['region']}}</option>
                                               @endforeach
                                           </select>
                                           @else
                                           <select name="region" id="region" class="select-chosen" data-live-search="true" data-size="10"title="Seleccione">
                                               @foreach($regions as $region)
                                                    <option value="{{$region['region']}}" @if($region['region']== $user->region) selected @endif>{{$region['region']}}</option>
                                               @endforeach
                                           </select>
                                           @endif
                                            <span class="label label-danger">{{$errors->first('region') }}</span>
                                      </div>
                                  </div>
                                  <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }} @if(!$user->region) hidden @endif" id="divcity">
                                    <label for="city" class="col-md-3 control-label">Ciudad</label>
                                    <div class="col-md-6">
                                        @if(!$user->region)
                                       <select name="city" id="city" class="select-chosen" data-live-search="true" data-size="10" title="Seleccione">
                                       </select>
                                       @else
                                        <select name="city" id="city" class="select-chosen" data-live-search="true" data-size="10" title="Seleccione">
                                            @foreach($cities as $city)
                                                <option value="{{$city['city']}}" @if($city['city']== $user->city) selected @endif>{{$city['city']}}</option>
                                            @endforeach
                                       </select>
                                       @endif
                                        <span class="label label-danger">{{$errors->first('city') }}</span>
                                    </div>
                                </div>
                              </div>
                              </div>
                                <div class="row">
                                  <div class="col-md-12" align="center">
                                    <h3>Cambiar contraseña</h3>
                                        <button class="btn btn-danger" type="button" onclick="changepass()" id="cambiar">Cambiar contraseña</button>
                                    <div id="changePass" class="hidden">
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label for="password">Password<span class="kv-reqd">*</span></label>
                                            <input type="password" class="form-control" id="password" name="password" >
                                          </div>
                                        </div>
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label for="password_confirmation">Confirmar password<span class="kv-reqd">*</span></label>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <hr>
                                <div class="form-group ">
                                    <div class="col-md-12" align="center">
                                        <button type="submit" class="btn btn-effect-ripple btn-primary">Enviar</button>
                                        <a href="{{url('operator/home')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                            <!-- END Form Validation Form -->
                    </div>
          </div>
      </div>
      <!-- END Example Block -->
        
  </div>

@endsection
@section('aditionalScript')
<script type="text/javascript">
  function changepass()
  {
      $("#changePass").removeClass('hidden');
      $("#cambiar").attr('onclick','cancel()');
      $("#cambiar").text('Cancelar');
      $("#password").attr('required','required')
      $("#password_confirmation").attr('required','required')
  }
function cancel()
  {
      $("#changePass").addClass('hidden');
      $("#cambiar").attr('onclick','changepass()');
      $("#cambiar").text('Cambiar contraseña');
      $("#password").removeAttr('required')
  }

$("#region").change(function()
{
    var region = $(this).val(); 
    var region = region.replace(/ /g, "%20");
    $.ajax({
        url: 'https://battuta.medunes.net/api/city/co/search/?region='+region+'&key=00000000000000000000000000000000',
        type: 'GET',
        dataType: 'JSON',     
        complete:function(transport)
        {
            response = transport.responseJSON;
            $('#city').empty().trigger("chosen:updated");
            $('#divcity').removeClass('hidden');
            if(response.length>0)
            {
                for(x in response)
                {
                    $("#city").append('<option value="'+response[x].city+'">'+response[x].city+'</option>').trigger("chosen:updated");
                }
            }
            else
            {
                $("#city").removeAttr('required', 'required');;
            }

        }
    })
});
$(document).ready(function()
{
  $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="@if($user->avatar) {{asset($user->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="Tu avatar" style="width:160px"><h6 class="text-muted">Click para cambiar</h6>',
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
});
</script>
@endsection