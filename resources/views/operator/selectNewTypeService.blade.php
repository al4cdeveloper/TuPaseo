@extends('operator.layout.auth')

@section('title', 'Crear nuevo Servicio')

@section('content')
    <div id="page-content">

     <!-- Page Header -->
      <div class="content-header">
          <div class="row">
              <form>
                  <div class="col-sm-12">
                      <div class="header-section">
                          <h1>Crear nuevo servicio</h1>
                      </div>
                      
                  </div>
              </form>
          </div>
      </div>
      <!-- END Page Header -->

      <!-- Example Block -->
      <div class="block" > 
        <div class="row">
          <div class="col-md-10 col-md-offset-1" align="center">
              <p>A continuación seleccione el servicio que desea crear</p>
            <!-- Form Validation Content -->
            @if($hotels < Auth::user()->cuantity_hotels)
              <div class="col-md-4" >
               <a href="{{asset('operator/establishment/hotel/create')}}" class="widget">
                    <div class="widget-content themed-background-amethyst clearfix">
                       <div class="widget-icon pull-left">
                            <i class="fa fa-hotel text-light-op"></i>
                        </div>
                        <h2 class="widget-heading h3 text-light"><strong>Hotel</strong></h2>
                        <span class="text-light-op">Registrar nuevo hotel</span>
                    </div>
                </a>
              </div>
            @endif
            @if($restaurants < Auth::user()->cuantity_restaurants)
              <div class="col-md-4" >
               <a href="{{asset('operator/establishment/restaurant/create')}}" class="widget">
                    <div class="widget-content themed-background-social clearfix">
                       <div class="widget-icon pull-left">
                            <i class="fa fa-cutlery text-light-op"></i>
                        </div>
                        <h2 class="widget-heading h3 text-light"><strong>Restaurante</strong></h2>
                        <span class="text-light-op">Registrar nuevo restaurante</span>
                    </div>
                </a>
              </div>
            @endif
            @if($services < Auth::user()->cuantity_services)
              <div class="col-md-4" >
                 <a href="{{route('operator.services.create')}}" class="widget">
                    <div class="widget-content themed-background-flat clearfix">
                       <div class="widget-icon pull-left">
                            <i class="fa fa-cutlery text-light-op"></i>
                        </div>
                        <h2 class="widget-heading h3 text-light"><strong>Actividad turística</strong></h2>
                        <span class="text-light-op">Registrar nueva actividad</span>
                    </div>
                </a>
              </div>
            @endif
          </div>
          </div>
      </div>
      <!-- END Example Block -->
        
  </div>

@endsection