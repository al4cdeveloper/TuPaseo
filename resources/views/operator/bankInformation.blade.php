@extends('operator.layout.auth')

@section('title', 'Información bancaria')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
      <div class="content-header">
          <div class="row">
              <form>
                  <div class="col-sm-12">
                      <div class="header-section">
                          <h1>Información bancaria</h1>
                      </div>
                      
                  </div>
              </form>
          </div>
      </div>
      <!-- END Page Header -->

      <!-- Example Block -->
      <div class="block" id="service">
          <div class="row">
            <!-- Form Validation Content -->
                    <div class="col-md-12">
                      <h2>Editar información</h2>
                      <small>A continuación por favor ingresar la información que desees cambiar y luego clickea en el botón "Guardar".</small>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            <!-- Form Validation Form -->
                            {!!Form::model($user,['url'=>'operator/updateinfo','method'=>'POST','class'=>'form-horizontal form-bordered'])!!}
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Entidad bancaria <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                      {!!Form::text('bank_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de entidad bancaria', 'required'])!!}
                                      <span class="label label-danger">{{$errors->first('bank_name') }}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Numero de cuenta <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                      {!!Form::text('account_number', null, ['class'=>'form-control', 'placeholder' => 'Numero de cuente', 'required'])!!}
                                      <span class="label label-danger">{{$errors->first('account_number') }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="contact_phone">Tipo de cuenta<span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        {!!Form::select('type_account',$options,['class'=>'select-chosen',' data-placeholder'=>'Seleccione tipo de cuenta'])!!}
                                        <span class="label label-danger">{{$errors->first('type_account') }}</span>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-effect-ripple btn-primary">Enviar</button>
                                        <a href="{{url('operator/home')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                                    </div>
                                </div>
                            {!!Form::close()!!}
                            <!-- END Form Validation Form -->
                    </div>
          </div>
      </div>
      <!-- END Example Block -->
        
  </div>

@endsection