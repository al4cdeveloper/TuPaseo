@extends('operator.layout.auth')

@section('title', 'Dashboard')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Panel administrativo</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block">
            
            <!-- Example Content -->
            <h3>Panel administrativo tupaseo.travel</h3>
            <!-- END Example Content -->
            <div class="row">
                @if($services<=Auth::user()->cuantity_services && $restaurants<=Auth::user()->cuantity_restaurants && $hotels <= Auth::user()->cuantity_hotels)
                 <div class="col-sm-6 col-lg-3">
                    <a href="{{url('operator/newservice')}}" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-social">
                            <strong class="text-light-op">Crear nuevo servicio</strong>
                        </div>
                        <div class="widget-content themed-background-social clearfix">
                            <div class="widget-icon pull-right">
                                 <i class="fa fa-ravelry text-light-op"></i>
                            </div>
                            <h2 class="widget-heading h3 text-light"><strong>1+ </strong></h2>
                            <span class="text-light-op">Crear un nuevo servicio</span>
                        </div>
                    </a>
                </div>
                @endif
            </div>
        </div>
        <!-- END Example Block -->
    </div>
@endsection
