@extends('operator.layout.auth')

@section('title', 'Reportes de pagos')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('content')
    <div id="page-content">

     <!-- Page Header -->
      <div class="content-header">
          <div class="row">
              <form>
                  <div class="col-sm-12">
                      <div class="header-section">
                          <h1>Reportes de pagos</h1>
                      </div>
                      
                  </div>
              </form>
          </div>
      </div>
      <!-- END Page Header -->

      <!-- Example Block -->
      <div class="block">

          @if(count($reports)>0)
          <!-- Vertical Pricing Tables Content -->
          <table class="table table-hover table-vcenter table-borderless table-pricing table-pricing-nohover">
              <thead>
                  <tr class="">
                      <th style="width:25%;">Información</th>
                      <th class="hidden-xs" style="width:25%;">Detalles</th>
                      <th style="width:25%;"></th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($reports as $idReport => $reservations)
                    <tr>
                        <th class="themed-background-dark-default text-light">
                            <div class="h5 "><strong> # RPO-{{str_pad($idReport, 6, "0", STR_PAD_LEFT)}}</strong>
                              <br>{{$reservations[0]->reportReservation->Report->date_start}} - {{$reservations[0]->reportReservation->Report->date_finish}}
                              <br>
                             Rango de fechas
                            </div>
                        </th>
                        <td class="hidden-xs">
                            <div class="push-bit">
                                <strong>Cantidad de servicios pagados: </strong> {{count($reservations)}}
                            </div>
                            <div class="push-bit ">
                                <strong>Estado</strong> {{$reservations[0]->reportReservation->Report->state}}
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="{{url('operator/generatepayreport/'.Crypt::encrypt($idReport))}}" target="_blank" class="btn btn-effect-ripple btn-primary"><i class="fa fa-arrow-right"></i> Ver reporte de pago</a><br>
                            <small class="text-muted"><em>PDF De descarga</em></small>
                        </td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
          @else
          <p>No se han realizado pagos.</p>
          @endif
          <!-- END Vertical Pricing Tables Content -->
      </div>
  </div>

@endsection