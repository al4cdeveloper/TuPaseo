<!DOCTYPE html>
<html lang="en">
<head>
<title>Beneficios para operadores turísticos | Tu Paseo</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia" />
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
     </script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--// Meta tag Keywords -->
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon"/>

<!-- css files -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"> <!-- Bootstrap-Core-CSS -->
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"> <!-- Font-Awesome-Icons-CSS -->
<link rel="stylesheet" href="{{asset('css/chocolat.css')}}" type="text/css" media="screen"><!-- chocolate css for gallery light box-->
<!-- //css files -->
<link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('style.css')}}">
<link href="{{asset('css/color/white.css')}}" rel="stylesheet">

<!-- Custom Responsive stylesheet -->
<link href="{{asset('css/responsive.css')}}" rel="stylesheet">

<!-- //online-fonts -->
</head>
<body> 


    <section id="menu">
          
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">TuPaseo</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/tupaseo.png')}}" /></a>
            </div> <!-- end .navbar-header -->              
            
            <div class="">
              <ul class="nav navbar-nav navbar-left">
        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
        <li><a href="{{url('/')}}">Home</a></li>
        <li class="active"><a class="" href="{{url('operator')}}">¿Quieres que vean tu negocio?</a></li>
        <li><a href="{{url('contact')}}">Contactános</a></li>
              </ul>

      <div class="social-icons">
        <a class="facebook" href="https://www.facebook.com/tupaseo.co/" target="_blank"><i class="fa fa-facebook"></i></a>
        <a class="twitter" href="https://www.instagram.com/tupaseo/" target="_blank"><i class="fa fa-instagram"></i></a>
        <a class="linkedin" href="{{url('contact')}}"><i class="fa fa-envelope"></i></a>
      </div>
    </div>
    </section>
<!-- ====== Header Section ====== -->
<!-- ====== Header Section ====== -->
    <header id="top" style="background-image: url({{asset('images/bg5.jpg')}})">
      <div class="bg-color">
        <div class="top section-padding">
          <div class="container">
            <div class="row">

             

            </div> <!-- end .top> .container> .row -->
          </div> <!-- end .top> .container -->
        </div> <!-- end .top -->
      </div> <!-- end .bg-color -->
    </header>

<section >
      <div class="description">
        <div class="description-one section-padding">
          <div class="container" >
            <div class="row">
                <h3>Recuperar contraseña</h3>
                <div class="">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/operator/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo electrónico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar link de recuperación
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
           </div>
        </div>
    </div>
</section>

</body>
</html>