
@extends('layouts.frontheader')

@section('title','Beneficios para operadores turísticos | Tu Paseo')
 @section('additionalStyle')
 <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/tabs_home.css')}}" rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('mystyle.css')}}">

    <!-- SPECIFIC CSS -->
    <link rel="stylesheet" href="{{asset('front/css/weather.css')}}">
        <!-- Radio and check inputs -->
    <link href="{{asset('front/css/skins/square/grey.css')}}" rel="stylesheet">

    <!-- Range slider -->
    <link href="{{asset('front/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
    <style>
        .marker_info
        {
            height: 420px!important;
        }
        iframe.pattern{
            height: 450px !important;
            width: 100% !important;
            position: relative!important;
        }
        .modal-backdrop
        {
            display: none!important;
        }
    </style>
    
    <link href="{{asset('front/js/layerslider/css/layerslider.css')}}" rel="stylesheet">

    @endsection
@section('main')   
    <!-- Slider -->
<!-- Slider -->
    <main>
        <div id="full-slider-wrapper">
            <div id="layerslider" style="width:100%;height:600px;">
                <!-- first slide -->
                <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;">
                    <img src="{{asset('front/img/slides/slide_1.jpg')}}" class="ls-bg" alt="Slide background">
                    <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Conéctate</h3>
                    <p class="ls-l slide_typo_2" style="top:52%; left:50%; font-size: 30px" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">con los amantes de la aventura</p>
                    <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='#'>Crear Perfil</a>
                </div>

                <!-- second slide -->
                <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
                    <img src="{{asset('front/img/slides/slide_2.jpg')}}" class="ls-bg" alt="Slide background">
                    <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Únete a una gran comunidad</h3>
                    <p class="ls-l slide_typo_2" style="top:52%; left:50%; font-size: 30px" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Que está en continuo crecimiento</p>
                    <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='#'>Crear Perfil</a>
                </div>

                <!-- third slide -->
                <div class="ls-slide" data-ls="slidedelay:5000; transition2d:103;">
                    <img src="{{asset('front/img/slides/slide_3.jpg')}}" class="ls-bg" alt="Slide background">
                    <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">TuPaseo.Travel</h3>
                    <p class="ls-l slide_typo_2" style="top:52%; left:50%; font-size: 30px" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Una plataforma para descubrir Colombia</p>
                    <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='tours.html'>Ver Ofertas</a>
                </div>

            </div>
        </div> <!-- End Slider -->

        <!-- Position -->
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>Registro operador</li>
                </ul>
            </div>
        </div> <!-- End Position -->

        <!-- Description -->
        <section class="margin_60">
            <div class="container">

                <div class="col-sm-5 col-md-6">
                    <div class="col-md-12">
                        <h3 align="center"><b>¿Eres operador turístico y quieres obtener más clientes mientras estás en operación?</b></h3>
                        <p class="text-w3l-services" align="center">Con nuestro sistema de reservas y pagos podrás organizar<br>
                        tu disponibilidad y capacidad de servicio para mantener la continuidad de su negocio.</p>
                        <h3 align="center"><b>Características del asistente de TuPaseo</b></h3>
                        <br>
                        <h4>Disponibilidad de servicio</h4>
                            <p>Indicar el tiempo de servicio del operador.</p>
                        <h4>Últimas ofertas</h4>
                            <p>Ofertas de acuerdo a temporadas.</p>
                        <h4>Accesibilidad</h4>
                            <p>Accesibilidad y usabilidad de la plataforma para mejorar experiencia de usuario.</p>
                        <h4>Reportes</h4>
                            <p>Reportes de reservas e ingresos.</p>
                        <h4>Notificaciones</h4>
                            <p>Notificaciones de reservas y pagos vía correo electrónico.</p>
                        <h4>Capacidad de servicio</h4>
                            <p>Indicar la cantidad mínima y máxina de turistas que puede atender.</p>
                    </div>
                </div>

                <div class="col-sm-7 col-md-6" id="enlaceRegistro" name="enlaceRegistro">
                    <div class="content">
                        <h2 align="center"><b>CREA TU PERFIL</b></h2>
                        <span>¿Quieres que manejemos tus reservas y pagos mientras te encuentras en operación?</span><br><br></br>
                    <form class="form contact" method="post" action="{{url('operator/register')}}">

                        {{csrf_field()}}
                            <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="text" class="sr-only">Nombre</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre o Razón social" required>
                                    <span class="label label-danger">{{$errors->first('name') }}</span>

                                        </div>
                        
                            <div class="form-group col-md-6 {{ $errors->has('national_register') ? ' has-error' : '' }}">
                                            <label for="text" class="sr-only">Registro nacional de turismo</label>
                                            <input type="text" class="form-control" id="national_register" name="national_register" placeholder="Registro Nacional de Turismo" required>
                                    <span class="label label-danger">{{$errors->first('national_register') }}</span>

                                        </div>
                                                
                            <div class="form-group col-md-12 {{ $errors->has('contact_personal') ? ' has-error' : '' }}">
                                            <label for="text" class="sr-only">Persona de contacto</label>
                                            <input type="text" class="form-control" id="contact_personal" name="contact_personal" placeholder="Persona de contacto" required>
                                    <span class="label label-danger">{{$errors->first('contact_personal') }}</span>

                                        </div>

                            <div class="form-group col-md-4 {{ $errors->has('contact_phone') ? ' has-error' : '' }}">
                                            <label for="text" class="sr-only">Teléfono de contacto</label>
                                            <input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Teléfono de contacto" required>
                                    <span class="label label-danger">{{$errors->first('contact_phone') }}</span>
                                        </div>

                            <div class="form-group col-md-8 {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="text" class="sr-only">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
                                    <span class="label label-danger">{{$errors->first('email') }}</span>

                                        </div>
                            
                            <div class="form-group col-md-12 {{ $errors->has('web') ? ' has-error' : '' }}">
                                <label for="text" class="sr-only">Página web</label>
                                <input type="text" class="form-control" id="web" name="web" placeholder="Página web" required>
                          <span class="label label-danger">{{$errors->first('web') }}</span>
                            </div>          
                            <div class="form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                          <span class="label label-danger">{{$errors->first('password') }}</span>

                            </div>
                            <div class="form-group col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password_confirmation" class="sr-only">Confirmar Password</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirmar password" required>
                            </div>
                          <div class="form-group col-md-12">
                              <label class="col-md-8 "> <input type="checkbox" id="val-terms" name="val-terms" value="1" required><a href="#modal-terms" style="color: #6c9e2e" data-toggle="modal">Acepto terminos y condiciones</a> <span class="text-danger">*</span></label>
                              <br>
                              <label class="col-md-8 "> <input type="checkbox" name="data-protection" value="1" required><a href="{{url('politica_proteccion_datos')}}" target="_blank" style="color: #6c9e2e">Acepto politica de protección de datos</a> <span class="text-danger">*</span></label>
                               <br>
                              <label class="col-md-8 "> <input type="checkbox" name="sustainability" value="1" required><a href="{{url('politica_sostenibilidad')}}" target="_blank" style="color: #6c9e2e">Acepto politica de sostenibilidad</a> <span class="text-danger">*</span></label>
                              <br>
                              <label class="col-md-8 "style="color: #6c9e2e"> <input type="checkbox" name="promotionals_mails" value="1" >Acepto recibir información promocional </label>
                          </div>

                            <div class="col-sm-6">
                                <button class="btn_1 green contact-submit custom-btn" type="submit"><i class="icon-right-hand"></i> Crear perfil</button></div>
                            <br>
                            <br>
                            <div class="col-lg-offset-8">¿Ya tienes una cuenta?</div>
                            <div class="col-lg-offset-9">
                                <a style="color: #6c9e2e" href="{{url('/login')}}">Iniciar sesión</a>
                            </div> <!-- end .button -->
                        </form>

                    </div>
                </div>

            </div> <!-- End row -->
        </section>
            <!-- Modal Review -->
    <div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title text-center"><strong>Terminos y Condiciones</strong></h3>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="page-header">1. <strong>General</strong></h4>
                                            <p>TUPASEO.TRAVEL es un dominio de Internet y marca registrada por TU PASEO SAS, sociedad legalmente constituida bajo las leyes colombianas. Por medio del presente documento se mencionan los términos y condiciones generales que el EMPRESARIO debe tener en cuenta y que son aplicados por TU PASEO SAS en la página web TUPASEO.TRAVEL.</p>
                                            <br>
                                            <p>En el sitio web, TUPASEO.TRAVEL, los productos comercializados, los servicios publicitados por terceros, las páginas a las que se pueda acceder a través del mismo, su uso y/o la descarga de información contenida en él, se encuentran condicionado(a)s a la aceptación completa de los términos y condiciones que se expresan a continuación.</p>
                                            <br>
                                            <h4 class="page-header">2. <strong>Aplicación</strong></h4>
                                            <p>Estos Términos de uso se aplican a TU PASEO SAS y el EMPRESARIO que publica ofertas de productos y/o servicios en la página web, versión móvil o táctil. No se aplica a particulares que adquieren ofertas a través de EL SITIO cuyo acceso, uso, compra o relación está regida por los Términos y Condiciones y la Política de Privacidad asociada a los usuarios.</p>
                                            <h4 class="page-header">3. <strong>Código de conducta en nuestra página web</strong></h4>
                                            <p>La página web y otras páginas web propiedad de TU PASEO SAS que están disponibles para los usuarios finales son propiedad privada (en adelante, denominadas colectivamente las “páginas web de TU PASEO SAS”). Todas las interacciones en las páginas web de TU PASEO SAS deben ser legales y cumplir con estos Términos de uso. En la medida en que tu conducta (juzgada por nosotros y a nuestra entera discreción) restrinja o inhiba a otros usuarios del uso o disfrute de cualquier parte de las páginas web de TU PASEO SAS, podríamos limitar tus privilegios en la página web y buscar otras soluciones.</p>
                                            <h4 class="page-header">4. <strong>Servicios</strong></h4>
                                            <p>Salvo que se establezca expresamente lo contrario, los servicios son prestados por el EMPRESARIO que publica las ofertas de bienes y/o servicios a través de TUPASEO.TRAVEL.</p>
                                            <br>
                                            <p>El cumplimiento de las obligaciones asociadas a las ofertas de prestaciones de servicios, son siempre a cargo del EMPRESARIO, quien estará debidamente identificado en la respectiva publicación. TUPASEO.TRAVEL no es responsable por el cumplimiento de tales obligaciones ni por el contenido de las ofertas, así como el EMPRESARIO es el único responsable por los posibles daños o perjuicios que se cause al usuario dentro del ámbito de la prestación del servicio.</p>
                                            <br>
                                            <h4 class="page-header">5. <strong>Ofertas</strong></h4>
                                            <p>No hay un coste por adelantado al publicar ofertas de productos y/o servicios en TUPASEO.TRAVEL. En su lugar, TU PASEO SAS cobra una tarifa por publicitar la oferta, sobre el producto y/o servicio vendido y sobre el precio neto de la oferta. La tarifa queda consignada en el CONTRATO DE SERVICIOS que firma el EMPRESARIO con TU PASEO SAS.</p>
                                            <h4 class="page-header">6. <strong>Modificación de los términos de uso</strong></h4>
                                            <p>Nos reservamos el derecho, en cualquier momento, de suspender o modificar cualquier parte de estos Términos de uso, según lo consideremos necesario o deseable. Si realizamos cambios que puedan afectar materialmente al uso de nuestra página web o de nuestros servicios, te informaremos del cambio publicando un aviso en la página web. Cualquier modificación de estos Términos de uso será efectivo a partir de la publicación del aviso de los cambios en nuestra página web.</p>
                                            <br>
                                            <h4 class="page-header"><strong>JURISDICCIÓN Y LEY APLICABLE</strong></h4>
                                            <p>Este acuerdo estará regido por las leyes de la República de Colombia.
                                                Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a las leyes aplicables y a los Tribunales competentes para su conocimiento.</p>
                                            <br>
                                            <p>Estimado Prestador de Servicios Turísticos, puedes consultar en la Cuenta/Perfil el documento completo de estos <a href="{{url('terminos_operador')}}" target="_blank">Términos y Condiciones</a>.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="text-center">
                                                <button type="button" class="btn btn-effect-ripple btn-primary" data-dismiss="modal" style="overflow: hidden; position: relative;">¡Los he leído!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    </div>
    <!-- End modal review -->
        <!-- Apps.Co -->
        <div class="subscribe section-padding">
          <div class="container">
            <div class="col-md-12" align="center">
              <img src="{{asset('front/img/footerapps.png')}}" class="img-responsive">
            </div>
            <div class="col-md-12" align="center">
              <img src="{{asset('front/img/footerpcis.png')}}" class="img-responsive">
            </div>
          </div>
        </div>
        <hr>

        <!-- Video -->
        <section class="promo_full">
            <div class="promo_full_wp magnific">
                <div>
                    <iframe src="https://www.youtube.com/embed/8emAHIcaxLo?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </section> <!-- End video -->
        <hr>
        <hr>
    </main>
  @endsection
     
  @section('additionalScript')
        <!-- Specific scripts -->
    <script src="{{asset('front/js/layerslider/js/greensock.js')}}"></script>
    <script src="{{asset('front/js/layerslider/js/layerslider.transitions.js')}}"></script>
    <script src="{{asset('front/js/layerslider/js/layerslider.kreaturamedia.jquery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            'use strict';
            $('#layerslider').layerSlider({
                autoStart: true,
                responsive: true,
                responsiveUnder: 1280,
                layersContainer: 1170,
                skinsPath: 'js/layerslider/skins/'
                    // Please make sure that you didn't forget to add a comma to the line endings
                    // except the last line!
            });
        });
    </script>

@endsection
