@extends('operator.layout.auth')

@section('title', 'Reservaciones')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="header-section">
                            <h1>Reservaciones</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            <form action="{{url('/operator/reservas')}}" style="margin:3%;" method="get">
                    <div class="row" >
                        
                        <div class="col-md-3 col-md-offset-4">
                            <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-chosen">Estado</label>
                                    <div class="col-md-9">
                                        <select name="state" id="state" class="select-chosen" data-placeholder="Seleccione estado">
                                            <option value=""></option>
                                            <option value="por_realizar">Por realizar</option>
                                            <option value="historial">Historial</option>
                                        </select>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success" type="submit" value="search" name="button">Buscar</button>
                        </div>
                    </div>
                </form>
                <hr>
                <!--<div class="col-sm-12">
            <pre style="background: white;color: black">
                @{{$data}} 
            </pre>
        </div>-->
            <table id="example2-datatable" class="table table-striped table-bordered table-vcenter" role="grid" aria-describedby="datatable_info">
                <thead>
                    <th>Código </th>
                    <th>Servicio</th>
                    <th>Actividad</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Estado</th>
                    <th width="70px"></th>
                </thead>
                <tbody>
                    @foreach($reservations as $reservation)
                    <tr @if($reservation->state == "Realizada") style="background: #04ff5661;" @endif>
                        <td># RSVT-{{str_pad($reservation->id_reservation, 6, "0", STR_PAD_LEFT)}}</td>
                        <td>{{$reservation->Service->service_name}}</td>
                        <td>{{$reservation->Service->PrincipalService->service}}</td>
                        <td>{{$reservation->date}}</td>
                        <td>{{$reservation->time}}</td>
                        <td>@if($reservation->state=="pagado") Por realizar @else {{$reservation->state}} @endif</td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#details{{$reservation->id_reservation}}" ><i class="fa fa-eye"></i></button>
                            @if(count($reservation->Reprograms)>0)
                            <button class="btn btn-default btn-sm" data-toggle="tooltip" title="Historial de repogramaciones" onclick="reprogramModal({{$reservation->id_reservation}})"><i class="fa fa-history"></i></button> 
                            @endif
                        @if($reservation->state=="pagado" && $reservation->date <=  date("Y-m-d"))
                            <a href="{{url('operator/servicesuccess/'.$reservation->id_reservation)}}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Marcar como realizado" ><i class="fa fa-check"></i></a> 
                        @elseif($reservation->state=="pagado")
                            <button class="btn btn-danger btn-sm" data-toggle="tooltip" title="Reprogramar" v-on:click="configModal({{$reservation->Service->id_service_operator}},{{$reservation->cuantity}},'{{$reservation->Service->slug}}',{{$reservation->id_reservation}})"><i class="fa fa-clock-o"></i> </button> 
                        @endif
                        </td>
    

                        <!---Historial de reprogramaciones -->
                        @if(count($reservation->Reprograms)>0)
                        <div id="reprogramModal{{$reservation->id_reservation}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h3 class="modal-title"><strong>Reprogramaciones de reservación</strong></h3>
                                        </div>
                                        <div class="modal-body">
                                            @foreach($reservation->Reprograms as $reprogram)
                                            <div class="row">
                                                <h4><strong>Razones de repogramación:</strong> {{$reprogram->reason}}</h4>
                                                <div class="col-md-6">
                                                    <h5>Fecha anterior</h5>
                                                    <p> {{$reprogram->previous_date}} a la(s) {{$reprogram->previous_time}}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5>Fecha de reprogramación</h5>
                                                    <p>{{$reprogram->new_date}} a la(s) {{$reprogram->new_time}}</p>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-effect-ripple btn-success" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div id="details{{$reservation->id_reservation}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header-tabs">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <i class="fa fa-shopping-basket"></i>Información de compra # RSVT-{{str_pad($reservation->id_reservation, 6, "0", STR_PAD_LEFT)}}</a></li>
                                    </div>
                                    <div class="modal-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="modal-tabs-home">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3>{{$reservation->Service->service_name}} <br>
                                                        <!--<small>Estado: {{$reservation->state}}</small>-->
                                                        </h3>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <strong>Fecha reserva </strong>
                                                                    <br>
                                                                    {{$reservation->date}} - {{$reservation->time}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Cantidad</strong>
                                                                    <br>{{$reservation->cuantity}}
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                              <strong>Actividad a realizar</strong>
                                                              <br>{{$reservation->Service->PrincipalService->service}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Lugar</strong>
                                                              <br>{{$reservation->Service->Municipality->municipality_name}}
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" align="center">
                                                        La información detallada del servicio puede encontrarla <a href="{{url('/home/service/'.$reservation->Service->slug)}}">aquí</a>.
                                                    </div>
                                                </div>
                                            </div>  
                                            <hr>
                                            <div class="row">
                                                <h4><i class="fa fa-info"></i> Información de pagador</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                        <strong>Email: </strong>{{$reservation->orderProduct->order->shipping_order->email}} <br>
                                                        <strong>Identificación: </strong>{{$reservation->orderProduct->order->shipping_order->type_dni}} - {{$reservation->orderProduct->order->shipping_order->dni}} <br>
                                                        <strong>Nombres: </strong>{{$reservation->orderProduct->order->shipping_order->first_name}} {{$reservation->orderProduct->order->shipping_order->last_name}}  <br>
                                                        <strong>Teléfono: </strong>{{$reservation->orderProduct->order->shipping_order->phone}} <br>
                                                  </div>
                                                  <div class="col-md-6">
                                                        <strong>Direccion: </strong>{{$reservation->orderProduct->order->shipping_order->direccion}} <br>
                                                        <strong>País: </strong>{{$reservation->orderProduct->order->shipping_order->country}} <br>
                                                        <strong>Dpto/Provincia: </strong>{{$reservation->orderProduct->order->shipping_order->region}} <br>
                                                        <strong>Ciudad: </strong>{{$reservation->orderProduct->order->shipping_order->city}} <br>
                                                  </div>
                                            </div>
                                            <br>
                                            <br>
                                            @if($reservation->state == 'Calificada')
                                            <div class="row">
                                                <h4>Calificación: {{$reservation->calification}}</h4>
                                                <h4>Comentario</h4>
                                                <p>{{$reservation->comment}}</p>
                                            </div>
                                            @endif
                                            <div class="row">
                                            <div class="col-md-12">
                                                        ¿Deseas una copia de la información en tu correo? <a href="javascript:void(0);" onclick="SendCopy({{$reservation->id_reservation}})">Click aquí</a>
                                                      </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- Regular Fade -->
                                    <div id="modal-reprogram" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                {{Form::open(['url'=>'operator/reprogram','metod'=>'post'])}}
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title"><strong>Reprogramar Reservación</strong></h3>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="hidden" id="numPersons">
                                                    <input type="hidden" id="id_reservation" name="id_reservation">
                                                    <div class="row">
                                                            <label for="service" class="col-md-4 control-label">Fecha de reprogramación</label>
                                                            <div class="col-md-6">

                                                                <input class="date-pick form-control" type="text" v-model="fillReservation.date" v-on:blur="validateDate" id="dateService" name="date">
                                                            </div>
                                                    </div>
                                                    <div class="form-group " v-if="countHours>0">
                                                        <div class="row">
                                                            <label for="hour" class="col-md-4 control-label">Hora</label>
                                                            <div class="col-md-6">
                                                                <select id="hour" class="form-control" name="time" required v-model="fillReservation.time">
                                                                    <option v-for="hour in hours">
                                                                        @{{hour}}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label for="hour" class="col-md-4 control-label">Razón de reprogramación</label>
                                                            <div class="col-md-6">
                                                                <textarea id="example-textarea-input" name="reason" rows="7" class="form-control" placeholder="Descripción.."></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div v-else-if="countHours==-1" align="center">
                                                        No se encuentran horarios disponibles para este día, por favor seleccione otro
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                                                    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cancelar</button>
                                                </div>
                                                {{Form::close()}}
                                            </div>
                                        </div>
                                    </div>
    </div>

@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script src="{{asset('js/reprogram-vue.js')}}"></script>
<script src="https://unpkg.com/sweetalert2@7.1.1/dist/sweetalert2.all.js"></script>

        <script> /* Initialize Datatables */
            App.datatables();
            /* Initialize Datatables */
            $('#example2-datatable').dataTable({
                order: [[ 0, "desc" ]],
            });
            
            function SendCopy(cod)
            {

                token = '{{csrf_token()}}';

                $.ajax({
                  url: '/api/resenreservation',
                  type:'POST',
                  data:{reservation:cod},
                  headers:{'X-CSRF-TOKEN':token},
                  complete:function (transport)
                  {
                    response = transport.responseText;
                    if(response != "fail")
                      {
                        swal('Listo!','hemos enviado la copia de la reservación a :'+ response,'success');
                      }
                    else
                    {
                      swal('Error','Solo se puede reenviar copia de reservación cada 20 minutos, para más información comunicate con soporte.','error');
                    }
                  }
                });
            }

            function reprogramModal(id)
            {
                $("#reprogramModal"+id).modal();
            }
        </script>

@endsection