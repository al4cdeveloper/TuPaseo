@extends('operator.layout.auth')

@section('title', 'Servicios')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="header-section">
                            <h1>Servicios</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            <div class="row">
                <form method="post" action="{{url('operator/updateshedule')}}">
                    {{csrf_field()}}
                    <h4>Horarios laborales</h4>
                    <p>Por favor ingrese los horarios laborales en cada uno de los días.</p>
                    @foreach($horario as $days)
                        <div class="form-horizontal">
                            <h4>Día {{$days->day}}</h4>
                            <div class="form-group ">
                                <label for="address" class="col-md-4 control-label">Hora de inicio</label>
                                <div class="col-md-6">
                                    <input id="hora_inicio" type="text" class="form-control inicio" name="service[{{$days->id_schedule}}][hora_inicio]" value="{{$days->hora_inicio}}" required readonly="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="address" class="col-md-4 control-label">Hora de receso</label>
                                <div class="col-md-6">
                                    <input id="hora_receso" type="text" class="form-control receso" name="service[{{$days->id_schedule}}][hora_receso]" value="{{$days->hora_receso}}" required readonly="">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="address" class="col-md-4 control-label">Tiempo de receso en horas <small>Si no tiene tiempo de receso por favor ingresar 0</small></label>
                                <div class="col-md-6">
                                    <input id="tiempo_receso" type="number" class="form-control" name="service[{{$days->id_schedule}}][tiempo_receso]" value="{{$days->tiempo_receso}}" required placeholder="Tiempo de receso" max="6">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="address" class="col-md-4 control-label">Hora de finalización</label>
                                <div class="col-md-6">
                                    <input id="hora_final" type="text" class="form-control final" name="service[{{$days->id_schedule}}][hora_final]" value="{{$days->hora_final}}" required placeholder="Hora de finalización" readonly="">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div align="center">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <a href="{{url('operator/services')}}" class="btn btn-danger">Cancelar</a>
                        </div>
                </form>
            </div>
        </div>
            <!-- END Example Block -->
        
{{--         <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <ul class="breadcrumb breadcrumb-top">
                                <li>Category</li>
                                <li><a href="">Page</a></li>
                            </ul>
                        </div>
                    </div>
        </div> --}}


@endsection
@section('aditionalScript')
{{-- http://timepicker.co/ --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <script type="text/javascript">

            $('.inicio').timepicker({
                timeFormat: 'HH:mm ',
                interval: 30,
                minTime: '05:00am',
                maxTime: '10:00pm',
                defaultTime: '8',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
            $('.receso').timepicker({
                timeFormat: 'HH:mm ',
                interval: 30,
                minTime: '05:00am',
                maxTime: '10:00pm',
                defaultTime: '12',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
            $('.final').timepicker({
                timeFormat: 'HH:mm ',
                interval: 30,
                minTime: '05:00am',
                maxTime: '10:00pm',
                defaultTime: '18',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        </script>
        {{-- <script src="{{asset('auth-panel/js/plugins/moment/min/moment.min.js')}}"></script>
        {{-- <script src="{{asset('auth-panel/js/pages/compCalendar.js')}}"></script> --}}
        {{-- <script src="{{asset('vue/js/Service.js')}}"></script> --}}

        {{-- <script type="text/javascript"> --}}
{{-- 
            var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
            $(document).ready(function() {

                // page is now ready, initialize the calendar...

                $('#calendar').fullCalendar({
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles',
                 'Jueves', 'Viernes', 'Sábado'],
                 dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                  buttonText:
                  {
                      prev:     ' ◄ ',
                      next:     ' ► ',
                      prevYear: ' &lt;&lt; ',
                      nextYear: ' &gt;&gt; ',
                      today:    'hoy',
                      month:    'mes',
                      week:     'semana',
                      day:      'día'
                  },
                 header: {
                     left: 'prev,next today',
                     center: 'title',
                     right: 'month,basicWeek,basicDay'
                 },
                 defaultView: 'agendaWeek',
                 editable:true, --}}
   {{--  //              dayClick: function(date, jsEvent, view) {

    //     alert('Clicked on: ' + date.format());

    //     alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

    //     alert('Current view: ' + view.name);

    //     // change the day's background color just for fun
    //     $(this).css('background-color', 'red');

    // },

            //         selectable : true,
            //       selectHelper: true,
            //       select: function(start, end, allDay) {
            //         $('#fc_create').click();

            //         started = start;
            //         ended = end;

            //         $(".antosubmit").on("click", function() {
            //           var title = $("#title").val();
            //           if (end) {
            //             ended = end;
            //           }

            //           categoryClass = $("#event_type").val();

            //           if (title) {
            //             calendar.fullCalendar('renderEvent', {
            //                 title: title,
            //                 start: started,
            //                 end: end,
            //                 allDay: allDay
            //               },
            //               true // make the event "stick"
            //             );
            //           }

            //           $('#title').val('');

            //           calendar.fullCalendar('unselect');

            //           $('.antoclose').click();

            //           return false;
            //         });
            //       },

            //     events: [

            //         {
            //             title: 'Long Event',
            //             start: new Date(y, m, d-5),
            //             end: new Date(y, m, d-2)
            //         },
            //         {
            //             title: 'Meeting',
            //             start: new Date(y, m, d, 10, 30),
            //             start: new Date(y, m, d, 10, 30),
            //       },

            //         {
            //             title: 'Birthday Party',
            //             start: new Date(y, m, d+1, 19, 0),
            //             end: new Date(y, m, d+1, 22, 30),
            //             allDay: false
            //         },

            //     ]
            //     })
            // }); --}}
@endsection