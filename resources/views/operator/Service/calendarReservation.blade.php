@extends('operator.layout.auth')

@section('title', 'Servicios')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="header-section">
                            <h1>Servicios</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            <div class="row">
                <div id="calendar"></div>
              
            </div>
        </div>

                    @foreach($reservations as $reservation)

        <div id="details{{$reservation->id_reservation}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header-tabs">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <i class="fa fa-shopping-basket"></i>Información de compra # RSVT-{{str_pad($reservation->id_reservation, 6, "0", STR_PAD_LEFT)}}</a></li>
                                    </div>
                                    <div class="modal-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="modal-tabs-home">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3>{{$reservation->Service->service_name}} <br>
                                                      <!-- <small>Estado: {{$reservation->state}}</small>-->
                                                        </h3>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <strong>Fecha reserva </strong>
                                                                    <br>
                                                                    {{$reservation->date}} - {{$reservation->time}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Cantidad</strong>
                                                                    <br>{{$reservation->cuantity}}
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                              <strong>Actividad a realizar</strong>
                                                              <br>{{$reservation->Service->PrincipalService->service}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Lugar</strong>
                                                              <br>{{$reservation->Service->Municipality->municipality_name}}
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" align="center">
                                                        La información detallada del servicio puede encontrarla <a href="{{url('/home/service/'.$reservation->Service->slug)}}">aquí</a>.
                                                    </div>
                                                </div>
                                            </div>  
                                            <hr>
                                            <div class="row">
                                                <h4><i class="fa fa-info"></i> Información de pagador</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                        <strong>Email: </strong>{{$reservation->orderProduct->order->shipping_order->email}} <br>
                                                        <strong>Identificación: </strong>{{$reservation->orderProduct->order->shipping_order->type_dni}} - {{$reservation->orderProduct->order->shipping_order->dni}} <br>
                                                        <strong>Nombres: </strong>{{$reservation->orderProduct->order->shipping_order->first_name}} {{$reservation->orderProduct->order->shipping_order->last_name}}  <br>
                                                        <strong>Teléfono: </strong>{{$reservation->orderProduct->order->shipping_order->phone}} <br>
                                                  </div>
                                                  <div class="col-md-6">
                                                        <strong>Direccion: </strong>{{$reservation->orderProduct->order->shipping_order->direccion}} <br>
                                                        <strong>País: </strong>{{$reservation->orderProduct->order->shipping_order->country}} <br>
                                                        <strong>Dpto/Provincia: </strong>{{$reservation->orderProduct->order->shipping_order->region}} <br>
                                                        <strong>Ciudad: </strong>{{$reservation->orderProduct->order->shipping_order->city}} <br>
                                                  </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="row">
                                            <div class="col-md-12">
                                                        ¿Deseas una copia de la información en tu correo? <a href="javascript:void(0);" onclick="SendCopy({{$reservation->id_reservation}})">Click aquí</a>
                                                      </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
            <!-- END Example Block -->
        
{{--         <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <ul class="breadcrumb breadcrumb-top">
                                <li>Category</li>
                                <li><a href="">Page</a></li>
                            </ul>
                        </div>
                    </div>
        </div> --}}


@endsection
@section('aditionalScript')
    <script src="{{asset('auth-panel/js/plugins/moment/min/moment.min.js')}}"></script>
       <script src="{{asset('auth-panel/js/pages/compCalendar.js')}}"></script>
<script src="https://unpkg.com/sweetalert2@7.1.1/dist/sweetalert2.all.js"></script>

<script type="text/javascript">
       $(document).ready(function() {

                // page is now ready, initialize the calendar...

                $('#calendar').fullCalendar({
                  locale: 'es',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles',
                 'Jueves', 'Viernes', 'Sábado'],
                 dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                  buttonText:
                  {
                      prev:     ' ◄ ',
                      next:     ' ► ',
                      prevYear: ' &lt;&lt; ',
                      nextYear: ' &gt;&gt; ',
                      today:    'hoy',
                      month:    'mes',
                      week:     'semana',
                      day:      'día'
                  },
                 header: {
                     left: 'prev,next today',
                     center: 'title',
                     right: 'month,basicWeek,basicDay'
                 },
                  eventLimit: true, // for all non-agenda views
                  views: {
                    agenda: {
                      eventLimit: 6 // adjust to 6 only for agendaWeek/agendaDay
                    }
                  },
                  eventClick: function(calEvent, jsEvent, view) {

                    $("#details"+calEvent.id).modal()

                  },
                  events: [
                    @foreach($reservations as $reservation)
                  {
                    id: "{{$reservation->id_reservation}}",
                  title: "{{$reservation->Service->PrincipalService->service}}",
                  start: '{{$reservation->date}}T{{$reservation->time}}-05:00',
                  @if($reservation->state =="Realizada")
                  backgroundColor: "green",
                  editable: false,
                  @elseif($reservation->state =="pendiente")
                  backgroundColor: "gray",
                  editable: false,
                  @endif
                  },@endforeach
                   ]
                 })
             });

       function SendCopy(cod)
            {

                token = '{{csrf_token()}}';

                $.ajax({
                  url: '/api/resenreservation',
                  type:'POST',
                  data:{reservation:cod},
                  headers:{'X-CSRF-TOKEN':token},
                  complete:function (transport)
                  {
                    response = transport.responseText;
                    if(response != "fail")
                      {
                        swal('Listo!','hemos enviado la copia de la reservación a :'+ response,'success');
                      }
                    else
                    {
                      swal('Error','Solo se puede reenviar copia de reservación cada 20 minutos, para más información comunicate con soporte.','error');
                    }
                  }
                });
            }
            </script>
@endsection