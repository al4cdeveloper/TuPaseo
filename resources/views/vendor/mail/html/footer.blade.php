<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content-cell" align="center">
                	<div class="col-md-12 col-sm-12">
                        <div class="text-center">
                            <img src="http://tupaseo.travel/images/mail_logo.png" alt="signature" class="height-100">
                            <h6>Equipo</h6>
                            <p class="text-muted">{{ Illuminate\Mail\Markdown::parse($slot) }}</p>
                        </div>
                    </div> <!-- End Total -->
                    
                </td>
            </tr>
        </table>
    </td>
</tr>
