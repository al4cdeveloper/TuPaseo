 <!-- Top Menú -->
        <div id="top_line">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6"><i class="icon-phone"></i><strong>+57 (1) 3121540 | +57 316 5287931</strong></div>
                    
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <ul id="top_links">
                            <li>
                                <div class="dropdown dropdown-access">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">Ingresar | Registrarse</a>
                                    <div class="dropdown-menu">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_facebook">
                                                <i class="icon-facebook"></i>Facebook </a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_paypal">
                                                <i class="icon-google"></i>Google </a>
                                            </div>
                                        </div>
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">o</span>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="inputUsernameEmail" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                        </div>
                                        <a id="forgot_pw" href="#">Recordar contraseña</a>
                                            <input type="submit" name="Sign_in" value="Iniciar sesión" id="Sign_in" class="button_drop">
                                            <input type="submit" name="Sign_up" value="Registrarse" id="Sign_up" class="button_drop outline">
                                    </div>
                                </div><!-- End Dropdown access -->
                            </li>
                            <li id="lang_top"><i class="icon-globe-1"></i> <a href="#0">IDIOMA</a></a>
                            </li>
                            <li id="social_top"><a href="#0"><i class="icon-facebook"></i></a> <a href="#0"><i class="icon-instagram"></i></a> <a href="#0"><i class="icon-mail"></i></a>
                            </li>
                        </ul>
                    </div>
                </div><!-- End row -->
            </div><!-- End container-->
        </div><!-- End Top Menú -->
        
        <!-- Menú -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo_home">
                    	<h1><a href="index_20.html" title="TuPaseo">TuPaseo.Travel</a></h1>
                    </div>
                </div>
                <nav class="col-md-9 col-sm-9 col-xs-9">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="{{asset('front/img/logo.png')}}" alt="Tu Paseo" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                        <ul>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Home </a>
                            </li>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Planes </a>
                            </li>
                             <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Destinos <i class="icon-down-open-mini"></i></a>
                                <ul>
                                    <li><a href="#">Amazonia</a></li>
                                    <li><a href="#">Antioquia y Eje Cafetero</a></li>
                                    <li><a href="#">Centro</a></li>
                                    <li><a href="#">Costa Caribe</a></li>
                                    <li><a href="#">Costa Pacífica</a></li>
                                    <li><a href="#">Llanos Orientales</a></li>
                                    <li><a href="#">Suroccidente</a></li>
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Actividades <i class="icon-down-open-mini"></i></a>
                                <ul>
                                    <li><a href="#">En el Aire</a></li>
                                    <li><a href="#">En el Agua</a></li>
                                    <li><a href="#">En Montaña</a></li>
                                </ul>
                            </li>
                              <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Directorio de Servicios <i class="icon-down-open-mini"></i></a>
                                <ul>
                                    <li><a href="#">Hoteles</a></li>
                                    <li><a href="#">Restaurantes</a></li>
                                    <li><a href="#">Transporte</a></li>
                                    <li><a href="#">Otros</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- End main-menu -->
                    <ul id="top_tools">
                        <li>
                            <div class="dropdown dropdown-cart">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class=" icon-basket-1"></i>Reservas (0) </a>
                                <ul class="dropdown-menu" id="cart_items">
                                    <li>
                                        <div class="image"><img src="{{asset('front/img/cart/thumb_cart_1.jpg')}}" alt="image"></div>
                                        <strong>
										<a href="#">Louvre museum</a>1x $36.00 </strong>
                                        <a href="#" class="action"><i class="icon-trash"></i></a>
                                    </li>
                                    <li>
                                        <div class="image"><img src="{{asset('front/img/cart/thumb_cart_2.jpg')}}" alt="image"></div>
                                        <strong>
										<a href="#">Versailles tour</a>2x $36.00 </strong>
                                        <a href="#" class="action"><i class="icon-trash"></i></a>
                                    </li>
                                    <li>
                                        <div class="image"><img src="{{asset('front/img/cart/thumb_cart_3.jpg')}}" alt="image"></div>
                                        <strong>
										<a href="#">Versailles tour</a>1x $36.00 </strong>
                                        <a href="#" class="action"><i class="icon-trash"></i></a>
                                    </li>
                                    <li>
                                        <div>Total: <span>$120.00</span></div>
                                        <a href="cart.html" class="button_drop">Go to cart</a>
                                        <a href="payment.html" class="button_drop outline">Check out</a>
                                    </li>
                                </ul>
                            </div><!-- End dropdown-cart-->
                        </li>
                    </ul>
                </nav>
            </div>
        </div> <!-- End Menú -->