<div id="sidebar-scroll" style="margin-top: 15%">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Sidebar Navigation -->
        <ul class="sidebar-nav">
            <li>
                <a href="{{url('home')}}"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Mis reservaciones</span></a>
            </li>
        </ul>
        <!-- END Sidebar Navigation -->
    </div>
    <!-- END Sidebar Content -->
</div>