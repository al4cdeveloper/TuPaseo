@extends('layouts.frontheader')

@section('title','Beneficios para operadores turísticos | Tu Paseo')
 @section('additionalStyle')
    <link href="{{asset('front/css/date_time_picker.css')}}" rel="stylesheet">
    
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/tabs_home.css')}}" rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('mystyle.css')}}">

    <!-- SPECIFIC CSS -->
    <link rel="stylesheet" href="{{asset('front/css/weather.css')}}">
        <!-- Radio and check inputs -->
    <link href="{{asset('front/css/skins/square/grey.css')}}" rel="stylesheet">

    <!-- Range slider -->
    <link href="{{asset('front/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
    <style>
        .marker_info
        {
            height: 420px!important;
        }
        iframe.pattern{
            height: 450px !important;
            width: 100% !important;
            position: relative!important;
        }
        .modal-backdrop
        {
            display: none!important;
        }
    </style>
    @endsection
@section('main')   
    <!-- Slider -->
<!-- Slider -->
    <main>
        <div id="full-slider-wrapper">
            <div id="layerslider" style="width:100%;height:600px;">
                <!-- first slide -->
                <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;">
                    <img src="img/slides/slide_1.jpg" class="ls-bg" alt="Slide background">
                    <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Conéctate</h3>
                    <p class="ls-l slide_typo_2" style="top:52%; left:50%; font-size: 30px" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">con los amantes de la aventura</p>
                    <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='#'>Crear Perfil</a>
                </div>

                <!-- second slide -->
                <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
                    <img src="img/slides/slide_2.jpg" class="ls-bg" alt="Slide background">
                    <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Únete a una gran comunidad</h3>
                    <p class="ls-l slide_typo_2" style="top:52%; left:50%; font-size: 30px" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Que está en continuo crecimiento</p>
                    <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='#'>Crear Perfil</a>
                </div>

                <!-- third slide -->
                <div class="ls-slide" data-ls="slidedelay:5000; transition2d:103;">
                    <img src="img/slides/slide_3.jpg" class="ls-bg" alt="Slide background">
                    <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">TuPaseo.Travel</h3>
                    <p class="ls-l slide_typo_2" style="top:52%; left:50%; font-size: 30px" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Una plataforma para descubrir Colombia</p>
                    <a class="ls-l button_intro_2 outline" style="top:370px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='tours.html'>Ver Ofertas</a>
                </div>

            </div>
        </div> <!-- End Slider -->

        <!-- Position -->
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Category</a></li>
                    <li>Page active</li>
                </ul>
            </div>
        </div> <!-- End Position -->

        <!-- Description -->
        <section class="margin_60">
            <div class="container">

                <div class="col-sm-5 col-md-6">
                    <div class="col-md-12">
                        <h3 align="center"><b>¿Eres operador turístico y quieres obtener más clientes mientras estás en operación?</b></h3>
                        <p class="text-w3l-services" align="center">Con nuestro sistema de reservas y pagos podrás organizar<br>
                        tu disponibilidad y capacidad de servicio para mantener la continuidad de su negocio.</p>
                        <h3 align="center"><b>Características del asistente de TuPaseo</b></h3>
                        <br>
                        <h4>Disponibilidad de servicio</h4>
                            <p>Indicar el tiempo de servicio del operador.</p>
                        <h4>Últimas ofertas</h4>
                            <p>Ofertas de acuerdo a temporadas.</p>
                        <h4>Accesibilidad</h4>
                            <p>Accesibilidad y usabilidad de la plataforma para mejorar experiencia de usuario.</p>
                        <h4>Reportes</h4>
                            <p>Reportes de reservas e ingresos.</p>
                        <h4>Notificaciones</h4>
                            <p>Notificaciones de reservas y pagos vía correo electrónico.</p>
                        <h4>Capacidad de servicio</h4>
                            <p>Indicar la cantidad mínima y máxina de turistas que puede atender.</p>
                    </div>
                </div>

                <div class="col-sm-7 col-md-6" id="enlaceRegistro" name="enlaceRegistro">
                    <div class="content">
                        <h2 align="center"><b>CREA TU PERFIL</b></h2>
                        <span>¿Quieres que manejemos tus reservas y pagos mientras te encuentras en operación?</span><br><br></br>
                        <form class="form contact" method="post" action="http://tupaseo.com.co/operator/register">
                            <input type="hidden" name="_token" value="uKJ7D1vE5MMumDWZJvpXQRqezgeOnZflx1ucAwEq">
                            <div class="form-group col-md-6 ">
                                <label for="text" class="sr-only">Nombre</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre o Razón social" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="text" class="sr-only">Registro nacional de turismo</label>
                                <input type="text" class="form-control" id="national_register" name="national_register" placeholder="Registro Nacional de Turismo" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-12 ">
                                <label for="text" class="sr-only">Persona de contacto</label>
                                <input type="text" class="form-control" id="contact_personal" name="contact_personal" placeholder="Persona de contacto" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-4 ">
                                <label for="text" class="sr-only">Teléfono de contacto</label>
                                <input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Teléfono de contacto" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-8 ">
                                <label for="text" class="sr-only">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Correo electrónico" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-12 ">
                                <label for="text" class="sr-only">Página web</label>
                                <input type="text" class="form-control" id="web" name="web" placeholder="Página web" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                                <span class="label label-danger"></span>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="password_confirmation" class="sr-only">Confirmar Password</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirmar password" required="">
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-8 "> <input type="checkbox" id="val-terms" name="val-terms" value="1" checked="" required=""><a href="#modal-terms" style="color: #6c9e2e" data-toggle="modal"> Acepto terminos y condiciones</a> <span class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="col-sm-6"><button class="btn_1 green contact-submit custom-btn" type="submit"><i class="icon-right-hand"></i> Crear perfil</button></div>
                            <br>
                            <br>
                            <div class="col-lg-offset-8">¿Ya posees una cuenta?</div>
                            <div class="col-lg-offset-9">
                                <a style="color: #6c9e2e" href="http://tupaseo.com.co/login">Iniciar sesión</a>
                            </div> <!-- end .button -->
                        </form>

                    </div>
                </div>

            </div> <!-- End row -->
        </section>

        <!-- Apps.Co -->
        <div class="subscribe section-padding">
          <div class="container">
            <div class="col-md-12" align="center">
              <img src="img/footerapps.png" class="img-responsive">
            </div>
            <div class="col-md-12" align="center">
              <img src="img/footerpcis.png" class="img-responsive">
            </div>
          </div>
        </div>
        <hr>

        <!-- Video -->
        <section class="promo_full">
            <div class="promo_full_wp magnific">
                <div>
                    <iframe src="https://www.youtube.com/embed/8emAHIcaxLo?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </section> <!-- End video -->
        <hr>
        <hr>
</main>
  @endsection
     
  @section('additionalScript')
      <script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
    <script type="text/javascript" src="js/infobox.js"></script>
        <!-- Common scripts -->
    <script src="{{asset('front/js/tabs.js')}}"></script>
    <script src="{{asset('front/js/cat_nav_mobile.js')}}"></script>
    <script src="{{asset('front/js/icheck.js')}}"></script>

@endsection