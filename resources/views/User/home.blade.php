
@extends('layouts.frontheader')

@section('title','Mis reservaciones | Tu Paseo')
 @section('additionalStyle')
    
    <!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">

    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free-5.2.0/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">

    <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">

    <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
    <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>


    <link rel="stylesheet" type="text/css" href="{{asset('plugins/DataTables/datatables.min.css')}}"/>

 
    <style>
       .modal.fade {

        background: #00000080;
      }
        .modal-backdrop
        {
            display: none!important;
        }
    .animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
        }

        .stars
        {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
        }
        .modal-dialog
        {
          margin-top: 15%;
        }
    </style>
    @endsection
@section('main')   
<main>
  
    <section id="hero" style="background: #4d536d url('/front/img/slides/rappel.jpg') no-repeat center center; width: 100%;height: 470px;background-size: cover;">
        <div class="intro_title">
            <h3 class="animated fadeInDown"></h3>
            <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8" style="text-align:left;">
                        <h1>Gracias por aventurar con nosotros</h1>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
      <!-- Position -->
      <div id="position">
          <div class="container">
              <ul>
                 <li ><a href="/home">Home</a>
                 </li>
                  <li >Historial de reservas</li>
              </ul>
          </div>
      </div>

    <div class="container margin_60">
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#reservation">Historial de reservaciones</a></li>
  <li><a data-toggle="tab" href="#pays">Historial de pagos</a></li>
</ul>

<div class="tab-content">
  <div id="reservation" class="tab-pane fade in active">
    <h3>Historial de reservaciones</h3>

            <div class="row">
              <div class="col-md-12">
                <div class=" table-responsive">
                  
                    <table  id="datatable" class="datatable" role="grid" aria-describedby="datatable_info">
                         <thead>
                            <th>Reservación</th>
                            <th>Servicio</th>
                            <th>Actividad</th>
                            <th>Cantidad</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Estado</th>
                            <th></th>
                            
                        </thead>
                        <tbody>
                            @foreach($reservations as $reservation)
                            <tr >
                                <td># RSVT-{{str_pad($reservation->id_reservation, 6, "0", STR_PAD_LEFT)}}</td>
                                <td>{{$reservation->Service->service_name}}</td>
                                <td>{{$reservation->Service->PrincipalService->service}}</td>
                                <td align="center">{{$reservation->cuantity}}</td>
                                <td>{{$reservation->date}}</td>
                                <td>{{$reservation->time}}</td>
                                <td>{{$reservation->state}}@if($reservation->state=="Realizada")<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalcomment" onclick="$('#id_service').val({{$reservation->id_reservation}})">Calificar</button>@elseif($reservation->state == "Calificada")<button type="button" class="btn btn-success" data-toggle="modal" data-target="#comment{{$reservation->id_reservation}}" >Ver calificación</button>
                                    @endif</td>
                                <td>
                                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#details{{$reservation->id_reservation}}"><i class="far fa-eye"></i></button>
                                  @if(count($reservation->Reprograms)>0)
                                  <button class="btn btn-default btn-sm" data-toggle="tooltip" title="Historial de repogramaciones" onclick="reprogramModal({{$reservation->id_reservation}})"><i class="fas fa-history"></i></button> 
                                  @endif
                                </td>
                            </tr>
                            @if(count($reservation->Reprograms)>0)
                        <div id="reprogramModal{{$reservation->id_reservation}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h3 class="modal-title"><strong>Reprogramaciones de reservación</strong></h3>
                                        </div>
                                        <div class="modal-body">
                                            @foreach($reservation->Reprograms as $reprogram)
                                            <div class="row">
                                                <h4><strong>Razones de repogramación:</strong> {{$reprogram->reason}}</h4>
                                                <div class="col-md-6">
                                                    <h5>Fecha anterior</h5>
                                                    <p> {{$reprogram->previous_date}} a la(s) {{$reprogram->previous_time}}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5>Fecha de reprogramación</h5>
                                                    <p>{{$reprogram->new_date}} a la(s) {{$reprogram->new_time}}</p>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-effect-ripple btn-success" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                            <div class="modal fade" id="details{{$reservation->id_reservation}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Detalle de servicio</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                          <div class="col-md-12">
                                              <div class="widget">
                                                  <div class="widget-image widget-image-sm">
                                                      <div class="widget-image-content text-center">
                                                          <img src="@if(count($reservation->Service->images)>0) {{asset($reservation->Service->images->first()->link_image)}} @else {{asset('front/img/slides/rafting.jpg')}} @endif" alt="avatar" class="img-circle img-thumbnail img-thumbnail-transparent img-thumbnail-avatar-2x push" style="height: 150px!important;width: 150px!important">
                                                          <h2 class="widget-heading text-light"><strong>{{$reservation->Service->service_name}}</strong></h2>
                                                          <h4 class="widget-heading text-light-op"><em>{{$reservation->Service->operador->name}}</em></h4>

                                                      </div>
                                                  </div>
                                                  <div class="widget-content widget-content-full border-bottom">
                                                      <div class="row text-center">
                                                          <div class="col-xs-6 push-inner-top-bottom border-right">
                                                              <h5 class="widget-heading"> <strong>Fecha reserva </strong>
                                                                <br>
                                                                {{$reservation->date}} - {{$reservation->time}}
                                                              </h5>
                                                          </div>
                                                          <div class="col-xs-6 push-inner-top-bottom">
                                                              <h5 class="widget-heading"> <strong>Cantidad</strong>
                                                                <br>{{$reservation->cuantity}}
                                                              </h5>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="widget-content widget-content-full border-bottom">
                                                      <div class="row text-center">
                                                        <div class="row">
                                                          <div class="col-md-6 push-inner-top-bottom">
                                                              <strong>Actividad a realizar</strong>
                                                              <br>{{$reservation->Service->PrincipalService->service}}
                                                          </div>
                                                          <div class="col-md-6 push-inner-top-bottom">
                                                              <strong>Lugar</strong>
                                                              <br>{{$reservation->Service->Municipality->municipality_name}}
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <br>
                                                      <br>
                                                      <div class="col-md-12">
                                                        La información detallada del servicio puede encontrarla <a href="{{url('/home/service/'.$reservation->Service->slug)}}">aquí</a>.
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                             <div class="modal fade" id="comment{{$reservation->id_reservation}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Calificación de servicio</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row" id="post-review-box" >
                                            <div class="col-md-12">
                                                    <input id="ratings-hidden" name="rating" type="hidden"> 
                                                    <textarea class="form-control animated" cols="50"  placeholder="Ingresa tu comentario aquí" rows="5" readonly>{{$reservation->comment}}</textarea>
                                    
                                                    <div class="text-right">
                                                        <div class="stars starrr" data-rating="{{$reservation->calification}}" ></div>
                                                    </div>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>
                                  </div>
                                </div>
                              </div>
                          @endforeach
                        </tbody>
                    </table>
                    <div class="modal fade" id="modalcomment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <form action="{{url('savecomment')}}" method="post" id="commentForm">
                                      {{csrf_field()}}
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Calificación de servicio</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row" id="post-review-box" >
                                            <div class="col-md-12">
                                                    <input id="ratings-hidden" name="rating" type="hidden"> 
                                                    <textarea class="form-control animated" cols="50" name="comment" placeholder="Ingresa tu comentario aquí" rows="5" id="comment" required></textarea>
                                    
                                                    <div class="text-right">
                                                        <div class="stars starrr" data-rating="0"></div>
                                                    </div>
                                                    <input type="hidden" id="cantstars" name="cantstars">
                                                    <input type="hidden" id="id_service" name="id_service">
                                            </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary" onclick="validar({{$reservation->id_reservation}})">Guardar</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                      </div>
                  </div>
              </div>
            </div>
  </div>
  <div id="pays" class="tab-pane fade">
    <h3>PAGOS</h3>
    <div class="row">
              <div class="col-md-12">
                <div class=" table-responsive">
              
                    <table  id="datatable" class=" table datatable" role="grid" aria-describedby="datatable_info">
                         <thead>
                            <th>Recibo </th>
                            <th>Fecha de pago</th>
                            <th>Metodo</th>
                            <th>Estado</th>
                            <th>Monto</th>
                            <th></th>
                            
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td># TP-{{str_pad($order->id_order, 6, "0", STR_PAD_LEFT)}}</td>
                                <td>{{$order->created_at}}</td>
                                <td>{{$order->tipo_pago}}</td>
                                <td>{{$order->estado}}</td>
                                <td>{{$order->costo}}</td>
                                <td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#detailsOrder{{$order->id_order}}">Ver detalles</button></td>
                                
                            </tr>
                            <div class="modal fade" id="detailsOrder{{$order->id_order}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Detalles de compra</h4>
                                        </div>
                                        <div class="modal-body">
                                          <div class="col-md-12" id="post-review-box">
                                            <div class="row" >
                                                <h3>Datos del comprador</h3>
                                                <div class="col-md-12">
                                                  <div class="col-md-6">
                                                        <strong>Email: </strong>{{$order->shipping_order->email}} <br>
                                                        <strong>Identificación: </strong>{{$order->shipping_order->type_dni}} - {{$order->shipping_order->dni}} <br>
                                                        <strong>Nombres: </strong>{{$order->shipping_order->first_name}} {{$order->shipping_order->last_name}}  <br>
                                                        <strong>Teléfono: </strong>{{$order->shipping_order->phone}} <br>
                                                  </div>
                                                  <div class="col-md-6">
                                                        <strong>Direccion: </strong>{{$order->shipping_order->direccion}} <br>
                                                        <strong>País: </strong>{{$order->shipping_order->country}} <br>
                                                        <strong>Dpto/Provincia: </strong>{{$order->shipping_order->region}} <br>
                                                        <strong>Ciudad: </strong>{{$order->shipping_order->city}} <br>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <h3>Detalle de compra</h3>
                                                <div class="col-md-12">
                                                        <strong>Fecha de compra: </strong>{{$order->created_at}} <br>
                                                        <strong>Tipo de pago: </strong>{{$order->tipo_pago}} <br>
                                                        <strong>Costo: </strong>{{$order->costo}} <br>
                                                        <strong>Estado de compra: </strong>{{$order->estado}} <br>
                                                        @if($order->description)
                                                        <strong>Descripción: </strong>{{$order->description}} <br>
                                                        @endif
                                                      <h4>Servicios</h4>
                                                      <ul>
                                                        @foreach($order->services as $service)
                                                        <li><a href="{{url('home/Service/'.$service->service->slug)}}">{{$service->service->service_name}}</a> x {{$service->units}}</li>
                                                        <ul>
                                                          @foreach($service->items as $item)
                                                            <li>{{$item->item_name}} x {{$service->units}}</li>
                                                          @endforeach
                                                        </ul>
                                                        @endforeach
                                                      </ul>
                                                      ¿Deseas una copia de tu recibo? <a href="javascript:void(0);" onclick="SendCopy({{$order->id_order}})">Click aquí</a>
                                                </div>
                                            </div>
                                          </div>
                                          <br>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                  </div>
                        </div>
                          @endforeach
                        </tbody>
                    </table>
                  </div>
              </div>
            </div>
    </div>
</div>
            
    </div>
  </main>
@endsection

@section('additionalScript')
    <script src="{{asset('plugins/fontawesome-free-5.2.0/js/all.js')}}"></script>
<script src="https://unpkg.com/sweetalert2@7.1.1/dist/sweetalert2.all.js"></script>

<!-- Codigo de estrellas para calificación-->
    <script>
        (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  var openReviewBtn = $('#open-review-box');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.hide();
    
  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
    $("#cantstars").val(value);
  });
});

function validar(id)
{
  if($("#comment").val()=="" ||  $("#cantstars").val()=="")
  {
    swal('Error','Debe completar todos los campos','error')
  }
  else
  {
    $('#commentForm').submit();
  }
}

function SendCopy(cod)
{

    token = '{{csrf_token()}}';

    $.ajax({
      url: '/api/resendorder',
      type:'POST',
      data:{order:cod},
      headers:{'X-CSRF-TOKEN':token},
      complete:function (transport)
      {
        response = transport.responseText;
        if(response != "fail")
          {
            swal('Listo!','hemos enviado la copia de tu recibo al correo:'+ response,'success');
          }
        else
        {
          swal('Error','Solo se puede reenviar copia de recibo cada 20 minutos, para más información comunicate con soporte.','error');
        }
      }
    });
}
$(document).ready( function () {
    $('.datatable').DataTable({
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
      },
      order: [[ 0, "desc" ]],
    });
  });
function reprogramModal(id)
            {
                $("#reprogramModal"+id).modal();
            }
    </script>
 
<script type="text/javascript" src="{{asset('plugins/DataTables/datatables.min.js')}}"></script>
@endsection
