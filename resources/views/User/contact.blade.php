@extends('layouts.frontheader')

@section('title','Contacto | Tu Paseo')
@section('additionalStyle')
    
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">


    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <style>
       
        .modal-backdrop
        {
            display: none!important;
        }
    .animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
        }

        .stars
        {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
        }
        .modal-dialog
        {
          margin-top: 15%;
        }
        main
        {
            overflow: hidden;
        }
    </style>
    @endsection
@section('main')   
<main>
  

    <section id="hero" style="background: #4d536d url('/front/img/slides/rappel.jpg') no-repeat center center; width: 100%;height: 470px;background-size: cover;">
        <div class="intro_title">
            <h3 class="animated fadeInDown"></h3>
            <div class="parallax-content-1">
                <div class="row">
                    <h1>TuPaseo.Travel</h1>
                    <p>¡Sal de la ciudad y experimenta nuevas sensaciones!</p>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
        <!-- Position -->
    <div id="position">
        <div class="container">
            <ul>
               <li><a href="/home">Home</a>
               </li>
                <li>Contacto</li>
            </ul>
        </div>
    </div>

    <div class="container margin_60">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="form_title">
                        <h3><strong><i class="icon-pencil"></i></strong>¡Disfruta la adrenalina, disfruta tu libertad!</h3>
                        <p>
                            Contáctanos
                        </p>
                    </div>
                    <div class="step">

                        <div id="message-contact"></div>
                        <form method="post" action="{{url('storeemail')}}" id="contactform">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Nombre y Apellidos</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nombre y Apellidos">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" id="email" name="email" class="form-control" placeholder="Correo Electrónico">
                                    </div>
                                </div>
                            </div>
                            <!-- End row -->
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="text" id="phone" name="phone" class="form-control" placeholder="Número de Teléfono">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>País</label>
                                         <select name="country" id="country" class="selectpicker form-control" data-live-search="true" data-size="10">
                                           @foreach($countries as $country)
                                                <option value="{{$country['code']}}" @if($country['code']=="co") selected @endif>{{$country['name']}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <!-- End row -->
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Región/Departamento</label>
                                        <select name="region" id="region" class="selectpicker form-control" data-live-search="true" data-size="10"title="Seleccione">
                                               @foreach($regions as $region)
                                                    <option value="{{$region['region']}}" >{{$region['region']}}</option>
                                               @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label>Ciudad</label>
                                        <select name="city" id="city" class="selectpicker form-control" data-live-search="true" data-size="10" title="Seleccione">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Mensaje</label>
                                        <textarea rows="5" id="message_contact" name="message_contact" class="form-control" placeholder="Mensaje" style="height:200px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" align="center">
                                    <!-- Tupaseo produccion <div class="g-recaptcha" data-sitekey="6Ld_JGoUAAAAAKcT8DklMylATqcPtWKb307Vscw8"></div>-->
                                    <div class="g-recaptcha" data-sitekey="6LejH2oUAAAAANqSjbHgawvKgaObr6gRg0_dI8H2"></div>
                                </div>
                                    <button type="submit" value="Enviar" class="btn_1" id="submit-contact">Enviar</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- End col-md-8 -->

                <div class="col-md-4 col-sm-4">
                    <div class="box_style_1">
                        <h4>Dirección <span><i class="icon-pin pull-right"></i></span></h4>
                        <p>
                            Cra 9 # 72-81 Oficina 504, Bogotá D.C., Colombia
                        </p>
                        <hr>
                        <h4>Centro de Ayuda <span><i class="icon-help pull-right"></i></span></h4>
                        <p>
                            <strong>¿Eres operador turístico y quieres obtener más clientes mientras estás en operación?</strong>
                            Con nuestro sistema de reservas y pagos podrás organizar
                            tu disponibilidad, capacidad de servicio y mantener la continuidad de tu negocio.
                        </p>
                        <ul id="contact-info">
                            <li>+ 57 (1) 3121540 / + 57 316 5287931</li>
                            <li><a href="mailto:info@rutascolombia.com">info@rutascolombia.com</a>
                            </li>
                        </ul>
                    </div>
                    <div class="box_style_4">
                        <i class="icon_set_1_icon-57"></i>
                        <h4>¿Requieres <span>ayuda?</span></h4>
                        <a href="tel://005713121540" class="phone">+ 57 (1) 3121540</a>
                        <small>Lunes a Viernes 8:30am - 5:30pm</small>
                    </div>
                </div>
                <!-- End col-md-4 -->
            </div>
            <!-- End row -->
    </div>
</main>
@section('additionalScript')
<!--Captcha by google-->

@if(isset(Request()->notify))
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>
              swal("Listo!", "Gracias por contactarte con nosotros, dentro de poco daremos respuesta a tu mensaje", "success");
        </script>
@endif

<script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>

    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-input/js/fileinput.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>

    <script src="{{asset('js/bootstrap.min.js')}}"></script>


<script type="text/javascript">

$("#country").change(function()
{
    $.ajax({
        url: 'https://battuta.medunes.net/api/region/'+$(this).val()+'/all/?key=00000000000000000000000000000000',
        type: 'GET',
        dataType: 'JSON',     
        complete:function(transport)
        {
            response = transport.responseJSON;
            $('#region').empty().selectpicker('refresh');
            $('#city').empty().selectpicker('refresh');
            $('#divcity').addClass('hidden');
                if(response.length>0)
                {
                    for(x in response)
                    {
                        $("#region").append('<option value="'+response[x].region+'">'+response[x].region+'</option>').selectpicker('refresh');
                    }
                }
                else
                {
                    $("#region").removeAttr('required', 'required');;
                }
        }
    })
});

$("#region").change(function()
{
    var region = $(this).val(); 
    var region = region.replace(/ /g, "%20");
    $.ajax({
        url: 'https://battuta.medunes.net/api/city/'+$('#country').val()+'/search/?region='+region+'&key=00000000000000000000000000000000',
        type: 'GET',
        dataType: 'JSON',     
        complete:function(transport)
        {
            response = transport.responseJSON;
            $('#city').empty().selectpicker('refresh');
            $('#divcity').removeClass('hidden');
            if(response.length>0)
            {
                for(x in response)
                {
                    $("#city").append('<option value="'+response[x].city+'">'+response[x].city+'</option>').selectpicker('refresh');
                }
            }
            else
            {
                $("#city").removeAttr('required', 'required');;
            }

        }
    })
});
</script>
@endsection