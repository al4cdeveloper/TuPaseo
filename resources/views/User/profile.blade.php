@extends('layouts.frontheader')

@section('title','Mi perfil | Tu Paseo')
@section('additionalStyle')
    
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
{{ header('Access-Control-Allow-Origin: *')}}
    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">


    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <style>
       
        .modal-backdrop
        {
            display: none!important;
        }
    .animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
        }

        .stars
        {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
        }
        .modal-dialog
        {
          margin-top: 15%;
        }
        main
        {
            overflow: hidden;
        }
    </style>
    @endsection
@section('main')   
<main>
  

    <section id="hero" style="background: #4d536d url('/front/img/slides/rappel.jpg') no-repeat center center; width: 100%;height: 470px;background-size: cover;">
        <div class="intro_title">
            <h3 class="animated fadeInDown"></h3>
            <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h1>Actualiza tu información</h1>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
        <!-- Position -->
    <div id="position">
        <div class="container">
            <ul>
               <li><a href="/home">Home</a>
               </li>
                <li>Perfil</li>
            </ul>
        </div>
    </div>

    <div class="container margin_60">

            <div class="row">
                <div class="col-md-12">
                      <h2>Editar información</h2>
                      <small>A continuación por favor ingresar la información que desees cambiar y luego clickea en el botón "Guardar".</small>
                </div>
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <!-- Form Validation Form -->

                        {!!Form::model(Auth::user(), ['url'=> 'updateprofile','method'=>'POST', 'novalidate', 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data'])!!}
                        
                            <div class="form-group">
                              <div class="">
                                <label class="col-md-3 control-label" for="name">Imagen de perfil <span class="text-danger">*</span></label>
                                
                                <div class="kv-avatar center-block text-center" style="width:200px">
                                    <input id="avatar-2" name="file" type="file" class="file-loading">
                                </div>
                              </div>
                            </div>

                            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-3 control-label">Nombres<span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                {!!Form::text('first_name', null, ['class'=>'form-control', 'placeholder' => 'Ingresa tus nombres', 'required', 'id' => 'first_name'])!!}
                                <span class="label label-danger">{{$errors->first('first_name') }}</span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-3 control-label"> Apellidos<span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                {!!Form::text('last_name', null, ['class'=>'form-control', 'placeholder' => 'Ingresa tus Apellidos', 'required', 'id' => 'last_name'])!!}
                                <span class="label label-danger">{{$errors->first('last_name') }}</span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} ">
                                <label for="email" class="col-md-3 control-label">Email<span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    {!!Form::email('email', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Correo Electrónico', 'required'])!!}
                                    <span class="label label-danger">{{$errors->first('email') }}</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }} ">
                                <label for="country" class="col-md-3 control-label">País</label>
                                <div class="col-md-6">
                                    @if(!$user->country)
                                   <select name="country" id="country" class="selectpicker" data-live-search="true" data-size="10">
                                       @foreach($countries as $country)
                                            <option value="{{$country['code']}}" @if($country['code']=="co") selected @endif>{{$country['name']}}</option>
                                       @endforeach
                                   </select>
                                   @else
                                   <select name="country" id="country" class="selectpicker" data-live-search="true" data-size="10">
                                       @foreach($countries as $country)
                                            <option value="{{$country['code']}}" @if($country['code']== $user->country) selected @endif>{{$country['name']}}</option>
                                       @endforeach
                                   </select>
                                   @endif
                                    <span class="label label-danger">{{$errors->first('country') }}</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('region') ? ' has-error' : '' }} ">
                                <label for="region" class="col-md-5 control-label">Región/Departamento</label>
                                <div class="col-md-6">
                                    @if(!$user->region)
                                   <select name="region" id="region" class="selectpicker" data-live-search="true" data-size="10"title="Seleccione">
                                       @foreach($regions as $region)
                                            <option value="{{$region['region']}}" >{{$region['region']}}</option>
                                       @endforeach
                                   </select>
                                   @else
                                   <select name="region" id="region" class="selectpicker" data-live-search="true" data-size="10"title="Seleccione">
                                       @foreach($regions as $region)
                                            <option value="{{$region['region']}}" @if($region['region']== $user->region) selected @endif>{{$region['region']}}</option>
                                       @endforeach
                                   </select>
                                   @endif
                                    <span class="label label-danger">{{$errors->first('region') }}</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }} @if(!$user->region) hidden @endif" id="divcity">
                                <label for="city" class="col-md-3 control-label">Ciudad</label>
                                <div class="col-md-6">
                                    @if(!$user->region)
                                   <select name="city" id="city" class="selectpicker" data-live-search="true" data-size="10" title="Seleccione">
                                   </select>
                                   @else
                                    <select name="city" id="city" class="selectpicker" data-live-search="true" data-size="10" title="Seleccione">
                                        @foreach($cities as $city)
                                            <option value="{{$city['city']}}" @if($city['city']== $user->city) selected @endif>{{$city['city']}}</option>
                                        @endforeach
                                   </select>
                                   @endif
                                    <span class="label label-danger">{{$errors->first('city') }}</span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }} ">
                                <label for="address" class="col-md-3 control-label">Dirección</label>
                                <div class="col-md-6">
                                    {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Ingrese tu dirección', 'required'])!!}
                                    <span class="label label-danger">{{$errors->first('address') }}</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }} ">
                                <label for="phone" class="col-md-3 control-label">Teléfono</label>
                                <div class="col-md-6">
                                    {!!Form::number('phone', null, ['class'=>'form-control', 'placeholder' => 'Ingrese tu número de teléfono'])!!}
                                    <span class="label label-danger">{{$errors->first('phone') }}</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('promotionals_mails') ? ' has-error' : '' }} ">
                                <label class="col-md-6 col-md-offset-3"> 
                                    {{Form::checkbox('promotionals_mails', 1, $user->promotionals_mails== 0 ? false : true)}}Acepto recibir información promocional
                                </label>
                            </div>
                            <div class="container">
                                <h3>Cambiar contraseña</h3>
                                    <button class="btn btn-danger" type="button" onclick="changepass()" id="cambiar">Cambiar contraseña</button>
                                <div id="changePass" class="hidden">
                                    <div class="row">
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label for="password">Password<span class="kv-reqd">*</span></label>
                                            <input type="password" class="form-control" id="password" name="password" >
                                          </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label for="password_confirmation">Confirmar password<span class="kv-reqd">*</span></label>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 2%;">
                                <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-effect-ripple btn-primary">Enviar</button>
                                        <a href="{{url('/')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END Form Validation Form -->
                </div>

            </div>
    </div>
</main>
@section('additionalScript')
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>

    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-input/js/fileinput.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>

    <script src="{{asset('js/bootstrap.min.js')}}"></script>


<script type="text/javascript">
  function changepass()
  {
      $("#changePass").removeClass('hidden');
      $("#cambiar").attr('onclick','cancel()');
      $("#cambiar").text('Cancelar');
      $("#password").attr('required','required')
      $("#password_confirmation").attr('required','required')
  }
function cancel()
  {
      $("#changePass").addClass('hidden');
      $("#cambiar").attr('onclick','changepass()');
      $("#cambiar").text('Cambiar contraseña');
      $("#password").removeAttr('required')
  }

$("#country").change(function()
{
    $.ajax({
        url: 'https://battuta.medunes.net/api/region/'+$(this).val()+'/all/?key=00000000000000000000000000000000',
        type: 'GET',
        dataType: 'JSON',     
        complete:function(transport)
        {
            response = transport.responseJSON;
            $('#region').empty().selectpicker('refresh');
            $('#city').empty().selectpicker('refresh');
            $('#divcity').addClass('hidden');
                if(response.length>0)
                {
                    for(x in response)
                    {
                        $("#region").append('<option value="'+response[x].region+'">'+response[x].region+'</option>').selectpicker('refresh');
                    }
                }
                else
                {
                    $("#region").removeAttr('required', 'required');;
                }
        }
    })
});

$("#region").change(function()
{
    var region = $(this).val(); 
    var region = region.replace(/ /g, "%20");
    $.ajax({
        url: 'https://battuta.medunes.net/api/city/'+$('#country').val()+'/search/?region='+region+'&key=00000000000000000000000000000000',
        type: 'GET',
        dataType: 'JSON',     
        complete:function(transport)
        {
            response = transport.responseJSON;
            $('#city').empty().selectpicker('refresh');
            $('#divcity').removeClass('hidden');
            if(response.length>0)
            {
                for(x in response)
                {
                    $("#city").append('<option value="'+response[x].city+'">'+response[x].city+'</option>').selectpicker('refresh');
                }
            }
            else
            {
                $("#city").removeAttr('required', 'required');;
            }

        }
    })
});
$(document).ready(function()
{
  $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="@if($user->avatar) {{asset($user->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="Tu avatar" style="width:160px"><h6 class="text-muted">Click para cambiar</h6>',
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
});
</script>
@endsection