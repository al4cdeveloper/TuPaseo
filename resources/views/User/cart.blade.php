@extends('layouts.frontheader')

@section('title','Finalizar compra | Tu Paseo')
 @section('additionalStyle')
    <link href="{{asset('front/css/date_time_picker.css')}}" rel="stylesheet">
    
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/tabs_home.css')}}" rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('mystyle.css')}}">

    <!-- SPECIFIC CSS -->
    <link rel="stylesheet" href="{{asset('front/css/weather.css')}}">
        <!-- Radio and check inputs -->
    <link href="{{asset('front/css/skins/square/grey.css')}}" rel="stylesheet">

    <!-- Range slider -->
    <link href="{{asset('front/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">

    <style>
        .marker_info
        {
            height: 420px!important;
        }
        iframe.pattern{
            height: 450px !important;
            width: 100% !important;
            position: relative!important;
        }
        .modal-backdrop
        {
            display: none!important;
        }
    </style>
    @endsection
@section('main')   
    <!-- Slider -->
<!-- Slider -->
    <main>
         <section id="hero_2">
            <div class="intro_title">
                <div class="parallax-content-1">
                    <div class="row">
                        <h1>Carrito de Compras</h1>
                    </div>
                </div>
            </div>
        </section>

        <!-- Position -->
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="/home">Home</a></li>
                    <li>Carrito de compras</li>
                </ul>
            </div>
        </div> <!-- End Position -->

        <!-- Description -->
        <section class="margin_60">
            <div class="container">

               <div class="col-md-8 col-md-offset-2">
                   <div class="box_style_1">
                        <h3 class="inner">- Resumen -</h3>
                        <table class=" table table-striped ">
                            <thead>
                                <th>Cant</th>
                                <th height="30">Precio Unitario</th>  
                                <th>Nombre </th>
                                <th>Precio</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach($cart as $product)
                                <tr>

                                    <td>{{$product->qty}}</td>
                                    <td>{{cop_format($product->price)}}</td>
                                    <td>
                                        {{$product->name}}
                                    </td>
                                    <td>
                                        {{cop_format($product->price * $product->qty)}}
                                    </td>
                                    <td>
                                        <a href="/remove_cart_pay/{{$product->rowId}}" class="action"><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                               @endforeach
                        </table>
                        <table class="table table_summary">
                            <tbody>
                                <tr class="total">
                                    <td colspan="2" class="text-right">
                                        Total
                                    </td>
                                    <td class="text-right">
                                        {{cop_format($subtotal)}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
               </div>
               <div class="col-md-12" align="center" id="confirmpay">

                   <hr>
                   <h1>Confirmar pago</h1>
                   <hr>
                </div>
               <div class="col-md-12">

                    
                    @if(!Auth::user())
                    <div class="col-md-6 add_bottom_15">
                        <div class="form_title">
                            <h3><strong>1</strong>Registro | Iniciar Sesión</h3>
                            <p>
                                Requiere iniciar sesión para continuar.
                            </p>
                        </div>
                        <div class="step">
                            <div class="content">
                                <div id="login" style="margin: 0 !Important; padding: 0; box-shadow: none; background: transparent;">
                                <div class="row">
                                <div class="col-md-4 col-sm-4 login_social">
                                    <a href="{{url('auth/facebook')}}" class="btn btn-primary btn-block"><i class="icon-facebook"></i> Facebook</a>
                                </div>
                                <div class="col-md-4 col-sm-4 login_social">
                                    <a href="{{url('auth/google')}}" class="btn btn-info btn-block "><i class="icon-google"></i>Google</a>
                                </div>
                                </div> <!-- end row -->
                                <div class="login-or"><hr class="hr-or"><span class="span-or">or</span></div>
                                <form action="logincart" method="post" id="loginForm">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class=" form-control " placeholder="email" name="email" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" class=" form-control" placeholder="Password" name="password" required="">
                                    </div>
                                    <p class="small">
                                        <a href="{{url('password/reset')}}">¿Olvidaste la contraseña?</a>
                                    </p>
                                    <button type="submit" class="btn_full">Iniciar Sesión</button>
                                    <a href="javascript:void(0);" class="btn_full_outline" onclick="registerForm()">¿Aún no tienes una cuenta?</a>
                                </form>
                                <form action="registercart" method="post" id="registerForm" class="hidden">
                                    {{csrf_field()}}
                                    <p>Ingresa los campos a continuación.</p>
                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <input type="text" class=" form-control " placeholder="Nombre" name="first_name" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Apellidos</label>
                                        <input type="text" class=" form-control " placeholder="Apellidos" name="last_name" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class=" form-control " placeholder="Email" name="email" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" class=" form-control " placeholder="Dirección" name="address" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="text" class=" form-control " placeholder="Teléfono" name="phone" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" class=" form-control" placeholder="Password" name="password" required="">
                                    </div>
                                    <p class="small">
                                        <a href="javascript:void(0);"  onclick="loginForm()">Iniciar sesión</a>
                                    </p>
                                    <button type="submit" class="btn_full">Registrarme</button>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                    @else
                    {!!Form::model(Auth::user(), ['url'=> 'pay_cart','method'=>'POST', 'novalidate', 'id' => 'pay','name'=>'pay'])!!}
                    <div class="col-md-6 add_bottom_15">
                        <div class="form_title">
                            <h3><strong>1</strong>Datos del comprador</h3>
                            <p class="label ">
                                Los datos marcados con * son obligatorios
                            </p>
                        </div>
                        <div class="step">
                            <div class="content">
                                <div id="login" style="margin: 0 !Important; padding: 0; box-shadow: none; background: transparent;">
                                    <div class="form-group {{ $errors->has('dni') ? ' has-error' : '' }} col-sm-12">
                                        <label for="dni"> Documento de identidad*</label>
                                        <br>
                                        <div class="form-group {{ $errors->has('type_dni') ? ' has-error' : '' }} col-sm-6">
                                            {!!Form::select('type_dni', $type_documents, null, ['class' => 'browser-default form-control', 'required', 'id' => 'type_dni'])!!}
                                            <span class="label label-danger">{{$errors->first('type_dni') }}</span>
                                        </div>

                                        <div class="form-group {{ $errors->has('dni') ? ' has-error' : '' }} col-sm-6">
                                            {!!Form::text('dni', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Documento de Identidad', 'required', 'id' => 'dni'])!!}
                                        <span class="label label-danger">{{$errors->first('dni') }}</span>
                                            <span class="label label-danger">{{$errors->first('dni') }}</span>
                                        </div>
                                        
                                        
                                    </div>

                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }} col-sm-6">
                                        <label for="first_name">Nombres*</label>

                                        {!!Form::text('first_name', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Nombres', 'required', 'id' => 'first_name'])!!}
                                        <span class="label label-danger">{{$errors->first('first_name') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }} col-sm-6">
                                        <label for="last_name"> Apellidos*</label>
                                        {!!Form::text('last_name', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Apellidos', 'required', 'id' => 'last_name'])!!}
                                        <span class="label label-danger">{{$errors->first('last_name') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }} col-sm-6">
                                        <label for="country">País*</label>
                                            @if(!$user->country)
                                           <select name="country" id="country" class="selectpicker" data-live-search="true" data-size="10">
                                               @foreach($countries as $country)
                                                    <option value="{{$country['code']}}" @if($country['code']=="co") selected @endif>{{$country['name']}}</option>
                                               @endforeach
                                           </select>
                                           @else
                                           <select name="country" id="country" class="selectpicker" data-live-search="true" data-size="10">
                                               @foreach($countries as $country)
                                                    <option value="{{$country['code']}}" @if($country['code']== $user->country) selected @endif>{{$country['name']}}</option>
                                               @endforeach
                                           </select>
                                           @endif

                                        <span class="label label-danger">{{$errors->first('country') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('region') ? ' has-error' : '' }} col-sm-6" id="divProvince">
                                        <label for="region">Provincia/estado/departamento*</label>
                                        @if(!$user->region)
                                           <select name="region" id="region" class="selectpicker" data-live-search="true" data-size="10"title="Seleccione">
                                               @foreach($regions as $region)
                                                    <option value="{{$region['region']}}" >{{$region['region']}}</option>
                                               @endforeach
                                           </select>
                                        @else
                                           <select name="region" id="region" class="selectpicker" data-live-search="true" data-size="10"title="Seleccione">
                                               @foreach($regions as $region)
                                                    <option value="{{$region['region']}}" @if($region['region']== $user->region) selected @endif>{{$region['region']}}</option>
                                               @endforeach
                                           </select>
                                        @endif
                                        <span class="label label-danger">{{$errors->first('region') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }} @if(!$user->region) hidden @endif col-sm-6" id="divcity">
                                        <label for="city" class="col-md-3 control-label">Ciudad</label>
                                        <div class="col-md-6">
                                            @if(!$user->region)
                                           <select name="city" id="city" class="selectpicker" data-live-search="true" data-size="10" title="Seleccione">
                                           </select>
                                           @else
                                            <select name="city" id="city" class="selectpicker" data-live-search="true" data-size="10" title="Seleccione">
                                                @foreach($cities as $city)
                                                    <option value="{{$city['city']}}" @if($city['city']== $user->city) selected @endif>{{$city['city']}}</option>
                                                @endforeach
                                           </select>

                                           @endif
                                            <span class="label label-danger">{{$errors->first('city') }}</span>
                                        </div>
                                    </div>
                                    @if(count($cities)>0)
                                    {!!Form::hidden('hascity',1,['id'=>'hascity'])!!}
                                    @else
                                    {!!Form::hidden('hascity',0,['id'=>'hascity'])!!}
                                    @endif

                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} col-sm-12">
                                        <label for="email">Email*</label>
                                        {!!Form::email('email', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Correo Electrónico', 'required'])!!}
                                        <span class="label label-danger">{{$errors->first('email') }}</span>
                                    </div>


                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }} col-sm-12">
                                        <label for="address">Dirección*</label>
                                        {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Dirección Principal', 'required'])!!}
                                        <span class="label label-danger">{{$errors->first('address') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }} col-sm-6">
                                        <label for="phone">Teléfono*</label>
                                        {!!Form::number('phone', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Teléfono Principal', 'required'])!!}
                                        <span class="label label-danger">{{$errors->first('phone') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('phone_2') ? ' has-error' : '' }} col-sm-6">
                                        <label for="phone_2">Teléfono secundario</label>

                                        {!!Form::number('phone_2', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Teléfono Secundario'])!!}
                                        <span class="label label-danger">{{$errors->first('phone_2') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 add_bottom_15">
            
                       <!--End step -->

                        <div class="form_title">
                            <h3><strong>2</strong>Método de Pago</h3>
                            <p>
                                Selecciona la opción para procesar el pago.
                            </p>
                        </div>

                        <!-- Food Type -->
                        <div class="step">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="styled-select-filters {{ $errors->has('payment_type') ? ' has-error' : '' }} col-sm-12">
                                        {!!Form::select('payment_type', $metodosPago, null, ['class' => 'browser-default form-control', 'required', 'id' => 'payment_type'])!!}
                                        <span class="label label-danger">{{$errors->first('payment_type') }}</span>
                                    </div>   
                                </div>
                            </div>
                        </div>

                        <div class="step">
                            
                            <div class="row credit_card hidden">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label">Tarjeta de credito<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select class="selectpicker {{ $errors->has('type_card') ? ' has-error' : '' }}" name="type_card"  id="type_card">
                                            <option value="VISA">VISA</option>
                                            <option value="MASTERCARD">MASTERCARD</option>
                                            <option value="AMEX">AMEX</option>
                                            <option value="DINERS">DINERS</option>
                                      </select>
                                        <span class="label label-danger">{{$errors->first('type_card') }}</span>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="cardholderName" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre en la tarjeta:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="cardholderName" name="cardholderName"  class="form-control col-md-7 col-xs-12 {{ $errors->has('cardholderName') ? ' has-error' : '' }}" required=""  value="REJECTED" />
                                    <span class="label label-danger">{{$errors->first('cardholderName') }}</span>

                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="cardNumber" class="control-label col-md-3 col-sm-3 col-xs-12">Numero de tarjeta de credito:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="cardNumber" name="cardNumber" class="form-control col-md-7 col-xs-12 {{ $errors->has('cardNumber') ? ' has-error' : '' }}" data-inputmask="'mask' : '9999-9999-9999-9999'" required value="4097440000000004" />
                                    </div>
                                    <span class="label label-danger">{{$errors->first('cardNumber') }}</span>

                                </div>
                                <div class="form-group col-sm-6">
                                    <label  class="control-label col-md-6 col-sm-6 col-xs-12">Mes de Expiración:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control col-sm-2 {{ $errors->has('month_exp') ? ' has-error' : '' }}" name="month_exp" id="expiry-month" >
                                          <option value="01">Ene (01)</option>
                                          <option value="02">Feb (02)</option>
                                          <option value="03">Mar (03)</option>
                                          <option value="04">Abr (04)</option>
                                          <option value="05">May (05)</option>
                                          <option value="06">Jun (06)</option>
                                          <option value="07">Jul (07)</option>
                                          <option value="08">Ago (08)</option>
                                          <option value="09">Sep (09)</option>
                                          <option value="10">Oct (10)</option>
                                          <option value="11">Nov (11)</option>
                                          <option value="12" selected>Dic (12)</option>
                                      </select>
                                    <span class="label label-danger">{{$errors->first('month_exp') }}</span>

                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="cardExpirationYear" class="control-label col-md-6 col-sm-6 col-xs-12">Año de expiración:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" id="cardExpirationYear" name="cardExpirationYear"  maxlength="4" class="form-control col-md-7 col-xs-12 {{ $errors->has('cardExpirationYear') ? ' has-error' : '' }}" required=""  value="2014" />
                                    <span class="label label-danger">{{$errors->first('cardExpirationYear') }}</span>

                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="securityCode" class="control-label col-md-3 col-sm-3 col-xs-12">Codigo de seguridad:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" id="securityCode" name="securityCode"  class="form-control col-md-7 col-xs-12 {{ $errors->has('securityCode') ? ' has-error' : '' }}" required="" value="321" />
                                    <span class="label label-danger">{{$errors->first('securityCode') }}</span>

                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="docType" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de documento:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('docNumber') ? ' has-error' : '' }}">
                                    {!!Form::select('docType', $type_documents, null, ['class' => 'form-control col-md-7 col-xs-12', 'required', 'id' => 'docType'])!!}
                                    </div>
                                    <span class="label label-danger">{{$errors->first('docType') }}</span>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="docNumber" class="control-label col-md-3 col-sm-3 col-xs-12">Numero de documento:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="docNumber" name="docNumber"  class="form-control col-md-7 col-xs-12  {{ $errors->has('docNumber') ? ' has-error' : '' }}" required="" value="5415668464654" />
                                        <span class="label label-danger">{{$errors->first('docNumber') }}</span>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-3 control-label">Numero de cuotas<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="number" class="form-control  {{ $errors->has('numero_coutas') ? ' has-error' : '' }}" name="numero_coutas" id="numero_coutas" placeholder="Numero de cuotas"  maxlength="2" value="1">
                                    <span class="label label-danger">{{$errors->first('numero_coutas') }}</span>

                                    </div>
                                 </div>
                            </div>

                            <div class="row others hidden">
                                <div class="form-group {{ $errors->has('identification_type') ? ' has-error' : '' }} col-sm-6">
                                    {!!Form::select('identification_type', $type_documents, null, ['class' => 'browser-default form-control', 'required', 'id' => 'identification_type'])!!}
                                    <span class="label label-danger">{{$errors->first('identification_type') }}</span>
                                </div>

                                <div class="form-group {{ $errors->has('identification_number') ? ' has-error' : '' }} col-sm-6">
                                    {!!Form::text('identification_number', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Documento de Identidad', 'required', 'id' => 'identification_number'])!!}
                                    <span class="label label-danger">{{$errors->first('identification_number') }}</span>
                                </div>
                                <div class="form-group col-md-12 hidden" id="tipoPersona" >
                                    <label class="col-sm-3 control-label">Tipo de cliente<span class="required">*</span></label>

                                    <div class="form-group {{ $errors->has('type_person') ? ' has-error' : '' }} col-sm-6">
                                        {!!Form::select('type_person', $type_person, null, ['class' => 'browser-default form-control', 'required', 'id' => 'type_person'])!!}
                                        <span class="label label-danger">{{$errors->first('type_person') }}</span>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 hidden" id="entidadefectivo">
                                    <label class="col-sm-4 control-label">Entidad de pago<span class="required">*</span></label>
                                    <div class="form-group {{ $errors->has('type_person') ? ' has-error' : '' }} col-sm-6">
                                        <select name="entity_cash" id="entity_cash" class="browser-default form-control">
                                            <option value="BALOTO">Baloto</option>
                                            <option value="EFECTY">Efecty</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('all_name') ? ' has-error' : '' }} col-sm-12">
                                    {!!Form::text('all_name', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Nombre Completo', 'required', 'id' => 'cardholder_name'])!!}
                                    <span class="label label-danger">{{$errors->first('all_name') }}</span>
                                </div>
                                
                                <div class="form-group col-md-12 hidden" id="bancos" >
                                    <label class="col-sm-3 control-label">Entidad bancaria<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <div class="input-group">
                                        <select class="{{ $errors->has('all_name') ? ' has-error' : '' }}" name="bank"  required="" id="bank">
                                            @foreach($banks->banks as $bank)
                                                <option value="{{$bank->pseCode}}">{{$bank->description}}</option>
                                              @endforeach
                                        </select>
                                        <span class="label label-danger">{{$errors->first('bank') }}</span>

                                      </div>
                                    </div>
                                 </div>
                            </div>
                            <!--End row -->
                            <div id="policy" class="hidden">
                                <h4>Políticas del Sitio</h4>
                                <div class="form-group">
                                    <label>
                                        {!!Form::checkbox('terms', null,false, ['name' => 'terms', 'required'])!!}Acepto los <a href="{{url('terminos')}}" target="_blank">Términos y Condiciones</a> y<br>
                                        la <a href="habeasdata.html"> Política de Tratamiento de Datos</a>.</label>
                                        <span class="label label-danger">{{$errors->first('terms') }}</span>
                                </div>
                                    {!!Form::button('Pagar', ['type' => 'submit', 'class'=>'btn btn-success btn-block btn-lg','id'=>'paynow'])!!}
                            </div>
                            
                        </div> <!-- STEP -->
                    </div>
                    {{Form::close()}}
                    @endif
               </div> <!-- End col-12 -->
        </section>
<div class="modal fade" id="pleaseWaitDialog" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h1><i class="fa fa-spinner fa-spin"></i> Cargando...</h1>
                </div>
            </div>
        </div>
    </div>

        <hr>
</main>
  @endsection
     
  @section('additionalScript')
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>

    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>

    <script src="{{asset('plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <script src="{{asset('js/jquery.parallax-1.1.3.js')}}"></script>

      <script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
        <!-- Common scripts -->
    <script src="{{asset('front/js/tabs.js')}}"></script>
    <script src="{{asset('front/js/cat_nav_mobile.js')}}"></script>
    <script src="{{asset('front/js/icheck.js')}}"></script>
    <script>
       
        $(document).ready(function() 
        {
            method = $('#payment_type').val();
            if (method.length > 0) 
            {
                myApp.showPleaseWait();
                
                if (method == 'tarjeta_credito') 
                {
                    $('.credit_card').removeClass('hidden');
                    $('#policy').removeClass('hidden');
                    $('.others').addClass('hidden');
                } 
                else if(method == 'pse')
                {
                    $('.others').removeClass('hidden');
                    $('#bancos').removeClass('hidden');
                    $('#tipoPersona').removeClass('hidden');
                    $('#entidadefectivo').addClass('hidden');
                    $('#policy').removeClass('hidden');
                    $('.credit_card').addClass('hidden');
                } 
                else 
                {
                    $('#bancos').addClass('hidden');
                    $('#entidadefectivo').removeClass('hidden');
                    $('#tipoPersona').addClass('hidden');
                    $('#policy').removeClass('hidden');
                    $('.credit_card').addClass('hidden');
                    $('.others').removeClass('hidden');
                }
                myApp.hidePleaseWait();
            }
            nombres    = $('#first_name').val();
            apellidos  = $("#last_name").val();
            cardholder = nombres.trim()+' '+apellidos.trim();

            $('#cardholder_name').val(cardholder);
            });

        $('#payment_type').change(function(e) {
        method = $(this).val();
        method = $('#payment_type').val();
        if (method.length > 0) {
            myApp.showPleaseWait();


            if (method == 'tarjeta_credito') 
            {
                $('.credit_card').removeClass('hidden');
                $('#policy').removeClass('hidden');
                $('.others').addClass('hidden');
            } 
            else if(method == 'pse')
            {
                $('.others').removeClass('hidden');
                $('#entidadefectivo').addClass('hidden');
                $('#bancos').removeClass('hidden');
                $('#tipoPersona').removeClass('hidden');
                $('#policy').removeClass('hidden');
                $('.credit_card').addClass('hidden');
            } 
            else 
            {
                $('#bancos').addClass('hidden');
                $('#tipoPersona').addClass('hidden');
                $('#entidadefectivo').removeClass('hidden');
                $('#policy').removeClass('hidden');
                $('.credit_card').addClass('hidden');
                $('.others').removeClass('hidden');
            }
            myApp.hidePleaseWait();
        }
        else
        {
            $('#bancos').addClass('hidden');
            $('#tipoPersona').addClass('hidden');
                $('.credit_card').addClass('hidden');
                $('.others').addClass('hidden');
                $('#policy').addClass('hidden');

        }
    });
         $("#paynow").click(function()
        {
            myApp.showPleaseWait();

        });
    $('#dni').keyup(function(e) {
        dni = $(this).val();
        $('#identification_number').val(dni);
    });

    $('#type_dni').change(function(e) {
        type_dni = $(this).val();
        $('#identification_type').val(type_dni);
    });

 $('#nombres').keyup(function(e) {
        nombres    = $(this).val();
        apellidos  = $('#apellidos').val();
        cardholder = nombres.trim()+' '+apellidos.trim();

        $('#cardholder_name').val(cardholder);
    });

    $('#apellidos').keyup(function(e) {
        nombres    = $('#nombres').val();
        apellidos  = $(this).val();
        cardholder = nombres.trim()+' '+apellidos.trim();

        $('#cardholder_name').val(cardholder);
    });


    $("#country").change(function()
    {
        myApp.showPleaseWait();

        $.ajax({
            url: 'https://battuta.medunes.net/api/region/'+$(this).val()+'/all/?key=00000000000000000000000000000000',
            type: 'GET',
            dataType: 'JSON',     
            complete:function(transport)
            {
                response = transport.responseJSON;
                $('#region').empty().selectpicker('refresh');
                $('#city').empty().selectpicker('refresh');
                $('#divcity').addClass('hidden');
                    if(response.length>0)
                    {
                        for(x in response)
                        {
                            $("#region").append('<option value="'+response[x].region+'">'+response[x].region+'</option>').selectpicker('refresh');
                        }
                    }
                    else
                    {
                        $("#region").removeAttr('required', 'required');;
                    }
            }
        })
        myApp.hidePleaseWait();

    });

    $("#region").change(function()
    {
        myApp.showPleaseWait();

        var region = $(this).val(); 
        var region = region.replace(/ /g, "%20");
        $.ajax({
            url: 'https://battuta.medunes.net/api/city/'+$('#country').val()+'/search/?region='+region+'&key=00000000000000000000000000000000',
            type: 'GET',
            dataType: 'JSON',     
            complete:function(transport)
            {
                response = transport.responseJSON;
                if(response.length>0)
                {
                    $("#hascity").val(1);

                    $('#city').empty().selectpicker('refresh');
                    $('#divcity').removeClass('hidden');
                    for(x in response)
                    {
                        $("#city").append('<option value="'+response[x].city+'">'+response[x].city+'</option>').selectpicker('refresh');
                    }
                }
                else
                {
                    $("#city").removeAttr('required', 'required');
                    $("#city").addClass('hidden');
                    $("#hascity").val(0);
                }

            }
        });
        myApp.hidePleaseWait();

    });




        function registerForm()
        {
            $("#loginForm").addClass('hidden');
            $("#registerForm").removeClass('hidden');
        }
        function loginForm()
        {
            $("#loginForm").removeClass('hidden');
            $("#registerForm").addClass('hidden');
        }

        var myApp;
    myApp = myApp || (function () {
        var pleaseWaitDiv = $('#pleaseWaitDialog');
        return {
            showPleaseWait: function() {
                pleaseWaitDiv.modal();
            },
            hidePleaseWait: function () {
                pleaseWaitDiv.modal('hide');
            },

        };
    })();


    </script>
@endsection