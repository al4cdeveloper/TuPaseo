<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$service->PrincipalService->service}} - {{$service->location}} TuPaseo</title>

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="alternate" href="http://tupaseo.travel" hreflang="es-co" />


    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-input/css/fileinput.css')}}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('Backstyle.css')}}">
    <!-- END Stylesheets -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->

    <link rel="stylesheet" href="{{asset('plugins/simpleStar/SimpleStarRating.css')}}">
    <!-- Modernizr (browser feature detection library) -->
    <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>
     <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
     </script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script> <!-- termina GTM -->
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    </head>
    <body>

        <div id="page-wrapper" class="page-loading">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>

            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">

                
                <!-- END Main Sidebar -->

                                <!-- Main Container -->
                <div id="main-container" style="margin-left: 0px!important; ">
                    <!-- Header -->
                    <!-- In the PHP version you can set the following options from inc/config file -->
                    <!--
                        Available header.navbar classes:

                        'navbar-default'            for the default light header
                        'navbar-inverse'            for an alternative dark header

                        'navbar-fixed-top'          for a top fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                            'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                        'navbar-fixed-bottom'       for a bottom fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                            'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                    -->
                    <header class="navbar navbar-inverse navbar-fixed-top" style="left: 0px!important;">
                        <ul class="nav navbar-nav-custom pull-left">
                            <a href="{{url('/')}}" class="sidebar-title">
                                <img src="{{asset('images/logomenu.png')}}">
                            </a>
                        </ul>

                        <ul class="nav navbar-nav-custom">
                            <!-- Header Link -->
                            <li class="animation-fadeInQuick">
                                <a href="#"><strong>{{$service->PrincipalService->service}} - {{$service->location}}</strong></a>
                            </li>
                            <!-- END Header Link -->
                        </ul>
                        <!-- END Left Header Navigation -->

                        <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
                            <!-- Search Form -->
                            <!-- END Search Form -->
                            <!-- User Dropdown -->
                            @if(Auth::user()!=null)
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="@if(Auth::user()->avatar) {{asset(Auth::user()->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="avatar">
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{url('/home')}}">
                                            <i class="fa fa-inbox fa-fw pull-right"></i>
                                            Mis reservaciones
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('userprofile')}}">
                                            <i class="fa fa-pencil-square fa-fw pull-right"></i>
                                            Perfil
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/operator/logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                            <i class="fa fa-power-off fa-fw pull-right"></i>
                                            Log out
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                      </form>
                                    </li>
                                </ul>
                            </li>
                            @else
                            <li>
                                <a href="{{url('login')}}">
                                    Iniciar sesion
                                </a>
                            </li> 
                            <li>
                                <a href="https://www.facebook.com/tupaseo.co/" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/tupaseo/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('contact')}}">
                                    <i class="fa fa-envelope"></i>
                                </a>
                            </li>
                            @endif
                            <!-- END User Dropdown -->
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    <!-- Page content -->
                    <div id="page-content" class="inner-sidebar-right vuejs">
                        <!-- Inner Sidebar -->
                            {{-- <div class="col-sm-5">
                                <pre>
                                    @{{$data}}  Las variables que tienen arroba son de vue
                                </pre>
                            </div> --}}
                        <div id="page-content-sidebar" v-if="itemsCart>0">
                            <!-- Collapsible Shopping Cart -->
                            <a href="javascript:void(0)" class="btn btn-block btn-default visible-xs" data-toggle="collapse" data-target="#shopping-cart"><i class="fa fa-shopping-cart"></i> Ver carro (@{{itemsCart}})</a>
                            <div id="shopping-cart" class="collapse navbar-collapse remove-padding">
                                <h4 class="inner-sidebar-header">
                                    <a href="javascript:void(0)" @click="deleteCart()" class="btn btn-effect-ripple btn-xs btn-warning pull-right"><i class="fa fa-trash"></i></a>
                                    Carrito de compras (@{{itemsCart}})
                                </h4>
                                <table class="table table-striped table-borderless table-vcenter">
                                    <tbody >
                                        <tr v-for="service in cart">
                                            <td class="text-center">
                                                <button @click="removeCart(service.rowId)" class="btn btn-effect-ripple btn-xs btn-danger pull-right"><i class="fa fa-times"></i></button>
                                            </td>
                                            <td style="width: 80px;">
                                                <h5>@{{service.name}}</h5>
                                            </td>
                                            <td style="width: 60px;" class="text-center">
                                                <h5>@{{service.qty}}</h5>
                                            </td>
                                            <td class="text-right">
                                                <strong>$ @{{service.subtotal}}</strong>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td colspan="3">Total</td>
                                            <td class="text-right">
                                            <strong>$@{{total}}</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @if(Auth::user())
                                <a href="{{url('logeruser/reservation')}}" class="btn btn-effect-ripple btn-block btn-success push-bit-top-bottom" data-toggle="modal">
                                    <i class="fa fa-shopping-cart"></i> Reservar
                                </a>
                                @else
                                    <a href="#modal-checkout" class="btn btn-effect-ripple btn-block btn-success push-bit-top-bottom" data-toggle="modal">
                                        <i class="fa fa-shopping-cart"></i> Reservar
                                    </a>
                                @endif
                            </div>
                            <!-- END Collapsible Shopping Cart -->
                        </div>
                        <!-- END Inner Sidebar -->

                        <!-- eStore Header -->
                        <div class="content-header">
                            <div class="header-section">
                                <form action="{{url('search')}}" method="get" class="form-horizontal">
                                    <div class="form-group remove-margin-bottom">
                                        <div class="col-sm-6 push-bit">
                                            <select id="example-chosen" name="location" class="select-chosen" data-placeholder="Seleccione lugar para filtrar" style="width: 250px;" required>
                                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    @foreach($sites as $site)
                                                        <option value="{{$site->location}}">{{$site->location}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-4">
                                            <button type="submit" class="btn btn-effect-ripple btn-effect-ripple btn-primary" style="background-color: #fd8c02;border-color: #fd8c02;"><i class="fa fa-search"></i> Encuentra ahora!</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END eStore Header -->
                        
                        <div class="row">
                            <div class="col-md-11 col-lg-11 ">
                                <!-- Article Block -->
                                <div class="block">
                                    <!-- Article Content -->
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-10">
                                            <h1>{{$service->PrincipalService->service}} - {{$service->location}}</h1>
                                            <i class="fa fa-heartbeat" aria-hidden="true"></i><small>{{$service->PrincipalService->type_service}}</small>
                                            <br>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i> {{$service->address}}
                                            <hr>
                                            <h3>Descripcion</h3>
                                            {!!$service->description!!}
                                            <hr>
                                            <h3>Recomendaciones</h3>
                                            {!!$service->requisites!!}
                                        </div>
                                        <div class="col-lg-5 col-sm-10 mt-2 col-lg-offset-1">
                                            <div class="">
                                                <div id="animation-carousel" class="carousel slide remove-margin" data-ride="carousel" >
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner" >
                                                        @foreach($service->images as $image)
                                                        <div class="item @if($contador==1) active @endif"  >
                                                            <img src="{{asset($image->link_image)}}" alt="image">
                                                        </div {{$contador++}}>
                                                        @endforeach
                                                    </div>
                                                    <!-- Controls -->
                                                    <a class="left carousel-control" href="#animation-carousel" data-slide="prev">
                                                        <span><i class="fa fa-chevron-left"></i></span>
                                                    </a>
                                                    <a class="right carousel-control" href="#animation-carousel" data-slide="next">
                                                        <span><i class="fa fa-chevron-right"></i></span>
                                                    </a>
                                                    <!-- END Controls -->
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2><span class="text-muted">$</span> <strong>{{$service->cost}}</strong></h2>
                                                        <div class="text-warning">
                                                            @if(count($comments)>0)
                                                            <a href="#" data-toggle="modal" data-target="#comments">
                                                            <span class="rating" data-default-rating="{{$service->average}}" disabled style="color:rgb(255, 205, 5)"></span>
                                                            <span>{{count($comments)}} calificaciones</span>
                                                            </a>
                                                            @else
                                                            <span class="rating" data-default-rating="5" disabled style="color:rgb(255, 205, 5)"></span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4" align="center" style="padding: 5%">
                                                        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#detailService{{$service->id_service_operator}}">Reservar</button>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="block-section text-center">
                                                        <a href="{!!Share::load(URL::current(), $service->PrincipalService->service." - ".$service->location."Con TuPaseo ")->facebook()!!}" class="btn btn-effect-ripple btn-default" target="_blank"><i class="fa fa-facebook"></i></a>
                                                        <a href="{!!Share::load(URL::current(), $service->PrincipalService->service." - ".$service->location."Con TuPaseo ")->twitter()!!}" class="btn btn-effect-ripple btn-default" target="_blank"><i class="fa fa-twitter"></i></a>
                                                        <a href='{!!Share::load(URL::current(), $service->PrincipalService->service." - ".$service->location."Con TuPaseo ")->gplus()!!}' class="btn btn-effect-ripple btn-default" target="_blank"><i class="fa fa-google-plus"></i></a>
                                                        <a href="{!!Share::load(URL::current(), $service->PrincipalService->service." - ".$service->location."Con TuPaseo ")->pinterest()!!}" class="btn btn-effect-ripple btn-default" target="_blank"><i class="fa fa-pinterest"></i></a>
                                                    </div>
                                                </div>
                                                 <!-- Modal -->
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- END Article Block -->
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="comments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Comentarios</h4>
                              </div>
                              <div class="modal-body">
                                    Comentarios realizados sobre el servicio. <br>
                                    @foreach($comments as $comment)
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <img src="@if($comment->user->avatar){{asset($comment->user->avatar)}}@else{{asset('auth-panel/img/placeholders/avatars/avatar9.jpg')}}@endif" alt="image" class="img-circle" style="height: 50px;width: 50px;">
                                        </div>
                                        <div class="col-md-10">
                                            <h4>{{$comment->user->first_name}}</h4>
                                            {!!$comment->comment!!}
                                        </div>
                                    </div>
                                    @endforeach
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- eStore Content -->
                        <div class="modal fade" id="detailService{{$service->id_service_operator}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                    <div class="form-horizontal">
                                        <div class="form-group ">
                                            <label for="service" class="col-md-4 control-label">Fecha</label>
                                            <div class="col-md-6">
                                                <input type="text" name="date" class="form-control input-datepicker datepicker" data-date-format="mm/dd/yy" placeholder="mm/dd/yy" v-model="fillReservation.date" min="{{$service->since_date}}" v-on:blur="validateDate({{$service->id_service_operator}})" id="dateService{{$service->id_service_operator}}" v-on:click="ValidateInput('{{$service->id_service_operator}}')" >
                                            </div>
                                            <button class="btn btn-primary" v-on:click="ValidateInput('dateService{{$service->id_service_operator}}')"> Ver</button>
                                            <div class="col-sm-8 col-sm-offset-2">
                                                
                                                <p>En caso de que no se despliegue el calendario por favor oprimir el boton de "Ver" e intentarlo nuevamente.</p>
                                            </div>
                                        </div>

                                        <div class="form-group " v-if="countHours>0">

                                            <label for="hour" class="col-md-4 control-label">Hora</label>
                                            <div class="col-md-6">
                                                <select id="hour" class="form-control" name="time" required v-model="fillReservation.time">
                                                    <option v-for="hour in hours">
                                                        @{{hour}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div v-else-if="countHours==-1" align="center">
                                            No se encuentran horarios disponibles para este día.
                                        </div>
                                        <div class="form-group ">
                                            <label for="service" class="col-md-4 control-label">Cantidad personas</label>
                                            <div class="col-md-6">
                                                <input id="cantidad_personas{{$service->id_service_operator}}" type="text" class="form-control" name="persons" max="{{$service->capacity}}" v-model="fillReservation.cuantity" required onchange=>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label for="service" class="col-md-4 control-label">Tipo de pago:</label>
                                            <div class="col-md-6">
                                                <input id="pay_type" type="text" class="form-control" name="pay_type" value="En sitio" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-effect-ripple btn-success" {{-- onclick="Enviar('@{{service->id_service_operator}}')" --}} v-on:click.prevent="addcar({{$service->id_service_operator}},{{$service->capacity}})" hidden="hidden" id="Reservar"><i class="fa fa-shopping-cart" ></i> Reservar ahora</button>
                                            <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal" >Cerrar</button>
{{--                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button> --}}
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- END eStore Content -->
                    </div>
                <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->
            <!-- Checkout Modal -->
        <div id="modal-checkout" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="push" action="{{ route('registercart') }}" method="post">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="modal-title"><strong>Finalizar reserva</strong></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="page-header text-center">{{-- <i class="fa fa-credit-card"></i>  --}}<strong>Ingresa tu información</strong></h4>
                                    <p>A continuación diligencia el formulario para realizar el registro de tu reserva y de tu información para proximas compras. Si ya te has registrado anteriormente, inicia sesión.</p>
                                </div>
                                <input type="hidden" name="time" id="timePay">
                                <input type="hidden" name="date" id="datePay">
                                <input type="hidden" name="service" id="servicePay">
                                <input type="hidden" name="persons_quantity" id="persons_quantity">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label for="checkout-payment-name">Nombres</label>
                                        <input type="text"  name="nombres" class="form-control" placeholder="Nombres" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-payment-name">Apellidos</label>
                                        <input type="text"  name="apellidos" class="form-control" placeholder="Apellidos" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-payment-name">Email</label>
                                        <input type="email"  name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-name">Dirección</label>
                                                <input type="text"  name="direccion" class="form-control" placeholder="Direccion" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-cvc">Telefono</label>
                                                <input type="number" id="checkout-payment-cvc" name="telefono" class="form-control" placeholder="Telefono">
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-name">Password</label>
                                                <input type="password"  name="password" class="form-control" placeholder="Password" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="checkout-payment-cvc">Confirmar Password</label>
                                                <input type="password" name="password_confirmation" class="form-control" placeholder="Conrfimar password">
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{-- <h4 class="pull-left">$ <strong class="text-primary-dark">@{{total}}</strong></h4> --}}
                            <button type="button" class="btn btn-effect-ripple btn-info" onclick="iniciar()">Ya poseo una cuenta</button>
                            <button type="submit" class="btn btn-effect-ripple btn-success"><i class="fa fa-check"></i> Completar compra</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Login Modal -->
        <div id="iniciarsesion" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="push" action="{{ route('logincart') }}" method="post">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="modal-title"><strong>Finalizar reserva</strong></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="page-header text-center">{{-- <i class="fa fa-credit-card"></i>  --}}<strong>Iniciar sesión</strong></h4>
                                </div>

                                <input type="hidden" name="origen" value="reservation">
    

                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="checkout-payment-name">Email</label>
                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="checkout-payment-name">Password</label>
                                        <input type="password"  name="password" class="form-control" placeholder="Password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{-- <h4 class="pull-left">$ <strong class="text-primary-dark">@{{total}}</strong></h4> --}}
                            
                            <button type="submit" class="btn btn-effect-ripple btn-success"><i class="fa fa-check"></i> Completar compra</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        

    <section id="foot">
        <div class="subscribe section-padding" style="background: white;   border-top: 1px solid #dae0e8;">
          <div class="container">
            <div class="col-md-12" align="center">
              <img src="{{asset('images/footerapps.png')}}" class="img-responsive">
            </div>
            <div class="col-md-12" align="center">
              <img src="{{asset('images/footerpcis.png')}}" class="img-responsive">
            </div>
          </div>
           <div class="container" align="center">
                <p>&copy; 2017 <a href="{{url('contact')}}">TuPaseo</a>, Todos los derechos reservados</p>
            </div> <!-- end .container -->
        </div>
    </section>
    
    <script src="{{asset('vue/js/Cart.js')}}"></script>
    <script src="{{ asset('js/share.js') }}"></script>
    <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
    <script src="{{asset('auth-panel/js/app.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-input/js/fileinput.js')}}"></script>
    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>

    
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.0/sweetalert2.all.js"></script>

    <script src="{{asset('plugins/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('plugins/simpleStar/SimpleStarRating.js')}}"></script>
    <script>
      jQuery(document).ready(function($) {
        "use strict";

        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools:false});
      });
    </script>
     <script>
        var ratings = document.getElementsByClassName('rating');

        for (var i = 0; i < ratings.length; i++) {
            var r = new SimpleStarRating(ratings[i]);

            ratings[i].addEventListener('rate', function(e) {
                console.log('Rating: ' + e.detail);
            });
        }
    </script>
    <script>$(function(){ FormsComponents.init(); });</script>
    
    <script type="text/javascript">
        function Enviar(id)
        {
            $("#timePay").val($("#timeForm").val());
            $("#datePay").val($("#dateForm").val());
            $("#servicePay").val(id);
            $("#persons_quantity").val($("#cantidad_personas").val())
            $("#btnpagar").removeAttr('hidden');
            // $("#modal-checkout").modal("show");
        }
        function iniciar()
        {
            $("#modal-checkout").modal("hide");
            $("#iniciarsesion").modal("show");
        }
        $(document).ready(function() {
            $(".datepicker").datepicker();
                $("#dateService{{$service->id_service_operator}}").datepicker().show();

        });

        function noExcursion(date){ 
            var day = date.getDay();
            // aqui indicamos el numero correspondiente a los dias que ha de bloquearse (el 0 es Domingo, 1 Lunes, etc...) en el ejemplo bloqueo todos menos los lunes y jueves.
            return [(
                day = day
                @foreach($service->days as $day)
                    @if($day =="Domingo")
                        && day != 0

                    @elseif($day =="Lunes")
                        && day != 1 
                    @elseif($day =="Martes")
                        && day != 2 
                    @elseif($day =="Miercoles")
                        && day != 3 
                    @elseif($day =="Jueves")
                        && day != 4 
                    @elseif($day =="Viernes")
                        && day != 5 
                    @elseif($day =="Sabado")
                        && day != 6 
                    @endif
                @endforeach
                ), ''];
            };

            //Crear el datepicker
            $('#dateService{{$service->id_service_operator}}').datepicker({
            beforeShowDay: noExcursion,
            });
            $("#dateService{{$service->id_service_operator}}").click(function(event) {
                $("#dateService{{$service->id_service_operator}}").datepicker().show();
            });

            $('#dateService{{$service->id_service_operator}}').change(function(event) 
            {
                var d = new Date();
                var strDate = d.getDate()+ "/" + (d.getMonth()+1) + "/" +  d.getFullYear();
                valuesStart=strDate.split("/");
                 valuesEnd=$(this).val().split("/");
                // Verificamos que la fecha no sea posterior a la actual
                var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
                var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
                if(dateStart<=dateEnd)
                {

                }
                else
                {
                    swal(
                      'Error!!!',
                      'La fecha de agendamiento debe ser superior a hoy.',
                      'error'
                    );
                    $(this).val("");
                }
            });

            $("#cantidad_personas{{$service->id_service_operator}}").change(function()
            {
                if($(this).val()>{{$service->capacity}})
                {
                    swal(
                      'Error!!!',
                      'No puede sobrepasar la capacidad.',
                      'error'
                    );
                    $(this).val("");
                }
                else{

                }
            });
            
            // var day = date.getDay();

            // .datepicker({
            //     beforeShowDay:[(day != 0 && day != 2 && day != 3 && day != 5 && day != 6), '']
            // })

         $(function() {
    
         //Array para dar formato en español
          $.datepicker.regional['es'] = 
          {
          closeText: 'Cerrar', 
          prevText: 'Previo', 
          nextText: 'Próximo',
          
          monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
          'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
          monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
          'Jul','Ago','Sep','Oct','Nov','Dic'],
          monthStatus: 'Ver otro mes', yearStatus: 'Ver otro año',
          dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
          dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
          dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
          dateFormat: 'dd/mm/yy', firstDay: 0, 
          initStatus: 'Selecciona la fecha', isRTL: false};
         $.datepicker.setDefaults($.datepicker.regional['es']);
         
         //miDate: fecha de comienzo D=días | M=mes | Y=año
         //maxDate: fecha tope D=días | M=mes | Y=año
            $( "#datepicker" ).datepicker({ minDate: "-1D", maxDate: "+1M +10D" });
          });
         
        // function noExcursion(date){ 
        //     var day = date.getDay();
        //     // aqui indicamos el numero correspondiente a los dias que ha de bloquearse (el 0 es Domingo, 1 Lunes, etc...) en el ejemplo bloqueo todos menos los lunes y jueves.
        //     return [(day != 0 && day != 2 && day != 3 && day != 5 && day != 6), ''];
        //     };

        //     //Crear el datepicker
        //     $("#fechaSur").datepicker({
        //     beforeShowDay: noExcursion,
        //     });


    </script>

</body>
</html>
