@component('mail::message')

Hola {{$name}}!, gracias por registrarte en **TuPaseo.travel**.

Por favor confirma tu correo electrónico.

Para ello simplemente debes hacer click en el siguiente enlace:

@component('mail::button', ['url' => url("/$typeUser/register/verify/$confirmation_code")])
Confirmar
@endcomponent

@endcomponent
