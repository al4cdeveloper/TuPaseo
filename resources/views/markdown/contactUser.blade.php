@component('mail::message')
# Nuevo mensaje de contacto

El usuario **{{$contact->name}}** envió mensaje mediante el formulario de contacto.

Nombre: **{{$contact->name}}**

Teléfono: **{{$contact->phone}}**

Email: **{{$contact->email}}**

País: **{{$contact->country}}**

Región/Departamento: **{{$contact->region}}**

Ciudad: **{{$contact->city}}**

Mensaje:

{{$contact->message}}
@endcomponent
