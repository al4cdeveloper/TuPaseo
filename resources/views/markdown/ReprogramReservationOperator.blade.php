@component('mail::message')
## Se ha reprogramado la reservación

La reservación realizada para el usuario {{$reservation->orderProduct->order->shipping_order->first_name}} {{$reservation->orderProduct->order->shipping_order->last_name}} del servicio {{$reservation->Service->service_name}} **({{$reservation->Service->PrincipalService->service}})**

La reservación fue reprogramada para:
@component('mail::table')
	
| Fecha   			| Hora    |
| ---------------------	| ------: |
|  {{$reprogram->new_date}}   | {{$reprogram->new_time}} |

@endcomponent

Para más detalles del servicio visita [este link]({{url('/operator/reservas')}}).

@endcomponent
