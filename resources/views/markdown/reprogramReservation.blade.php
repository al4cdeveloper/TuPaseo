@component('mail::message')
## Se ha reprogramado tu reservación

Tu reservación de {{$reservation->Service->service_name}}, anteriormente programada para el día {{$reprogram->previous_date}} a la(s) {{$reprogram->previous_time}}, ha sido modificada por el operador.

Se realiza la modificación por el motivo:

{{$reprogram->reason}}

La reservación fue reprogramada para:
@component('mail::table')
	
| Fecha   			| Hora    |
| ---------------------	| ------: |
|  {{$reprogram->new_date}}   | {{$reprogram->new_time}} |

@endcomponent

Para más detalles del servicio visita [este link]({{url('/user/home')}}).

@endcomponent