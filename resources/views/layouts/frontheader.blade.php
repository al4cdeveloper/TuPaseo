<!DOCTYPE html>

<html lang="es">

<head>
    <!-- Basic Meta -->
    <meta name="encoding" charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Most Important Meta -->
    <meta name="robots" content="index, follow">
    <meta name="description" content="TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia">
    <meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia, escapadas, fin de semana">
    <meta name="author" content="Puntos Suspensivos Editores Consultores Ltda">
    <meta name="creator" content="Al4c Developer">
    <meta name="creator" content="Sergio David Hernández Goyeneche">
    <meta name="creator" content="Euclides Rafael Alsina">
    <title>@yield('title')</title>

    <meta name="keywords" content="@yield('keywords')">
    <!-- Favicons-->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" type="image/x-icon" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        
      <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i" rel="stylesheet">
    @include('alerts.messages')

    <!-- CSS -->


     @yield('additionalStyle')


    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
    </script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
 function hideURLbar(){ window.scrollTo(0,1); }
    </script>
    <!-- termina GTM -->
    <!-- TWITTER OG -->
    <meta name="twitter:creator" content="@" />
    <meta name="twitter:site" content="@" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="TuPaseo.Travel" />
    <meta name="twitter:description" content="TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia" />
    <meta name="twitter:image" content="{{asset('front/img/logo.png')}}" />

    <!-- FACEBOOK OG -->
    <meta property="fb:app_id" content="" /> 
    <meta property="og:type"   content="article" /> 
    <meta property="og:url"    content="http://www.tupaseo.travel/" /> 
    <meta property="og:title"  content="TuPaseo.Travel" /> 
    <meta property="og:image"  content="http://www.tupaseo.travel/img/logo.png" />
    <meta property="og:description" content="TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia" />
    <meta name="author" content="TuPaseo.Travel">

    <!-- Marcado JSON-LD generado por el Asistente para el marcado de datos estructurados de Google. -->
    <script type="application/ld+json">
        {
          "@context" : "http://schema.org",
          "@type" : "LocalBusiness",
          "name" : "TuPaseo.Travel",
          "image" : "http://pseditores.com/seminario/img/logo.png",
          "telephone" : "+57 (1) 3121540",
          "email" : "info@rutascolombia.com",
          "address" : {
            "@type" : "PostalAddress",
            "streetAddress" : "Cra. 9 # 72-81 Oficina 504",
            "addressLocality" : "Bogotá D.C.",
            "addressCountry" : "Colombia"
          },
          "openingHoursSpecification" : {
            "@type" : "OpeningHoursSpecification",
            "dayOfWeek" : {
              "@type" : "DayOfWeek",
              "name" : "8:30 a.m. - 5:30 p.m."
            }
          },
          "url" : "http://www.tupaseo.travel/",
          "review" : {
            "@type" : "Review",
            "author" : {
              "@type" : "Person",
              "name" : "TuPaseo.Travel"
            },
            "reviewBody" : "TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia"
          }
        }
    </script>

</head>
<body >
<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave" id="spinner">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Header================================================== -->

    <header class="app">   
        <Navbar></Navbar>
    </header><!-- End Header -->

    @yield('additionalPreMain')

    @yield('main')

    @include('layouts.footer')
    

    <!-- Common scripts -->
    <script src="{{asset('front/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('front/js/common_scripts_min.js')}}"></script>
    <script src="{{asset('front/js/functions.js')}}"></script>

    <script src="{{asset('/js/vue-app.js')}}"></script>
    
    <script>

        $('a.open_close').on("click",function()
        {
            $(this).toggleClass('active');
            $('.main-menu').toggleClass('show');
        })

        $('a.show-submenu').on("click", function () 
        {
            $(this).next().toggleClass("show_normal");
        });
    </script>

    @yield('additionalScript')
</body>
</html>


