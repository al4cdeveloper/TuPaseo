<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia, Lideres en esacapadas de fines de semana" />
     <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
     </script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script> <!-- termina GTM -->
<style>


.dropdown-content {
    display: none;
    position: absolute;
    background-color: transparent;
    border-radius: 7%;
    border: solid 2px orange;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: orange}

.show {display:block;}
</style>
    <!-- Template Title -->
    <title>@yield('title')</title>

            <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->
  
    <link rel="alternate" href="http://tupaseo.travel" hreflang="es-co" />


    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.css')}}">

    <!-- Bootstrap 3.2.0 stylesheet -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Font Awesome Icon stylesheet -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Owl Carousel stylesheet -->
    <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
    
    <!-- Pretty Photo stylesheet -->
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">

    <!-- Custom stylesheet -->
    <link href="{{asset('style.css')}}" rel="stylesheet">
    
    <link href="{{asset('css/color/white.css')}}" rel="stylesheet">


    <!-- Custom Responsive stylesheet -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

    <!-- jQuery -->

    <link rel="stylesheet" href="{{asset('plugins/jquery-ui-1.12.1/jquery-ui.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">
    <script src="http://code.jquery.com/jquery.min.js"></script>

    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>
    
    @yield('aditionalStyle')

@include('alerts.messages')


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>

      <!-- ====== Menu Section ====== -->
    <section id="menu">
              
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">TuPaseo</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
              <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/tupaseo.png')}}" /></a>
            </div> <!-- end .navbar-header -->              
            
            <div class="">
              <ul class="nav navbar-nav navbar-left">
              <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li><a href="{{url('/')}}">Home</a></li>
                @if(Auth::user()==null && Auth::guard('operator')->user()== null)
                <li><a class="" href="{{url('operator')}}">¿Quieres que vean tu negocio?</a></li>
                @endif
                <li><a href="{{url('contact')}}">Contactános</a></li>
              </ul>
            @if(Auth::user()==null && Auth::guard('operator')->user()== null)
            <div class="social-icons">
              <ul class="nav navbar-nav navbar-rigth">
              <a class="facebook" href="https://www.facebook.com/tupaseo.co/" target="_blank"><i class="fa fa-facebook"></i></a>
              <a class="twitter" href="https://www.instagram.com/tupaseo/" target="_blank"><i class="fa fa-instagram"></i></a>
              <a class="linkedin" href="{{url('contact')}}"><i class="fa fa-envelope"></i></a>    
                <li><a href="{{url('/login')}}">Iniciar Sesión</a></li>
              <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
              </ul>
            </div>
            @else
            <div class="social-icons">
              @if(Auth::user()!=null)
              <ul class="nav navbar-nav pull-right">
                <li><a href="{{url('/home')}}">Mis reservaciones</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" onclick="myFunction()" class="dropbtn">
                        <img src="@if(Auth::user()->avatar) {{asset(Auth::user()->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="avatar" style="width: 40px!important;height: 40px!important;" class="mg-circle img-thumbnail img-thumbnail-avatar-2x">
                        {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}<span class="caret"></span>
                    </a>

                    <div id="myDropdown" class="dropdown-content">
                      <a href="{{url('userprofile')}}">Mi perfil</a>
                      <a href="{{ url('/user/logout') }}"
                              onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                              Cerrar sesión
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                    </div>
            
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ url('/user/logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Cerrar sesión
                            </a>

                            <form id="logout-form" action="{{ url('/user/logout') }}" method="POST" >
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
              </ul>
              @elseif(Auth::guard('operator')->user()!=null)
                <ul class="nav navbar-nav pull-right">
                <li><a href="{{url('operator/reservas')}}">Panel administrativo</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" onclick="myFunction()" class="dropbtn">
                        <img src="@if(Auth::guard('operator')->user()->avatar) {{asset(Auth::guard('operator')->user()->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="avatar" style="width: 40px!important;height: 40px!important;" class="mg-circle img-thumbnail img-thumbnail-avatar-2x">
                        {{ Auth::guard('operator')->user()->name }} <span class="caret"></span>
                    </a>

                    <div id="myDropdown" class="dropdown-content">
                      <a href="{{url('operator/profile')}}">Mi perfil</a>
                      <a href="{{ url('/operator/logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Cerrar sesión
                            </a>

                            <form id="logout-form" action="{{ url('/operator/logout') }}" method="POST" >
                                {{ csrf_field() }}
                            </form>
                    </div>
                </li>
              </ul>
              @endif
              </div>
              @endif
{{--               <ul class="nav navbar-nav navbar-rigth">
              <a class="facebook" href="https://www.facebook.com/tupaseo.co/" target="_blank"><i class="fa fa-facebook"></i></a>
              <a class="twitter" href="https://www.instagram.com/tupaseo/" target="_blank"><i class="fa fa-instagram"></i></a>
              <a class="linkedin" href="{{url('contact')}}"><i class="fa fa-envelope"></i></a>    
                <li><a href="{{url('/login')}}">Iniciar Sesión</a></li>
              <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
              </ul> --}}
            </div>

          </div>
    </section>


    @yield('content')





    <section id="foot">
        <div class="subscribe section-padding">
          <div class="container">
            <div class="col-md-12" align="center">
              <img src="{{asset('images/footerapps.png')}}" class="img-responsive">
            </div>
            <div class="col-md-12" align="center">
              <img src="{{asset('images/footerpcis.png')}}" class="img-responsive">
            </div>
          </div>
        </div>
    </section>
    <!-- ====== Copyright Section ====== -->
    <section class="copyright dark-bg">
      <div class="container">
        <p>&copy; 2017 <a href="{{url('contact')}}">TuPaseo</a>, Todos los derechos reservados</p>
      </div> <!-- end .container -->
    </section>
    <!-- ====== End Copyright Section ====== -->
    
    <!-- Modernizr js -->
    <script src="{{asset('js/modernizr-latest.js')}}"></script>

    <!-- Bootstrap 3.2.0 js -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <!-- Owl Carousel plugin -->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>

    <!-- ScrollTo js -->
    <script src="{{asset('js/jquery.scrollto.min.js')}}"></script>

    <!-- LocalScroll js -->
    <script src="{{asset('js/jquery.localScroll.min.js')}}"></script>

    <!-- jQuery Parallax plugin -->
    <script src="{{asset('js/jquery.parallax-1.1.3.js')}}"></script>

    <!-- Skrollr js plugin -->
    <script src="{{asset('js/skrollr.min.js')}}"></script>

    <!-- jQuery One Page Nav Plugin -->
    <script src="{{asset('js/jquery.nav.js')}}"></script>
    
    <!-- jQuery Pretty Photo Plugin -->
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>


    <!-- Custom JS -->
    <script src="{{asset('js/main.js')}}"></script>

     <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
    <script src="{{asset('auth-panel/js/app.js')}}"></script>


    <script src="{{asset('plugins/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Load and execute javascript code used only in this page -->
    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-input/js/fileinput.js')}}"></script>
    <script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
      
    @yield('additionalScript')
{{--     <script>
      jQuery(document).ready(function($) {
        "use strict";

        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools:false});
        $("html, body").animate({scrollTop: $('#encuesta').offset().top }, 1000);
      });
    </script> --}}
  </body>
</html>