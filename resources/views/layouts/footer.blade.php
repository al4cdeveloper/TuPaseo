
<footer>

    	<div class="container">
    			<div class="col-md-12">
    				<div class="col-md-3 col-sm-6">
    					<h3>¿Necesitas ayuda?</h3>
    					<a href="tel://+573165287931" id="phone">+57 316 528 7931</a>
    					<a href="mailto:info@tupaseo.travel" id="email_footer">info@tupaseo.travel</a>
    				</div>
    				<div class="col-md-2 col-sm-6">
    					<h3>Nosotros</h3>
    					<ul>
    						<li><a href="{{url('about')}}">Qué es Tu Paseo</a></li>
                            <li><a href="{{url('contact')}}">Contacto</a></li>
                            <li><a href="http://rutascolombia.com" target="_blank">rutascolombia.com</a></li>
    					</ul>
    				</div>
    				<div class="col-md-3 col-sm-6">
    					<h3>Más información</h3>
    					<ul>
    						<li><a href="{{url('operator/register')}}">¿Eres operador turístico?</a>
    						</li>
    						<!--<li><a href="#">Preguntas frecuentes</a>
    						</li>-->
                            <li><a href="{{url('politica_proteccion_datos')}}">Política de Privacidad</a></li>
    						<li><a href="{{url('/terminos')}}">Terminos y Condiciones</a>
    						</li>
    					</ul>
    				</div>
    				<div class="col-md-4 col-sm-6">
    					<p><img src="{{asset('front/img/slides/footerapps.png')}}" width="231" height="30" alt="Image" data-retina="true" class="img-responsive">
    					</p>
    					<strong>Transacciones seguras gracias a</strong>
    					<p><img src="{{asset('front/img/payments.png')}}" width="231" height="30" alt="Image" data-retina="true" class="img-responsive">
    					</p>
    					
    				</div>
    			</div>
    			<!-- End row -->
    			<div class="row">
    				<div class="col-md-12">
    					<div id="social_footer">
    						<ul>
    							<li><a href="https://www.facebook.com/tupaseoColombia/" target="_blank"><i class="icon-facebook"></i></a>
    							</li>
    							<li><a href="https://www.instagram.com/tupaseo/" target="_blank"><i class="icon-instagram"></i></a>
    							</li>
    							<!--<li><a href="#"><i class="icon-youtube-play"></i></a>
                                </li>-->
                                <li><a href="{{url('/contact')}}"><i class="icon-mail"></i></a>
    							</li>
    						</ul>
    						<p>© Puntos Suspensivos Editores 2018</p>
    					</div>
    				</div>
    			</div>
    			<!-- End row -->
    	</div> <!-- End container -->
	</footer>