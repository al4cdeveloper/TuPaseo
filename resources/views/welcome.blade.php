 @extends('layouts.frontheader')

    @section('title','Inicio')
    @section('additionalStyle')
    <link href="{{asset('front/css/date_time_picker.css')}}" rel="stylesheet">
    
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/tabs_home.css')}}" rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('mystyle.css')}}">

    <!-- SPECIFIC CSS -->
    <link rel="stylesheet" href="{{asset('front/css/weather.css')}}">
        <!-- Radio and check inputs -->
    <link href="{{asset('front/css/skins/square/grey.css')}}" rel="stylesheet">

    <!-- Range slider -->
    <link href="{{asset('front/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
    <style>
        .marker_info
        {
            height: 420px!important;
        }
        iframe.pattern{
            height: 450px !important;
            width: 100% !important;
            position: relative!important;
        }
        .modal-backdrop
        {
            display: none!important;
        }
        .intro_title
        {
            display: initial!important;
        }
 @media only screen and (max-width: 600px)
 {
        .textHiddenSmall
        {
            display: none!important;
        }
        .buttonBanner
        {
            font-size: 200%!important;
        }
        .button_intro, .button_intro
        {
            padding: 6px 23px!important;
        }
        .tp-caption
        {
            transform: none!important;
        }
 }

    </style>
    @endsection

@section('main')
    <div class="app">
      <router-view></router-view>
    </div>
    <!--<button type="button"  class="btn btn-primary" onclick="Spinner()">Spinner</button>-->

@endsection

@section('additionalScript')



 <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script id="hola1" src="{{asset('front/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
    <script id="hola2"  src="{{asset('front/rs-plugin/js/jquery.themepunch.revolution.js')}}"></script>
<!-- Date and time pickers -->
<script src="{{asset('front/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('front/js/bootstrap-timepicker.js')}}"></script>
<!--WEATHER-->
<script src="{{asset('front/js/jquery.simpleWeather.min.js')}}"></script>
    <script>
        $('#cmn-toggle-switch').removeClass('active');
        function weather(municipality)
        {
            var municipio = municipality;
        
            $.simpleWeather({
                location: municipio+', Colombia',
                woeid: '',
                unit: 'c',
                success: function (weather) {
                    html ='<hr class="add_bottom_45"><div class="main_title"><h2><span>Pronóstico </span>del tiempo en '+municipio+'</h2><p>Consulta el pronóstico del clima para tu destino.</p></div>';
                    html +='<div class="clearfix">'
                    html += '<h4><i class="weathericon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h4>';
                    html += '<ul><li>' + weather.city + ', ' + weather.region + '</li>';
                    html += '<li class="currently">' + weather.currently + '</li>';
                    html += '<li>' + weather.wind.direction + ' ' + weather.wind.speed + ' ' + weather.units.speed + '</li></ul>';
                    html += '</div>';

                    $("#weather").html(html);
                },
                error: function (error) {
                    $("#weather").append('<p>' + error + '</p>');
                }
            });
            
        };
    </script>
 
<script type="text/javascript">

    function banner()
    {
        
        setTimeout(function()
        {
            jQuery('.tp-banner').show().revolution({
                        dottedOverlay: "none",
                        delay: 16000,
                        startwidth: 1170,
                        startheight: 550,
                        hideThumbs: 200,

                        thumbWidth: 100,
                        thumbHeight: 50,
                        thumbAmount: 5,

                        navigationType: "bullet",
                        navigationArrows: "solo",
                        navigationStyle: "none",

                        touchenabled: "on",
                        onHoverStop: "on",

                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,

                        parallax: "mouse",
                        parallaxBgFreeze: "on",
                        parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

                        keyboardNavigation: "off",

                        navigationHAlign: "center",
                        navigationVAlign: "bottom",
                        navigationHOffset: 0,
                        navigationVOffset: 20,

                        soloArrowLeftHalign: "left",
                        soloArrowLeftValign: "center",
                        soloArrowLeftHOffset: 20,
                        soloArrowLeftVOffset: 0,

                        soloArrowRightHalign: "right",
                        soloArrowRightValign: "center",
                        soloArrowRightHOffset: 20,
                        soloArrowRightVOffset: 0,

                        shadow: 0,
                        fullWidth: "on",
                        fullScreen: "off",

                        spinner: "spinner4",

                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,

                        shuffle: "off",

                        autoHeight: "off",
                        forceFullWidth: "on",

                        hideThumbsOnMobile: "off",
                        hideNavDelayOnMobile: 1500,
                        hideBulletsOnMobile: "off",
                        hideArrowsOnMobile: "on",
                        hideThumbsUnderResolution: 0,

                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 480,
                        hideAllCaptionAtLilmit: 480,
                        startWithSlide: 0,
                        videoJsPath: "rs-plugin/videojs/",
                        fullScreenOffsetContainer: "",
                        hideSliderAtLimit:0
                    });
        },50);
       
    }
</script>
 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw" ></script>
    <script type="text/javascript" src="{{asset('front/js/infobox.js')}}"></script>

<script id="newScripts">

        function Spinner()
        {
            $("#preloader").css('display', 'initial');
            setTimeout(function()
            {
                $("#preloader").css('display', 'none');
            },1000);

        }
        function setpoints(object,typeIcon)
        {
            var
            mapObject,
            markers = [];

            var funciona;
            var mapOptions = {
                zoom: 14,
                center: new google.maps.LatLng(48.865633, 2.321236),
                mapTypeId: google.maps.MapTypeId.ROADMAP,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                styles: [
                             {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "hue": "#FFBB00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "hue": "#FFC200"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "stylers": [
                        {
                            "hue": "#FF0300"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "stylers": [
                        {
                            "hue": "#00FF6A"
                        },
                        {
                            "saturation": -1.0989010989011234
                        },
                        {
                            "lightness": 11.200000000000017
                        },
                        {
                            "gamma": 1
                        }
                    ]
                }
                ]
            };
            var marker;
            mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
            object.forEach(function (item) {

                if(typeIcon == 'municipality' || typeIcon == 'hotel' || typeIcon=='restaurant')
                {
                     marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(item.latitude, item.longitude),
                                    map: mapObject,
                                    icon: '/front/img/pins/'+typeIcon+'.png',
                                });
                }
                else
                {
                    marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(item.latitude, item.longitude),
                                    map: mapObject,
                                    icon: '/front/img/pins/default.png',
                                });
                }
               
                mapObject.setCenter(new google.maps.LatLng(item.latitude, item.longitude));
                 google.maps.event.addListener(marker, 'click', (function () {
                  closeInfoBox();
                    if(typeIcon == 'municipality')
                    {
                        getInfoBox(item).open(mapObject, this);
                    }
                    else if(typeIcon == 'hotel' || typeIcon=='restaurant')
                    {
                        getInfoBoxEstablishment(item).open(mapObject, this);
                    }
                    else
                    {
                        getInfoBoxService(item).open(mapObject, this);
                    }
                 }));
            })
        }

        function closeInfoBox() {
            $('div.infoBox').remove();
        };

        function getInfoBox(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.link_image + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.municipality_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function getInfoBoxService(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.image_link + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.service_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function getInfoBoxEstablishment(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="/' + item.link_image + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.establishment_name +'</h3>' +
                '<span>'+ item.description.substr(0,110) +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ '" type="hidden"><input type="hidden" name="daddr" value="'+ item.latitude +',' +item.longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">Como llegar</button></form>' +
                    //'<a href="tel://'+  +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    //'<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });
        };

        function specificCarousel()
        {
            setTimeout(function()
                {
                $('#Img_carousel').sliderPro({
                    width: 960,
                    height: 500,
                    fade: true,
                    arrows: true,
                    buttons: false,
                    fullScreen: false,
                    smallSize: 500,
                    startSlide: 0,
                    mediumSize: 1000,
                    largeSize: 3000,
                    thumbnailArrows: true,
                    autoplay: false
                });
                },200);
        }
</script>   
    <script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
    <script type="text/javascript" src="js/infobox.js"></script>
        <!-- Common scripts -->
    <script src="{{asset('front/js/tabs.js')}}"></script>
    <script src="{{asset('front/js/cat_nav_mobile.js')}}"></script>
    <script src="{{asset('front/js/icheck.js')}}"></script>

@endsection