@extends('layouts.frontheader')

@section('title','Acerca de nosotros | Tu Paseo')
 @section('additionalStyle')

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('mystyle.css')}}">


    @endsection
@section('main')   
    <!-- Slider -->
<!-- Slider -->
    <main>
         <!-- Section -->
        <section id="hero_2" style="background: #4d536d url({{asset('/front/img/slide_hero_2.jpg')}}) no-repeat center center;">
            <div class="intro_title animated fadeInDown">
                <h1>TUPASEO.TRAVEL</h1>
                <p>Plataforma digital que conecta operadores turísticos con viajeros</p>
            </div>
        </section> <!-- End Section -->


        <!-- Position -->
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="/home">Home</a></li>
                    <li>Terminos y condiciones</li>
                </ul>
            </div>
        </div> <!-- End Position -->
        
<div class="container margin_60">

            <div class="main_title">
                <h2><span>Conéctate</span> con los amantes de la aventura</h2>
                <p> </p>
            </div>

            <div class="row">
                <div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
                    <div class="feature">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span>Qué</span> somos</h3>
                        <p>
                            TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">
                    <div class="feature">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>Qué</span> hacemos</h3>
                        <p>
                            TuPaseo pone a disposición de los pequeños y micro operadores turísticos un sistema que les ayuda a organizar su disponibilidad de operación y capacidad de servicio.
                        </p>
                    </div>
                </div>
            </div> <!-- End row -->

            <div class="row">
                <div class="col-md-6 wow fadeIn" data-wow-delay="0.3s">
                    <div class="feature">
                        <i class="icon_set_1_icon-57"></i>
                        <h3><span>¿Cómo </span>lo hacemos?</h3>
                        <p>
                            A tráves de una búsqueda ágil y rápida, los viajeros pueden conocer la oferta de actividades extremas, los lugares y el operador turístico con quien realizarlas.<br>
                            <br>
                            Una vez realizada la elección, tiene la opción de reservar y pagar en línea.<br>
                            <br>
                            El operador turístico, a través de una herramienta digital, puede revisar las reservas, organizar su agenda y administrar los servicios que tenga ofertados en la plataforma.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
                    <div class="feature">
                        <i class="icon_set_1_icon-61"></i>
                        <h3><span>Para quién</span> lo hacemos</h3>
                        <p>
                            Nuestra propuesta está dirigida a turistas que quieren tomar una decisión de compra con conocimiento sobre los servicios, experiencia y beneficios que los medianos y pequeños operadores turísticos tienen para ofrecerles.
                        </p>
                    </div>
                </div>
            </div> <!-- End row -->

            <hr>
            <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 nopadding features-intro-img">
                    <div class="features-bg" style="background: #4d536d url({{asset('/front/img/features-intro-01.jpg')}}) no-repeat center center;">
                        <div class="features-img"></div>
                    </div>
                </div>
                <div class="col-md-6 nopadding">
                    <div class="features-content">
                        <h3>"La vida no se mide por las veces que respiras, sino por aquellos momentos que te dejan sin aliento"</h3>
                        <p></p>
                        <p><a href="{{url('home/services')}}" class=" btn_1 white">Ver Experiencias Únicas</a>
                        </p>
                    </div>
                </div>
            </div>
        </div> <!-- End container-fluid  -->

        </div> <!-- End container -->

</main>
@endsection