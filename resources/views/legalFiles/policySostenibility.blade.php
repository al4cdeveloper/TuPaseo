@extends('layouts.frontheader')

@section('title','Política de Sostenibilidad | Tu Paseo')
 @section('additionalStyle')
    <link href="{{asset('front/css/date_time_picker.css')}}" rel="stylesheet">
    
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/tabs_home.css')}}" rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('mystyle.css')}}">


    @endsection
@section('main')   
    <!-- Slider -->
<!-- Slider -->
    <main>
         <!-- Section -->
        <section id="hero_2">
            <div class="intro_title animated fadeInDown" style="background: #4d536d url({{asset('/front/img/slide_hero_2.jpg')}}) no-repeat center center;">
                <h1>Política de Sostenibilidad</h1>
                <p>Comprometidos con el desarrollo turístico sostenible</p>
            </div>
        </section> <!-- End Section -->


        <!-- Position -->
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="/home">Home</a></li>
                    <li>Política de Sostenibilidad</li>
                </ul>
            </div>
        </div> <!-- End Position -->


        <div class="container margin_60">

            <section class="margin_60">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="row">
                        <p>En <strong>TU PASEO SAS</strong> nos comprometemos con el control de las actividades, productos o servicios que puedan afectar aspectos ambientales, socioculturales o econ&oacute;micos y buscamos prevenir, eliminar o reducir el impacto de nuestras instalaciones y actividades en los destinos que ofertamos en nuestro portafolio de servicios, as&iacute; como optimizar la sostenibilidad tur&iacute;stica mejorando nuestro comportamiento con el entorno.</p>
<p>Conscientes de la importancia de mantener un desarrollo tur&iacute;stico sostenible y en cumplimiento de la NTS &ndash; TS 003, nos comprometemos a adoptar compromisos orientados a cumplir con los lineamientos de sostenibilidad en:</p>
<ol>
<li>Implementar buenas pr&aacute;cticas ambientales con el fin de hacer uso racional del agua, la energ&iacute;a, utilizaci&oacute;n de papel reciclado y separaci&oacute;n de residuos, involucrando a todo el equipo de trabajo de la agencia.</li>
<li>Promover la visita, la preservaci&oacute;n y conservaci&oacute;n de los atractivos tur&iacute;sticos regionales y nacionales que hagan parte del patrimonio natural y cultural del pa&iacute;s.</li>
<li>Informar a los visitantes de abstenerse a participar en el tr&aacute;fico il&iacute;cito de bienes culturales y especies de flora y fauna.</li>
<li>Rechazar la explotaci&oacute;n laboral infantil.&nbsp;</li>
<li>Vincular personal de la comunidad local sin discriminaci&oacute;n de edad, raza, sexo, g&eacute;nero o discapacidad, en condiciones justas y equitativas.</li>
<li>Informar y hacer part&iacute;cipes a colaboradores, clientes y proveedores de los compromisos asumidos por TU PASEO SAS frente a la sostenibilidad.&nbsp;</li>
<li>Se proh&iacute;be todo tipo de discriminaci&oacute;n racial, social, religiosa, etc.</li>
<li>Nos comprometemos a motivar a nuestro personal con acciones formativas y de concientizaci&oacute;n, sobre los principios del turismo sostenible, a promover las buenas pr&aacute;cticas medioambientales en el entorno, participar en actividades externas, e informar tanto interna como externamente los avances y actuaciones medioambientales de la empresa.</li>
<li>Se someter&aacute;n los proyectos de futuras ampliaciones de las instalaciones o actividades a criterios de sostenibilidad y eficiencia en el uso los recursos.</li>
<li>Promover la compra y el consumo de bienes y servicios producidos y comercializados por la comunidad local de los destinos operados.</li>
<li>Esta pol&iacute;tica de turismo sostenible se actualizar&aacute; siempre que las circunstancias lo requieran, adoptando nuevos objetivos de sostenibilidad.</li>
<li>Se restringe la contrataci&oacute;n de manera directa e indirecta a menores de edad.</li>
<li>Manejar de manera respetuosa la informaci&oacute;n acerca de los diferentes atractivos y actividades relacionadas con el patrimonio cultural del pa&iacute;s y promover su visita.</li>
</ol>
<p>Sabemos que el contacto con la naturaleza, el intercambio con comunidades locales y la convivencia con los compa&ntilde;eros, estimulan habilidades para el desarrollo del ser humano y que enfrentar retos y conquistar metas estimulan f&iacute;sica y psicol&oacute;gicamente a los turistas, formando su car&aacute;cter y fortaleciendo su autoestima, por eso nuestros planes est&aacute;n cargados de experiencias cargadas de adrenalina que conducen al viajero a tener un contacto directo con la naturaleza y la diversidad cultural colombiana.</p>
<p><strong>TU PASEO SAS</strong> rechaza toda clase de explotaci&oacute;n y violencia sexual infantil, en cumplimiento de los art&iacute;culos 16 y 17 de la Ley 679 de 2001 y 1336 de 2009. Adem&aacute;s, advertimos a operadores y turistas que este tipo de comportamientos son sancionados penal y administrativamente, conforme a las disposiciones legales vigentes en Colombia.</p>
<p>Apoyamos el comportamiento responsable con el entorno e invitamos a operadores y turistas a protejer la fauna y la flora y evitar su comercializaci&oacute;n de forma ilegal, seg&uacute;n lo establecido en la ley 17 de 1981 y la ley 299 de 1996.</p>
<p>Estamos comprometidos contra el tr&aacute;fico del patrimonio cultural, Ley 397 de 1997.</p>
                    </div>

                    <div class="row">


<h4><strong>DERECHOS Y DEBERES DE COLABORADORES, PROVEEDORES Y CLIENTES CON LA SOSTENIBILIDAD</strong></h4>
                        <P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Se
establecen los siguientes Derechos y Deberes para los Colaboradores,
Proveedores y Clientes de TU PASEO SAS:</FONT></P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
    <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><B>Colaboradores</B></FONT></P>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
    <OL>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><I><B>Derechos</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            un pago justo y oportuno por los servicios suministrados.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocer
            los proyectos e iniciativas que desarrolla la Agencia en cuanto a
            temas de sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Ser
            vinculado a los proyectos e iniciativas de la Agencia.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            retroalimentación sobre la prestación de sus servicios en
            aspectos sostenibles.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Seguridad
            en las instalaciones de la Agencia.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Respeto
            y trato amable durante el desarrollo de sus actividades dentro de
            la Agencia.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            información o capacitación para aprender a utilizar materiales
            peligrosos.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            bonificaciones por las capacitaciones certificadas que haya
            recibido en sostenibilidad ambiental.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            bonificaciones y/o incentivos por las capacitaciones certificadas
            que haya recibido contra la Explotación Sexual Comercial de
            Niños, Niñas y Adolescentes, la explotación laboral infantil y
            cualquier práctica discriminatoria.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
    <OL START=2>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><I><B>Deberes</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Reducir
            el consumo de electricidad, agua, gas, papel y detergentes
            haciendo un uso responsable de los mismos.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Clasificar
            y recolectar de forma adecuada los residuos sólidos, líquidos y
            orgánicos con el fin de que sean reutilizados o recuperados.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>No
            arrojar comida ni residuos a los desagües.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Informar
            eventuales fugas de los equipos sanitarios o disfunciones de los
            mismos y los equipos eléctricos y de refrigeración.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Reducir
            la potencia o apagar los equipos que no se estén utilizando.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Rechazar
            el ESCNNA (Explotación Sexual Comercial de Niños, Niñas y
            Adolescentes), la explotación laboral infantil y cualquier
            práctica discriminatoria.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Atender
            la norma legal vigente respecto al tráfico ilícito de especies
            de fauna y flora.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocimiento
            y puesta en práctica de los requisitos legales con relación a la
            sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Cumplir
            con las directrices de la Agencia relacionadas con la
            Sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Mantenerse
            informado sobre los avances y nuevas prácticas de Sostenibilidad
            en la Agencia de Viajes.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Informar
            oportunamente sobre irregularidades o incumplimientos de los
            compromisos adquiridos en torno a la Sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Participar
            activamente del programa de capacitaciones implementado por la
            Agencia.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Realizar
            sugerencias para el fortalecimiento de los programas de
            Sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Brindar
            de manera responsable información acerca de los atractivos del
            patrimonio natural y cultural del orden Nacional y regional.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Brindar
            información sobre la normatividad legal vigente, sobre el tráfico
            ilícito de flora y fauna, bienes culturales y explotación sexual
            y comercial con niños y niñas adolescentes.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Participar
            de acciones sociales y benéficas convocadas por la Agencia de
            Viajes</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><B>Clientes</B></FONT></P>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><I><B>Derechos</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            los servicios de conformidad con las condiciones convenidas.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Contar
            con asistencia médica y seguro contra accidentes provisto por los
            proveedores de la Agencia, durante la prestación de los servicios
            adquiridos.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocer
            las políticas, acciones, proyectos e iniciativas que desarrolla
            la agencia en cuanto a la Sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            respeto y trato amable en la prestación de los servicios y
            contacto con el personal de la Agencia.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>El
            respeto a sus creencias religiosas, filosóficas y morales.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocer
            la información de los requisitos legales aplicables a las
            actividades de turismo que va a realizar.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocer
            las políticas y lineamientos de la prestación de servicios del
            TU PASEO SAS.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocer
            toda la información respecto a la seguridad, rutas de evacuación,
            punto de encuentro y plan de emergencias de las oficinas de la
            Agencia.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL START=2>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><I><B>Deberes</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            y apropiarse de las directrices informadas por la Agencia
            relacionadas con la sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Hacer
            un uso eficiente de los recursos naturales como agua, energía y
            gas.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Respetar
            y acatar las recomendaciones sobre el patrimonio natural y
            cultural de los destinos promovidos por la Agencia.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Tener
            conocimiento y poner en práctica los requisitos legales
            informados por la Agencia respecto al ESCNNA, Patrimonio Natural y
            Patrimonio Cultural.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Cumplir
            con los lineamientos de la Agencia, relacionados con la prestación
            de los servicios.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Aportar
            sugerencias a la Agencia para el fortalecimiento de los programas
            de sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Respetar
            la tranquilidad de las otras personas que lo acompañen en la
            practica de las actividades turísticas.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>No
            ingresar materiales inflamables o explosivos a las oficinas de TU
            PASEO SAS, sus proveedores o a los lugares turísticos que visita.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Atender
            la norma legal vigente respecto al tráfico ilícito de especies
            de fauna y flora.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><B>Proveedores</B></FONT></P>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><I><B>Derechos</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            un pago justo y oportuno por los servicios suministrados.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Conocer
            las políticas, acciones, proyectos e iniciativas que desarrolla
            la Agencia en cuanto a la Sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            respeto y trato amable en la prestación de los servicios y
            contacto con el personal de la Agencia.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL START=2>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT><I><B>Deberes</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Recibir
            y apropiarse de las directrices informadas por la Agencia
            relacionadas con la sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Tener
            conocimiento y poner en práctica los requisitos legales
            informados por la Agencia respecto al ESCNNA, Patrimonio Natural y
            Patrimonio Cultural.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Aportar
            sugerencias a la Agencia para el fortalecimiento de los programas
            de sostenibilidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Hacer
            un uso eficiente de los recursos naturales como agua, energía y
            gas.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Presentar
            documentación solicitada por la Agencia de Viajes para verificar
            su idoneidad y legalidad.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Atender
            la norma legal vigente respecto al tráfico ilícito de especies
            de fauna y flora.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT>Respetar
            y acatar las recomendaciones sobre el patrimonio natural y
            cultural de los destinos promovidos por la Agencia.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>

                    </div>
                    <div class="row">
                        
    <P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<FONT>In <strong>TU PASEO SAS </strong> we are committed to
the control of activities, products or services that may affect
environmental, sociocultural or economic aspects and we seek to
prevent, eliminate or reduce the impact of our facilities and
activities in the destinations that we offer in our portfolio of
services, as well as optimizing tourism sustainability by improving
our behavior with the environment.</FONT></P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<BR>
</P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<FONT>Aware of the importance of maintaining
a sustainable tourism development and in compliance with the NTS - TS
003, we commit ourselves to adopt commitments aimed at complying with
the sustainability guidelines in:</FONT></P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<BR>
</P>
<OL>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Implement good environmental
    practices in order to make rational use of water, energy, use of
    recycled paper and separation of waste, involving the entire work
    team of the agency.</FONT></P>
</OL>
<OL START=2>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Promote the visit, preservation and
    conservation of regional and national tourist attractions that are
    part of the country's natural and cultural heritage.</FONT></P>
</OL>
<OL START=3>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Inform visitors of abstaining from
    participating in illicit trafficking in cultural property and flora
    and fauna species.</FONT></P>
</OL>

<OL START=4>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Reject child labor exploitation.</FONT></P>
</OL>

<OL START=5>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Link personnel from the local
    community without discrimination of age, race, sex, gender or
    disability, in fair and equitable conditions.</FONT></P>
</OL>

<OL START=6>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Inform and involve employees,
    customers and suppliers of the commitments assumed by TU PASEO SAS
    against sustainability.</FONT></P>
</OL>

<OL START=7>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>All types of racial, social,
    religious discrimination, etc. are prohibited.</FONT></P>
</OL>

<OL START=8>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>We commit ourselves to motivate our
    staff with training and awareness actions on the principles of
    sustainable tourism, to promote good environmental practices in the
    environment, participate in external activities, and inform both
    internally and externally the progress and environmental actions of
    the company.</FONT></P>
</OL>

<OL START=9>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>The projects of future extensions of
    the facilities or activities will be submitted to criteria of
    sustainability and efficiency in the use of resources.</FONT></P>
</OL>

<OL START=10>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Promote the purchase and consumption
    of goods and services produced and marketed by the local community
    of the operated destinations.</FONT></P>
</OL>


<OL START=11>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>This sustainable tourism policy will
    be updated whenever circumstances require, adopting new
    sustainability objectives.</FONT></P>
</OL>

<OL START=12>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Contracting directly and indirectly
    to minors.</FONT></P>
</OL>

<OL START=13>
    <LI><P  CLASS="western" ALIGN=LEFT >
    <FONT>Respectfully manage information about
    the different attractions and activities related to the country's
    cultural heritage and promote their visit.</FONT></P>
</OL>

<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<FONT>We know that the contact with nature,
the exchange with local communities and the coexistence with the
companions, stimulate skills for the development of the human being
and that facing challenges and conquering goals physically and
psychologically stimulate the tourists, forming their character and
strengthening their self-esteem That's why our plans are full of
adrenaline-laden experiences that lead the traveler to have direct
contact with nature and Colombian cultural diversity.</FONT></P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<BR>
</P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<FONT>TU PASEO SAS rejects any kind of
exploitation and sexual violence against children, in compliance with
articles 16 and 17 of Law 679 of 2001 and 1336 of 2009. In addition,
we warn operators and tourists that this type of behavior is
criminally and administratively sanctioned, in accordance with the
legal provisions in force in Colombia.</FONT></P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<BR>
</P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<FONT>We support responsible behavior with
the environment and invite operators and tourists to protect the
fauna and flora and prevent their commercialization illegally,
according to the provisions of Law 17 of 1981 and Law 299 of 1996.</FONT></P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<BR>
</P>
<P  CLASS="western" ALIGN=LEFT STYLE="margin-bottom: 0in">
<FONT>We are committed against the
trafficking of cultural heritage, Law 397 of 1997.</FONT></P>

                    </div>
                    <div class="row">
<h4><strong>RIGHTS AND DUTIES BY COLLABORATORS, SUPPLIERS AND CUSTOMERS WITH THE SUSTAINABILITY</strong></h4>
                        

                        <P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >The following Rights and Duties are established for the Collaborators, Suppliers and Customers of TU PASEO SAS:</FONT></P>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
    <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><B>Collaborators</B></FONT></P>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
    <OL>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><I><B>Rigths</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive a fair and timely payment for the services provided.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >To know the projects and initiatives developed by the Agency in terms of sustainability issues.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Be linked to the projects and initiatives of the Agency.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive feedback on the provision of its services in sustainable aspects.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Security in the facilities of the Agency.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Respect and kind treatment during the development of their activities within the Agency.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive information or training to learn how to use hazardous materials.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive bonuses for certified training received in environmental sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive bonuses and / or incentives for certified training received against the Commercial Sexual Exploitation of Children and Adolescents, child labor exploitation and any discriminatory practices.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
    <OL START=2>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><I><B>Duties</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Reduce the consumption of electricity, water, gas, paper and detergents by making responsible use of them.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Classify and collect in an appropriate manner solid, liquid and organic waste in order to be reused or recovered.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Do not throw food or waste into the drains.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Report possible leaks of the sanitary equipment or dysfunctions thereof and the electrical and refrigeration equipment.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Reduce the power or turn off equipment that is not being used.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Reject ESCNNA (Commercial Sexual Exploitation of Children and Adolescents), child labor exploitation and any discriminatory practice.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >To comply with the current legal norm regarding the illicit trafficking of species of fauna and flora.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Knowledge and implementation of the legal requirements in relation to sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Comply with Agency guidelines related to Sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Keep informed about the progress and new Sustainability practices in the Travel Agency.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Report opportunely about irregularities or breaches of the commitments acquired around Sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Actively participate in the training program implemented by the Agency.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Make suggestions for the strengthening of Sustainability programs.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Provide responsibly information about the attractions of the natural and cultural heritage of the national and regional order.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Provide information on the current legal regulations on illicit trafficking in flora and fauna, cultural goods and sexual and commercial exploitation of adolescent boys and girls.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Participate in social and charitable actions called by the Travel Agency</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><B>Customers</B></FONT></P>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><I><B>Rights</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive the services in accordance with the agreed conditions.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Have medical assistance and accident insurance provided by the Agency's suppliers during the provision of the services acquired.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Know the policies, actions, projects and initiatives developed by the agency in terms of Sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive respect and kind treatment in the provision of services and contact with Agency staff.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Respect for their religious, philosophical and moral beliefs.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >To know the information of the legal requirements applicable to the tourism activities that will be carried out.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Know the policies and guidelines for the provision of services of the TU PASEO SAS.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Know all the information regarding security, evacuation routes, meeting point and emergency plan of the offices of the Agency.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL START=2>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><I><B>Duties</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive and appropriate the guidelines reported by the Agency related to sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Make an efficient use of natural resources such as water, energy and gas.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Respect and abide by the recommendations on the natural and cultural heritage of the destinations promoted by the Agency.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Have knowledge and put into practice the legal requirements informed by the Agency regarding CSEC, Natural Heritage and Cultural Heritage.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Comply with the guidelines of the Agency, related to the provision of services.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Contribute suggestions to the Agency for the strengthening of sustainability programs.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Respect the tranquility of the other people who accompany him in the practice of tourist activities.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Do not enter flammable or explosive materials at the offices of TU PASEO SAS, its suppliers or the tourist places it visits.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Comply with the current legal norm regarding the illicit traffic of species of fauna and flora.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><B>Suppliers</B></FONT></P>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><I><B>Rights</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive a fair and timely payment for the services provided.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Know the policies, actions, projects and initiatives developed by the Agency in terms of Sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive respect and kind treatment in the provision of services and contact with Agency staff.</FONT></P>
        </OL>
    </OL>
</OL>
<P  CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
<OL START=2>
    <OL START=2>
        <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT ><I><B>Duties</B></I></FONT></P>
        <OL>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Receive and appropriate the guidelines reported by the Agency related to sustainability.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Have knowledge and put into practice the legal requirements informed by the Agency regarding CSEC, Natural Heritage and Cultural Heritage.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Contribute suggestions to the Agency for the strengthening of sustainability programs.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Make an efficient use of natural resources such as water, energy and gas.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Submit documentation requested by the Travel Agency to verify its suitability and legality.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >To comply with the current legal norm regarding the illicit trafficking of species of fauna and flora.</FONT></P>
            <LI><P  CLASS="western" STYLE="margin-bottom: 0in"><FONT >Respect and abide by the recommendations on the natural and cultural heritage of the destinations promoted by the Agency.</FONT></P>
        </OL>
    </OL>
</OL>
                    </div>
                </div>
            </section>
        </div>
        <!-- End Container -->

</main>
@endsection