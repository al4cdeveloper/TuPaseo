@extends('admin.layout.auth')

@section('title', 'Detalle de servicio')


@section('content')
 <div id="page-content">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Detalle de {{$service->PrincipalService->service}} - {{$service->location}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block full">

            <div class="row">
            	<div class="col-sm-2">
            		<strong>Descripcion</strong>
            	</div>
            	<div class="col-sm-10">
            		{!!$service->description!!}
            	</div>
            </div>
            <div class="row">
            	<div class="col-sm-2">
            		<strong>Recomendaciones</strong>
            	</div>
            	<div class="col-sm-10">
            		{!!$service->requisites!!}
            	</div>
            </div>
            <div class="row">
            	<div class="col-sm-2">
            		<strong>Costo</strong>
            	</div>
            	<div class="col-sm-10">
            		${{$service->cost}}
            	</div>
            </div>
            <div class="row">
            	<div class="col-sm-2">
            		<strong>Calificaciones</strong>
            	</div>
            	<div class="col-sm-10">
            		<div class="text-warning">
                        @if(count($comments)>0)
                        <a href="#" data-toggle="modal" data-target="#comments">
                        <span class="rating" data-default-rating="{{$service->average}}" disabled style="color:rgb(255, 205, 5)"></span>
                        <span>{{count($comments)}} calificaciones</span>
                        </a>
                        @else
                        <span class="rating" data-default-rating="5" disabled style="color:rgb(255, 205, 5)"></span>
                        @endif
                    </div>
            	</div>
            </div>

        </div>

 </div>
@endsection