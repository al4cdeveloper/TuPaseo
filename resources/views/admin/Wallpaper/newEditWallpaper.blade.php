@extends('admin.layout.auth')

@if(isset($wall))
    @section('title', 'Editar wallpaper')
@else
    @section('title', 'Crear wallpaper ')
@endif
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Wallpaper</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($wall))
            {!!Form::model($wall,['url'=>['admin/wallpapers/update',$wall->id_wallpaper],'method'=>'PUT', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'])!!}
            @else
            {!!Form::open(['url'=>'admin/wallpapers/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm'])!!}
            @endif
            <div class="col-sm-12">

                 <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Wallpaper</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                    <label >Texto principal </label>
                        {!!Form::text('primary_text',null,['class'=>'form-control'])!!}
                        <span class="label label-danger">{{$errors->first('primary_text') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-md-offset-1 col-xs-12">
                    <div class="form-group">
                    <label >Texto secundario (opcional) </label>
                        {!!Form::text('secundary_text',null,['class'=>'form-control'])!!}
                        <span class="label label-danger">{{$errors->first('secundary_text') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="costo">Titulo de botón 1</label>
                        {!!Form::text('first_text_button', null, ['class'=>'form-control', 'placeholder' => 'nombre del primer botón', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('first_text_button') }}</span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="form-group">
                        <label for="costo">URL de redirección botón 1</label>
                        {!!Form::text('first_url', null, ['class'=>'form-control', 'placeholder' => 'Ingrese URL de redirección de botón', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('first_url') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="costo">Titulo de botón 2 (opcional)</label>
                        {!!Form::text('second_text_button', null, ['class'=>'form-control', 'placeholder' => 'nombre del segundo botón'])!!}
                        <span class="label label-danger">{{$errors->first('second_text_button') }}</span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="form-group">
                        <label for="costo">URL de redirección botón 2 (opcional)</label>
                        {!!Form::text('second_url', null, ['class'=>'form-control', 'placeholder' => 'Ingrese URL de redirección de botón'])!!}
                        <span class="label label-danger">{{$errors->first('second_url') }}</span>
                    </div>
                </div>
            </div>

            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/wallpapers')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>
    
<script type="text/javascript">
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($wall) && $wall->link_image!=null)
        defaultPreviewContent: '<img src="{{asset($wall->link_image)}}" alt="Wallpaper por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Wallpaper por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
</script>
@endsection