@extends('admin.layout.auth')

@section('title', ($type=='hotel') ? "Lista de Hoteles" : "Lista de Restaurantes")


@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <div class="pull-right">
                            <a href="{{url("admin/operators")}}" class="btn btn-primary"> Volver a la lista de operadores</a>
                        </div>
                        <h1>Lista de @if($type=='hotel') Hoteles @else Restaurantes @endif del operador {{$operator->name}}</h1>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="block" id="service">
            
            <table id="datatable" class="table" role="grid" aria-describedby="datatable_info">
                <thead>
                    <th>Nombre</th>
                    <th>Telefóno</th>
                    <th>Dirección</th>
                    <th>Municipio</th>
                    <th>Servicios</th>
                    <th>Estado</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($establishments as $establishment)
                    <tr >
                        <td>{{$establishment->establishment_name}}</td>
                        <td>{{$establishment->phone}}</td>
                        <td>{{$establishment->address}}</td>
                        <td>{{$establishment->Municipality->municipality_name}}</td>
                        <td>
                            @if(isset($establishment->Services))
                                <ul>
                                @foreach($establishment->Services as $service)
                                    <li>{{$service->service_name}}</li>
                                @endforeach
                                </ul>
                            @else
                            No definidos
                            @endif
                        </td>
                        <td>
                            {{$establishment->state}}
                        </td>
                        <td >
                            <a href="{{url('admin/operators/establishment/edit',$establishment->slug)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar establecimiento"><i class="far fa-edit" aria-hidden="true"></i></a>
                            <a href="{{ url('admin/operators/establishment/images', $establishment->slug) }}" data-toggle="tooltip" title="Administrar imágenes" class="btn btn-info"><i class="fas fa-images" aria-hidden="true"></i></a>
                            @if($establishment->state=="activo")
                            <a href="{{url('admin/operators/establishment/desactivate/'.$establishment->slug)}}" data-toggle="tooltip" title="Desactivar establecimiento" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i></a>
                            @else
                            <a href="{{url('admin/operators/establishment/activate/'.$establishment->slug)}}" data-toggle="tooltip" title="Activar establecimiento" class="btn btn-effect-ripple btn-success"><i class="fa fa-check"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection