@extends('admin.layout.auth')

@section('title', 'Imágenes de establecimiento')

@section('content')
    <div id="page-content" class="inner-sidebar-left">
        <!-- Inner Sidebar -->
        <div id="page-content-sidebar">
            <!-- Collapsible Options -->
            
            <div id="media-options" class="collapse navbar-collapse remove-padding">
                <!-- Filter -->
                <div class="block-section">
                    <h4 class="inner-sidebar-header">
                        Filtro
                    </h4>
                    <!-- Filter by Type links -->
                    <!-- Custom files functionality is initialized in js/pages/appMedia.js -->
                    <!-- Add the category value you want each link in .media-filter to filter out in its data-category attribute. Add the value 'all' to show all items -->
                    <ul class="nav nav-pills nav-stacked nav-icons media-filter">
                        <li class="active">
                            <a href="javascript:void(0)" data-category="image" onclick="verdrop()">
                                <i class="fa fa-fw fa-folder text-success icon-push"></i> <strong>Imagenes</strong>
                            </a>
                        </li>
                    </ul>
                    <!-- END Filter by Type links -->
                </div>
                <!-- END Filter -->
            </div>
            <!-- END Collapsible Options -->
        </div>
        <!-- END Inner Sidebar -->

        <!-- Media Box Content -->
        <!-- Add the category value for each item in its data-category attribute (for the filter functionality to work) -->
        <div class="row media-filter-items">
            <div class="col-sm-12">
                <div class="col-sm-4 col-sm-offset-8">
                    <a href="{{url('admin/operators/establishment/'.$establishment->type_establishment.'/'.Crypt::encrypt($establishment->Operator->id_operator))}}" class="btn btn-effect-ripple btn-primary pull-right">Guardar</a>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="col-sm-12" >
                <div class="media-items animation-fadeInQuick2" data-category="image" >
                    {!!Form::open(['url'=>['admin/operators/establishment/upload_images',$establishment->id_establishment],'method'=>'PUT', 'class'=> 'form-horizontal dropzone', 'enctype' => 'multipart/form-data', 'id' => 'my-dropzone'])!!}
                <div class="panel-body">
                    <div class="dropzone-previews"></div>
                    <div class="dz-message" style="height:100px;">
                        Sube las imágenes aquí
                    </div>
                </div>
                {!! Form::close() !!}
                </div>
            </div>
            {!!Form::open(['url'=>['admin/operators/establishment/update_images'], 'method'=>'POST','class'=>'form-horizontal'])!!}
            @foreach($establishment->images as $image)
               <div class="col-sm-4 col-lg-3" style="height: 400px!important;">
                    <div class="media-items animation-fadeInQuick2" data-category="image">
                        <div class="media-items-options text-right">
                            <a href="{{asset($image->link_image)}}" class="btn btn-xs btn-info" data-toggle="lightbox-image">Ver</a>
                            <a href="{{url('admin/operators/establishment/delete_image/'.$image->id_establishment_image)}}" class="btn btn-xs btn-danger" onclick='return confirm("¿Desea eliminar éste registro?");'><i class="fa fa-times"></i></a>
                        </div>
                        <div class="media-items-content">
                            <img src="{{asset($image->link_image)}}" class="img-responsive">
                        </div>
                        <div>
                            <div class="form-group">
                            <label for="description">Descripción</label>
                            <textarea name="description[{{$image->id_establishment_image}}]" id="description" class="form-control" rows="3" style="width: 88%;margin: 12px">{{$image->description}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-12">
                <button type="submit" class="btn btn-info">Guardar descripciones</button>
            </div>
            {!!Form::close()!!}
            
        </div>
        <!-- END Media Box Content -->
    </div>
    <!-- END Page Content -->
			
       
@endsection
@section('aditionalScript')
<script src="{{asset('plugins/dropzone/dist/min/dropzone.min.js')}}"></script>
 <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>


 <!-- Load and execute javascript code used only in this page -->
    <script src="{{asset('auth-panel/js/pages/appMedia.js')}}"></script>
    <script>$(function(){ AppMedia.init(); });</script>
<script type="text/javascript">
    Dropzone.options.myDropzone = {
        autoProcessQueue: true,
        uploadMultiple: true,
        maxFiles: 4,
        maxFilesize: 2, // MB
        acceptedFiles: '.jpg, .jpeg, .png, .bmp',

        init: function() {
            var submitBtn = document.querySelector("#submit");
            myDropzone = this;

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    location.reload();
                }
            });

            this.on("success", function() {
                myDropzone.processQueue.bind(myDropzone);
            });
        }
    };

</script>