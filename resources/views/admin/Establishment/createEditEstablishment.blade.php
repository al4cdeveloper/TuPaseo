@extends('admin.layout.auth')

@section('title', ($type=='hotel') ? "Hotel" : "Restaurante")

@section('aditionalStyle')
     <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
@endsection
@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                    	@if(isset($establishment))
                        <h1>Editar @if($type=='hotel') Hotel @else Restaurante @endif</h1>
                        @else
                        <h1>Nuevo @if($type=='hotel') Hotel @else Restaurante @endif</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>


       <div class="block">
            <!-- Progress Bars Wizard Title -->
            <div class="block-title">
                <h2>Por favor diligencie los siguienes campos</h2>
            </div>
            <!-- END Progress Bar Wizard Title -->

            <!-- Progress Wizard Content -->
            {!!Form::model($establishment,['url'=>['admin/operators/establishment/update',$establishment->slug],'method'=>'PUT', 'class'=> 'form-horizontal form-bordered', 'enctype' => 'multipart/form-data', 'id'=>'progress-formulario'])!!}
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="progress progress-mini remove-margin">
                            <div id="progress-bar-wizard" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
                        </div>
                        <div id="step" class="pull-right">
                            Paso 1/4
                        </div>
                    </div>
                </div>
                <!-- END Progress Bar -->

                <!-- First Step -->
                <div id="progress-first" class="step">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="example-progress-username">Nombre de establecimiento</label>
                        <div class="col-md-6">
                            {{Form::text('establishment_name',null,['class'=>'form-control','placeholder'=>'Ingrese nombre de establecimiento'])}}
                            <span class="label label-danger">{{$errors->first('establishment_name') }}</span>
                        </div>
                    </div>
                    @if($type == 'restaurant')
                    <div class="form-group">
                        <div class="form-group">
                            <label for="costo">Tipo de cocina</label>
                            {!!Form::select('kitchen_type', $kitchenType, null, ['class'=>'select-chosen','id'=>'category'])!!}
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        @if(!isset($establishment))
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="location">Departamento</label>
                                {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','id'=>'departments'])!!}
                            </div>
                        </div>
                        @else
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="location">Departamento</label>
                                    <select name="departments" id="departments" class="select-chosen">
                                        @foreach($departments as $id => $departmentF)
                                            <option value="{{$id}}" @if($id == $department) selected @endif>{{$departmentF}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="col-sm-6 @if(!isset($establishment)) hidden @endif " id="municipalityDiv">
                            <div class="form-group">
                                <label for="location">Municipio</label>
                                <select name="fk_municipality" id="municipalities" class="select-chosen">
                                    @if(isset($establishment))
                                        @foreach($municipalities as $id => $municipalityF)
                                            <option value="{{$id}}" @if($establishment->fk_municipality==$id) selected @endif>{{$municipalityF}}</option>    
                                        @endforeach
                                    @endif
                                </select>
                                <span class="label label-danger">{{$errors->first('fk_municipality') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5 ">
                            <label class="col-md-4 control-label" for="example-progress-username">Dirección</label>
                            <div class="col-md-8">
                                {{Form::text('address',null,['class'=>'form-control','placeholder'=>'Ingrese dirección de establecimiento','id'=>'address'])}}
                                <span class="label label-danger">{{$errors->first('address') }}</span>
                            </div>
                            <small>Por favor ingrese su dirección y revise en el mapa que la ubicación contrada sea la correcta. <br> En caso de no serla por favor ingrese una dirección más exacta (Una opción puede ser poner el nombre de la ciudad después de la dirección).</small>
                            <div id="errors" style="color:red;"></div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <div id="map" style="height: 300px;width: 300px;"></div>
                        </div>

                        {!!Form::hidden('latitude',null,['id'=>'latitude'])!!}
                        {!!Form::hidden('longitude',null,['id'=>'longitude'])!!}
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5  col-xs-12">
                            <div class="form-group">
                                <label for="costo">Teléfono</label>
                                {{Form::number('phone',null,['class'=>'form-control','placeholder'=>'Ingrese teléfono de establecimiento'])}}
                                <span class="label label-danger">{{$errors->first('phone') }}</span>
                            </div>
                        </div>
                        <div class="col-sm-5  col-sm-offset-1  col-xs-12">
                            <div class="form-group">
                                <label for="costo">Web <small>(Opcional)</small></label>
                                {!!Form::text('web', null, ['class'=>'form-control', 'placeholder' => 'Inserte sitio web (en caso de tenerlo)'])!!}
                                <span class="label label-danger">{{$errors->first('web') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5  col-xs-12">
                            <div class="form-group">
                                <label for="costo">Facebook<small>(Opcional)</small></label>
                                {!!Form::text('facebook', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de facebook (en caso de tenerlo)'])!!}
                                <span class="label label-danger">{{$errors->first('facebook') }}</span>
                            </div>
                        </div>
                        <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                            <div class="form-group">
                                <label for="costo">Twitter<small>(Opcional)</small></label>
                                {!!Form::text('twitter', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de twitter (en caso de tenerlo)'])!!}
                                <span class="label label-danger">{{$errors->first('twitter') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5  col-xs-12">
                            <div class="form-group">
                                <label for="costo">Instagram<small>(Opcional)</small></label>
                                {!!Form::text('instagram', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de instagram (en caso de tenerlo)'])!!}
                                <span class="label label-danger">{{$errors->first('instagram') }}</span>
                            </div>
                        </div>
                        <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                            <div class="form-group">
                                <label for="costo">Vídeo de Youtube<small>(Opcional) Puede agregar un vídeo de su establecimiento</small></label>
                                {!!Form::text('video', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de vídeo de youtube (en caso de tenerlo)'])!!}
                                <span class="label label-danger">{{$errors->first('video') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="duration">Descripción</label>
                        <div class="form-group">
                            {{ Form::textarea('description',null,['class'=>'ckeditor','id'=>'textarea-ckeditor','id'=>'description','required']) }}
                            <span class="label label-danger">{{$errors->first('description') }}</span>
                        </div>
                    </div>
                </div>
                <!-- END First Step -->

                <!-- Second Step -->
                <div id="progress-second" class="step">
                     <div class="form-group">
                        <div class="col-md-12">
                            <h3>Seleccione los servicios con que cuenta su establecimiento</h3>
                            <div class="col-md-3">
                                @for($i = 0; $i < $cant ; $i++)
                                    <label class="csscheckbox csscheckbox-primary">
                                        <input type="checkbox" name="service[]" value="{{$services[$i]->id_service_establishment}}" @if(isset($establishment)) @if($establishment->Services) @foreach($establishment->services as $service)  @if($service->id_service_establishment == $services[$i]->id_service_establishment) checked @endif @endforeach @endif @endif><span></span> {{$services[$i]->service_name}}
                                    </label> 
                                    <br>
                                @endfor
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                @for($i = $cant; $i < ($cant*2) ; $i++)
                                    <label class="csscheckbox csscheckbox-primary">
                                        <input type="checkbox" name="service[]" value="{{$services[$i]->id_service_establishment}}" @if(isset($establishment)) @if($establishment->Services) @foreach($establishment->services as $service)  @if($service->id_service_establishment == $services[$i]->id_service_establishment) checked @endif @endforeach @endif @endif><span></span> {{$services[$i]->service_name}}
                                    </label> 
                                    <br>
                                @endfor
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                @for($i = ($cant*2); $i < count($services) ; $i++)
                                    <label class="csscheckbox csscheckbox-primary">
                                        <input type="checkbox" name="service[]" value="{{$services[$i]->id_service_establishment}}" @if(isset($establishment)) @if($establishment->Services) @foreach($establishment->services as $service)  @if($service->id_service_establishment == $services[$i]->id_service_establishment) checked @endif @endforeach @endif @endif><span></span> {{$services[$i]->service_name}}
                                    </label> 
                                    <br>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Second Step -->

                <!-- Third Step Habitaciones o platos-->
                <div id="progress-third" class="step">
                    <div class="form-group">

                    @if($type == 'hotel')
                        <h3>Ingrese los tipos de habitaciones que dispone <small>Máximo 6</small></h3>
                        <table id="roomstable" class="table  table-vcenter table-condensed">
                            <thead>
                                <tr>
                                    <th>Habitaciones</th>
                                    <th style="width: 120px;">Cantidad</th>
                                    <th style="width: 320px;">Capacidad</th>
                                    <th >Camas</th>
                                    <th  class="text-center">Marque si es Suite</th>
                                    <th style="width: 80px;"></th>
                                </tr>
                            </thead>
                        @if(isset($establishment))
                            <tbody {{$row = 1}}>
                                @foreach($establishment->Rooms as $room)
                                <tr id="room{{$row}}">
                                    <td><input type="text" name="room[{{$row}}]" class="form-control" placeholder="Ej:Habitación Premier" value="{{$room->room}}"></td>
                                    <td><input type="number" name="cuantity[{{$row}}]" id="" class="form-control" placeholder="Ej:15" value="{{$room->cuantity}}"></td>
                                    <td>
                                        <input type="text" placeholder="Ej:Tres adultos, o dos adultos y un niño" name="capacity[{{$row}}]" class="form-control" value="{{$room->capacity}}">
                                    </td>
                                    <td >
                                        <input type="text" placeholder="Ej:Una cama King o dos camas individuales" name="bed[{{$row}}]" class="form-control" value="{{$room->bed}}">
                                    </td>
                                    <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox" name="suite[{{$row}}]" @if($room->suite) checked @endif><span></span></label></td>
                                    <td id="add{{$row}}">
                                        <button type="button" data-toggle="tooltip" title="Quitar esta habitación" onclick="removeRoom({{$row}})" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button>
                                    </td>
                                </tr {{$row++}}>
                                @endforeach
                            </tbody>
                        @else
                            <tbody>
                                <tr id="room1">
                                    <td><input type="text" name="room[1]" class="form-control" placeholder="Ej:Habitación Premier"></td>
                                    <td><input type="number" name="cuantity[1]" id="" class="form-control" placeholder="Ej:15"></td>
                                    <td>
                                        <input type="text" placeholder="Ej:Tres adultos, o dos adultos y un niño" name="capacity[1]" class="form-control">
                                    </td>
                                    <td >
                                        <input type="text" placeholder="Ej:Una cama King o dos camas individuales" name="bed[1]" class="form-control">
                                    </td>
                                    <td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox" name="suite[1]"><span></span></label></td>
                                    <td id="add1">
                                        <button type="button" data-toggle="tooltip" title="Quitar esta habitación" onclick="removeRoom(1)" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        @endif
                            <span class="label label-danger">{{$errors->first('rooms') }}</span>
                        </table>
                        <div class="col-md-12" align="center">
                            <button class="btn btn-primary" type="button" onclick="addRoom()" id="addRoombutton">Agregar otra habitación</button>
                        </div>
                    @else
                        <h3>Ingrese los principales platos que ofrece</h3>
                        <small>A continuación ingrese el nombre de la categoría de los platos que ofrece, puede crear un máximo de 3 categorías con máximo 3 platos cada una.</small>
                        @if(isset($establishment))
                            <div class="form-group" {{$table=1}}>
                                <div id="divCategory">
                                        @foreach($establishment->Plates->groupBy('category') as $category => $plates)
                                            <label class="col-md-3 control-label" for="example-progress-username">Nombre de categoría de platos</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control categoryPlates" placeholder="Ejemplo: Entradas, Platos típicos, Etc..." name="category[{{$table}}]" id="category{{$table}}" value="{{$category}}" >
                                                <span class="label label-danger">{{$errors->first('category') }}</span>
                                            </div>
                                            <div class="col-md-10 col-md-offset-2">
                                                <table id="platestable{{$table}}" class="table  table-vcenter table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>Plato</th>
                                                            <th>Costo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody {{$colum=1}}>
                                                        @foreach($plates as $plate)
                                                        <tr id="plate{{$table.$colum}}">
                                                            <td><input type="text" name="plate[{{$table}}][{{$colum}}]" class="form-control" placeholder="Ej:Crema de espinaca" value="{{$plate->plate}}">
                                                            <span class="label label-danger">{{$errors->first('plates') }}</span>
                                                            </td>
                                                            <td>
                                                                <div class="col-md-2">
                                                                    $
                                                                </div>
                                                                <div class="col-md-10">
                                                                <input type="number" name="cost[{{$table}}][{{$colum}}]" id="" class="form-control" placeholder="Ej:15000" value="{{$plate->cost}}"></td>
                                                                </div>
                                                            <td>
                                                                <button type="button" data-toggle="tooltip" title="Quitar este plato" onclick="removePlate({{$table}},{{$colum}})" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button>
                                                            </td>
                                                        </tr {{$colum++}}>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <div class="col-md-10" align="center">
                                                    <button class="btn btn-primary" type="button" onclick="addPlate({{$table}})" id="addPlatebutton{{$table}}">Agregar plato</button>
                                                </div>
                                            </div {{$table++}}>
                                        @endforeach
                                        <div class="col-md-12" align="center">
                                            <hr>
                                            <button class="btn btn-primary" type="button" onclick="addCategory()" id="addCategoryButton">Agregar otra categoría</button>
                                        </div>
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <div id="divCategory">
                                    <label class="col-md-3 control-label" for="example-progress-username">Nombre de categoría de platos</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control categoryPlates" placeholder="Ejemplo: Entradas, Platos típicos, Etc..." name="category[1]" id="category1" >
                                        <span class="label label-danger">{{$errors->first('category') }}</span>
                                    </div>
                                    <div class="col-md-10 col-md-offset-2">
                                        <table id="platestable1" class="table  table-vcenter table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Plato</th>
                                                    <th>Costo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id="plate11">
                                                    <td><input type="text" name="plate[1][1]" class="form-control" placeholder="Ej:Crema de espinaca">
                                                    <span class="label label-danger">{{$errors->first('plates') }}</span>
                                                    </td>
                                                    <td>
                                                        <div class="col-md-2">
                                                            $
                                                        </div>
                                                        <div class="col-md-10">
                                                        <input type="number" name="cost[1][1]" id="" class="form-control" placeholder="Ej:15000"></td>
                                                        </div>
                                                    <td>
                                                        <button type="button" data-toggle="tooltip" title="Quitar este plato" onclick="removePlate(1,1)" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="col-md-10" align="center">
                                            <button class="btn btn-primary" type="button" onclick="addPlate(1)" id="addPlatebutton1">Agregar plato</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" align="center">
                                        <hr>
                                        <button class="btn btn-primary" type="button" onclick="addCategory()" id="addCategoryButton">Agregar otra categoría</button>
                                </div>
                            </div>
                        @endif
                    @endif
                    </div>
                </div>
                 <!-- Third Step -->
                <div id="progress-fourth" class="step">
                    <div class="form-group">
                        <h3>Horarios</h3>
                        @if($type == 'restaurant')
                             <div class="col-md-10 col-md-offset-2">
                                    <table id="" class="table  table-vcenter table-condensed">
                                        <thead>
                                            <tr>
                                                <th width="160" align="center"></th>
                                                <th width="35">Hora inicio</th>
                                                <th width="35">Hora fin</th>
                                                <th width="60">No se presta servicio este día</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($establishment))
                                                @foreach($establishment->Schedule as $schedule)
                                                <tr>
                                                    <td>{{$schedule->name}}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-10 col-sm-12">
                                                                <div class="input-group bootstrap-timepicker">
                                                                    {!!Form::text($schedule->name.'[inicio]',$schedule->start,["class"=>"form-control input-timepicker24",'id'=>'inicio'.$schedule->name])!!}
                                                                    <span class="input-group-btn">
                                                                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                    </span>
                                                                </div>
                                                                <span class="label label-danger">{{$errors->first($schedule->name.'inicio') }}</span>

                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-10 col-sm-12">
                                                                <div class="input-group bootstrap-timepicker">
                                                                    {!!Form::text($schedule->name.'[fin]',$schedule->end,["class"=>"form-control input-timepicker24",'id'=>'fin'.$schedule->name])!!}
                                                                    <span class="input-group-btn">
                                                                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                    </span>
                                                                </div>
                                                                <span class="label label-danger">{{$errors->first($schedule->name.'fin') }}</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <label class="csscheckbox csscheckbox-primary">
                                                            <input type="checkbox" name="{{$schedule->name}}[noservice]" class="form-control" onclick="inactiveDay('{{$schedule->name}}')" id="check{{$schedule->name}}" @if($schedule->inactive) checked  @endif> <span></span>
                                                        </label> 
                                                        <br>
                                                        @if($schedule->name == "Lunes")
                                                            <a onclick="ApplyTime()" href="javascript:void(0)"> Aplicar esta hora a todos los demás días</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                            @foreach($days as $day)
                                            <tr>
                                                <td>{{$day}}</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                {!!Form::text($day.'[inicio]','00:000:00',["class"=>"form-control input-timepicker24",'id'=>'inicio'.$day])!!}
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                            <span class="label label-danger">{{$errors->first($day.'inicio') }}</span>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                {!!Form::text($day.'[fin]','00:000:00',["class"=>"form-control input-timepicker24",'id'=>'fin'.$day])!!}
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                            <span class="label label-danger">{{$errors->first($day.'fin') }}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <label class="csscheckbox csscheckbox-primary">
                                                        {!!Form::checkbox($day.'[noservice]',null,false,["class"=>"form-control input-timepicker24",'onclick'=>"inactiveDay('".$day."')",'id'=>'check'.$day])!!}<span></span>
                                                    </label> 
                                                    <br>
                                                    @if($day == "Lunes")
                                                        <a onclick="ApplyTime()" href="javascript:void(0)"> Aplicar esta hora a todos los demás días</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                            </div>
                        @else
                            <div class="col-md-10 col-md-offset-2">
                                    <table id="" class="table  table-vcenter table-condensed">
                                        <thead>
                                            <tr>
                                                <th width="160" align="center"></th>
                                                <th width="35">Hora inicio</th>
                                                <th width="35">Hora fin</th>
                                            </tr>
                                        </thead>
                                        @if(isset($establishment))
                                        <tbody>
                                            @foreach($establishment->schedule as $schedule)
                                            <tr>
                                                <td>{{$schedule->name}}</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                <input type="text" class="form-control input-timepicker24" name="{{$schedule->name}}[inicio]" value="{{$schedule->start}}">
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                <input type="text" class="form-control input-timepicker24" name="{{$schedule->name}}[fin]" value="{{$schedule->end}}">
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        @else
                                        <tbody>
                                            <tr>
                                                <td>CheckIn</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                {!!Form::text('checkin[inicio]','00:000:00',["class"=>"form-control input-timepicker24"])!!}
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                {!!Form::text('checkin[fin]','00:000:00',["class"=>"form-control input-timepicker24"])!!}
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Checkout</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                {!!Form::text('checkout[inicio]','00:000:00',["class"=>"form-control input-timepicker24"])!!}
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-10 col-sm-12">
                                                            <div class="input-group bootstrap-timepicker">
                                                                {!!Form::text('checkout[fin]','00:000:00',["class"=>"form-control input-timepicker24"])!!}
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endif
                                    </table>
                            </div>
                        @endif

                    </div>
                </div>
            
                <!-- END Third Step -->

                <!-- Form Buttons -->
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="reset" class="btn btn-effect-ripple btn-danger" id="back2">Anterior</button>
                        <button type="submit" class="btn btn-effect-ripple btn-primary" id="next2">Siguiente</button>
                    </div>
                </div>
            {!!Form::close()!!}

                
                <!-- END Form Buttons -->
            <!-- END Progress Bar Wizard Content -->
        </div>
	</div>
@endsection

@section('aditionalScript')
	   
    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
            <!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/formsWizard.js')}}"></script>
        <script>$(function(){ FormsWizard.init(); });</script>

        <script>
        $("#departments").change(function()
        {   
            token = '{{csrf_token()}}' 
            $.ajax({
                url: '/api/getmunicipalities',
                type: 'POST',
                headers:{'X-CSRF-TOKEN':token},
                dataType: 'JSON',
                data: {department: $(this).val()},
                complete:function(transport)
                {
                    response = transport.responseJSON;
                    $('#municipalities').empty();
                    $("#municipalityDiv").removeClass('hidden');
                    $("#interesSite").empty();
                    $("#siteDiv").addClass('hidden');
                    $("#municipalities").append('<option value=""></option>').trigger("chosen:updated");

                    for(x in response)
                    {
                        $("#municipalities").append('<option value="'+x+'">'+response[x]+'</option>').trigger("chosen:updated");
                    }
                }
            })
        })

    var controladorTiempo = "";
            var markers = [];
           function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 10,
                  center: {lat: 4.570868, lng: -74.29733299999998}
                });
                var geocoder = new google.maps.Geocoder();
                $("#address").on("keyup", function() {
                    clearTimeout(controladorTiempo);
                    controladorTiempo = setTimeout(geocodeAddress(geocoder, map), 2000);
                });
               
              }

        $("#address").on('blur',function()
        {
            if($("#latitude").val() == "" || $("#longitude").val()=="")
            {
                $("#address").val('');
            }
        });
        @if($errors->first('address'))
            $("#address").val('');
        @endif

        @if(isset($establishment))
            @foreach($establishment->Schedule as $schedule)
                @if($schedule->inactive)
                   setTimeout(function()
                    { 
                        $('#inicio{{$schedule->name}}').attr('disabled', 'disabled');
                        $('#inicio{{$schedule->name}}').val('00:00:00');
                        $('#fin{{$schedule->name}}').attr('disabled', 'disabled');
                        $('#fin{{$schedule->name}}').val('00:00:00');
                    },20);
                @endif
            @endforeach
        @endif
        @if(!isset($establishment))
        $("#departments").val('');
        $("#address").val('');

        @endif

        function addRoom(add)
        {
            var trs=$("#roomstable tr").length;
            if(trs <7)
            {
                var nuevaFila=' <tr id="room'+trs+'"><td><input type="text" name="room['+trs+']" class="form-control" placeholder="Ej:Habitación Premier"></td><td><input type="number" name="cuantity['+trs+']" id="" class="form-control" placeholder="Ej:15"></td><td><input type="text" placeholder="Ej:Tres adultos, o dos adultos y un niño" name="capacity['+trs+']" class="form-control"></td><td ><input type="text" placeholder="Ej:Una cama King o dos camas individuales" name="bed['+trs+']" class="form-control"></td><td class="text-center"><label class="csscheckbox csscheckbox-primary"><input type="checkbox" name="suite['+trs+']"><span></span></label></td><td id="add'+trs+'"><button type="button" data-toggle="tooltip" title="Quitar esta habitación" onclick="removeRoom('+trs+')" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button></td></tr>';
                

                $("#roomstable").append(nuevaFila);
                if(trs==6)
                {
                    $("#addRoombutton").attr('disabled', 'disabled');
                }
            }
            
        }
        function removeRoom(room)
        {
            var trs=$("#roomstable tr").length;
            if(document.getElementById("addRoombutton").hasAttribute("disabled"))
            {
                $("#addRoombutton").removeAttr('disabled');
            }
            $("#room"+room).remove();

        }

        function addPlate(table)
        {
            var trs=$("#platestable"+table+" tr").length;
            console.log(trs);
            if(trs <4)
            {
                var nuevaFila='<tr id="plate'+table+trs+'"><td><input type="text" name="plate['+table+']['+trs+']" class="form-control" placeholder="Ej:Crema de espinaca"></td><td><div class="col-md-2">$</div><div class="col-md-10"><input type="number" name="cost['+table+']['+trs+']" class="form-control" placeholder="Ej:15000"></td></div><td><button type="button" data-toggle="tooltip" title="Quitar este plato" onclick="removePlate('+table+','+trs+')" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button></td></tr>';
                

                $("#platestable"+table).append(nuevaFila);
                if(trs==3)
                {
                    $("#addPlatebutton"+table).attr('disabled', 'disabled');
                }
            }   
        }

        function removePlate(table,plate)
        {
            var trs=$("#platestable"+table+" tr").length;
            if(document.getElementById("addPlatebutton"+table).hasAttribute("disabled"))
            {
                $("#addPlatebutton"+table).removeAttr('disabled');
            }
            $("#plate"+table+plate).remove();
        }

        function addCategory()
        {
            categories= $(".categoryPlates");
            var validation = true;
            var cantPlates = $(".categoryPlates").length;
            for (var i = cantPlates - 1; i >= 0; i--) 
            {
                console.log($(".categoryPlates")[i].id)
                var element = $(".categoryPlates")[i].id;
                if($("#"+element).val() == "")
                {
                    swal("Error", "Para crear una nueva categoría debe haber diligenciado la ya existente", "error");
                    validation= false;
                }
            }
            if(validation)
            {
                if(cantPlates<3)
                {
                    $("#divCategory").append('<label class="col-md-3 control-label" for="example-progress-username">Nombre de categoría de platos</label><div class="col-md-8"><input type="text" class="form-control categoryPlates" placeholder="Ejemplo: Entradas, Platos típicos, Etc..." name="category['+(cantPlates+1)+']" id="category'+(cantPlates+1)+'" ></div><div class="col-md-10 col-md-offset-2"><table id="platestable'+(cantPlates+1)+'" class="table  table-vcenter table-condensed"><thead><tr><th>Plato</th><th>Costo</th></tr></thead><tbody><tr id="plate'+(cantPlates+1)+'1"><td><input type="text" name="plate['+(cantPlates+1)+'][1]" class="form-control" placeholder="Ej:Crema de espinaca"></td><td><div class="col-md-2">$</div><div class="col-md-10"><input type="number" name="cost['+(cantPlates+1)+'][1]" id="" class="form-control" placeholder="Ej:15000"></td></div><td><button type="button" data-toggle="tooltip" title="Quitar este plato" onclick="removePlate('+(cantPlates+1)+',1)" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button></td></tr></tbody></table><div class="col-md-10" align="center"><button class="btn btn-primary" type="button" onclick="addPlate('+(cantPlates+1)+')" id="addPlatebutton'+(cantPlates+1)+'">Agregar plato</button></div></div>');   
                }
                else
                {
                    swal("Error", "Solo puede agregar 3 categorías", "error");
                }
            }
            
        }

        function inactiveDay(day)
        {
            if($("#check"+day).is(":checked"))
            {
                $("#inicio"+day).attr('disabled', 'disabled');
                $("#inicio"+day).val('00:00:00');
                $("#fin"+day).attr('disabled', 'disabled');
                $("#fin"+day).val('00:00:00');
            }
            else
            {
                $("#inicio"+day).removeAttr('disabled');
                $("#fin"+day).removeAttr('disabled');
            }
        }

        function ApplyTime()
        {
            inicio = $("#inicioLunes").val();
            fin = $("#finLunes").val();

            $('#inicioMartes').val(inicio);
            $('#finMartes').val(fin);
            $('#inicioMiercoles').val(inicio);
            $('#finMiercoles').val(fin);
            $('#inicioJueves').val(inicio);
            $('#finJueves').val(fin);
            $('#inicioViernes').val(inicio);
            $('#finViernes').val(fin);
            $('#inicioSabado').val(inicio);
            $('#finSabado').val(fin);
            $('#inicioDomingo').val(inicio);
            $('#finDomingo').val(fin);
        }

        function geocodeAddress(geocoder, resultsMap) 
        {
            var address = document.getElementById('address').value+",Colombia" ;
            $('#errors').empty();
            clearMarkers();
            if(address!= "")
            {
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === 'OK') {
                    clearMarkers();
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                     markers.push(marker);
                    $('#errors').empty();

                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                  } else {
                    $('#errors').empty();
                     $("#latitude").val('');
                    $("#longitude").val('');
                    $('#errors').append('No se puede encontrar su localización, intentelo nuevamente');
                  }
                });
            }
            
          }
          // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw&callback=initMap"
        async defer></script>

@endsection