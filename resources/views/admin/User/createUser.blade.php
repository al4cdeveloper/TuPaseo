@extends('admin.layout.auth')

@section('title', 'Crear administrador')

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Crear administrador</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            {!!Form::open(['url'=>'admin/admin/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipage/form-data', 'id' => 'userForm', 'novalidate'])!!}
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre </label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de administrador', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div> 
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Correo Electrónico</label>
                    {!!Form::email('email', null, ['class'=>'form-control', 'placeholder' => 'Inserte dde correo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('email') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Rol</label>
                    {!!Form::select('role', $role, null, ['class'=>'select-chosen'])!!}
                    <span class="label label-danger">{{$errors->first('role') }}</span>
                </div>
            </div>

            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/admin')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection
