@extends('admin.layout.auth')

@section('title', 'Lista de administraciones')


@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Lista de administraciones</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Lista de administradores</h2>
                    <div class="pull-right" style="padding: 5px;">
                        <a class="btn btn-primary btn-sm" href="{{url('admin/admin/create')}}"><i class="far fa-plus-square fa-lg"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="example2-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Cargo</th>
                                <th>Estado</th>
                                <th class="text-center" style="width: 90px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->role}}</td>
                                    <td>@if($user->state=="active")Activo @else Inactivo @endif</td>
                                    <td>
                                        <button type="button" class="btn btn-effect-ripple btn-xs btn-warning" data-toggle="modal" data-target="#edit{{$user->id}}"><i class="fa fa-edit"></i></button>
                                        <a href="{{url('admin/admin/restorepassword/'.Crypt::encrypt($user->id))}}" data-toggle="tooltip" title="Restaurar contraseña" class="btn btn-effect-ripple btn-xs btn-info"><i class="fas fa-unlock-alt"></i></i></a>
                                        @if($user->state=="active")
                                        <a href="{{url('admin/admin/desactivate/'.Crypt::encrypt($user->id))}}" data-toggle="tooltip" title="Desactivar cuenta" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                        <a href="{{url('admin/admin/activate/'.Crypt::encrypt($user->id))}}" data-toggle="tooltip" title="Activar cuenta" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <div class="modal fade" id="edit{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                         {!!Form::model($user,['url'=>['admin/admin/update',$user->id],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'])!!}
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Editar información del usuario</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group col-md-5 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="costo">Nombre</label>
                                                        {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre', 'required'])!!}
                                                        <span class="label label-danger">{{$errors->first('name') }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-md-offset-1 col-xs-12">
                                                    <label for="costo">Email</label>
                                                    {!!Form::text('email', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre', 'required'])!!}
                                                    <span class="label label-danger">{{$errors->first('email') }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <div class="form-group">
                                                    <label for="costo">Rol</label>
                                                    {!!Form::select('role', $role, null, ['class'=>'select-chosen'])!!}
                                                </div>
                                            </div>
                                        </div>
                                       
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <button type="Submit" class="btn btn-primary">Guardar</button>
                                      </div>
                                       {!!Form::close()!!}
                                    </div>
                                  </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection