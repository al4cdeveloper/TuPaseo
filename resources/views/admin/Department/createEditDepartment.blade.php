@extends('admin.layout.auth')

@if(isset($department))
    @section('title', 'Actualizar departamento')
@else
    @section('title', 'Crear departamento')
@endif
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Departamentos</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($department))
			{!!Form::model($department,['url'=>['admin/departments/update',$department->id_department],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/departments/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Región</label>
                    {!!Form::select('fk_region', $regions, null, ['class'=>'select-chosen'])!!}
                </div>
            </div>
			<div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('department_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del departamento', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('department_name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Descripción corta</label>
                    {!!Form::text('short_description', null, ['class'=>'form-control', 'placeholder' => 'Inserte breve descripción del departamento', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('short_description') }}</span>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/departments')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection