@extends('admin.layout.auth')

@section('title', 'Listado de departamentos')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Departamentos</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        {{-- <div class="block"> --}}
            {{-- <div class="row"> --}}
                <div class="block full">
                    <div class="block-title">
                        <h2>Departamentos</h2>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/departments/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Departamento</th>
                                    <th>Región a la que pertenece</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($departments as $department)
                                <tr>
                                    <td>{{$department->department_name}}</td>
                                    <td>{{$department->Region->region_name}}</td>
                                    <td>@if($department->state=="activo")
                                        <a class="label label-info">{{$department->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$department->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/departments/edit/'.$department->slug)}}" data-toggle="tooltip" title="Editar departamento" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        @if($department->state=="activo")
                                        <a href="{{url('admin/departments/desactivate/'.$department->id_department)}}" data-toggle="tooltip" title="Desactivar departamento" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                        <a href="{{url('admin/departments/activate/'.$department->id_department)}}" data-toggle="tooltip" title="Activar departamento" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection