@extends('admin.layout.auth')

@section('title', 'Listado de tipos de actividad')


@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Tipos de actividades</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block full">
             <form action="{{url('admin/activities/servicecategory/store')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">   
                    <div class="col-md-5">
                        {!!Form::text('service_category', null, ['class'=>'form-control', 'placeholder' => 'Nombre de tipo de actividad', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('service_category') }}</span>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary" type="submit" >Registrar</button>
                    </div>
                </div>
            </form> 

        </div>

            <div class="block full">
                <div class="block-title">
                    <h2>Tipos de actividades</h2>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th>Nombre de tipo de actividad</th>
                                <th class="text-center" style="width: 90px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{$category->service_category}}</td>
                                <td> 
                                    <button type="button" class="btn btn-effect-ripple btn-xs btn-warning" data-toggle="modal" data-target="#edit{{$category->id_category}}">
                                    <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="edit{{$category->id_category}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                     {!!Form::model($category,['url'=>['admin/activities/servicecategory/update',$category->id_category],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data'])!!}
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Editar</h4>
                                  </div>
                                  <div class="modal-body">
                                         {!!Form::text('service_category', null, ['class'=>'form-control', 'placeholder' => 'Nombre de tipo de actividad', 'required'])!!}
                                        <span class="label label-danger">{{$errors->first('service_category') }}</span>
                                   
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="Submit" class="btn btn-primary">Guardar</button>
                                  </div>
                                   {!!Form::close()!!}
                                </div>
                              </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection