@extends('admin.layout.auth')

@section('title', 'Listado de servicios')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Servicios</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        {{-- <div class="block"> --}}
            {{-- <div class="row"> --}}
                <div class="block full">
                    <div class="block-title">
                        <h2>Servicios</h2>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Servicio</th>
                                    <th>Ubicación</th>
                                    <th>Dirección</th>
                                    <th>Operador</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fa fa-flash"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($services as $service)
                                <tr>
                                    <td class="text-center">{{$service->PrincipalService->service}}</td>
                                    <td><strong>{{$service->location}}</strong></td>
                                    <td>{{$service->address}}</td>
                                    <td>{{$service->operador->name}}</td>
                                    <td>@if($service->state=="Activo")
                                        <a class="label label-info">{{$service->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$service->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/detailservice/'.$service->slug)}}" data-toggle="tooltip" title="Ver detalle" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-ravelry"></i></a>
                                        <a href="javascript:void(0)" data-toggle="tooltip" title="Desactivar servicio" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection