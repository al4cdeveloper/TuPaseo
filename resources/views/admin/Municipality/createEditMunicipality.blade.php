@extends('admin.layout.auth')

@if(isset($municipality))
    @section('title', 'Actualizar municipio')
@else
    @section('title', 'Crear municipio')
@endif
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Municipio</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($municipality))
			{!!Form::model($municipality,['url'=>['admin/municipalities/update',$municipality->id_municipality],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/municipalities/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            @if(isset($municipality))
                <div class="col-sm-6 col-sm-offset-6">
                     <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/municipalities/keydata/'.$municipality->slug)}}" class="btn btn-primary btn-sm">Datos Clave  <i class="fa fa-info-circle fa-lg"></i></a></div>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/municipalities/images/'.$municipality->slug)}}" class="btn btn-primary btn-sm">Administrar multimedia  <i class="fa fa-images fa-lg"></i></a></div>
                </div> 
            @endif
            <div class="col-sm-12">

                 <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Icono</label>
                            <div class="file-loading">

                                {!!Form::file('link_icon',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_icon')}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Departamento</label>
                    {!!Form::select('fk_department', $departments, null, ['class'=>'select-chosen'])!!}
                </div>
            </div>
			<div class="col-sm-12">
                <div class="form-group col-md-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('municipality_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del municipio', 'required','id'=>'municipality_name'])!!}
                        <span class="label label-danger">{{$errors->first('municipality_name') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-md-offset-1 col-xs-12">
                        <div class="form-group">
                            <label for="costo">Clima</label>
                            {!!Form::select('weather', $weather, null, ['class'=>'select-chosen'])!!}
                        </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div id="errors" style="color:red;"></div>
                <div class="col-md-4 col-md-offset-4">
                        <div id="map" style="height: 300px;width: 300px;"></div>
                    </div>

                    {!!Form::hidden('latitude',null,['id'=>'latitude'])!!}
                    {!!Form::hidden('longitude',null,['id'=>'longitude'])!!}
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Vídeo de youtube (Opcional)</label>
                    {!!Form::text('iframe', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de youtube en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('iframe') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($municipality)){!!$municipality->description!!}@endif</textarea>
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary" name="typesubmit" value="guardarysalir">Guardar y salir</button>
                <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>

                <a href="{{url('admin/municipalities')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
        <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
        defaultPreviewContent: '<img src="{{asset($municipality->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality)&& $municipality->link_icon!=null)
        defaultPreviewContent: '<img src="{{asset($municipality->link_icon)}}" alt="Icono por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-icon.png')}}" alt="Icono por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["ico", "png",]
    });

    $("#municipality_name").on('blur',function()
        {
            if($("#latitude").val() == "" || $("#longitude").val()=="")
            {
                $("#municipality_name").val('');
            }
        });
        @if($errors->first('address'))
            $("#municipality_name").val('');
        @endif

        @if(!isset($municipality))
        $("#municipality_name").val('');

        @endif

        $("#interesSite").change(function(event) {
            $('#type').val('InterestSite');
        });
        var controladorTiempo = "";
            var markers = [];
           function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 10,
                  center: {lat: 4.570868, lng: -74.29733299999998}
                });
                var geocoder = new google.maps.Geocoder();
                $("#municipality_name").on("keyup", function() {
                    clearTimeout(controladorTiempo);
                    controladorTiempo = setTimeout(geocodeAddress(geocoder, map), 2000);
                });
               
              }
        function geocodeAddress(geocoder, resultsMap) 
        {
            var address = document.getElementById('municipality_name').value+",Colombia" ;
            $('#errors').empty();
            clearMarkers();
            if(address!= "")
            {
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === 'OK') {
                    clearMarkers();
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                     markers.push(marker);
                    $('#errors').empty();

                    $("#latitude").val(results[0].geometry.location.lat());
                    $("#longitude").val(results[0].geometry.location.lng());
                    console.log(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
                  } else {
                    $('#errors').empty();
                     $("#latitude").val('');
                    $("#longitude").val('');
                    $('#errors').append('No se puede encontrar su localización, intentelo nuevamente');
                  }
                });
            }
            
          }
          // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw&callback=initMap"
        async defer></script>
</script>
@endsection