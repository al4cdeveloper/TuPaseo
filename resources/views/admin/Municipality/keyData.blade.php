@extends('admin.layout.auth')


    @section('title', 'Datos Clave de '.$municipality->municipality_name)

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Datos Clave de {{$municipality->municipality_name}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">

                <h2>Ingrese la información solicitada</h2>
            </div>
			{!!Form::model($municipality,['url'=>['admin/municipalities/keydata',$municipality->id_municipality],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
                <a href="{{url('admin/municipalities/edit/'.$municipality->slug)}}" class="btn btn-effect-ripple btn-warning pull-right">Volver al municipio</a>

                        <table id="datatable" class="table  table-vcenter table-condensed">
                            <thead>
                                <tr>
                                    <th style="width: 500px;" >Nombre</th>
                                    <th style="width: 120px;">Información</th>
                                    <th style="width: 320px;">Categoría</th>
                                    <th style="width: 80px;"></th>
                                </tr>
                            </thead>
                            <tbody {{$row = 1}}>
                                @foreach($municipality->KeyData as $keydata)
                                <tr id="data{{$row}}">
                                    <td><input type="text" name="name[{{$row}}]" class="form-control" placeholder="Ej:Alcaldía Municipal" value="{{$keydata->keydata_name}}"></td>
                                    <td><input type="text" name="value[{{$row}}]"  class="form-control" placeholder="Ej:(057)(1)8563400" value="{{$keydata->keydata_value}}"></td>
                                    <td>
                                        <select name="category[{{$row}}]" class="select-chosen">
                                            @foreach($categories as $category)
                                                <option value="{{$category}}" @if($keydata->category == $category) selected @endif>{{$category}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td id="add{{$row}}">
                                        <button type="button" data-toggle="tooltip" title="Quitar este dato" onclick="removeData({{$row}})" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button>
                                    </td>
                                </tr {{$row++}}>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12" align="center">
                            <button class="btn btn-primary" type="button" onclick="addData()" id="addRoombutton">Agregar nuevo</button>
                        </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="form-group form-actions" align="center">
                        <button type="submit" class="btn btn-effect-ripple btn-info" name="typesubmit" value="guardartodo">Guardar todo</button>
                        <a href="{{url('admin/municipalities/edit/'.$municipality->slug)}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                    </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')
    
    <script>
        function addData(add)
        {
            var trs=$("#datatable tr").length;

                var nuevaFila=' <tr id="data'+trs+'"><td><input type="text" name="name['+trs+']" class="form-control" placeholder="Ej:Alcaldía Municipal" ></td><td><input type="text" name="value['+trs+']"  class="form-control" placeholder="Ej:(057)(1)8563400" ></td><td><select name="category['+trs+']" class="select-chosen">@foreach($categories as $category) <option value="{{$category}}" >{{$category}}</option>@endforeach</select></td><td id="add'+trs+'"><button type="button" data-toggle="tooltip" title="Quitar este dato" onclick="removeData('+trs+')" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times-circle" ></i></button></td></tr>';

                $("#datatable").append(nuevaFila);            
        }
        function removeData(row)
        {           
            $("#data"+row).remove();
        }
    </script>
   
@endsection