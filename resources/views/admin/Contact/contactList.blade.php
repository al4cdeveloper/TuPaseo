@extends('admin.layout.auth')

@section('title', 'Listado de mensajes de contacto')

@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Historial de mensajes de contacto</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Historial de mensajes de contacto</h2>
                </div>
                <form action="{{url('admin/contact')}}" style="margin:3%;" method="get">
                    <div class="row" >
                        
                        <div class="col-md-3 col-md-offset-4">
                            <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-chosen">Estado</label>
                                    <div class="col-md-9">
                                        <select name="state" id="state" class="select-chosen" data-placeholder="Seleccione estado">
                                            <option value=""></option>
                                            <option value="new">Nuevos</option>
                                            <option value="archivado">Archivado</option>
                                        </select>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-info" type="submit" value="search" name="button">Buscar</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <div class="table-responsive">
                        <table id="example2-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center">Cod</th>
                                    <th>Nombre</th>
                                    <th>Mail</th>
                                    <th>Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $contact)
                                    <tr>
                                        <td>{{$contact->id_contact}}</td>
                                        <td>{{$contact->name}}</td>
                                        <td>{{$contact->email}}</td>
                                        <td>@if($contact->state == 'new') <span class="label label-success">Nuevo </span>  @else <span class="label label-info">{{ucwords($contact->state)}} </span> @endif</td>
                                        <td><button class="btn btn-effect-ripple btn-xs btn-danger" data-toggle="modal" data-target="#detail{{$contact->id_contact}}"><i class="fa fa-eye"></i></button>
                                          @if($contact->state == 'new')
                                          <a href="{{url('admin/contact/'.$contact->id_contact.'/archive')}}"  data-toggle="tooltip" title="Archivar" class="btn btn-xs btn-success btn-effect-ripple"><i class="fas fa-archive"></i></a>
                                          @else
                                          <a href="{{url('admin/contact/'.$contact->id_contact.'/active')}}"  data-toggle="tooltip" title="Desarchivar" class="btn btn-xs btn-danger btn-effect-ripple"><i class="fas fa-archive"></i></a>
                                          @endif
                                        </td>
                                        <div id="detail{{$contact->id_contact}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header-tabs">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            Información contacto</a></li>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="tab-content">
                                                            <h3><i class="fas fa-info-circle"></i>  Información del usuario</h3>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                        <strong>Nombre: </strong>{{$contact->name}} <br>
                                                                        <strong>Teléfono: </strong>{{$contact->phone}} <br>
                                                                        <strong>Email: </strong>{{$contact->email}} <br>
                                                                        <strong>País: </strong>{{$contact->country}} <br>
                                                                        <strong>Región/Departamento: </strong>{{$contact->region}} <br>
                                                                        <strong>Ciudad: </strong>{{$contact->city}} <br>
                                                                        <strong>Mensaje: </strong><br>
                                                                        <p>{{$contact->message}}</p>
                                                                  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    
@endsection