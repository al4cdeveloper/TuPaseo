@extends('admin.layout.auth')

@section('title', 'Listado de sitios de interés')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Categorias de sitios de interés</h1>
                    </div>
                </div>

            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Categorias de sitios de interés</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/interestsites/category/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">Categoria</th>
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{$category->category_name}}</td>
                                <td>@if($category->state=="activo")
                                    <a class="label label-info">{{$category->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$category->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/interestsites/category/edit/'.$category->slug)}}" data-toggle="tooltip" title="Editar categoria" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($category->state=="activo")
                                    <a href="{{url('admin/interestsites/category/desactivate/'.$category->id_category)}}" data-toggle="tooltip" title="Desactivar categoria" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/interestsites/category/activate/'.$category->id_category)}}" data-toggle="tooltip" title="Activar categoria" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection