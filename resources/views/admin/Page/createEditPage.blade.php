@extends('admin.layout.auth')

@if(isset($page))
    @section('title', 'Actualizar página')
@else
    @section('title', 'Crear página')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Página</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($page))
            {!!Form::model($page,['url'=>['admin/pages/update',$page->id_page],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipage/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>'admin/pages/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipage/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre pagina</label>
                    {!!Form::text('page_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de pagina', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('page_name') }}</span>
                </div>
            </div> 
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">URL</label>
                    {!!Form::text('url', null, ['class'=>'form-control', 'placeholder' => 'Inserte dirección de pagina', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('url') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="duration">Descripción</label>
                     <textarea id="example-textarea-input" name="description" rows="4" class="form-control" placeholder="Descripción..">@if(isset($page)){!!$page->description!!}@endif</textarea>
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">

                        <label for="costo">Partes de página</label>
                         <select name="parts[]" id="services" class="select-chosen" multiple="">
                        @if(isset($page))
                            @foreach($parts as $partO)
                                <option value="{{$partO->id_part}}" 

                                    @foreach($page->Parts as $part)
                                     @if($part->id_part == $partO->id_part) selected @endif
                                    @endforeach
                                >{{$partO->part_name}}</option>
                            @endforeach
                        @else
                            @foreach($parts as $part)
                                <option value="{{$part->id_part}}" >{{$part->part_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/pages')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

@endsection