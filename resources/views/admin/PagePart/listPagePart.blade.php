@extends('admin.layout.auth')

@section('title', 'Listado de secciones')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Secciones de página</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Secciones de página</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/pageparts/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center">Sección</th>
                                <th>Tipo</th>
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($parts as $part)
                            <tr>
                                <td>{{$part->part_name}}</td>
                                <td>{{$part->type}}</td>
                                <td>@if($part->state=="activo")
                                    <a class="label label-info">{{$part->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$part->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/pageparts/edit/'.$part->slug)}}" data-toggle="tooltip" title="Editar sección" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($part->state=="activo")
                                    <a href="{{url('admin/pageparts/desactivate/'.$part->id_part)}}" data-toggle="tooltip" title="Desactivar sección" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/pageparts/activate/'.$part->id_part)}}" data-toggle="tooltip" title="Activar sección" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection