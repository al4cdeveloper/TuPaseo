@extends('admin.layout.auth')

@section('title', 'Dashboard')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Panel administrativo</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block">
            <div class="row">

                <div class="col-sm-6 col-lg-3">
                    <a href="javascript:void(0)" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-social">
                            <strong class="text-light-op">Hoy</strong>
                        </div>
                        <div class="widget-content themed-background-social clearfix">
                            <div class="widget-icon pull-right">
                                <i class="gi gi-airplane text-light-op"></i>
                            </div>
                            <h2 class="widget-heading h3 text-light"><strong>+ {{$reservations}}</strong></h2>
                            <span class="text-light-op">Nuevas Reservaciones</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <a href="{{url('admin/services')}}" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-flat">
                            <strong class="text-light-op">Este mes</strong>
                        </div>
                        <div class="widget-content themed-background-flat clearfix">
                            <div class="widget-icon pull-right">
                                <i class="fa fa-ravelry text-light-op"></i>
                            </div>
                            <h2 class="widget-heading h3 text-light"><strong>+ {{$services}}</strong></h2>
                            <span class="text-light-op">Servicios disponibles</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <a href="{{url('admin/operators')}}" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-creme">
                            <strong class="text-light-op">Ahora</strong>
                        </div>
                        <div class="widget-content themed-background-creme clearfix">
                            <div class="widget-icon pull-right">
                                <i class="fa fa-id-card text-light-op" aria-hidden="true"></i>

                            </div>
                            <h2 class="widget-heading h3 text-light"><strong>{{$operators}}</strong></h2>
                            <span class="text-light-op">Operadores</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <a href="{{url('/admin/contact')}}" class="widget">
                        <div class="widget-content widget-content-mini themed-background-dark-amethyst">
                            {{-- <span class="pull-right text-muted">+250%</span> --}}
                            <strong class="text-light-op">Hoy</strong>
                        </div>
                        <div class="widget-content themed-background-amethyst clearfix">
                            <div class="widget-icon pull-right">
                                <i class="fa fa-commenting text-light-op"></i>
                            </div>
                            <h2 class="widget-heading h3 text-light"><strong>{{$contacto}}</strong></h2>
                            <span class="text-light-op">Mensajes de contacto</span>
                        </div>
                    </a>
                </div>
            </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>
@endsection
