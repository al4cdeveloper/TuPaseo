@extends('admin.layout.auth')

@section('title', 'Lista de medios en que se realizan actividades')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Medios en que se realizan actividades</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Medios en que se realizan actividades</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/activities/ecosystem/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">Nombre de medio</th>
                                <th style="width: 120px;">Subtitulo</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ecosystems as $ecosystem)
                            <tr>
                                <td>{{$ecosystem->ecosystem_category}}</td>
                                <td>{{$ecosystem->subtitle}}</td>
                                <td class="text-center">
                                    <a href="{{url('admin/activities/ecosystem/edit/'.$ecosystem->id_ecosystem_category)}}" data-toggle="tooltip" title="Editar medio" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection