@extends('admin.layout.auth')

@section('title', 'Listado de Publicidad')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Publicidad</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Publicidad</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/patterns/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center">Cliente</th>
                                <th>Tipo</th>
                                <th>Clicks</th>
                                <th>Día de publicación</th>
                                <th>Día de caducidad</th>
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($patterns as $pattern)
                            <tr>
                                <td>{{$pattern->Customer->name}}</td>
                                <td>{{$pattern->multimedia_type}}</td>
                                <td>@if($pattern->clicks==null) No disponible @else {{$pattern->clicks}} @endif</td>
                                <td>{{$pattern->publication_day}}</td>
                                <td>{{$pattern->publication_finish}}</td>
                                <td>@if($pattern->state=="activo")
                                    <a class="label label-info">{{$pattern->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$pattern->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/patterns/edit/'.$pattern->id_pattern)}}" data-toggle="tooltip" title="Editar publicidad" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($pattern->state=="activo")
                                    <a href="{{url('admin/patterns/desactivate/'.$pattern->id_pattern)}}" data-toggle="tooltip" title="Desactivar publicidad" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                   
                                    @endif
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection