@extends('admin.layout.auth')

@if(isset($service))
    @section('title', 'Actualizar servicio')
@else
    @section('title', 'Crear servicio')
@endif

@section('aditionalStyle')
<link rel="stylesheet" href="{{asset('front/css/fontello/css/all-fontello.min.css')}}">
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Regiones</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($service))
			{!!Form::model($service,['url'=>['admin/activities/service/update',$service->id_service],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/activities/service/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="col-sm-3 text-center col-sm-offset-2">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen principal de servicio</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-center col-sm-offset-2">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen del mapa del servicio</label>
                            <div class="file-loading">

                                {!!Form::file('map_ico',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('map_ico') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="costo">Categoría de servicio</label>
                        {!!Form::select('fk_service_category', $categories, null, ['class'=>'select-chosen'])!!}
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1">
                    <div class="form-group">
                        <label for="costo">Medio en que se realiza</label>
                        {!!Form::select('fk_ecosystem_category', $ecosystems, null, ['class'=>'select-chosen'])!!}
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('service', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de servicio', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('service') }}</span>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="form-group">
                        <label for="costo">Nivel de impacto</label>
                        {!!Form::select('type_service', $typeService, null, ['class'=>'select-chosen'])!!}
                    </div>
                </div>
                <div class="col-sm-2 col-sm-offset-1">
                    <div class="form-group">
                        <li class="dropdown" style="list-style: none">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Icono
                                </a>

                                <ul class="dropdown-menu" role="menu" style="min-width: 130px!important;">
                                    <li>
                                        @foreach($icons as $icon)
                                        <ul> <button type="button" onclick="seleccion('{{$icon}}')" style="background: none; border: none;font-size: 30px;"><i class="{{$icon}}"></i></button></ul>
                                        @endforeach
                                    </li>
                                </ul>
                                <div id="result" style="    font-size: 25px;">
                                    
                                </div>
                            </li>
 
                    </div>
                </div>
            </div>
			
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Video <small>Opcional</small></label>
                    {!!Form::text('video_url', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de vídeo', 'required'])!!}
					<span class="label label-danger">{{$errors->first('video_url') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($service)){!!$service->description!!}@endif</textarea>
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            {!!Form::hidden('icon_class',null,['id'=>'icon_class'])!!}
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/activities/service')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection
@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
    script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>
    <script>
        @if(isset($service))
        jQuery(document).ready(function($) {
            $("#result").append('<i class="{{$service->icon_class}}"></i>');
            
        });
        @endif
        function seleccion(variable)
        {
            $("#result").empty();
            $("#result").append('<i class="'+variable+'"></i>');
            $("#icon_class").val(variable);

        }
    </script>
<script>
    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($service))
        defaultPreviewContent: '<img src="{{asset($service->image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","webp"]
    });
    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($service))
        defaultPreviewContent: '<img src="{{asset($service->map_ico)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","webp"]
    });
</script>
@endsection