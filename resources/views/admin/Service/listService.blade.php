@extends('admin.layout.auth')


@section('aditionalStyle')
<link rel="stylesheet" href="{{asset('front/css/fontello/css/all-fontello.min.css')}}">
@endsection
@section('title', 'Listado de servicios')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Servicios</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Servicios</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/activities/service/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">Servicio</th>
                                <th>Categoría</th>
                                <th>Medio</th>
                                <th>Icono</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td>{{$service->service}}</td>
                                <td>{{$service->Category->service_category}}</td>
                                <td>{{$service->ecosystem->ecosystem_category}}</td>
                                <td><i class="{{$service->icon_class}}"></i></td>
                                <td class="text-center">
                                    <a href="{{url('admin/activities/service/edit/'.$service->slug)}}" data-toggle="tooltip" title="Editar servicio" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection