@extends('admin.layout.auth')

@section('title', 'Listado de pagos a operadores')

@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Historial de pagos a operadores</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Historial de pagos a operadores</h2>
                </div>
                <form action="{{url('/admin/economy/history')}}" style="margin:3%;" method="get">
                    <div class="row" >
                        
                        <div class="col-md-3 col-md-offset-4">
                            <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-chosen">Estado</label>
                                    <div class="col-md-9">
                                        <select name="state" id="state" class="select-chosen" data-placeholder="Seleccione estado">
                                            <option value=""></option>
                                            <option value="generado">generado</option>
                                            <option value="cancelado">cancelado</option>
                                            <option value="pagado">pagado</option>
                                        </select>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success" type="submit" value="search" name="button">Buscar</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <div class="table-responsive">
                        <table id="example2-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center">Cod</th>
                                    <th>Fecha inicial</th>
                                    <th>Fecha final</th>
                                    <th>Reporte</th>
                                    <th>Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pays as $pay)
                                    <tr>
                                        <td>{{$pay->id_report}}</td>
                                        <td> {{\Carbon\Carbon::parse($pay->date_start)->toFormattedDateString()}}</td>
                                        <td> {{\Carbon\Carbon::parse($pay->date_finish)->toFormattedDateString()}}</td>
                                        <td> <a href="{{url('admin/economy/report/'.$pay->id_report.'/payreportreservation')}}" target="_blank"><span class="label label-danger">Ver reporte PDF</span></a></td>
                                        <td>{{$pay->state}}</td>
                                        <td>@if($pay->state=="generado") 
                                            <button type="button" class="btn btn-success btn-xs" onclick="checkPay({{$pay->id_report}})" data-toggle="tooltip" title="Se checkea un pago cuando ya se ha realizado el pago a los operadores (se notifica a los operadores sobre el pago)"><span class="glyphicon glyphicon-ok" aria-hidden="true" ></span> Checkear pago</button>
                                            <button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Al cancelar el pago se desactiva la opción de que los operadores puedan recibir el pago de este reporte" onclick="cancelPay({{$pay->id_report}})"><span class="glyphicon glyphicon-remove" aria-hidden="true" ></span> Cancelar pago</button>
                                        @elseif($pay->state == "cancelado") 
                                            <button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" title="Regresa el reporte a estado de 'generado', permitiendo realizar nuevamente el pago a los operadores del reporte" aria-hidden="true" onclick="reactivatePay({{$pay->id_report}})"><span class="glyphicon glyphicon-ok" ></span>Reactivar pago</button>
                                        @elseif("pagado") 
                                            <button type="button" class="btn btn-danger btn-xs"data-toggle="tooltip" title="Al cancelar el pago el reporte vuelve al estado de 'generado'." onclick="restartPay({{$pay->id_report}})"><span class="glyphicon glyphicon-remove" aria-hidden="true" ></span> Reiniciar Reporte</button>
                                        @endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <script>
            function checkPay(id)
            {
                swal({
                  title: "Estás seguro?",
                  text: "Se marcará este reporte como pagado y se notificará a los operadores. ¿Desea continuar?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  buttons: ["Cancelar", "Pagar reporte"],
                })
                .then((Activar) => {
                  if (Activar) 
                  {
                    swal("Se ha realizado el pago del reporte.", {
                      icon: "success",
                    });
                    setTimeout(function() {
                        window.location.href = '/admin/economy/checkpay/'+id;
                    }, 2000);
                    
                  } else {
                    swal("Acción cancelada");
                  }
                });
            }

            function restartPay(id)
            {
                swal({
                  title: "Estás seguro?",
                  text: "Se volverá este reporte a estado de 'Generado' y los operadores no podrán verificar el pago de este reporte. ¿Desea continuar?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  buttons: ["Cancelar", "Reiniciar reporte"],
                })
                .then((Activar) => {
                  if (Activar) 
                  {
                    swal("Se ha realizado el reinicio del reporte.", {
                      icon: "success",
                    });
                    setTimeout(function() {
                        window.location.href = '/admin/economy/restartpay/'+id;
                    }, 2000);
                    
                  } else {
                    swal("Acción cancelada");
                  }
                });
            }

            function cancelPay(id)
            {
                swal({
                  title: "Estás seguro?",
                  text: "El reporte seleccionado será cancelado, es decir, no se puede realizar el pago al operador. ¿Desea continuar?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  buttons: ["Cancelar", "Cancelar reporte"],
                })
                .then((Activar) => {
                  if (Activar) 
                  {
                    swal("Se ha realizado la cancelación del reporte.", {
                      icon: "success",
                    });
                    setTimeout(function() {
                        window.location.href = '/admin/economy/cancelpay/'+id;
                    }, 2000);
                    
                  } else {
                    swal("Acción cancelada");
                  }
                });
            }

            function reactivatePay(id)
            {
                swal({
                  title: "Estás seguro?",
                  text: "El reporte seleccionado volverá a estado de 'Generado' y permite realizar el pago a los operadores. ¿Desea continuar?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  buttons: ["Cancelar", "Reactivar reporte"],
                })
                .then((Activar) => {
                  if (Activar) 
                  {
                    swal("Se ha realizado la reactivación del reporte.", {
                      icon: "success",
                    });
                    setTimeout(function() {
                        window.location.href = '/admin/economy/reactivatepay/'+id;
                    }, 2000);
                    
                  } else {
                    swal("Acción cancelada");
                  }
                });
            }
        </script>
    
@endsection