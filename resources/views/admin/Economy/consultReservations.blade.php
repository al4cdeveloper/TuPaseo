@extends('admin.layout.auth')

@section('title', 'Listado de reservaciones')
        <!-- EL CÓDIGO COMENTADO ES PARA REALIZAR FACTURACIÓN POR CHECKS, NO SE VE COMO UNA ALTERNATIVA, SIN EMBARGO SE DEJA EL CÓDIGO -->
@section('aditionalStyle')
    
    <style>
    .has-error
    {
        border: 2px red solid;
    }
    </style>
@endsection

@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Historial de reservaciones</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Historial de reservaciones</h2>
                </div>
                <form action="{{url('/admin/economy/listreport')}}" style="margin:3%;" method="get">
                    <div class="row">
                        <div class="col-md-5">
                            <label class="col-md-3 control-label" for="example-daterange1">Rango de fechas</label>
                            <div class="col-md-9">
                                <div class="input-group input-daterange" data-date-format="yyyy-mm-dd">
                                    <input type="text" id="initialDate" name="initialDate" value="{{$inital}}" class="form-control {{ $errors->has('initialDate') ? ' has-error' : '' }}" placeholder="Desde">
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input type="text" id="finishDate" name="finishDate" value="{{$finish}}" class="form-control {{ $errors->has('finishDate') ? ' has-error' : '' }}" placeholder="Hasta">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-chosen">Estado</label>
                                    <div class="col-md-9">
                                        {!!Form::select('state',$options,$state,['class'=>'select-chosen',' data-placeholder'=>'Seleccione estado'])!!}
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label class="col-md-8 control-label" for="example-chosen">Reservas pendientes de pagar a operador</label>
                            <label class="csscheckbox csscheckbox-success">{!!Form::checkbox('paid','1',$paid)!!}<span></span></label>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger" type="submit" value="search" name="button">Buscar</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row" style="margin: 1%">
                    @if($paid && count($reservations)>0 && $state=='realizadas')
                    {!!Form::open(['url'=>'/admin/economy/generatereport','method'=>'post'])!!}
                        <input type="hidden" name="initial" value="{{$inital}}">
                        <input type="hidden" name="finish" value="{{$finish}}">
                        <button id="facturar" class="btn btn-success pull-right" type="submit">Generar Reporte</button>
                    {!!Form::close()!!}
                    @endif
                </div>
                <div class="row">
                    <div class="table-responsive">
                        @if(count($reservations)>0)
                        <table id="example2-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center">Cod</th>
                                    <th>Servicio</th>
                                    <th>Actividad</th>
                                    <th>Operador</th>
                                    <th>Fecha de realización</th>
                                    <th>Cantidad</th>
                                    <th>Costo total</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($reservations as $reservation)
                                    <tr>
                                        <td>
                                            <!--@if(!$reservation->paid)
                                            <input type="checkbox" id="facture{{$reservation->id_reservation}}" class="multireservations" onclick="multiReservations()" value="{{$reservation->id_reservation}}"> @endif-->
                                            {{$reservation->id_reservation}}</td>
                                        <td>{{$reservation->Service->service_name}}
                                            @if(count($reservation->orderProduct->items)>0)
                                            @foreach($reservation->orderProduct->items as $item)
                                                + {{$item->item_name}}
                                            @endforeach
                                        @endif</td>
                                        <td>{{$reservation->Service->PrincipalService->service}}</td>
                                        <td><a href="{{url('admin/operators?operator='.$reservation->Service->operador->id_operator)}}">{{$reservation->Service->operador->name}}</a></td>
                                        <td>{{$reservation->date}}</td>
                                        <td>{{$reservation->orderProduct->units}}</td>
                                        <td>{{cop_format($reservation->orderProduct->units*$reservation->orderProduct->unit_price)}}</td>
                                        <td>{{$reservation->state}}</td>
                                        <td><button class="btn btn-effect-ripple btn-xs btn-success" data-toggle="modal" data-target="#detail{{$reservation->id_reservation}}"><i class="fa fa-eye"></i></button>
                                        @if(count($reservation->Reprograms)>0)
                                        <button class="btn btn-default btn-sm" data-toggle="tooltip" title="Historial de repogramaciones" onclick="reprogramModal({{$reservation->id_reservation}})"><i class="fa fa-history"></i></button> 
                                        @endif</td>

                                        @if(count($reservation->Reprograms)>0)
                        <div id="reprogramModal{{$reservation->id_reservation}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h3 class="modal-title"><strong>Reprogramaciones de reservación</strong></h3>
                                        </div>
                                        <div class="modal-body">
                                            @foreach($reservation->Reprograms as $reprogram)
                                            <div class="row">
                                                <h4><strong>Razones de repogramación:</strong> {{$reprogram->reason}}</h4>
                                                <div class="col-md-6">
                                                    <h5>Fecha anterior</h5>
                                                    <p> {{$reprogram->previous_date}} a la(s) {{$reprogram->previous_time}}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5>Fecha de reprogramación</h5>
                                                    <p>{{$reprogram->new_date}} a la(s) {{$reprogram->new_time}}</p>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-effect-ripple btn-success" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                                        <div id="detail{{$reservation->id_reservation}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header-tabs">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <i class="fa fa-shopping-basket"></i>Información de compra</a></li>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="modal-tabs-home">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <h3>{{$reservation->Service->service_name}} <br>
                                                                        <small>Estado: {{$reservation->state}}</small>
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-6">
                                                                            <strong>Fecha reserva </strong>
                                                                                    <br>
                                                                                    {{$reservation->date}} - {{$reservation->time}}
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <strong>Cantidad</strong>
                                                                                    <br>{{$reservation->cuantity}}
                                                                        </div>
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-6">
                                                                              <strong>Actividad a realizar</strong>
                                                                              <br>{{$reservation->Service->PrincipalService->service}}
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <strong>Lugar</strong>
                                                                              <br>{{$reservation->Service->Municipality->municipality_name}}
                                                                        </div>
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12" align="center">
                                                                        La información detallada del servicio puede encontrarla <a href="{{url('/home/service/'.$reservation->Service->slug)}}">aquí</a>.
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                            <hr>
                                                            <div class="row">
                                                                <h4><i class="fa fa-info"></i> Información de pagador</h4>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                        <strong>Email: </strong>{{$reservation->orderProduct->order->shipping_order->email}} <br>
                                                                        <strong>Identificación: </strong>{{$reservation->orderProduct->order->shipping_order->type_dni}} - {{$reservation->orderProduct->order->shipping_order->dni}} <br>
                                                                        <strong>Nombres: </strong>{{$reservation->orderProduct->order->shipping_order->first_name}} {{$reservation->orderProduct->order->shipping_order->last_name}}  <br>
                                                                        <strong>Teléfono: </strong>{{$reservation->orderProduct->order->shipping_order->phone}} <br>
                                                                  </div>
                                                                  <div class="col-md-6">
                                                                        <strong>Direccion: </strong>{{$reservation->orderProduct->order->shipping_order->direccion}} <br>
                                                                        <strong>País: </strong>{{$reservation->orderProduct->order->shipping_order->country}} <br>
                                                                        <strong>Dpto/Provincia: </strong>{{$reservation->orderProduct->order->shipping_order->region}} <br>
                                                                        <strong>Ciudad: </strong>{{$reservation->orderProduct->order->shipping_order->city}} <br>
                                                                  </div>
                                                            </div>
                                                            <div class="row">
                                                                <h4><i class="fa fa-info"></i> Información de cuenta <small>Con la que se hizo la compra</small></h4>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                        <strong>Email: </strong>{{$reservation->orderProduct->order->customer->email}} <br>
                                                                        <strong>Identificación: </strong>{{$reservation->orderProduct->order->customer->type_dni}} - {{$reservation->orderProduct->order->customer->dni}} <br>
                                                                        <strong>Nombres: </strong>{{$reservation->orderProduct->order->customer->first_name}} {{$reservation->orderProduct->order->customer->last_name}}  <br>
                                                                        <strong>Teléfono: </strong>{{$reservation->orderProduct->order->customer->phone}} <br>
                                                                  </div>
                                                                  <div class="col-md-6">
                                                                        <strong>Direccion: </strong>{{$reservation->orderProduct->order->customer->direccion}} <br>
                                                                        <strong>País: </strong>{{$reservation->orderProduct->order->customer->country}} <br>
                                                                        <strong>Dpto/Provincia: </strong>{{$reservation->orderProduct->order->customer->region}} <br>
                                                                        <strong>Ciudad: </strong>{{$reservation->orderProduct->order->customer->city}} <br>
                                                                  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @elseif($reservations)

            
                                No se han encontrado resultados con los parametros de busqueda.

                        @endif
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script>
            /*var factures = [];

            function multiReservations()
            {
                var count = $(":checkbox.multireservations:checked").length;
                if(count>=1)
                {
                    $("#facturar").removeClass('hidden');
                }
                else
                {
                    $("#facturar").addClass('hidden');
                }
            }
            function facturar()
            {
                var val = $(":checkbox.multireservations:checked").val();
                var ids = [];

                $("input.multireservations[type=checkbox]:checked").each(function()
                {
                    //cada elemento seleccionado
                    ids.push($(this).val());
                });

                console.log(ids);

            }*/

            function reprogramModal(id)
            {
                $("#reprogramModal"+id).modal();
            }
        </script>


@endsection