@extends('admin.layout.auth')
@section('title', 'Servicios adicionales')
@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <a href="{{url('admin/operators/services/'.$service->operador->id_operator)}}" class="btn btn-effect-ripple btn-danger">Volver</a>
                    </div>
                    <div class="header-section">
                        <h1>Servicios adicionales del servicio <strong>{{$service->PrincipalService->service}}</strong></h1>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            
            <table id="datatable" class="table" role="grid" aria-describedby="datatable_info">
                <thead>
                    <th>Servicio adicional</th>
                    <th>Descripción</th>
                    <th>Costo</th>
                    <th>Estado</th>
                    <th style="width: 120px;">
                            &nbsp;
                        </th>
                </thead>
                <tbody>
                    @foreach($items as $item)
                    <tr >
                        <td>{{$item->item_name}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->cost}}</td>
                        <td>@if($item->state=="activo")
                            <a class="label label-info">{{$item->state}}</a>
                            @else
                            <span class="label label-danger">{{$item->state}}</span>
                            @endif
                        </td>
                        <td class="text-center">

                            <a href="{{url('admin/operators/items/edit/'.$item->id_service_item)}}" data-toggle="tooltip" title="Editar servicio adicional" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                            @if($item->state=="activo")
                            <a href="{{url('admin/operators/items/desactivate/'.$item->id_service_item)}}" data-toggle="tooltip" title="Desactivar servicio adicional" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            @else
                            <a href="{{url('admin/operators/items/activate/'.$item->id_service_item)}}" data-toggle="tooltip" title="Activar servicio adicional" class="btn btn-effect-ripple btn-xs btn-success"><i class="fa fa-check"></i></a>
                            @endif
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
       
    </div>
@endsection
