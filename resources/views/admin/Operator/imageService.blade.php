@extends('admin.layout.auth')

@section('title', 'Imágenes de servicios')


@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="pull-right">
                            <a href="{{url('admin/operators/services/'.$service->operador->id_operator)}}" class="btn btn-effect-ripple btn-danger">Volver</a>
                        </div>
                        <div class="header-section">
                            <h1>Imágenes de servicio</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->
     <!-- Example Block -->
        <div class="block">
			{!!Form::open(['url'=>['admin/operators/service/upload_images',$service->id_service_operator],'method'=>'PUT', 'class'=> 'form-horizontal dropzone', 'enctype' => 'multipart/form-data', 'id' => 'my-dropzone'])!!}
                <div class="panel-body">
                    <div class="dropzone-previews"></div>
                    <div class="dz-message" style="height:100px;">
                        Sube las imágenes aquí
                    </div>
                </div>
                {!! Form::close() !!}
        </div>

        <div class="block">
            <h2>Listado de Imágenes Existentes</h2>
            <div class="panel-body">
                <div class="gallery">
                    <ul class="">
		                @foreach($images as $image)

		                    <li id="image_li_{{$image->id_picture}}" class="ui-sortable-handle"><a href="javascript:void(0);" style="float:none;" class="image_link"><img src="{{asset($image->link_image)}}" alt=""></a>
		                     {!!Form::open(['url'=> ['admin/operators/service/delete_image', $image->id_picture] , 'method'=>'DELETE', 'class'=> 'form-horizontal'])!!}{!!Form::button('<span class="fa fa-trash" aria-hidden="true"></span> Eliminar', array('type' => 'submit', 'class'=>'btn btn-danger btn-block', 'onclick' => 'return confirm("¿Desea eliminar éste registro?");'))!!}{!!Form::close()!!}</li>
		                @endforeach
                    </ul>
                </div>
            </div>
	    </div>
@endsection
@section('aditionalScript')
<script src="{{asset('plugins/dropzone/dist/min/dropzone.min.js')}}"></script>

<script type="text/javascript">
    Dropzone.options.myDropzone = {
        autoProcessQueue: true,
        uploadMultiple: true,
        maxFiles: 4,
        maxFilesize: 2, // MB
        acceptedFiles: '.jpg, .jpeg, .png, .bmp',

        init: function() {
            var submitBtn = document.querySelector("#submit");
            myDropzone = this;

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    location.reload();
                }
            });

            this.on("success", function() {
                myDropzone.processQueue.bind(myDropzone);
            });
        }
    };

</script>