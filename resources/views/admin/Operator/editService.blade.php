@extends('admin.layout.auth')

@section('title', 'Editar servicio')
@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Editar servicio</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            <!-- END Labels on top Form Title -->

            <!-- Labels on top Form Content -->
			{!!Form::model($service,['url'=>['admin/operators/services/update',$service->id_service_operator],'method'=>'PUT', 'class'=> 'form-bordered', 'enctype' => 'multipart/form-data','novalidate'])!!}
                <div class="form-group">
                    <label for="example-nf-email">Servicio</label>
                    <select id="example-chosen" name="service" class="select-chosen" data-placeholder="Seleccione el servicio.." required>
                        <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
						@foreach($services as $listService)
                          <option value="{{$listService->id_service}}" @if(isset($service)) @if($service->PrincipalService->service == $listService->service) selected @endif @endif>{{$listService->service}}</option>
                        @endforeach
                    </select>
					<span class="label label-danger">{{$errors->first('service') }}</span>

                </div>
                 <div class="col-sm-12">
                    <div class="form-group">
                        <label for="costo">Nombre del servicio</label>
                        {!!Form::text('service_name', null, ['class'=>'form-control', 'placeholder' => 'Ej: Parapente + fotos + vídeo', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('service_name') }}</span>

                    </div>
                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="costo">Costo</label>
	                    {!!Form::number('cost', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo por persona', 'required'])!!}
						<span class="label label-danger">{{$errors->first('cost') }}</span>

	                </div>
                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="address">Dirección</label>
	                    {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Inserte dirección', 'required'])!!}
						<span class="label label-danger">{{$errors->first('address') }}</span>

	                </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group">

                            {!!Form::label('days', 'Días de funcionamiento')!!}
                            <select name="days[]" class="select-chosen" data-placeholder="Selecciona los días de servicio" multiple required disabled>
                                @foreach($days as $day)
                                    <option value="{{$day}}" @if(isset($service))

                                        @foreach($service->Schedule as $daySave)

                                                @if($daySave->day == $day) selected @endif 
                                        @endforeach

                                    @endif>{{$day}}</option>
                                @endforeach
                            </select>
                            <span class="label label-danger">{{$errors->first('days') }}</span>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="capacity">Capacidad de personas</label>
                            {!!Form::number('capacity', null, ['class'=>'form-control', 'placeholder' => 'Ingrese capacidad de personas', 'required'])!!}
                            <span class="label label-danger">{{$errors->first('capacity') }}</span>
                        </div>

                    </div>
                </div>
	            <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="duration">Horas de duración del servicio</label>
                            {!!Form::number('duration', null, ['class'=>'form-control', 'placeholder' => 'Ingrese horas de duración del servicio', 'required'])!!}
                            <span class="label label-danger">{{$errors->first('duration') }}</span>
                        </div>
                    </div>
                    @if(!isset($service))
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="location">Departamento</label>
                            {!!Form::select('departments', $departments, null, ['class'=>'select-chosen','id'=>'departments'])!!}
                        </div>
                    </div>
                    @else
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="location">Departamento</label>
                                <select name="departments" id="departments" class="select-chosen">
                                    @foreach($departments as $id => $departmentF)
                                        <option value="{{$id}}" @if($id == $department) selected @endif>{{$departmentF}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-sm-12" id="exactSite">
                    <div class="col-sm-6 @if(!isset($service)) hidden @endif " id="municipalityDiv">
                        <div class="form-group">
                            <label for="location">Municipio</label>
                            <select name="fk_municipality" id="municipalities" class="select-chosen">
                                @if(isset($service))
                                    @if($service->type_relation=="Municipality")
                                        @foreach($municipalities as $id => $municipalityF)
                                            <option value="{{$id}}" @if($service->fk_location==$id) selected @endif>{{$municipalityF}}</option>    
                                        @endforeach
                                    @else
                                        @foreach($municipalities as $id => $municipalityF)
                                            <option value="{{$id}}" @if($service->fk_location==$id) selected @endif>{{$municipalityF}}</option>    
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>
                     <div class="col-sm-6" >
                        <div class="form-group @if(isset($service) && $service->type_relation!="InterestSite") hidden @endif" id="siteDiv">
                            <label for="location">Sitio de interés</label>
                            <select name="fk_site" id="interesSite" class="select-chosen">
                                @if(isset($service))
                                    @if($service->type_relation=="InterestSite")
                                        @foreach($municipality->InterestSite as $site)
                                            <option value="{{$site->id_site}}" @if($service->fk_location==$site->id_site) selected @endif>{{$site->site_name}}</option>    
                                        @endforeach
                                    @else
                                        @if(count($municipality->InterestSite)>0)
                                            <option value=""></option>
                                            @foreach($municipality->InterestSite as $site)
                                                <option value="{{$site->id_site}}" >{{$site->site_name}}</option>    
                                            @endforeach
                                        @endif
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
	                <label for="duration">Descripción</label>
                    <div class="form-group">
                        <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($service)){!!$service->description!!}@endif</textarea>
						<span class="label label-danger">{{$errors->first('description') }}</span>
                    </div>
	            </div>
	            <div class="form-group">
	                <label for="duration">Recomendaciones</label>
                    <div class="form-group">
                        <textarea id="textarea-ckeditor" name="requisites" class="ckeditor">@if(isset($service)){!!$service->requisites!!}@endif</textarea>
						<span class="label label-danger">{{$errors->first('requisites') }}</span>
                    </div>
	            </div>
                
                 {!!Form::hidden('type_relation', null, ['id'=>'type'])!!}

                <div class="form-group form-actions" align="center">
                    <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                    <a href="{{url('admin/operators/services/'.$service->operador->id_operator)}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                </div>
			{!!Form::close()!!}
            <!-- END Labels on top Form Content -->
        </div>
	</div>
@endsection
@section('aditionalScript')
	
    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
 <script>
        /*$(document).ready(function()
        {
            if($("#departments").val()!="")
            {
                token = '{{csrf_token()}}' 
                $.ajax({
                    url: '/api/getmunicipalities',
                    type: 'POST',
                    headers:{'X-CSRF-TOKEN':token},
                    dataType: 'JSON',
                    data: {department: $("#departments").val()},
                    complete:function(transport)
                    {
                        response = transport.responseJSON;
                        $('#municipalities').empty();
                        $("#municipalityDiv").removeClass('hidden');
                        $("#municipalities").append('<option value=""></option>').trigger("chosen:updated");

                        for(x in response)
                        {
                            $("#municipalities").append('<option value="'+x+'">'+response[x]+'</option>').trigger("chosen:updated");
                        }
                    }
            })
            }
        });*/
        $("#departments").change(function()
        {   
            token = '{{csrf_token()}}' 
            $.ajax({
                url: '/api/getmunicipalities',
                type: 'POST',
                headers:{'X-CSRF-TOKEN':token},
                dataType: 'JSON',
                data: {department: $(this).val()},
                complete:function(transport)
                {
                    response = transport.responseJSON;
                    $('#municipalities').empty();
                    $("#municipalityDiv").removeClass('hidden');
                    $("#interesSite").empty();
                    $("#siteDiv").addClass('hidden');
                    $("#municipalities").append('<option value=""></option>').trigger("chosen:updated");

                    for(x in response)
                    {
                        $("#municipalities").append('<option value="'+x+'">'+response[x]+'</option>').trigger("chosen:updated");
                    }
                }
            })
        })
        $('#municipalities').change(function(event) 
        {
            token = '{{csrf_token()}}';
            $.ajax({
                url: '/api/getinterestsites',
                type: 'POST',
                dataType: 'JSON',
                data: {municipality: $(this).val()},
                headers:{'X-CSRF-TOKEN':token},
                complete:function (transport)
                {
                    response = transport.responseJSON;
                    if(response['number']>0)
                    {
                        $("#siteDiv").removeClass('hidden');
                        $("#interesSite").empty();
                        $("#interesSite").append('<option value=""></option>').trigger("chosen:updated");
                        for(x in response['sites'])
                        {
                            $("#interesSite").append('<option value="'+x+'">'+response['sites'][x]+'</option>').trigger("chosen:updated");
                        }
                    }
                    else
                    {
                        $("#siteDiv").addClass('hidden');
                        $("#interesSite").empty();
                    }
                }
            });
            $('#type').val('Municipality');
        });

        $("#interesSite").change(function(event) {
            $('#type').val('InterestSite');
        });
    </script>
@endsection