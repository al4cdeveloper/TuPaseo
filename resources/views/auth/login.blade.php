<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Login tu paseo</title>


        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->

    <link rel="alternate" href="http://tupaseo.travel" hreflang="es-co" />
    
        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
        <!-- END Stylesheets -->
        <style>
            .modal-backdrop.in
            {
                z-index: 1;
            }
            #myModal
            {
                margin-top: 15%;
            }
        </style>
        <!-- Modernizr (browser feature detection library) -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>
@include('alerts.messages')

    </head>
    <body>
        <!-- Full Background -->
        <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
        <img src="{{asset('images/backlogin.jpg')}}" alt="Full Background" class="full-bg animation-pulseSlow">
        <!-- END Full Background -->

        <!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
                <a href="{{url('/')}}"><img src="{{asset('images/tupaseo.png')}}"></a>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuick">
                <!-- Login Title -->
                <div class="block-title">
                    <h2>Iniciar sesión</h2>
                </div>
                <!-- END Login Title -->

                <!-- Login Form -->
                @if(session('cart'))
                    <form id="form-login" action="{{ route('logincart') }}" method="post" class="form-horizontal">
                @else
                <form id="form-login" action="{{ url('/login') }}" method="post" class="form-horizontal">
                
                @endif
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-xs-12">Correo Electrónico</label>
                        <div class="col-xs-12">
                            <input type="text" id="login-email" name="email" class="form-control" placeholder="Correo Electrónico" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                        
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-xs-12">Contraseña</label>
                        <div class="col-xs-12">
                            <input type="password" id="login-password" name="password" class="form-control" placeholder="Tu password">
                             @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-8">
                            <label class="csscheckbox csscheckbox-primary">
                                <input type="checkbox" id="login-remember-me" name="login-remember-me"{{ old('remember') ? 'checked' : '' }} ><span></span> ¿Recordarme?
                            </label>
                        </div>
                        <div class="col-xs-4 text-right">
                            <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Iniciar sesión</button>
                        </div>
                        <div class="col-xs-6 ">
                            <a href="#" data-toggle="modal" data-target="#myModal" >Registrarme</a>
                        </div>
                            
                        <div class="col-xs-6">
                            <a href="{{url('password/reset')}}" >Olvidé mi contraseña</a>
                        </div>
                    </div>
                </form>
                <!-- END Login Form -->

                <!-- Social Login -->
                <hr>
                @if (session('pending'))
                    <div class="row push" >
                        <div class="col-xs-8 col-xs-offset-2">
                            <a href="{{url('/verifycode/resend')}}" class="btn btn-effect-ripple btn-sm btn-warning btn-block"><i class="fa fa-share-square"></i> Reenviar código de verificación</a>
                        </div>
                    </div>
                @else
                <div class="push text-center">o inicia sesión con</div>
                <div class="row push">
                    <div class="col-xs-6">
                        <a href="{{route('social.auth', 'facebook')}}" class="btn btn-effect-ripple btn-sm btn-info btn-block"><i class="fa fa-facebook"></i> Facebook</a>
                    </div>
                    <div class="col-xs-6">
                        <a href="{{route('social.auth', 'google')}}" class="btn btn-effect-ripple btn-sm btn-danger btn-block"><i class="fa fa-google" aria-hidden="true"></i> Google</a>
                    </div>
                </div>
                @endif
                <!-- END Social Login -->
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 5111;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body" align="center">
                        <div class="row">
                            <a href="{{url('/operator/register')}}" class="btn btn-danger">Soy Operador</a>
                        </div>
                        <br>
                        <div class="row">
                            <a href="{{url('register')}}" class="btn btn-warning">Soy Usuario</a>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <footer class="text-muted text-center animation-pullUp">
                <small> &copy; 2016-2018 <a href="{{url("/")}}" target="_blank">TuPaseo.travel</a> Reservados todos los derechos.</small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->

        <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
        <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
        <script src="{{asset('auth-panel/js/app.js')}}"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/readyLogin.js')}}"></script>
        <script>$(function(){ ReadyLogin.init(); });</script>
    </body>
</html>

