
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Registro tu paseo</title>


        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
        <!-- END Icons -->

    <link rel="alternate" href="http://tupaseo.travel" hreflang="es-co" />

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">
        <!-- END Stylesheets -->
        <style>
            .modal-backdrop.in
            {
                z-index: 1;
            }
            #myModal
            {
                margin-top: 15%;
            }
        </style>
        <!-- Modernizr (browser feature detection library) -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>
@include('alerts.messages')

    </head>
    <body>
        <!-- Full Background -->
        <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
        <img src="{{asset('images/backlogin.jpg')}}" alt="Full Background" class="full-bg animation-pulseSlow">
        <!-- END Full Background -->

        <!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
                <a href="{{url('/')}}"><img src="{{asset('images/tupaseo.png')}}"></a>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuick">
                <!-- Login Title -->
                <div class="block-title">
                    <h2>Registro de usuario</h2>
                </div>
                <!-- END Login Title -->

                <!-- Login Form -->
                    <form id="form-login" action="{{ url('register') }}" method="post" class="form-horizontal">

                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="first_name" class="col-xs-12">Nombre(s)</label>
                        <div class="col-xs-12">
                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Nombres" value="{{ old('first_name') }}" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="last_name" class="col-xs-12">Apellidos</label>
                        <div class="col-xs-12">
                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Apellidos" value="{{ old('last_name') }}" required autofocus>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-xs-12">Telefono</label>
                        <div class="col-xs-12">
                            <input type="number" id="phone" name="phone" class="form-control" placeholder="Telefono" value="{{ old('phone') }}" required autofocus>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-xs-12">Email</label>
                        <div class="col-xs-12">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                        
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-xs-12">Password</label>
                        <div class="col-xs-12">
                            <input type="password" id="login-password" name="password" class="form-control" placeholder="Tu password.." required>
                             @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password-confirm" class="col-xs-12">Confirmar Password</label>

                        <div class="col-xs-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                          <label class="col-md-6 "> <input type="checkbox" id="val-terms" name="terms" value="1"  required><a href="#modal-terms" style="color: #6c9e2e" data-toggle="modal">Acepto terminos y condiciones</a> <span class="text-danger">*</span></label>
                          <label class="col-md-6 "> <input type="checkbox" name="promotionals_mails" value="1"> <a href="{{url('politica_proteccion_datos')}}" style="color: #6c9e2e" target="_blank">Acepto recibir información promocional</a></label>
                      </div>
                      <!-- Terms Modal -->
                            <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title text-center"><strong>Terminos y Condiciones</strong></h3>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="page-header">1. <strong>General</strong></h4>
                                            <p>TUPASEO.TRAVEL es una agencia de viajes en línea, y su plataforma tecnológica permite a los prestadores de servicios turísticos ofrecer y comercializar sus servicios, actividades y demás servicios turísticos, y le permite a los usuarios comparar y reservar dichas prestaciones en tiempo real, y adquirirlas por separado o combinadas, armando y gestionando su propio viaje, de acuerdo a sus intereses y necesidades personales.</p>
                                            <br>
                                            <p>TUPASEO.TRAVEL actúa como intermediario entre Usted y los Proveedores de Servicios Turísticos y no actúa en nombre y representación del Proveedor. TUPASEO.TRAVEL no es el proveedor de los Servicios Turísticos y no actúa en nombre y por cuenta de los Proveedores, por lo que no está obligado directa ni indirectamente a la ejecución del Servicio Turístico. Cuando Usted contrata Servicios Turísticos a través de TUPASEO.TRAVEL, está celebrando un contrato directamente con el Proveedor que ofrece sus Servicios Turísticos a través de la Plataforma.
                                            <br>
                                            Al contratar Servicios Turísticos a través de TUPASEO.TRAVEL, Usted acepta haber leído, entendido y estar de acuerdo con los términos y condiciones que se muestran a continuación. Así mismo, Usted garantiza que: (i) es mayor de edad; (ii) posee plena capacidad para celebrar contratos; (iii) solo utiliza la Plataforma para reservar o contratar Servicios Turísticos para Usted y/o para otra persona para quien Usted tenga autorización de actuar; y (iv) toda la información que Usted brinda a TU PASEO es verídica, exacta, actual y completa.</p>
                                            <br>
                                            <h4 class="page-header">2. <strong>Datos Personales e Información de Contacto</strong></h4>
                                            <p>Usted deberá completar todos los campos con datos exactos para poder realizar reservas válidas de Servicios Turísticos a través de TU PASEO. Al ingresar sus datos personales en la Plataforma, Usted declara que dichos datos son exactos, precisos y verdaderos, y asume el compromiso de actualizar dicha información conforme resulte necesario. Usted garantiza y responde por la veracidad, exactitud, vigencia y autenticidad de los Datos Personales ingresados.
                                            <br>
                                            Usted deberá ingresar una dirección de correo electrónico en la sección “Información de Contacto”. Ese correo es nuestra vía de contacto con Usted. Revise que el correo electrónico proporcionado como punto de contacto sea correcto y manténgase atento a la información que se le enviará a dicho correo. Se considerará que Usted tomó conocimiento oportuno de la confirmación de su Solicitud de Compra o de algún inconveniente o variación a la misma con el envío de dicha información al correo electrónico proporcionado por Usted. Tenga presente que si el correo electrónico ingresado por Ud. contiene algún error no recibirá comunicaciones importantes para la gestión de su Solicitud de Compra. De todas maneras, Ud. puede ingresar a su Perfil y realizar el seguimiento y/o gestionar todas sus Solicitudes de Compra desde allí.</p>
                                            <h4 class="page-header">3. <strong>Información que encontrará en TUPASEO.TRAVEL</strong></h4>
                                            <p>La información publicada en la Plataforma de TU PASEO relacionada con actividades, destinos, operadores, información comercial o patrocinada, precios, características de los Servicios Turísticos, su disponibilidad, condiciones de venta, restricciones y políticas de cancelación o reembolso (entre otras) es establecida y proporcionada por los Proveedores de los Servicios Turísticos o por Terceros en línea y en tiempo real. En su calidad de intermediario, TU PASEO le informa acerca de las características de los Servicios Turísticos, gestiona sus Solicitudes de Compra con los Proveedores, recauda, de ser aplicable, los valores correspondientes a las tarifas y le apoya en la búsqueda de soluciones en caso de presentarse una inquietud o un inconveniente.
                                            <br>
                                            TU PASEO realiza sus mayores esfuerzos a fin de brindar información correcta y precisa, pero no se responsabiliza por errores (como errores manifiestos y tipográficos), interrupciones (debido a caídas temporales y/o parciales del servidor o a reparaciones), imprecisiones, información engañosa o falsa emanada de los Proveedores de los Servicios Turísticos.
                                            <br>
                                            Le aconsejamos leer los comentarios de otros usuarios que figuran en la Plataforma para asesorarse sobre el (los) Servicio(s) Turístico(s) elegido(s) y valorar si éste se ajusta a sus necesidades y expectativas.</p>
                                            <h4 class="page-header">4. <strong>Actividades Riesgosas</strong></h4>
                                            <p>En los casos en los que la Actividad contratada sea una actividad de riesgo Ud. asume el riesgo de participar en la Actividad y Ud. declara estar sano y gozar de buena salud física, mental y estar debidamente entrenado para participar de esa Actividad. Mediante el presente Ud. acepta y asume la total responsabilidad por cualquier y todos los riesgos de enfermedad, herida o muerte que pudiesen ocurrir en el desarrollo de la Actividad.
                                            <br>
                                            Tenga muy presente que cada operador turístico y cada actividad tiene sus propias regulaciones del servicio y tarifas aplicables a las actividades, excursiones, traslados, tickets y atracciones que proveen y Ud, debe presentar el carnet de Entidad Prestadora de Salud, vigente, activa y al día.</p>
                                            
                                            <h4 class="page-header">5. <strong>Responsabilidad</strong></h4>
                                            <p>TU PASEO no será responsable de las condiciones de las atracciones o lugares, ni de actos de comisión u omisión por parte de terceros en cualquier actividad, lugar o servicio.
                                            <br>
                                            TU PASEO se reserva el derecho, a su sola discreción, de modificar, alterar o de otra manera actualizar, estos Términos y Condiciones en cualquier momento. Las modificaciones entrarán en vigencia desde el momento que se indique; en su defecto, se entenderá que su aplicación es inmediata. Usando esta Plataforma después de publicadas las modificaciones, Usted acepta estar sujeto a dichas modificaciones, alteraciones o actualizaciones, sin derecho a efectuar reclamo alguno con relación a ello.</p>

                                            <h4 class="page-header"><strong>JURISDICCIÓN Y LEY APLICABLE</strong></h4>
                                            <p>Nos reservamos el derecho, en cualquier momento, de suspender o modificar cualquier parte de estos Términos de uso, según lo consideremos necesario o deseable. Si realizamos cambios que puedan afectar materialmente al uso de nuestra página web o de nuestros servicios, te informaremos del cambio publicando un aviso en la página web. Cualquier modificación de estos Términos de uso será efectivo a partir de la publicación del aviso de los cambios en nuestra página web.</p>
                                            <p>TUPASEO SAS, NIT 901193027-4, se encuentra ubicada en la Carrera 9 # 72-81 Oficina 504, Bogotá Colombia, Teléfono: (+57) 1 3121540.</p>
                                            <br>
                                            <p>Estimado Usuario, puedes consultar el documento completo de estos<a href="{{url('terminos')}}" target="_blank">Términos y Condiciones haciendo clic aquí</a>.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="text-center">
                                                <button type="button" class="btn btn-effect-ripple btn-primary" data-dismiss="modal" style="overflow: hidden; position: relative;">¡Los he leído!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                      <!-- END Terms Modal -->
                    <div class="form-group form-actions" align="center">
       
                        <div class="col-xs-6 ">
                            <button class="btn btn-primary">Registrarme</button>
                        </div>
                    </div>
                </form>
                <!-- END Login Form -->

                <!-- Social Login -->
                <hr>
                <div class="push text-center">o inicia sesión con</div>
                <div class="row push">
                    <div class="col-xs-6">
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-sm btn-info btn-block"><i class="fa fa-facebook"></i> Facebook</a>
                    </div>
                    <div class="col-xs-6">
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-sm btn-danger btn-block"><i class="fa fa-google" aria-hidden="true"></i> Google</a>
                    </div>
                </div>
                <!-- END Social Login -->
            </div>
            
            

            <!-- Footer -->
            <footer class="text-muted text-center animation-pullUp">
                <small> &copy; 2016-2018 <a href="{{url("/")}}" target="_blank">TuPaseo.travel</a> Reservados todos los derechos.</small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->

        <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
        <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
        <script src="{{asset('auth-panel/js/app.js')}}"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/readyLogin.js')}}"></script>
        <script>$(function(){ ReadyLogin.init(); });</script>
    </body>
</html>

