<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Página no encontrada | TuPaseo</title>
	<!-- Most Important Meta -->
    <meta name="robots" content="error">
    <meta name="description" content="TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia">
    <meta name="keywords" content="turismo, paseo, pasear, actividades de aventura, turismo de naturaleza, deportes de alto riesgo, deportes extremos, colombia, escapadas, fin de semana">
    <meta name="author" content="Puntos Suspensivos Editores Consultores Ltda">
    <meta name="creator" content="Sergio David Hernández Goyeneche">
    <meta name="creator" content="Euclides Rafael Alsina">
	 <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107504893-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107504893-1');
    </script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
 function hideURLbar(){ window.scrollTo(0,1); }
    </script>
    <!-- termina GTM -->
    <!-- TWITTER OG -->
    <meta name="twitter:creator" content="@" />
    <meta name="twitter:site" content="@" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="TuPaseo.Travel" />
    <meta name="twitter:description" content="TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia" />
    <meta name="twitter:image" content="{{asset('front/img/logo.png')}}" />
 <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" type="image/x-icon" href="{{asset('img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/icon180.png')}}" sizes="180x180">
    <!-- FACEBOOK OG -->
    <meta property="fb:app_id" content="" /> 
    <meta property="og:type"   content="article" /> 
    <meta property="og:url"    content="http://www.tupaseo.travel/" /> 
    <meta property="og:title"  content="TuPaseo.Travel" /> 
    <meta property="og:image"  content="http://www.tupaseo.travel/img/logo.png" />
    <meta property="og:description" content="TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia" />
    <meta name="author" content="TuPaseo.Travel">

    <!-- Marcado JSON-LD generado por el Asistente para el marcado de datos estructurados de Google. -->
    <script type="application/ld+json">
        {
          "@context" : "http://schema.org",
          "@type" : "LocalBusiness",
          "name" : "TuPaseo.Travel",
          "image" : "http://pseditores.com/seminario/img/logo.png",
          "telephone" : "+57 (1) 3121540",
          "email" : "info@rutascolombia.com",
          "address" : {
            "@type" : "PostalAddress",
            "streetAddress" : "Cra. 9 # 72-81 Oficina 504",
            "addressLocality" : "Bogotá D.C.",
            "addressCountry" : "Colombia"
          },
          "openingHoursSpecification" : {
            "@type" : "OpeningHoursSpecification",
            "dayOfWeek" : {
              "@type" : "DayOfWeek",
              "name" : "8:30 a.m. - 5:30 p.m."
            }
          },
          "url" : "http://www.tupaseo.travel/",
          "review" : {
            "@type" : "Review",
            "author" : {
              "@type" : "Person",
              "name" : "TuPaseo.Travel"
            },
            "reviewBody" : "TuPaseo es una plataforma digital que conecta a los operadores turísticos con viajeros que buscan realizar actividades de aventura en Colombia"
          }
        }
    </script>

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="{{asset('errors/css/base.css')}}">  
   <link rel="stylesheet" href="{{asset('errors/css/main.css')}}">
   <link rel="stylesheet" href="{{asset('errors/css/vendor.css')}}">     

   <!-- script
   ================================================== -->
	<script src="{{asset('errors/js/modernizr.js')}}"></script>

   <!-- favicons
	================================================== -->
	<link rel="icon" type="image/png" href="favicon.png">

</head>

<body>

	<!-- header 
   ================================================== -->
   <header class="main-header">
   	<div class="row">
   		<div class="logo ">
	         <a href="/"><img src="{{asset('high_logo.png')}}" alt="TuPaseo"></a>
	      </div>   		
   	</div>   
   </header> <!-- /header -->

	<!-- main content
   ================================================== -->
   <main id="main-404-content" class="main-content-slides">

   	<div class="content-wrap">

		   <div class="shadow-overlay"></div>

		   <div class="main-content">
		   	<div class="row">
		   		<div class="col-twelve">
			  		
			  			<h1 class="kern-this">Error 419.</h1>
			  			<p>
						Parece que no hay nada en esta ubicación. 
			  			</p>
			   	</div> <!-- /twelve --> 		   			
		   	</div> <!-- /row -->    		 		
		   </div> <!-- /main-content --> 

		   <footer>
		   	<div class="row">

		   		<div class="col-seven tab-full social-links pull-right">
			   		<ul>
				   		<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					      <li><a href="#"><i class="fa fa-instagram"></i></a></li>   			
				   	</ul>
			   	</div>
		   			
		  			<div class="col-five tab-full bottom-links">
			   		<ul class="links">
				   		<li><a href="/home">Inicio</a></li>
				         <li><a href="/home/services">Planes</a></li>		                    
				   	</ul>

				   	<div class="credits">
				   		<p>TuPaseo 2018</p>
				   	</div>
			   	</div>   		   		

		   	</div> <!-- /row -->    		  		
		   </footer>

		</div> <!-- /content-wrap -->
   
   </main> <!-- /main-404-content -->

   <div id="preloader"> 
    	<div id="loader"></div>
   </div> 

   <!-- Java Script
   ================================================== --> 
   <script src="{{asset('errors/js/jquery-2.1.3.min.js')}}"></script>
   <script src="{{asset('errors/js/plugins.js')}}"></script>
   <script src="{{asset('errors/js/main.js')}}"></script>

</body>

</html>