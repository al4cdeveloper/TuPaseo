let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// mix.scripts([
// 	'resources/assets/js/toastr.js',

// 	'resources/assets/js/vue.js',
// 	'resources/assets/js/axios.js',
// 	'resources/assets/js/require.js'
// 	'resources/assets/js/Service.js',
// 	], 'public/vue/js/Service.js')
// 	.styles([
// 	'resources/assets/css/toastr.css',
// 	], 'public/vue/css/app.css');

//Servicio VUE
// mix.js('resources/assets/js/Service.js', 'public/vue/js/Service.js');

//Cart desde vue :3
/*mix.scripts([
	'resources/assets/js/toastr.js',

	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'resources/assets/js/vue-router.js',
	'resources/assets/js/cart.js',
	], 'public/vue/js/Cart.js')
	.styles([
	'resources/assets/css/toastr.css',
	], 'public/vue/css/cart.css');

*/
mix.js('resources/assets/js/vue-app.js', 'public/js/vue-app.js');

//Reprogramación de reservas operador
mix.babel(['resources/assets/js/vue.js',
'resources/assets/js/axios.js',
'resources/assets/js/reprogramReservations.js'],'public/js/reprogram-vue.js');