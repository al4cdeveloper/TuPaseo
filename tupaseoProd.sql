-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 27-08-2018 a las 15:26:40
-- Versión del servidor: 5.6.38
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tupaseoProd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Developer', 'dev@tupaseo.travel', '$2y$10$VB1eAj69/MluJ715LX/rwOb9PFTW9xrezDBKyL/zsdE5inZCx4RvC', 'x6eLB8iTPM8M21KiWRftlvEsBzq20LTv2UpNmUsWRnbr8DlW0ESEAOupXWUS', '2018-01-11 22:03:08', '2018-01-11 22:03:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `state` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `nit` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone_contact` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_contact` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id_department` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `short_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ecosystem_categories`
--

CREATE TABLE `ecosystem_categories` (
  `id_ecosystem_category` int(10) UNSIGNED NOT NULL,
  `ecosystem_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_image` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ecosystem_categories`
--

INSERT INTO `ecosystem_categories` (`id_ecosystem_category`, `ecosystem_category`, `subtitle`, `slug`, `link_image`, `created_at`, `updated_at`) VALUES
(1, 'Agua', 'Vive tu libertad', 'agua', 'images/ecosystem/Ecosystem_20180522132400796266540.jpg', '2017-10-23 15:55:27', '2018-05-22 18:24:00'),
(2, 'Tierra', 'Actividades de tierra bien chingonas', 'tierra', 'images/ecosystem/Ecosystem_20180823154416817657492.jpg', '2017-10-23 15:55:27', '2018-08-23 20:44:16'),
(3, 'Aire', NULL, 'aire', '', '2017-10-23 15:55:27', '2017-10-23 15:55:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishments`
--

CREATE TABLE `establishments` (
  `id_establishment` int(10) UNSIGNED NOT NULL,
  `type_establishment` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `establishment_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kitchen_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_operator` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_images`
--

CREATE TABLE `establishment_images` (
  `id_establishment_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_schedules`
--

CREATE TABLE `establishment_schedules` (
  `id_establishment_schedule` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `start` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishmen_images`
--

CREATE TABLE `establishmen_images` (
  `id_establishment_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_sites`
--

CREATE TABLE `interest_sites` (
  `id_site` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_category` int(10) UNSIGNED NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_icon` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `front_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_categories`
--

CREATE TABLE `interest_site_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `default_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `default_icon` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_images`
--

CREATE TABLE `interest_site_images` (
  `id_site_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `keydatas`
--

CREATE TABLE `keydatas` (
  `id_keydata` int(10) UNSIGNED NOT NULL,
  `keydata_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `keydata_value` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(67, '2014_10_12_000000_create_users_table', 1),
(68, '2014_10_12_100000_create_password_resets_table', 1),
(69, '2017_10_09_142538_create_operators_table', 1),
(70, '2017_10_09_165344_create_service_categories_table', 1),
(71, '2017_10_09_165703_create_ecosystem_categories_table', 1),
(72, '2017_10_09_165704_create_services_table', 1),
(73, '2017_10_09_185627_create_service_operators_table', 1),
(74, '2017_10_10_144259_create_contacts_table', 1),
(75, '2017_10_11_165651_create_operator_password_resets_table', 1),
(76, '2017_10_21_134427_create_schedules_table', 1),
(78, '2017_10_30_215700_create_service_pictures_table', 3),
(81, '2017_11_07_123037_create_orders_table', 4),
(83, '2018_01_11_164729_create_admins_table', 5),
(84, '2018_01_11_164730_create_admin_password_resets_table', 5),
(85, '2018_01_12_154748_create_polls_table', 6),
(94, '2018_01_22_195432_create_regions_table', 7),
(95, '2018_01_23_132002_create_reporters_table', 7),
(96, '2018_01_23_132003_create_reporter_password_resets_table', 7),
(97, '2018_01_25_172538_create_departments_table', 7),
(101, '2018_01_25_212046_create_municipalities_table', 8),
(102, '2018_01_29_170933_create_municipality_images_table', 8),
(103, '2018_02_08_174141_create_videos_table', 8),
(107, '2018_01_29_180040_create_interest_site_categories_table', 9),
(108, '2018_01_29_214958_create_interest_sites_table', 9),
(109, '2018_02_01_175959_create_interest_site_images_table', 9),
(110, '2018_02_12_212803_create_wallpapers_table', 10),
(111, '2018_03_07_161448_create_customers_table', 11),
(112, '2018_03_09_164310_create_page_parts_table', 12),
(113, '2018_03_09_175043_create_pages_table', 12),
(114, '2018_03_09_175907_create_page_part_pages_table', 12),
(115, '2018_03_12_170357_create_patterns_table', 12),
(116, '2018_03_12_182258_create_pattern_parts_table', 12),
(117, '2018_03_19_203947_create_service_items_table', 13),
(133, '2018_01_23_181617_create_region_images_table', 14),
(134, '2018_05_24_133954_create_establishments_table', 15),
(135, '2018_05_25_105415_create_service_establishments_table', 15),
(136, '2018_05_25_110456_create_service_establishment_pivots_table', 15),
(137, '2018_06_06_134451_create_plates_table', 15),
(138, '2018_06_06_144412_create_establishment_schedules_table', 15),
(139, '2018_06_12_133839_create_rooms_table', 16),
(140, '2018_06_12_164035_create_establishmen_images_table', 17),
(141, '2018_06_12_164820_create_establishment_images_table', 18),
(142, '2018_06_14_132751_create_keydatas_table', 19),
(173, '2018_07_13_134350_create_shipping_orders_table', 21),
(199, '2018_07_16_125933_create_order_products_table', 22),
(200, '2018_07_17_110310_create_reservations_table', 22),
(201, '2018_07_23_170208_create_order_product_items_table', 22),
(204, '2018_08_03_132616_create_pay_reports_table', 23),
(205, '2018_08_03_132809_create_report_reservations_table', 23),
(206, '2018_08_22_140900_create_reprograms_table', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_municipality` int(10) UNSIGNED NOT NULL,
  `municipality_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `weather` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link_icon` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `fk_department` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `front_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `front_state_hotel` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `front_state_restaurant` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_images`
--

CREATE TABLE `municipality_images` (
  `id_municipality_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operators`
--

CREATE TABLE `operators` (
  `id_operator` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_register` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_personal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nit` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days_for_reservation` int(11) NOT NULL,
  `terms` date NOT NULL,
  `data_protection` date NOT NULL,
  `sustainability` date NOT NULL,
  `promotionals_mails` tinyint(1) NOT NULL DEFAULT '0',
  `bank_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_account` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `confirmation_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotel_panel` tinyint(1) NOT NULL DEFAULT '0',
  `restaurant_panel` tinyint(1) NOT NULL DEFAULT '0',
  `service_panel` tinyint(1) NOT NULL DEFAULT '0',
  `commission` int(11) NOT NULL,
  `option_iva` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operator_password_resets`
--

CREATE TABLE `operator_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `fk_user` int(10) UNSIGNED NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_pago` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_payu` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` int(11) NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_products`
--

CREATE TABLE `order_products` (
  `id_order_product` int(10) UNSIGNED NOT NULL,
  `fk_order` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `units` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_product_items`
--

CREATE TABLE `order_product_items` (
  `id_product_item` int(10) UNSIGNED NOT NULL,
  `fk_order_product` int(10) UNSIGNED NOT NULL,
  `fk_service_item` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_parts`
--

CREATE TABLE `page_parts` (
  `id_part` int(10) UNSIGNED NOT NULL,
  `part_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_part_pages`
--

CREATE TABLE `page_part_pages` (
  `id_part_page` int(10) UNSIGNED NOT NULL,
  `fk_page` int(10) UNSIGNED NOT NULL,
  `fk_part` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patterns`
--

CREATE TABLE `patterns` (
  `id_pattern` int(10) UNSIGNED NOT NULL,
  `fk_customer` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `clicks` int(11) DEFAULT NULL,
  `redirection` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publication_day` date NOT NULL,
  `publication_finish` date NOT NULL,
  `pattern` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pattern_parts`
--

CREATE TABLE `pattern_parts` (
  `id_pattern_part` int(10) UNSIGNED NOT NULL,
  `fk_pattern` int(10) UNSIGNED NOT NULL,
  `fk_pagepart` int(10) UNSIGNED NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pay_reports`
--

CREATE TABLE `pay_reports` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` date NOT NULL,
  `date_finish` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plates`
--

CREATE TABLE `plates` (
  `id_plate` int(10) UNSIGNED NOT NULL,
  `plate` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `category` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `polls`
--

CREATE TABLE `polls` (
  `id_poll` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `activities` text COLLATE utf8_unicode_ci NOT NULL,
  `observations` text COLLATE utf8_unicode_ci,
  `state` varchar(131) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `id_region` int(10) UNSIGNED NOT NULL,
  `region_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `regions`
--

INSERT INTO `regions` (`id_region`, `region_name`, `sentence`, `link_image`, `description`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Caribe', 'Frase de prueba', 'images/regions/Region_20180508151847895918555.jpg', '<h3>Regi&oacute;n Caribe,</h3>\r\n\r\n<p>&nbsp;Est&aacute; ubicada en la parte norte de Colombia y Am&eacute;rica del Sur. Limita al norte con el mar caribe, al que debe su nombre, al este con Venezuela,&nbsp;al sur con la regi&oacute;n Andina&nbsp;y al oeste con la&nbsp;<a href=\"https://es.wikipedia.org/wiki/Regi%C3%B3n_del_Pac%C3%ADfico_(Colombia)\" title=\"Región del Pacífico (Colombia)\">r</a>egi&oacute;n del Pac&iacute;fico. Sus principales centros urbanos son:</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>Barranquilla</li>\r\n	<li>Cartagena de Indias</li>\r\n	<li>Soldead.</li>\r\n	<li>Santa Marta</li>\r\n	<li>Valledupar</li>\r\n	<li>Monter&iacute;a</li>\r\n	<li>Sincelejo</li>\r\n	<li>Valledupar</li>\r\n</ul>', 'caribe', 'inactivo', '2018-04-09 18:37:52', '2018-08-15 16:53:02'),
(2, 'Amazonia', '', '', 'required', 'amazonia', 'activo', '2018-05-07 18:07:09', '2018-05-07 18:07:09'),
(3, 'Antioquia y eje cafetero', '', '', 'required', 'antioquia-y-eje-cafetero', 'activo', '2018-05-07 18:07:36', '2018-05-07 18:07:36'),
(4, 'Centro', '', '', 'required', 'centro', 'activo', '2018-05-07 18:07:57', '2018-05-07 18:07:57'),
(5, 'Costa Caribe', '', '', 'required', 'costa-caribe', 'activo', '2018-05-07 18:08:15', '2018-05-07 18:08:15'),
(6, 'Costa pacífica', '', '', 'required', 'costa-pacifica', 'activo', '2018-05-07 18:09:18', '2018-05-07 18:09:18'),
(7, 'Llanos orientales', '', '', 'required', 'llanos-orientales', 'activo', '2018-05-07 18:09:36', '2018-05-07 18:09:36'),
(8, 'Suroccidente', '', '', 'required', 'suroccidente', 'activo', '2018-05-07 18:09:54', '2018-05-07 18:09:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region_images`
--

CREATE TABLE `region_images` (
  `id_region_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `region_images`
--

INSERT INTO `region_images` (`id_region_image`, `link_image`, `fk_region`, `created_at`, `updated_at`) VALUES
(1, 'images/regions/Region_20180613110428738985031.jpg', 1, '2018-06-13 16:04:28', '2018-06-13 16:04:28'),
(2, 'images/regions/Region_201806131104291271849480.jpg', 1, '2018-06-13 16:04:29', '2018-06-13 16:04:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporters`
--

CREATE TABLE `reporters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporter_password_resets`
--

CREATE TABLE `reporter_password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `report_reservations`
--

CREATE TABLE `report_reservations` (
  `id_report_reservation` int(10) UNSIGNED NOT NULL,
  `fk_report` int(10) UNSIGNED NOT NULL,
  `fk_reservation` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reprograms`
--

CREATE TABLE `reprograms` (
  `id_reprogram` int(10) UNSIGNED NOT NULL,
  `fk_reservation` int(10) UNSIGNED NOT NULL,
  `previous_date` date NOT NULL,
  `previous_time` time NOT NULL,
  `new_date` date NOT NULL,
  `new_time` time NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id_reservation` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_user` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `fk_order_product` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `calification` double(8,2) DEFAULT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id_room` int(10) UNSIGNED NOT NULL,
  `room` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `capacity` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `bed` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `suite` tinyint(1) NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules`
--

CREATE TABLE `schedules` (
  `id_schedule` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_receso` time DEFAULT NULL,
  `tiempo_receso` int(11) DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id_service` int(10) UNSIGNED NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_ico` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_class` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_url` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fk_service_category` int(10) UNSIGNED NOT NULL,
  `fk_ecosystem_category` int(10) UNSIGNED NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id_service`, `service`, `type_service`, `description`, `image`, `map_ico`, `icon_class`, `video_url`, `fk_service_category`, `fk_ecosystem_category`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Torrentismo', 'Alto impacto', '<p>asda sdas das dasd asd as</p>', 'images/torentismo-1.jpg', '', 'icon_set_2_icon-117', 'H34LnX0Azjc', 1, 1, 'torrentismo', '2017-10-23 15:55:27', '2018-06-22 19:59:07'),
(2, 'Rafting', 'Alto impacto', '', 'images/rafting.jpg', '', 'icon_set_2_icon-117', NULL, 1, 1, 'rafting', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
(3, 'Rafting', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'rafting-2', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
(4, 'Buceo ', 'Bajo impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'buceo ', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
(5, 'Pesca Deportiva ', 'Bajo impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'pesca-deportiva ', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
(6, 'Canyoning ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'canyoning ', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
(7, 'Balsaje', 'Alto impacto', '<p>asd asdasd asd asdas dasd asd asda sasd asd asd asd asd as asd asd a dsa</p>', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'balsaje ', '2017-10-23 15:55:29', '2018-05-24 16:34:15'),
(8, 'Bote ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'bote', '2017-10-23 15:55:29', '2017-10-23 15:55:29'),
(9, 'Kayaking ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'kayaking', '2017-10-23 15:55:29', '2017-10-23 15:55:29'),
(10, 'Jet Sky ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'jet-sky', '2017-10-23 15:55:29', '2017-10-23 15:55:29'),
(11, 'Surf ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'surf', '2017-10-23 15:55:29', '2017-10-23 15:55:29'),
(12, 'Barranquismo ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'barranquismo', '2017-10-23 15:55:29', '2017-10-23 15:55:29'),
(13, 'Parapente ', 'Alto impacto', '', 'images/parapente.jpg', '', 'icon_set_2_icon-117', NULL, 1, 3, 'parapente', '2017-10-23 15:55:29', '2017-10-23 15:55:29'),
(14, 'Ala Delta nomames', 'Alto impacto', '<p>lreomasd asdas dasd asd&nbsp;</p>', NULL, '', 'icon_set_2_icon-102', 'uTwcgLcbXSc', 1, 3, 'ala-delta', '2017-10-23 15:55:30', '2018-05-24 16:55:11'),
(15, 'Paracaidismo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'paracaidismo', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(16, 'Globo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'globo', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(17, 'Ultraliviano', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'ultraliviano', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(18, 'Canopy', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'canopy', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(19, 'Tirolesa', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'tirolesa', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(20, 'Tirolina ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'tirolina', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(21, 'Tirolina ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'tirolina-2', '2017-10-23 15:55:30', '2017-10-23 15:55:30'),
(22, 'Rappel ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'rappel', '2017-10-23 15:55:31', '2017-10-23 15:55:31'),
(23, 'Trekking ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'trekking', '2017-10-23 15:55:31', '2017-10-23 15:55:31'),
(24, 'Alta montaña', 'Alto impacto', '<p>mOnta&ntilde;a&nbsp;</p>', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'alta-monatana', '2017-10-23 15:55:31', '2018-05-24 16:35:25'),
(25, 'Espeleología ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'espeleologia', '2017-10-23 15:55:32', '2017-10-23 15:55:32'),
(26, 'Bungee Jumping  ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'bungee-jumping', '2017-10-23 15:55:32', '2017-10-23 15:55:32'),
(27, 'Escalada  ', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'escalada', '2017-10-23 15:55:32', '2017-10-23 15:55:32'),
(28, 'Espeleísmo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'espeleismo', '2017-10-23 15:55:33', '2017-10-23 15:55:33'),
(29, 'Excursiones 4x4', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'excursiones-4x4', '2017-10-23 15:55:33', '2017-10-23 15:55:33'),
(30, 'Cabalgata', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'cabalgata', '2017-10-23 15:55:34', '2017-10-23 15:55:34'),
(31, 'Comida rapida', 'Bajo impacto', '<p>asd asdasd asd asd&nbsp;</p>', 'images/Service/Service_201805241151281853367346.png', 'images/Service/Service_20180524114443842058172.png', 'icon_set_2_icon-106', 'uTwcgLcbXSc', 1, 2, 'comida-rapida', '2018-05-24 16:44:43', '2018-05-24 16:51:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_categories`
--

CREATE TABLE `service_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `service_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `service_categories`
--

INSERT INTO `service_categories` (`id_category`, `service_category`, `created_at`, `updated_at`) VALUES
(1, 'Aventura', '2017-10-23 15:55:27', '2018-05-22 16:40:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishments`
--

CREATE TABLE `service_establishments` (
  `id_service_establishment` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_establishments`
--

INSERT INTO `service_establishments` (`id_service_establishment`, `service_name`, `created_at`, `updated_at`) VALUES
(2, 'Wifi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Plasma Tv', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Carta de vinos y licores', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Eventos especiales', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Cenas V.I.P.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Salón de espera', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Música en vivo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Música ambiental', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Acceso para sillas de ruedas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Opciones sin gluten', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Parqueadero', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Valet parking', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Traductor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Caja de seguridad', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Catering para eventos', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Pet Friendly', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Entrega a domicilio', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Acepta tarjetas de crédito', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Opciones veganas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Apto para vegetarianos', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'No se permiten mascotas   ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Zona de fumadores', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishment_pivots`
--

CREATE TABLE `service_establishment_pivots` (
  `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_items`
--

CREATE TABLE `service_items` (
  `id_service_item` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_operators`
--

CREATE TABLE `service_operators` (
  `id_service_operator` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_operator` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `video` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requisites` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `policies` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_municipality` int(11) NOT NULL,
  `fk_interest_site` int(11) DEFAULT NULL,
  `slug` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Inhabilitado',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_pictures`
--

CREATE TABLE `service_pictures` (
  `id_picture` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping_orders`
--

CREATE TABLE `shipping_orders` (
  `orders_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type_dni` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dni` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone_2` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `avatar` varchar(131) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotionals_mails` tinyint(1) NOT NULL DEFAULT '0',
  `password_change` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `confirmation_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `address`, `phone`, `role`, `avatar`, `country`, `region`, `city`, `promotionals_mails`, `password_change`, `password`, `status`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'David', 'Hernandez', 'sergio.hernandez@gmail.com', 'cra 147 # 142 50, cra 142#134-33', '3203233082', 'user', '/images/profile/Profile_20180116134012323574128.wall.jpg', NULL, NULL, NULL, 0, 1, '$2y$10$9t1PMSkLuUGWRNQsWLUnUuxqNSkldBb2lfAaNmM2zINUxzosN41fG', 'pending', '', 's6hoPx3LLURFInbpIZ0cqw9sbzXk1PJNqHtUeXWgH2lIFNDKuEI46aWjmMQI', '2017-11-25 14:55:25', '2018-06-29 20:13:40'),
(2, 'Desarrollo Web Guía de Rutas por Colombia', '', 'desarrollo@rutascolombia.com', '', '', 'user', 'https://lh3.googleusercontent.com/-YQWVhiZJJoU/AAAAAAAAAAI/AAAAAAAAAAA/AAnnY7qpPZdOqC-LVqp99S6T60eRE-x7EQ/mo/photo.jpg?sz=50', NULL, NULL, NULL, 0, 0, '', 'active', '', 'vkhDRYyCJNn5XzwShXJQ6VFq2dhnQ4JMgeRIP5rgkTfOPxcBXc6Ql8VFmP3V', '2018-07-10 15:24:22', '2018-07-10 15:24:22'),
(3, 'Sergio David', 'Hernandez Goyeneche', 'al4cdeveloper@gmail.com', 'cra 147 # 142 50, cra 142#134-33', '3203233082', 'user', 'https://lh6.googleusercontent.com/-zB0LCBQB5TE/AAAAAAAAAAI/AAAAAAAAACA/JB3MSE0rttQ/photo.jpg?sz=50', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', 1, 0, '$2y$10$oA7l8Gf6SfJWWLgtv13yNu3ob2tFKcdQ0DuSFvuaHOeLHY6D.cPfK', 'active', NULL, 'tSePOnYVyV4av71xdKW0Vmawro0dK3CKYAT2AvBpBv54wmxp7QDW5VDGJi51', '2018-07-11 16:08:39', '2018-08-17 17:55:25'),
(4, 'Sergio', 'Hernandez', 'sergio.hernandez.222goyeneche@gmail.com', 'cra 147 # 142 50, cra 142#134-33', '3203233082', 'user', NULL, NULL, NULL, NULL, 0, 1, '$2y$10$WT.DUO/3gnk6kUqOdkER.OlMwezyh0N/za7sqfTRWJEj4IKkjWeee', 'active', '', NULL, '2018-07-12 16:59:11', '2018-07-12 16:59:11'),
(5, 'Sergio', 'Hernandez', 'sergio.hernandez.goyenech2323423e@gmail.com', NULL, '3203233082', 'user', NULL, 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', 1, 1, '$2y$10$b6aQpcgefmlZcs.UouOMX.I.8WDi5P2/KC1zHfWqIZQooqPeype7G', 'active', '', NULL, '2018-07-19 17:10:20', '2018-07-23 16:44:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id_video` int(10) UNSIGNED NOT NULL,
  `link_video` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallpapers`
--

CREATE TABLE `wallpapers` (
  `id_wallpaper` int(10) UNSIGNED NOT NULL,
  `link_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `primary_text` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secundary_text` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `second_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_text_button` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_text_button` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `wallpapers`
--

INSERT INTO `wallpapers` (`id_wallpaper`, `link_image`, `primary_text`, `secundary_text`, `first_url`, `second_url`, `first_text_button`, `second_text_button`, `state`, `created_at`, `updated_at`) VALUES
(1, 'wallpapers/wall_201804261216191406070230.jpg', 'Darkar', 'vete a la versh!', '/', '/olol', 'Vete', 'A la versh!', 'activo', '2018-04-10 18:53:05', '2018-04-27 21:40:33'),
(2, 'wallpapers/wall_20180426114120737408976.png', 'Prueba', NULL, '/', NULL, 'boton 1', NULL, 'activo', '2018-04-26 16:41:20', '2018-04-26 17:15:04');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id_department`),
  ADD KEY `departments_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `ecosystem_categories`
--
ALTER TABLE `ecosystem_categories`
  ADD PRIMARY KEY (`id_ecosystem_category`);

--
-- Indices de la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD PRIMARY KEY (`id_establishment`),
  ADD KEY `establishments_fk_operator_foreign` (`fk_operator`),
  ADD KEY `establishments_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD PRIMARY KEY (`id_establishment_image`),
  ADD KEY `establishment_images_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD PRIMARY KEY (`id_establishment_schedule`),
  ADD KEY `establishment_schedules_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `establishmen_images`
--
ALTER TABLE `establishmen_images`
  ADD PRIMARY KEY (`id_establishment_image`),
  ADD KEY `establishmen_images_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD PRIMARY KEY (`id_site`),
  ADD KEY `interest_sites_fk_category_foreign` (`fk_category`),
  ADD KEY `interest_sites_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD PRIMARY KEY (`id_site_image`),
  ADD KEY `interest_site_images_fk_site_foreign` (`fk_site`);

--
-- Indices de la tabla `keydatas`
--
ALTER TABLE `keydatas`
  ADD PRIMARY KEY (`id_keydata`),
  ADD KEY `keydatas_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_municipality`),
  ADD KEY `municipalities_fk_department_foreign` (`fk_department`);

--
-- Indices de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD PRIMARY KEY (`id_municipality_image`),
  ADD KEY `municipality_images_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id_operator`),
  ADD UNIQUE KEY `operators_email_unique` (`email`);

--
-- Indices de la tabla `operator_password_resets`
--
ALTER TABLE `operator_password_resets`
  ADD KEY `operator_password_resets_email_index` (`email`),
  ADD KEY `operator_password_resets_token_index` (`token`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `orders_fk_user_foreign` (`fk_user`);

--
-- Indices de la tabla `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id_order_product`),
  ADD KEY `order_products_fk_order_foreign` (`fk_order`),
  ADD KEY `order_products_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `order_product_items`
--
ALTER TABLE `order_product_items`
  ADD PRIMARY KEY (`id_product_item`),
  ADD KEY `order_product_items_fk_order_product_foreign` (`fk_order_product`),
  ADD KEY `order_product_items_fk_service_item_foreign` (`fk_service_item`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id_page`);

--
-- Indices de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  ADD PRIMARY KEY (`id_part`);

--
-- Indices de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD PRIMARY KEY (`id_part_page`),
  ADD KEY `page_part_pages_fk_page_foreign` (`fk_page`),
  ADD KEY `page_part_pages_fk_part_foreign` (`fk_part`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD PRIMARY KEY (`id_pattern`),
  ADD KEY `patterns_fk_customer_foreign` (`fk_customer`);

--
-- Indices de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD PRIMARY KEY (`id_pattern_part`),
  ADD KEY `pattern_parts_fk_pattern_foreign` (`fk_pattern`),
  ADD KEY `pattern_parts_fk_pagepart_foreign` (`fk_pagepart`);

--
-- Indices de la tabla `pay_reports`
--
ALTER TABLE `pay_reports`
  ADD PRIMARY KEY (`id_report`);

--
-- Indices de la tabla `plates`
--
ALTER TABLE `plates`
  ADD PRIMARY KEY (`id_plate`),
  ADD KEY `plates_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id_poll`);

--
-- Indices de la tabla `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD PRIMARY KEY (`id_region_image`),
  ADD KEY `region_images_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `reporters`
--
ALTER TABLE `reporters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reporters_email_unique` (`email`);

--
-- Indices de la tabla `reporter_password_resets`
--
ALTER TABLE `reporter_password_resets`
  ADD KEY `reporter_password_resets_email_index` (`email`),
  ADD KEY `reporter_password_resets_token_index` (`token`);

--
-- Indices de la tabla `report_reservations`
--
ALTER TABLE `report_reservations`
  ADD PRIMARY KEY (`id_report_reservation`),
  ADD KEY `report_reservations_fk_report_foreign` (`fk_report`),
  ADD KEY `report_reservations_fk_reservation_foreign` (`fk_reservation`);

--
-- Indices de la tabla `reprograms`
--
ALTER TABLE `reprograms`
  ADD PRIMARY KEY (`id_reprogram`),
  ADD KEY `reprograms_fk_reservation_foreign` (`fk_reservation`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id_reservation`),
  ADD KEY `reservations_fk_service_foreign` (`fk_service`),
  ADD KEY `reservations_fk_user_foreign` (`fk_user`),
  ADD KEY `reservations_fk_order_product_foreign` (`fk_order_product`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id_room`),
  ADD KEY `rooms_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id_schedule`),
  ADD KEY `schedules_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `services_fk_service_category_foreign` (`fk_service_category`),
  ADD KEY `services_fk_ecosystem_category_foreign` (`fk_ecosystem_category`);

--
-- Indices de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  ADD PRIMARY KEY (`id_service_establishment`);

--
-- Indices de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  ADD PRIMARY KEY (`id_service_establishment_pivot`),
  ADD KEY `service_establishment_pivots_fk_service_foreign` (`fk_service`),
  ADD KEY `service_establishment_pivots_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD PRIMARY KEY (`id_service_item`),
  ADD KEY `service_items_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD PRIMARY KEY (`id_service_operator`),
  ADD KEY `service_operators_fk_service_foreign` (`fk_service`),
  ADD KEY `service_operators_fk_operator_foreign` (`fk_operator`);

--
-- Indices de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD PRIMARY KEY (`id_picture`),
  ADD KEY `service_pictures_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `shipping_orders`
--
ALTER TABLE `shipping_orders`
  ADD PRIMARY KEY (`orders_id`),
  ADD KEY `shipping_orders_orders_id_index` (`orders_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id_video`);

--
-- Indices de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  ADD PRIMARY KEY (`id_wallpaper`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id_department` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ecosystem_categories`
--
ALTER TABLE `ecosystem_categories`
  MODIFY `id_ecosystem_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `establishments`
--
ALTER TABLE `establishments`
  MODIFY `id_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  MODIFY `id_establishment_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  MODIFY `id_establishment_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `establishmen_images`
--
ALTER TABLE `establishmen_images`
  MODIFY `id_establishment_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  MODIFY `id_site` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  MODIFY `id_site_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `keydatas`
--
ALTER TABLE `keydatas`
  MODIFY `id_keydata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_municipality` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  MODIFY `id_municipality_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `operators`
--
ALTER TABLE `operators`
  MODIFY `id_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id_order_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `order_product_items`
--
ALTER TABLE `order_product_items`
  MODIFY `id_product_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  MODIFY `id_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  MODIFY `id_part_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `patterns`
--
ALTER TABLE `patterns`
  MODIFY `id_pattern` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  MODIFY `id_pattern_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pay_reports`
--
ALTER TABLE `pay_reports`
  MODIFY `id_report` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plates`
--
ALTER TABLE `plates`
  MODIFY `id_plate` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `polls`
--
ALTER TABLE `polls`
  MODIFY `id_poll` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `regions`
--
ALTER TABLE `regions`
  MODIFY `id_region` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `region_images`
--
ALTER TABLE `region_images`
  MODIFY `id_region_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reporters`
--
ALTER TABLE `reporters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `report_reservations`
--
ALTER TABLE `report_reservations`
  MODIFY `id_report_reservation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reprograms`
--
ALTER TABLE `reprograms`
  MODIFY `id_reprogram` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id_reservation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id_room` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id_service` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  MODIFY `id_service_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  MODIFY `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service_items`
--
ALTER TABLE `service_items`
  MODIFY `id_service_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  MODIFY `id_service_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  MODIFY `id_picture` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id_video` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  MODIFY `id_wallpaper` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD CONSTRAINT `establishments_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`),
  ADD CONSTRAINT `establishments_fk_operator_foreign` FOREIGN KEY (`fk_operator`) REFERENCES `operators` (`id_operator`);

--
-- Filtros para la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD CONSTRAINT `establishment_images_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD CONSTRAINT `establishment_schedules_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `establishmen_images`
--
ALTER TABLE `establishmen_images`
  ADD CONSTRAINT `establishmen_images_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD CONSTRAINT `interest_sites_fk_category_foreign` FOREIGN KEY (`fk_category`) REFERENCES `interest_site_categories` (`id_category`),
  ADD CONSTRAINT `interest_sites_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD CONSTRAINT `interest_site_images_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `keydatas`
--
ALTER TABLE `keydatas`
  ADD CONSTRAINT `keydatas_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD CONSTRAINT `municipalities_fk_department_foreign` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`id_department`);

--
-- Filtros para la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD CONSTRAINT `municipality_images_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_fk_order_foreign` FOREIGN KEY (`fk_order`) REFERENCES `orders` (`id_order`),
  ADD CONSTRAINT `order_products_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `order_product_items`
--
ALTER TABLE `order_product_items`
  ADD CONSTRAINT `order_product_items_fk_order_product_foreign` FOREIGN KEY (`fk_order_product`) REFERENCES `order_products` (`id_order_product`),
  ADD CONSTRAINT `order_product_items_fk_service_item_foreign` FOREIGN KEY (`fk_service_item`) REFERENCES `service_items` (`id_service_item`);

--
-- Filtros para la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD CONSTRAINT `page_part_pages_fk_page_foreign` FOREIGN KEY (`fk_page`) REFERENCES `pages` (`id_page`),
  ADD CONSTRAINT `page_part_pages_fk_part_foreign` FOREIGN KEY (`fk_part`) REFERENCES `page_parts` (`id_part`);

--
-- Filtros para la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD CONSTRAINT `patterns_fk_customer_foreign` FOREIGN KEY (`fk_customer`) REFERENCES `customers` (`id_customer`);

--
-- Filtros para la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD CONSTRAINT `pattern_parts_fk_pagepart_foreign` FOREIGN KEY (`fk_pagepart`) REFERENCES `page_part_pages` (`id_part_page`),
  ADD CONSTRAINT `pattern_parts_fk_pattern_foreign` FOREIGN KEY (`fk_pattern`) REFERENCES `patterns` (`id_pattern`);

--
-- Filtros para la tabla `plates`
--
ALTER TABLE `plates`
  ADD CONSTRAINT `plates_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD CONSTRAINT `region_images_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `report_reservations`
--
ALTER TABLE `report_reservations`
  ADD CONSTRAINT `report_reservations_fk_report_foreign` FOREIGN KEY (`fk_report`) REFERENCES `pay_reports` (`id_report`),
  ADD CONSTRAINT `report_reservations_fk_reservation_foreign` FOREIGN KEY (`fk_reservation`) REFERENCES `reservations` (`id_reservation`);

--
-- Filtros para la tabla `reprograms`
--
ALTER TABLE `reprograms`
  ADD CONSTRAINT `reprograms_fk_reservation_foreign` FOREIGN KEY (`fk_reservation`) REFERENCES `reservations` (`id_reservation`);

--
-- Filtros para la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_fk_order_product_foreign` FOREIGN KEY (`fk_order_product`) REFERENCES `order_products` (`id_order_product`),
  ADD CONSTRAINT `reservations_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`),
  ADD CONSTRAINT `reservations_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fk_ecosystem_category_foreign` FOREIGN KEY (`fk_ecosystem_category`) REFERENCES `ecosystem_categories` (`id_ecosystem_category`),
  ADD CONSTRAINT `services_fk_service_category_foreign` FOREIGN KEY (`fk_service_category`) REFERENCES `service_categories` (`id_category`);

--
-- Filtros para la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD CONSTRAINT `service_items_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD CONSTRAINT `service_operators_ibfk_1` FOREIGN KEY (`fk_service`) REFERENCES `services` (`id_service`);

--
-- Filtros para la tabla `shipping_orders`
--
ALTER TABLE `shipping_orders`
  ADD CONSTRAINT `shipping_orders_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id_order`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
