-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-08-2018 a las 18:18:13
-- Versión del servidor: 5.6.38
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tupaseo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Developer', 'dev@tupaseo.travel', '$2y$10$VB1eAj69/MluJ715LX/rwOb9PFTW9xrezDBKyL/zsdE5inZCx4RvC', 'x6eLB8iTPM8M21KiWRftlvEsBzq20LTv2UpNmUsWRnbr8DlW0ESEAOupXWUS', '2018-01-11 22:03:08', '2018-01-11 22:03:08'),
(2, 'al4c', 'al4cdeveloper@gmail.com', '$2y$10$cnbszAIKT48KL0Jto1RjjOrPSwGHGg1Bq8ivsKIk3dAVhxtLMntZ.', 'JlieavEKjGd77m7ZDfn27bJiWh5wD29Ur5THQEFJ8VMbspwGlo7lklRjOqbq', '2018-04-20 17:59:11', '2018-04-20 17:59:11'),
(3, 'AL4C', 'chechito-232@hotmail.com', '$2y$10$96XIv7H2.0wVsl2YqQTIGOl2EmW1HKloafwZJhM4LClz0D1cQg5K2', '7CzUmiBgwMuPCUhhtGOpsMtWeB54cPO0bPOVipurjizF37SNnmUtaDC1D7Ku', '2018-07-03 15:18:25', '2018-07-03 15:18:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `state` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id_contact`, `name`, `phone`, `email`, `message`, `state`, `country`, `region`, `city`, `created_at`, `updated_at`) VALUES
(5, 'lizbeth herrera', '31444531986', 'lizbethpool@hotmail.com', 'Usuario', 'archivado', '', '', '', '2017-11-16 15:32:16', '2017-11-16 15:32:16'),
(6, 'Julieth Villalobos', '3203436080', 'jvillalobo6@uniminuto.edu.co', 'Cuponati.', 'archivado', '', '', '', '2017-11-16 15:39:48', '2017-11-16 15:39:48'),
(7, 'Patricia Rozo', '3125697040', 'exitoverde@yahoo.com', 'Operadora  - usuaria', 'archivado', '', '', '', '2017-11-16 15:53:27', '2017-11-16 15:53:27'),
(8, 'sonia chaves', '3175166442', 'soniachaves86@gmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 16:24:07', '2017-11-16 16:24:07'),
(9, 'Serigo Andres Rodriguez', '3203882679', 'sgonzalez17@uniminuto.edu.co', 'Usuario', 'archivado', '', '', '', '2017-11-16 17:04:06', '2017-11-16 17:04:06'),
(10, 'Karen Lara', '3193937789', 'klaracasas@uniminuto.edu.co', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:04:59', '2017-11-16 17:04:59'),
(11, 'Laura Valentina Muñoz', '3115449459', 'lvmg_21@hotmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:06:40', '2017-11-16 17:06:40'),
(12, 'luis felipe duarte saldaña', '3233653348', 'luis111sabastian@hotmail.com', 'Usuario', 'archivado', '', '', '', '2017-11-16 17:12:59', '2017-11-16 17:12:59'),
(13, 'Geraldine Monroy', '3175435632', 'geraldinemonroy7@gmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:19:13', '2017-11-16 17:19:13'),
(14, 'laura mesa', '3164842937', 'lmesa90@hotmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:37:44', '2017-11-16 17:37:44'),
(15, 'julian alwjansro garcia gomez', '3154483701', 'algarcia0530@hotmail.com', 'Usuario', 'archivado', '', '', '', '2017-11-16 17:38:39', '2017-11-16 17:38:39'),
(16, 'lina lozano', '3213941533', 'lina.madero@gmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:39:41', '2017-11-16 17:39:41'),
(17, 'Lorena Zorro', '3114552635', 'lorenita.4422@gmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:47:22', '2017-11-16 17:47:22'),
(18, 'Gabriela Suarez', '3164490418', 'gabisuarez0408@gmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:48:27', '2017-11-16 17:48:27'),
(19, 'Miguel Herrrera', '3043638685', 'mherrerama95@gmail.com', 'Usuario', 'archivado', '', '', '', '2017-11-16 17:57:35', '2017-11-16 17:57:35'),
(20, 'Angelica Loaiza', '3052240968', 'dloaizamarr@uniminuto.edu.co', 'Usuaria', 'archivado', '', '', '', '2017-11-16 17:58:46', '2017-11-16 17:58:46'),
(21, 'Jaime Alexander Arenas Coy', '3138491143', 'alex_arenas10@hotmail.com', 'Usuario', 'archivado', '', '', '', '2017-11-16 18:16:59', '2017-11-16 18:16:59'),
(22, 'karen andrea tello buitrago', '3115172000', 'karentello93@gmail.com', 'Usuaria', 'archivado', '', '', '', '2017-11-16 18:18:00', '2017-11-16 18:18:00'),
(23, 'juan jose gil castro', '3144352480', 'juanjose19114@gmail.com', 'Usuario', 'archivado', '', '', '', '2017-11-16 18:19:44', '2017-11-16 18:19:44'),
(24, 'Luz Gonzalez', '3144544288', 'ldgonzalezc@hotmail.com', 'Usuaria - La hermana es operadora de paintball', 'archivado', '', '', '', '2017-11-16 19:19:29', '2017-11-16 19:19:29'),
(25, 'Oscar Lopez', '3012116564', 'oscar.lopezs@uniminuto.edu.co', 'usuario', 'archivado', '', '', '', '2017-11-16 20:40:19', '2017-11-16 20:40:19'),
(26, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'xcvxcvxcvsdfv sdfsd fdsf sdf sdf', 'archivado', '', '', '', '2018-01-12 20:37:09', '2018-01-12 20:37:09'),
(27, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', NULL, 'archivado', '', '', '', '2018-01-15 20:34:06', '2018-01-15 20:34:06'),
(28, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'asdasda sdasd asd asd asdåsd asda sd ass asd asd asd asd', 'archivado', '', '', '', '2018-01-15 20:35:01', '2018-01-15 20:35:01'),
(29, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'asdasd as dasd asd a', 'new', '', '', '', '2018-01-15 20:53:21', '2018-01-15 20:53:21'),
(30, 'Bernice', '(219) 477-0619', 'hello@bernicebrown.me', 'Hello,\r\n\r\nHope you are doing great..\r\n\r\nI recently came across your website tupaseo.com.co and found it very interesting.\r\n\r\nI want to publish a guest post on your website.The article will be related to your website and will be appreciated by your readers.\r\n\r\nI guarantee you that the article will be 100% unique, top quality and copy-scape protected and will not be shared with any other site.\r\n\r\nThe article will be free of charge. All I need is one or two Dofollow link back to my site in exchange for the article.\r\n\r\nDo let me know so I can send you an article..\r\n\r\nWill be waiting for reply...\r\n\r\n\r\nThanks', 'archivado', '', '', '', '2018-02-04 10:20:11', '2018-08-16 17:10:00'),
(31, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'rtyui', 'archivado', 'co', 'Departamento de Arauca', 'Arauca', '2018-08-15 21:13:00', '2018-08-16 17:10:09'),
(32, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'rtyui', 'archivado', 'co', 'Departamento de Arauca', 'Arauca', '2018-08-15 21:13:11', '2018-08-16 17:10:08'),
(33, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'asdasd asd as', 'archivado', 'co', 'Departamento de Arauca', 'Arauquita', '2018-08-15 21:15:11', '2018-08-16 17:10:07'),
(34, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'asdasd asd as', 'archivado', 'co', 'Amazonas', 'La Pedrera', '2018-08-15 21:20:29', '2018-08-16 17:10:06'),
(35, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'asdasd asd as', 'archivado', 'co', 'Amazonas', 'La Pedrera', '2018-08-15 21:20:58', '2018-08-16 17:08:39'),
(36, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'new', 'co', 'Departamento de Caldas', 'La Merced', '2018-08-16 17:35:51', '2018-08-16 17:35:51'),
(37, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'new', 'co', 'Departamento de Caldas', 'La Merced', '2018-08-16 17:36:33', '2018-08-16 17:36:33'),
(38, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'new', 'co', 'Departamento de Caldas', 'La Merced', '2018-08-16 17:37:07', '2018-08-16 17:37:07'),
(39, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'new', 'co', 'Departamento de Caldas', 'La Merced', '2018-08-16 17:38:33', '2018-08-16 17:38:33'),
(40, 'Sergio Hernandez', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'archivado', 'co', 'Departamento de Caldas', 'La Merced', '2018-08-16 17:43:16', '2018-08-16 17:45:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `nit` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone_contact` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_contact` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `customers`
--

INSERT INTO `customers` (`id_customer`, `nit`, `name`, `contact`, `phone_contact`, `email_contact`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, '11111111', 'Sergio Hernandez', 'sdfdsf sd', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'activo', 'sergio-hernandez', '2018-04-10 22:06:54', '2018-04-10 22:07:10'),
(2, '1242342', 'Movistar', 'asdas dasd asd as', '123123123', 'email@movistar.com', 'activo', 'movistar', '2018-04-27 21:50:23', '2018-04-27 21:50:23'),
(3, '657823834', 'Red Bull', 'No hay :c', '58768', 'contacto@redbull.com', 'activo', 'red-bull', '2018-04-30 21:59:12', '2018-04-30 21:59:12'),
(4, '830.095.617-2', 'SATENA', 'MAYOR GENERAL DEL AIRE CARLOS EDUARDO MONTEALEGRE RODRÍGUEZ', '234234234234', 'mayor@satena.com', 'activo', 'satena', '2018-05-03 16:37:33', '2018-05-03 16:37:33'),
(5, '234234-234234-2', 'Envia', 'Santiago Rodríguez', '234234', 'sanrodri@envia.com', 'activo', 'envia', '2018-05-03 16:38:58', '2018-05-03 16:38:58'),
(6, '23423 234 234', 'Licores Nectar', 'Un borrachito :v', '234234234', 'borrachi@nectar.com', 'activo', 'licores-nectar', '2018-05-03 16:40:17', '2018-05-03 16:40:17'),
(7, '234 234 234', 'Cafam', 'Señor de cafam', '234524', 'info@cafam.com', 'activo', 'cafam', '2018-05-03 16:42:16', '2018-05-03 16:42:16'),
(8, '234234', 'Puntos Suspensivos', 'Jose Maria Gonzalez', '23423', 'jose@rutascolombia.com', 'activo', 'puntos-suspensivos', '2018-05-03 18:06:59', '2018-05-03 18:06:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id_department` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `short_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departments`
--

INSERT INTO `departments` (`id_department`, `department_name`, `fk_region`, `short_description`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Departamento', 1, 'La puerta de la biodiversidad colombiana', 'departamento', 'activo', '2018-04-09 18:38:06', '2018-06-13 16:47:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ecosystem_categories`
--

CREATE TABLE `ecosystem_categories` (
  `id_ecosystem_category` int(10) UNSIGNED NOT NULL,
  `ecosystem_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_image` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ecosystem_categories`
--

INSERT INTO `ecosystem_categories` (`id_ecosystem_category`, `ecosystem_category`, `subtitle`, `slug`, `link_image`, `created_at`, `updated_at`) VALUES
(1, 'Agua', 'Vive tu libertad', 'agua', 'images/ecosystem/Ecosystem_20180522132400796266540.jpg', '2017-10-23 15:55:27', '2018-05-22 18:24:00'),
(2, 'Tierra', 'Actividades de tierra bien chingonas', 'tierra', 'images/ecosystem/Ecosystem_20180823154416817657492.jpg', '2017-10-23 15:55:27', '2018-08-23 20:44:16'),
(3, 'Aire', NULL, 'aire', '', '2017-10-23 15:55:27', '2017-10-23 15:55:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishments`
--

CREATE TABLE `establishments` (
  `id_establishment` int(10) UNSIGNED NOT NULL,
  `type_establishment` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `establishment_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kitchen_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_operator` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `establishments`
--

INSERT INTO `establishments` (`id_establishment`, `type_establishment`, `establishment_name`, `phone`, `address`, `latitude`, `longitude`, `facebook`, `twitter`, `instagram`, `website`, `video`, `kitchen_type`, `slug`, `fk_operator`, `description`, `state`, `fk_municipality`, `created_at`, `updated_at`) VALUES
(1, 'restaurant', 'Establecimiento', 2147483647, 'cra 147 # 142 50, cra 142#134-33', '4.747999', '-74.11848680000003', NULL, NULL, NULL, NULL, NULL, 'Comida rápida', 'hary', 5, '<p>sdasd asdasd asd qsd asd asd asd asd asd&nbsp;</p>', 'activo', 1, '2018-06-07 21:04:18', '2018-08-21 16:48:29'),
(2, 'restaurant', 'lugar pro', 2147483647, 'cra 147 # 142 50, cra 142#134-33', '4.747999', '-74.11848680000003', NULL, NULL, NULL, NULL, 'V15BYnSr0P8', 'Comida de calle', 'lugar-pro', 5, '<p>adasd asdasd asd asd asd asdasd asd&nbsp;</p>', 'activo', 1, '2018-06-08 20:49:58', '2018-08-21 16:48:33'),
(4, 'hotel', 'Hotel bien mamon', 2147483647, 'cra 147 # 142 50', '4.747999', '-74.11848680000003', NULL, NULL, NULL, NULL, NULL, NULL, 'asdfasfsdfsdf-1', 5, '<p>Disfrute de la ubicaci&oacute;n c&eacute;ntrica y del elegante ambiente del&nbsp;<strong>hotel Hilton Bogota</strong>. Ubicado en el centro del distrito financiero, el hotel se encuentra cerca la Zona Rosa, uno de los vecindarios m&aacute;s prestigiosos de la ciudad y de la Zona G, que ofrece entretenimiento y restaurantes. Llegue al hotel desde el Aeropuerto Internacional El Dorado en solo 25 minutos.</p>\r\n\r\n<p>Descanse en una habitaci&oacute;n moderna con WiFi, escritorio y comodidades. Elija una suite para disfrutar de un ambiente sofisticado y espacio adicional en la sala de estar con un sof&aacute;. Hosp&eacute;dese en una habitaci&oacute;n o una suite Executive para tener acceso a los privilegios exclusivos de la sala de estar ejecutiva.</p>', 'activo', 1, '2018-06-12 19:07:04', '2018-08-21 20:14:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_images`
--

CREATE TABLE `establishment_images` (
  `id_establishment_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `establishment_images`
--

INSERT INTO `establishment_images` (`id_establishment_image`, `link_image`, `description`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(5, 'images/restaurant/restaurant_20180615160147525953225.jpg', 'Lo invitamos a disfrutar de la vista panorámica nocturna de Bogotá mientras se toma algo caliente en el Café de Santa Clara.', 2, '2018-06-15 21:01:47', '2018-06-15 21:02:42'),
(6, 'images/restaurant/restaurant_201806151602161854374723.jpg', NULL, 2, '2018-06-15 21:02:16', '2018-06-15 21:07:13'),
(7, 'images/restaurant/restaurant_20180615160319324568729.jpg', 'Ubicado en la cima del Cerro de Monserrate con la hermosa ciudad de Bogotá como paisaje principal, este restaurante ofrece a sus visitantes la oportunidad de saborear platos colombianos como el mero costeño, los tamales tolimenses, la bandeja paisa, cuajada con dulce de mamey, o sus famosas parrilladas de chuletitas o chuletones.', 2, '2018-06-15 21:03:19', '2018-06-15 21:03:21'),
(8, 'images/restaurant/restaurant_2018082111414662128603.jpg', 'asdas dasd', 1, '2018-08-21 16:41:46', '2018-08-21 16:42:21'),
(9, 'images/restaurant/restaurant_2018082111415711320226.jpg', 'asd asd 2', 1, '2018-08-21 16:41:57', '2018-08-21 16:42:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishment_schedules`
--

CREATE TABLE `establishment_schedules` (
  `id_establishment_schedule` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `start` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `establishment_schedules`
--

INSERT INTO `establishment_schedules` (`id_establishment_schedule`, `name`, `start`, `end`, `inactive`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(1, 'Lunes', '16:03:30', '16:03:30', 0, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(2, 'Martes', '16:03:30', '16:03:30', 0, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(3, 'Miercoles', '16:03:30', '16:03:30', 0, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(4, 'Jueves', '00:00:00', '00:00:00', 1, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(5, 'Viernes', '00:00:00', '00:00:00', 1, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(6, 'Sabado', '00:00:00', '00:00:00', 1, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(7, 'Domingo', '16:03:30', '16:03:30', 0, 1, '2018-06-07 21:04:18', '2018-08-21 16:21:48'),
(8, 'Lunes', '15:49:00', '15:49:00', 0, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(9, 'Martes', '00:00:00', '00:00:00', 1, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(10, 'Miercoles', '00:00:00', '00:00:00', 1, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(11, 'Jueves', '00:00:00', '00:00:00', 1, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(12, 'Viernes', '15:49:00', '15:49:00', 0, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(13, 'Sabado', '15:49:00', '15:49:00', 0, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(14, 'Domingo', '15:49:00', '15:49:00', 0, 2, '2018-06-08 20:49:58', '2018-06-15 21:51:13'),
(15, 'checkin', '13:26:30', '18:26:30', 0, 4, '2018-06-12 19:07:04', '2018-06-12 21:30:34'),
(16, 'checkout', '13:26:30', '18:26:30', 0, 4, '2018-06-12 19:07:04', '2018-06-12 21:30:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establishmen_images`
--

CREATE TABLE `establishmen_images` (
  `id_establishment_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_sites`
--

CREATE TABLE `interest_sites` (
  `id_site` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_category` int(10) UNSIGNED NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_icon` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `front_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `interest_sites`
--

INSERT INTO `interest_sites` (`id_site`, `site_name`, `fk_category`, `fk_municipality`, `address`, `multimedia_type`, `latitude`, `longitude`, `slug`, `link_image`, `link_icon`, `description`, `keywords`, `state`, `front_state`, `created_at`, `updated_at`) VALUES
(1, 'Sergio Hernandez', 1, 1, 'cra 147 # 142 50, cra 142#134-33', 'images', '324234', '234234', 'sergio-hernandez', 'images/interestsites/site20180410132040471813565.png', NULL, '<p>asdasd asd asda sdasd asd&nbsp;</p>', '2342v3,234,23,423,423s', 'activo', 'activo', '2018-04-10 18:20:40', '2018-04-23 19:55:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_categories`
--

CREATE TABLE `interest_site_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `default_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `default_icon` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `interest_site_categories`
--

INSERT INTO `interest_site_categories` (`id_category`, `category_name`, `default_image`, `default_icon`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Darkar', 'images/interestsitescategory/category201804101313332112568462.png', 'images/interestsitescategory/category201804101313331516230589.png', 'activo', 'darkar', '2018-04-10 18:13:33', '2018-04-10 18:13:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_images`
--

CREATE TABLE `interest_site_images` (
  `id_site_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `keydatas`
--

CREATE TABLE `keydatas` (
  `id_keydata` int(10) UNSIGNED NOT NULL,
  `keydata_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `keydata_value` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `keydatas`
--

INSERT INTO `keydatas` (`id_keydata`, `keydata_name`, `keydata_value`, `category`, `fk_municipality`, `created_at`, `updated_at`) VALUES
(3, 'Alcaldía Municipal', '(057)(1)8563400', 'Información de Contacto', 1, '2018-06-14 20:44:50', '2018-06-14 20:44:50'),
(4, 'Comando de Policía', '(057(1)8563160', 'Información de Contacto', 1, '2018-06-14 20:44:50', '2018-06-14 20:44:50'),
(5, 'Bogotá', '63 Km', 'Distancia estimada de recorrido terrestre', 1, '2018-06-14 20:44:50', '2018-06-14 20:44:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(67, '2014_10_12_000000_create_users_table', 1),
(68, '2014_10_12_100000_create_password_resets_table', 1),
(69, '2017_10_09_142538_create_operators_table', 1),
(70, '2017_10_09_165344_create_service_categories_table', 1),
(71, '2017_10_09_165703_create_ecosystem_categories_table', 1),
(72, '2017_10_09_165704_create_services_table', 1),
(73, '2017_10_09_185627_create_service_operators_table', 1),
(74, '2017_10_10_144259_create_contacts_table', 1),
(75, '2017_10_11_165651_create_operator_password_resets_table', 1),
(76, '2017_10_21_134427_create_schedules_table', 1),
(78, '2017_10_30_215700_create_service_pictures_table', 3),
(81, '2017_11_07_123037_create_orders_table', 4),
(83, '2018_01_11_164729_create_admins_table', 5),
(84, '2018_01_11_164730_create_admin_password_resets_table', 5),
(85, '2018_01_12_154748_create_polls_table', 6),
(94, '2018_01_22_195432_create_regions_table', 7),
(95, '2018_01_23_132002_create_reporters_table', 7),
(96, '2018_01_23_132003_create_reporter_password_resets_table', 7),
(97, '2018_01_25_172538_create_departments_table', 7),
(101, '2018_01_25_212046_create_municipalities_table', 8),
(102, '2018_01_29_170933_create_municipality_images_table', 8),
(103, '2018_02_08_174141_create_videos_table', 8),
(107, '2018_01_29_180040_create_interest_site_categories_table', 9),
(108, '2018_01_29_214958_create_interest_sites_table', 9),
(109, '2018_02_01_175959_create_interest_site_images_table', 9),
(110, '2018_02_12_212803_create_wallpapers_table', 10),
(111, '2018_03_07_161448_create_customers_table', 11),
(112, '2018_03_09_164310_create_page_parts_table', 12),
(113, '2018_03_09_175043_create_pages_table', 12),
(114, '2018_03_09_175907_create_page_part_pages_table', 12),
(115, '2018_03_12_170357_create_patterns_table', 12),
(116, '2018_03_12_182258_create_pattern_parts_table', 12),
(117, '2018_03_19_203947_create_service_items_table', 13),
(133, '2018_01_23_181617_create_region_images_table', 14),
(134, '2018_05_24_133954_create_establishments_table', 15),
(135, '2018_05_25_105415_create_service_establishments_table', 15),
(136, '2018_05_25_110456_create_service_establishment_pivots_table', 15),
(137, '2018_06_06_134451_create_plates_table', 15),
(138, '2018_06_06_144412_create_establishment_schedules_table', 15),
(139, '2018_06_12_133839_create_rooms_table', 16),
(140, '2018_06_12_164035_create_establishmen_images_table', 17),
(141, '2018_06_12_164820_create_establishment_images_table', 18),
(142, '2018_06_14_132751_create_keydatas_table', 19),
(173, '2018_07_13_134350_create_shipping_orders_table', 21),
(199, '2018_07_16_125933_create_order_products_table', 22),
(200, '2018_07_17_110310_create_reservations_table', 22),
(201, '2018_07_23_170208_create_order_product_items_table', 22),
(204, '2018_08_03_132616_create_pay_reports_table', 23),
(205, '2018_08_03_132809_create_report_reservations_table', 23),
(206, '2018_08_22_140900_create_reprograms_table', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE `municipalities` (
  `id_municipality` int(10) UNSIGNED NOT NULL,
  `municipality_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `weather` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link_icon` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `fk_department` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `front_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `front_state_hotel` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `front_state_restaurant` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `municipalities`
--

INSERT INTO `municipalities` (`id_municipality`, `municipality_name`, `description`, `multimedia_type`, `weather`, `latitude`, `longitude`, `slug`, `type_last_user`, `link_image`, `link_icon`, `fk_last_edition`, `fk_department`, `state`, `front_state`, `front_state_hotel`, `front_state_restaurant`, `created_at`, `updated_at`) VALUES
(1, 'Suesca', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam expedita, sapiente dolorum tempore cum aspernatur eaque molestias laboriosam in autem. Labore sint nostrum libero a similique aspernatur dolores animi aut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error est dolorem sed dolore laudantium cupiditate delectus, rerum enim eos iusto, itaque vel voluptates cum, nisi quia minima nemo qui ea! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis quas repellat commodi optio recusandae delectus fugiat voluptates veritatis laborum, cumque, dolor, pariatur explicabo placeat. Illo commodi facilis corporis accusantium ipsam?</p>', 'images', 'Seco', '48.865633', '2.321236', 'darkar', 'admin', 'images/municipalities/Municipality_201804101244191466427452.png', 'images/municipalities/Municipality_201804101244191579009088.png', 1, 1, 'activo', 'activo', 'activo', 'activo', '2018-04-10 17:44:19', '2018-06-13 15:23:31'),
(4, 'Mun2', '<p>El <strong>Municipio de Suesca</strong> tradicionalmente se ha caracterizado por ser un destino de turismo de aventura, reconocido nacional e internacional por las Rocas, como principal atractivo, alrededor del cual se han desarrollado diferentes servicios tur&iacute;sticos, principalmente el de escalada.</p>\r\n\r\n<p>Se pueden destacar atractivos naturales como las Rocas, los Monolitos, el Ca&ntilde;&oacute;n de la Lechuza, el r&iacute;o Bogot&aacute; a su paso por el Municipio, la Laguna, la Finca San Marino, el Bosque Nativo El Hatillo, el Valle de los Halcones, las cuevas, los termales, en fin, todas las veredas y sus paisajes, as&iacute; como la flora y fauna caracter&iacute;stica de la zona.</p>\r\n\r\n<p>En cuanto a los atractivo culturales, recalcar en algunos como la Casa de la Cultura Guillermo Herrera Camacho, el templo parroquial doctrinero Nuestra Se&ntilde;ora del Rosario (BICN), las tres (3) estaciones de ferrocarril (BICN), la plaza de mercado, los t&uacute;neles, las artesan&iacute;as (lana virgen, tejidos naturales, dulces, material reciclable, talla de madera y piedra), las artes y oficios (producci&oacute;n lanar y l&aacute;ctea sobretodo), la plaza del parque central (pila), el cementerio (mausoleo Pietro Cantini), pictogramas rupestres (ind&iacute;genas), entre otros como la gastronom&iacute;a, la historia muisca y la arquitectura colonial que caracteriza la zona central del Municipio.</p>', 'images', 'Templado', '48.854183', '2.354808', 'mun2', 'admin', 'images/municipalities/Municipality_201805181044191644929858.jpg', 'images/municipalities/Municipality_201804231115381635144830.png', 1, 1, 'activo', 'activo', 'inactivo', 'inactivo', '2018-04-23 16:15:38', '2018-06-12 21:35:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_images`
--

CREATE TABLE `municipality_images` (
  `id_municipality_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fk_municipality` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `municipality_images`
--

INSERT INTO `municipality_images` (`id_municipality_image`, `link_image`, `description`, `fk_municipality`, `created_at`, `updated_at`) VALUES
(1, 'images/municipalities/Municipality_201805181044531009062429.jpg', 'Ubicado en la cima del Cerro de Monserrate con la hermosa ciudad de Bogotá como paisaje principal, este restaurante ofrece a sus visitantes la oportunidad de saborear platos colombianos como el mero costeño, los tamales tolimenses, la bandeja paisa, cuajada con dulce de mamey, o sus famosas parrilladas de chuletitas o chuletones.', 4, '2018-05-18 15:44:53', '2018-05-18 16:26:40'),
(2, 'images/municipalities/Municipality_201805181044532113043307.jpg', 'Lo invitamos a disfrutar de la vista panorámica nocturna de Bogotá mientras se toma algo caliente en el Café de Santa Clara.', 4, '2018-05-18 15:44:53', '2018-05-18 16:26:53'),
(3, 'images/municipalities/Municipality_201805181044531330884433.jpg', 'Delicioso Filete de salmón cocido al sartén bañado en salsa de aguardiente y camarones acompañado de puré de papa criolla y verdura caliente. Sabores Colombianos.', 4, '2018-05-18 15:44:53', '2018-05-18 16:28:04'),
(4, 'images/municipalities/Municipality_201805181044531740907.jpg', 'Lo invitamos a disfrutar de la vista panorámica nocturna de Bogotá mientras se toma algo caliente en el Café de Santa Clara.', 4, '2018-05-18 15:44:53', '2018-05-18 16:27:55'),
(5, 'images/municipalities/Municipality_201805181045141988430245.jpg', 'Conoce más de la historia de Monserrate y de Bogotá recorriendo los salones de nuestra casa.', 4, '2018-05-18 15:45:14', '2018-05-18 16:27:14'),
(6, 'images/municipalities/Municipality_20180518104514695110946.jpg', 'Delicioso Filete de salmón cocido al sartén bañado en salsa de aguardiente y camarones acompañado de puré de papa criolla y verdura caliente. Sabores Colombianos.', 4, '2018-05-18 15:45:14', '2018-05-18 16:27:26'),
(7, 'images/municipalities/Municipality_2018051810451427758827.jpg', 'n clásico gastronómico que nunca pasara de moda, nuestro Ajiaco Santafereño.', 4, '2018-05-18 15:45:14', '2018-05-18 16:27:40'),
(8, 'images/municipalities/Municipality_201805181045141922468676.jpg', 'Ven y disfruta de las delicias colombianas bien calienticas del Café de Santa Clara.', 4, '2018-05-18 15:45:14', '2018-05-18 16:27:48'),
(9, 'images/municipalities/Municipality_20180613110346348759869.jpg', NULL, 1, '2018-06-13 16:03:46', '2018-06-13 16:03:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operators`
--

CREATE TABLE `operators` (
  `id_operator` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_register` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_personal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nit` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days_for_reservation` int(11) NOT NULL,
  `terms` date NOT NULL,
  `data_protection` date NOT NULL,
  `sustainability` date NOT NULL,
  `promotionals_mails` tinyint(1) NOT NULL DEFAULT '0',
  `bank_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_account` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `confirmation_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotel_panel` tinyint(1) NOT NULL DEFAULT '0',
  `restaurant_panel` tinyint(1) NOT NULL DEFAULT '0',
  `service_panel` tinyint(1) NOT NULL DEFAULT '0',
  `commission` int(11) NOT NULL,
  `option_iva` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `operators`
--

INSERT INTO `operators` (`id_operator`, `name`, `national_register`, `contact_personal`, `contact_phone`, `email`, `service_category`, `web`, `nit`, `address`, `region`, `city`, `days_for_reservation`, `terms`, `data_protection`, `sustainability`, `promotionals_mails`, `bank_name`, `account_number`, `type_account`, `avatar`, `password`, `status`, `confirmation_code`, `hotel_panel`, `restaurant_panel`, `service_panel`, `commission`, `option_iva`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sergio Hernandez', '1111', '3203233082', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', '1', '1234', NULL, NULL, NULL, NULL, 3, '0000-00-00', '0000-00-00', '0000-00-00', 0, 'Banco GNB Sudameris', '34567876543', 'Cuenta ahorros', '/images/profile/Profile_2017120415535610814.bro.jpg', '$2y$10$VUBediaCmihh7MJEQZse1.1Mc9lYdzxEV5mM3ZKBg9xKE/17gL86O', 'active', 'TsQbLNQUMukgHOD9n19utMLIS', 0, 0, 1, 10, 'incluido', 'MANIGG2j06ngeb7LADCDhvAG3A39yOMZuWC6OZOsoOee2PN0JBvI22CtQDRi', '2017-10-23 15:56:25', '2018-08-17 21:07:36'),
(2, 'Sergio Hernandez', '345345345345', '3203233082', '3203233082', 'sergio.hernandez2.goyeneche@gmail.com', '1', 'asdasd', NULL, NULL, NULL, NULL, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, NULL, NULL, NULL, NULL, '$2y$10$aSwQmDizImH1JrD1sBx5Ouau3k7ApoVVvFyMAsDvjey.LdHwI5wRm', 'active', NULL, 0, 0, 0, 10, 'mas', 'impHMQDyqaGbSOV2M0aGyqFVaJChK7TWyCXZdUoRPyWwgKKfKek6lfjSmCqn', '2018-01-12 16:49:34', '2018-08-21 20:13:51'),
(3, 'Sergio Hernandez', '3234234', '3203233082', '3203233082', 'sergio.hernand2ez.goyeneche@gmail.com', '1', '2342342', NULL, NULL, NULL, NULL, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, NULL, NULL, NULL, NULL, '$2y$10$0t3xKg1aX2tD6agEYhtmGOMKr8DynH7.lIoAs.JaDUImjneraEQue', 'active', NULL, 0, 0, 0, 15, 'mas', 'w30ZnpHeMDUFc1QyLxu2XubXLXWR2Lcsvsm9VCHockVbrOwjlA9p7I7lzeAg', '2018-01-12 16:53:52', '2018-01-12 16:53:52'),
(4, 'TechGrowth', '8723123', 'Sergio Hernandez', '3203233082', 'al4cdeveloqqqper@gmail.com', '1', 'www.techgrowth.com.co', NULL, NULL, NULL, NULL, 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, NULL, NULL, NULL, NULL, 'JlieavEKjGd77m7ZDfn27bJiWh5wD29Ur5THQEFJ8VMbspwGlo7lklRjOqbq', 'active', NULL, 0, 0, 0, 15, 'incluido', 'Bf5qpJC9BA3lr9A7sdxTFBA13ILCXCz6AqLKbEQ32oaBSBQUoBkGbePyThDz', '2018-04-09 16:15:16', '2018-07-11 16:08:18'),
(5, 'Nuevo Operador', '12234234', 'Sergio Hernandez', '234234', 'operator@tupaseo.travel', '1', 'uasdbashd', '8123123-123123', 'craxw342 asda', 'Distrito Capital de Bogota', 'Bogota  D.C.', 0, '0000-00-00', '0000-00-00', '0000-00-00', 0, 'Banco GNB Sudameris', '34567876543', 'Cuenta ahorros', '/images/profile/Profile_201805241418501793267016.darkar.png', '$2y$10$VB1eAj69/MluJ715LX/rwOb9PFTW9xrezDBKyL/zsdE5inZCx4RvC', 'active', NULL, 1, 1, 1, 15, 'mas', 'z6btDOCYFpusSFfOuM3uL6yKkcUfjT173hBsJjCT0M5k32APo8Ov91nduYsf', NULL, '2018-08-10 19:12:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operator_password_resets`
--

CREATE TABLE `operator_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `operator_password_resets`
--

INSERT INTO `operator_password_resets` (`email`, `token`, `created_at`) VALUES
('operator@tupaseo.travel', '$2y$10$M5gV6GrV1WrsmUY7Ioj4yOPwTBexwToWcaD.8YA3AubgP4e39oR4O', '2018-08-14 20:34:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `fk_user` int(10) UNSIGNED NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_pago` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_payu` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` int(11) NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id_order`, `fk_user`, `estado`, `tipo_pago`, `id_payu`, `costo`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 1, 'pendiente', 'en sitio', '', 0, NULL, '2017-11-25 14:55:26', '2017-11-25 14:55:26'),
(2, 1, 'pendiente', 'en sitio', '', 0, NULL, '2018-01-16 18:36:32', '2018-01-16 18:36:32'),
(3, 1, 'pendiente', 'en sitio', '', 0, NULL, '2018-01-16 21:58:57', '2018-01-16 21:58:57'),
(4, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 15:47:43', '2018-07-18 15:47:43'),
(5, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:34:01', '2018-07-18 16:34:01'),
(6, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:34:30', '2018-07-18 16:34:30'),
(7, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:35:29', '2018-07-18 16:35:29'),
(8, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:36:50', '2018-07-18 16:36:50'),
(9, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:37:28', '2018-07-18 16:37:28'),
(10, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:40:35', '2018-07-18 16:40:35'),
(11, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:41:19', '2018-07-18 16:41:19'),
(12, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:42:33', '2018-07-18 16:42:33'),
(13, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 16:43:09', '2018-07-18 16:43:22'),
(14, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:43:51', '2018-07-18 16:43:51'),
(15, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 16:44:26', '2018-07-18 16:44:36'),
(16, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 16:45:52', '2018-07-18 16:46:06'),
(17, 3, 'pagado', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:47:12', '2018-07-18 16:47:25'),
(18, 3, 'pagado', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:48:08', '2018-07-18 16:48:21'),
(19, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 16:52:13', '2018-07-18 16:52:25'),
(20, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 16:59:55', '2018-07-18 16:59:55'),
(21, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 17:00:07', '2018-07-18 17:00:07'),
(22, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 17:00:16', '2018-07-18 17:00:16'),
(23, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 17:00:26', '2018-07-18 17:00:26'),
(24, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 17:00:54', '2018-07-18 17:00:54'),
(25, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-18 17:01:52', '2018-07-18 17:01:52'),
(26, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 17:54:43', '2018-07-18 17:54:54'),
(27, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 17:55:20', '2018-07-18 17:55:30'),
(28, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 17:56:03', '2018-07-18 17:56:14'),
(29, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 17:56:48', '2018-07-18 17:56:58'),
(30, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 17:58:11', '2018-07-18 17:58:21'),
(31, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 18:02:44', '2018-07-18 18:02:54'),
(32, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 18:08:31', '2018-07-18 18:08:44'),
(33, 3, 'declinada', 'tarjeta_credito', '', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-18 18:09:55', '2018-07-18 18:10:07'),
(34, 3, 'pagado', 'tarjeta_credito', '', 0, NULL, '2018-07-18 18:26:21', '2018-07-18 18:26:34'),
(35, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:00:18', '2018-07-23 19:00:18'),
(36, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:01:00', '2018-07-23 19:01:00'),
(37, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:01:18', '2018-07-23 19:01:18'),
(38, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:02:49', '2018-07-23 19:02:49'),
(39, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:04:37', '2018-07-23 19:04:37'),
(40, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:05:30', '2018-07-23 19:05:30'),
(41, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:05:43', '2018-07-23 19:05:43'),
(42, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:06:07', '2018-07-23 19:06:07'),
(43, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:06:47', '2018-07-23 19:06:47'),
(44, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:08:12', '2018-07-23 19:08:12'),
(45, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:13:06', '2018-07-23 19:13:06'),
(46, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 19:14:07', '2018-07-23 19:14:07'),
(47, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:15:55', '2018-07-23 20:15:55'),
(48, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:17:11', '2018-07-23 20:17:11'),
(49, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:37:43', '2018-07-23 20:37:43'),
(50, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:42:54', '2018-07-23 20:42:54'),
(51, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:43:04', '2018-07-23 20:43:04'),
(52, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:43:23', '2018-07-23 20:43:23'),
(53, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 20:44:26', '2018-07-23 20:44:26'),
(54, 5, 'pagado', 'tarjeta_credito', '844462815', 0, NULL, '2018-07-23 20:48:41', '2018-07-23 20:48:50'),
(55, 5, 'declinada', 'tarjeta_credito', '844462831', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 20:50:10', '2018-07-23 20:50:20'),
(56, 5, 'declinada', 'tarjeta_credito', '844462904', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 21:11:29', '2018-07-23 21:11:39'),
(57, 5, 'declinada', 'tarjeta_credito', '844462907', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 21:11:49', '2018-07-23 21:11:58'),
(58, 5, 'declinada', 'tarjeta_credito', '844462921', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 21:15:34', '2018-07-23 21:15:43'),
(59, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:12:36', '2018-07-23 22:12:36'),
(60, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:12:52', '2018-07-23 22:12:52'),
(61, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:14:56', '2018-07-23 22:14:56'),
(62, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:16:02', '2018-07-23 22:16:02'),
(63, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:16:13', '2018-07-23 22:16:13'),
(64, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:16:14', '2018-07-23 22:16:14'),
(65, 5, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-23 22:16:45', '2018-07-23 22:16:45'),
(66, 5, 'declinada', 'tarjeta_credito', '844463141', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:17:55', '2018-07-23 22:18:03'),
(67, 5, 'declinada', 'tarjeta_credito', '844463145', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:19:24', '2018-07-23 22:19:33'),
(68, 5, 'declinada', 'tarjeta_credito', '844463148', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:19:59', '2018-07-23 22:20:09'),
(69, 5, 'declinada', 'tarjeta_credito', '844463152', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:20:41', '2018-07-23 22:20:51'),
(70, 5, 'declinada', 'tarjeta_credito', '844463154', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:20:59', '2018-07-23 22:21:08'),
(71, 5, 'declinada', 'tarjeta_credito', '844463177', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:28:54', '2018-07-23 22:29:02'),
(72, 5, 'declinada', 'tarjeta_credito', '844463179', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-23 22:29:08', '2018-07-23 22:29:16'),
(73, 3, 'declinada', 'tarjeta_credito', '844468266', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 16:17:09', '2018-07-24 16:17:22'),
(74, 3, 'declinada', 'tarjeta_credito', '844468313', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 16:26:57', '2018-07-24 16:27:10'),
(75, 3, 'declinada', 'tarjeta_credito', '844468316', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 16:27:29', '2018-07-24 16:27:42'),
(76, 3, 'declinada', 'tarjeta_credito', '844468318', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 16:27:54', '2018-07-24 16:28:17'),
(77, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 17:57:48', '2018-07-24 17:57:48'),
(78, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 17:58:56', '2018-07-24 17:58:56'),
(79, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 17:59:07', '2018-07-24 17:59:07'),
(80, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 17:59:20', '2018-07-24 17:59:20'),
(81, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 17:59:37', '2018-07-24 17:59:37'),
(82, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 18:00:07', '2018-07-24 18:00:07'),
(83, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 18:00:16', '2018-07-24 18:00:16'),
(84, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-24 18:00:36', '2018-07-24 18:00:36'),
(85, 3, 'declinada', 'tarjeta_credito', '844468776', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 18:01:03', '2018-07-24 18:01:12'),
(86, 3, 'declinada', 'tarjeta_credito', '844468809', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 18:05:58', '2018-07-24 18:06:07'),
(87, 3, 'declinada', 'tarjeta_credito', '844469510', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 19:53:35', '2018-07-24 19:53:50'),
(88, 3, 'declinada', 'tarjeta_credito', '844469512', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 19:54:24', '2018-07-24 19:54:38'),
(89, 3, 'declinada', 'tarjeta_credito', '844469513', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 19:55:19', '2018-07-24 19:55:33'),
(90, 3, 'declinada', 'tarjeta_credito', '844469515', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 19:56:53', '2018-07-24 19:57:04'),
(91, 3, 'declinada', 'tarjeta_credito', '844469519', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 19:58:07', '2018-07-24 19:58:18'),
(92, 3, 'declinada', 'tarjeta_credito', '844469525', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:00:19', '2018-07-24 20:00:33'),
(93, 3, 'declinada', 'tarjeta_credito', '844469553', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:08:04', '2018-07-24 20:08:13'),
(94, 3, 'declinada', 'tarjeta_credito', '844469573', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:13:41', '2018-07-24 20:13:47'),
(95, 3, 'declinada', 'tarjeta_credito', '844469574', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:13:59', '2018-07-24 20:14:11'),
(96, 3, 'declinada', 'tarjeta_credito', '844469575', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:14:20', '2018-07-24 20:14:32'),
(97, 3, 'declinada', 'tarjeta_credito', '844469583', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:16:53', '2018-07-24 20:17:07'),
(98, 3, 'declinada', 'tarjeta_credito', '844469587', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:17:58', '2018-07-24 20:18:06'),
(99, 3, 'declinada', 'tarjeta_credito', '844469589', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:19:32', '2018-07-24 20:19:42'),
(100, 3, 'declinada', 'tarjeta_credito', '844469590', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:19:52', '2018-07-24 20:20:04'),
(101, 3, 'declinada', 'tarjeta_credito', '844469594', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:20:36', '2018-07-24 20:20:47'),
(102, 3, 'declinada', 'tarjeta_credito', '844469597', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:21:54', '2018-07-24 20:22:02'),
(103, 3, 'declinada', 'tarjeta_credito', '844469598', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:22:11', '2018-07-24 20:22:22'),
(104, 3, 'declinada', 'tarjeta_credito', '844469603', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:24:08', '2018-07-24 20:24:18'),
(105, 3, 'declinada', 'tarjeta_credito', '844469604', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:24:33', '2018-07-24 20:24:43'),
(106, 3, 'declinada', 'tarjeta_credito', '844469607', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:25:42', '2018-07-24 20:25:47'),
(107, 3, 'declinada', 'tarjeta_credito', '844469608', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:26:38', '2018-07-24 20:26:51'),
(108, 3, 'declinada', 'tarjeta_credito', '844469654', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:46:45', '2018-07-24 20:47:01'),
(109, 3, 'declinada', 'tarjeta_credito', '844469658', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-24 20:47:24', '2018-07-24 20:47:37'),
(110, 3, 'declinada', 'tarjeta_credito', '844472161', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 15:28:46', '2018-07-25 15:28:54'),
(111, 3, 'declinada', 'tarjeta_credito', '844472420', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 15:51:40', '2018-07-25 15:51:50'),
(112, 3, 'declinada', 'tarjeta_credito', '844472422', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 15:52:13', '2018-07-25 15:52:22'),
(113, 3, 'declinada', 'tarjeta_credito', '844472433', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 15:54:29', '2018-07-25 15:54:36'),
(114, 3, 'declinada', 'tarjeta_credito', '844472717', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 16:42:46', '2018-07-25 16:42:50'),
(115, 3, 'declinada', 'tarjeta_credito', '844472722', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 16:43:43', '2018-07-25 16:43:46'),
(116, 3, 'declinada', 'tarjeta_credito', '844472754', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 16:48:58', '2018-07-25 16:49:03'),
(117, 3, 'pagado', 'tarjeta_credito', '844472799', 0, NULL, '2018-07-25 17:01:05', '2018-07-25 17:01:10'),
(118, 3, 'pagado', 'tarjeta_credito', '844472801', 0, NULL, '2018-07-25 17:01:21', '2018-07-25 17:01:25'),
(119, 3, 'pagado', 'tarjeta_credito', '844472802', 0, NULL, '2018-07-25 17:01:33', '2018-07-25 17:01:37'),
(120, 3, 'pagado', 'tarjeta_credito', '844472804', 0, NULL, '2018-07-25 17:01:45', '2018-07-25 17:01:50'),
(121, 3, 'pagado', 'tarjeta_credito', '844472806', 0, NULL, '2018-07-25 17:01:57', '2018-07-25 17:02:02'),
(122, 3, 'pagado', 'tarjeta_credito', '844472834', 0, NULL, '2018-07-25 17:12:33', '2018-07-25 17:12:38'),
(123, 3, 'pagado', 'tarjeta_credito', '844472839', 0, NULL, '2018-07-25 17:13:51', '2018-07-25 17:13:54'),
(124, 3, 'pagado', 'tarjeta_credito', '844472848', 0, NULL, '2018-07-25 17:16:34', '2018-07-25 17:16:38'),
(125, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 18:15:48', '2018-07-25 18:15:48'),
(126, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 18:16:22', '2018-07-25 18:16:22'),
(127, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 18:16:52', '2018-07-25 18:16:52'),
(128, 3, 'pagado', 'tarjeta_credito', '844473072', 0, NULL, '2018-07-25 18:17:26', '2018-07-25 18:17:30'),
(129, 3, 'pagado', 'tarjeta_credito', '844473707', 0, NULL, '2018-07-25 20:21:47', '2018-07-25 20:21:52'),
(130, 3, 'pagado', 'tarjeta_credito', '844473715', 0, NULL, '2018-07-25 20:22:19', '2018-07-25 20:22:24'),
(131, 3, 'pagado', 'tarjeta_credito', '844473716', 0, NULL, '2018-07-25 20:22:45', '2018-07-25 20:22:51'),
(132, 3, 'pagado', 'tarjeta_credito', '844473720', 0, NULL, '2018-07-25 20:23:13', '2018-07-25 20:23:18'),
(133, 3, 'pagado', 'tarjeta_credito', '844473723', 0, NULL, '2018-07-25 20:23:25', '2018-07-25 20:23:30'),
(134, 3, 'pagado', 'tarjeta_credito', '844473726', 0, NULL, '2018-07-25 20:23:52', '2018-07-25 20:23:58'),
(135, 3, 'pagado', 'tarjeta_credito', '844473727', 0, NULL, '2018-07-25 20:25:02', '2018-07-25 20:25:07'),
(136, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 20:26:55', '2018-07-25 20:26:55'),
(137, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 20:27:20', '2018-07-25 20:27:20'),
(138, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 20:28:05', '2018-07-25 20:28:05'),
(139, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 20:28:14', '2018-07-25 20:28:14'),
(140, 3, 'pendiente', 'tarjeta_credito', '', 0, NULL, '2018-07-25 20:29:11', '2018-07-25 20:29:11'),
(141, 3, 'pagado', 'tarjeta_credito', '844473754', 0, NULL, '2018-07-25 20:29:51', '2018-07-25 20:29:56'),
(142, 3, 'pagado', 'tarjeta_credito', '844473758', 0, NULL, '2018-07-25 20:30:36', '2018-07-25 20:30:40'),
(143, 3, 'pagado', 'tarjeta_credito', '844473761', 0, NULL, '2018-07-25 20:31:04', '2018-07-25 20:31:10'),
(144, 3, 'pagado', 'tarjeta_credito', '844473767', 0, NULL, '2018-07-25 20:32:05', '2018-07-25 20:32:19'),
(145, 3, 'pagado', 'tarjeta_credito', '844473770', 0, NULL, '2018-07-25 20:33:09', '2018-07-25 20:33:14'),
(146, 3, 'pagado', 'tarjeta_credito', '844473779', 0, NULL, '2018-07-25 20:35:22', '2018-07-25 20:35:27'),
(147, 3, 'pagado', 'tarjeta_credito', '844473787', 0, NULL, '2018-07-25 20:42:19', '2018-07-25 20:42:24'),
(148, 3, 'pagado', 'tarjeta_credito', '844473794', 0, NULL, '2018-07-25 20:43:18', '2018-07-25 20:43:24'),
(149, 3, 'pagado', 'tarjeta_credito', '844474012', 0, NULL, '2018-07-25 20:47:47', '2018-07-25 20:47:52'),
(150, 3, 'pagado', 'tarjeta_credito', '844474030', 0, NULL, '2018-07-25 20:51:10', '2018-07-25 20:51:16'),
(151, 3, 'declinada', 'tarjeta_credito', '844474064', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:03:06', '2018-07-25 21:03:11'),
(152, 3, 'declinada', 'tarjeta_credito', '844474069', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:04:06', '2018-07-25 21:04:11'),
(153, 3, 'declinada', 'tarjeta_credito', '844474080', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:07:49', '2018-07-25 21:07:54'),
(154, 3, 'declinada', 'tarjeta_credito', '844474089', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:08:57', '2018-07-25 21:09:02'),
(155, 3, 'declinada', 'tarjeta_credito', '844474094', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:10:58', '2018-07-25 21:11:03'),
(156, 3, 'declinada', 'tarjeta_credito', '844474095', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:11:21', '2018-07-25 21:11:27'),
(157, 3, 'declinada', 'tarjeta_credito', '844474172', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:34:13', '2018-07-25 21:34:18'),
(158, 3, 'declinada', 'tarjeta_credito', '844474173', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:34:29', '2018-07-25 21:34:33'),
(159, 3, 'declinada', 'tarjeta_credito', '844474178', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:35:54', '2018-07-25 21:35:59'),
(160, 3, 'declinada', 'tarjeta_credito', '844474181', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:36:23', '2018-07-25 21:36:27'),
(161, 3, 'declinada', 'tarjeta_credito', '844474184', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:36:49', '2018-07-25 21:36:53'),
(162, 3, 'declinada', 'tarjeta_credito', '844474195', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:39:34', '2018-07-25 21:39:40'),
(163, 3, 'declinada', 'tarjeta_credito', '844474204', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:40:18', '2018-07-25 21:40:24'),
(164, 3, 'declinada', 'tarjeta_credito', '844474217', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-25 21:42:15', '2018-07-25 21:42:20'),
(165, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 14:54:27', '2018-07-26 14:54:27'),
(166, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 15:02:55', '2018-07-26 15:02:55'),
(167, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 15:03:20', '2018-07-26 15:03:20'),
(168, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 15:05:34', '2018-07-26 15:05:34'),
(169, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 15:06:24', '2018-07-26 15:06:24'),
(170, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 15:48:55', '2018-07-26 15:48:55'),
(171, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 15:58:49', '2018-07-26 15:58:49'),
(172, 3, 'declinada', 'pse', '1049696114', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-26 16:10:35', '2018-07-26 16:10:39'),
(173, 3, 'declinada', 'pse', '1049696134', 0, 'DECLINED_TEST_MODE_NOT_ALLOWED', '2018-07-26 16:10:53', '2018-07-26 16:10:56'),
(174, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(175, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(176, 3, 'declinada', 'pse', '', 0, 'PAYMENT_NETWORK_REJECTED', '2018-07-26 16:16:04', '2018-07-26 17:21:54'),
(177, 3, 'pendiente', 'pse', '', 0, NULL, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(178, 3, 'declinada', 'pse', '', 0, 'PAYMENT_NETWORK_REJECTED', '2018-07-26 17:34:41', '2018-07-26 17:35:04'),
(179, 3, 'declinada', 'pse', '1049722475', 0, 'PAYMENT_NETWORK_REJECTED', '2018-07-26 17:50:26', '2018-07-26 17:50:40'),
(180, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(181, 3, 'declinada', 'pse', '1049724406', 0, 'PAYMENT_NETWORK_REJECTED', '2018-07-26 17:56:41', '2018-07-26 17:56:51'),
(182, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(183, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(184, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(185, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(186, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(187, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(188, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(189, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(190, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(191, 3, 'pendiente', 'efecty', '', 0, NULL, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(192, 3, 'pendiente', 'efectivo', '162050250', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-26 20:47:16', '2018-07-26 20:47:27'),
(193, 3, 'pendiente', 'efectivo', '162050506', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-26 20:48:50', '2018-07-26 20:48:51'),
(194, 3, 'pendiente', 'efectivo', '162867317', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 15:05:55', '2018-07-30 15:06:01'),
(195, 3, 'pendiente', 'efectivo', '162867940', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 15:08:43', '2018-07-30 15:08:47'),
(196, 3, 'pendiente', 'efectivo', '162868475', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 15:11:33', '2018-07-30 15:11:35'),
(197, 3, 'pendiente', 'efectivo', '162869321', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 15:15:31', '2018-07-30 15:15:38'),
(198, 3, 'declinada', 'pse', '1050747262', 0, 'PAYMENT_NETWORK_REJECTED', '2018-07-30 17:44:31', '2018-07-30 17:44:44'),
(199, 3, 'pendiente', 'pse', '1050747134', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 17:45:30', '2018-07-30 17:45:34'),
(200, 3, 'pendiente', 'efectivo', '162901640', 0, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 17:52:29', '2018-07-30 17:52:31'),
(201, 3, 'pendiente', 'efectivo', '162903772', 50500, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 18:03:17', '2018-07-30 18:03:24'),
(202, 3, 'pendiente', 'efectivo', '162920727', 50500, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 19:47:28', '2018-07-30 19:47:33'),
(203, 3, 'pendiente', 'efectivo', '', 50500, NULL, '2018-07-30 19:48:55', '2018-07-30 19:48:55'),
(204, 3, 'pendiente', 'efectivo', '162921021', 50500, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 19:49:07', '2018-07-30 19:49:14'),
(205, 3, 'pendiente', 'efectivo', '162938168', 70500, 'PENDING_TRANSACTION_CONFIRMATION', '2018-07-30 21:34:01', '2018-07-30 21:34:12'),
(206, 3, 'pendiente', 'efectivo', '163664170', 720000, 'PENDING_TRANSACTION_CONFIRMATION', '2018-08-03 17:48:32', '2018-08-03 17:48:34'),
(207, 3, 'pendiente', 'efectivo', '', 520000, NULL, '2018-08-03 17:56:07', '2018-08-03 17:56:07'),
(208, 3, 'pendiente', 'efectivo', '165353750', 180000, 'PENDING_TRANSACTION_CONFIRMATION', '2018-08-13 19:42:57', '2018-08-13 19:43:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_products`
--

CREATE TABLE `order_products` (
  `id_order_product` int(10) UNSIGNED NOT NULL,
  `fk_order` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `units` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `order_products`
--

INSERT INTO `order_products` (`id_order_product`, `fk_order`, `fk_service`, `units`, `unit_price`, `created_at`, `updated_at`) VALUES
(1, 126, 1, 1, 180000, '2018-07-25 18:16:22', '2018-07-25 18:16:22'),
(2, 127, 1, 1, 180000, '2018-07-25 18:16:52', '2018-07-25 18:16:52'),
(3, 128, 1, 1, 180000, '2018-07-25 18:17:26', '2018-07-25 18:17:26'),
(4, 128, 1, 2, 130000, '2018-07-25 18:17:26', '2018-07-25 18:17:26'),
(5, 129, 1, 3, 180000, '2018-07-25 20:21:47', '2018-07-25 20:21:47'),
(6, 130, 1, 3, 180000, '2018-07-25 20:22:19', '2018-07-25 20:22:19'),
(7, 131, 1, 3, 180000, '2018-07-25 20:22:45', '2018-07-25 20:22:45'),
(8, 132, 1, 3, 180000, '2018-07-25 20:23:13', '2018-07-25 20:23:13'),
(9, 133, 1, 3, 180000, '2018-07-25 20:23:25', '2018-07-25 20:23:25'),
(10, 134, 1, 3, 180000, '2018-07-25 20:23:52', '2018-07-25 20:23:52'),
(11, 135, 1, 3, 180000, '2018-07-25 20:25:02', '2018-07-25 20:25:02'),
(12, 136, 1, 3, 180000, '2018-07-25 20:26:55', '2018-07-25 20:26:55'),
(13, 138, 1, 3, 180000, '2018-07-25 20:28:05', '2018-07-25 20:28:05'),
(14, 139, 1, 3, 180000, '2018-07-25 20:28:14', '2018-07-25 20:28:14'),
(15, 140, 1, 3, 180000, '2018-07-25 20:29:11', '2018-07-25 20:29:11'),
(16, 141, 1, 3, 180000, '2018-07-25 20:29:51', '2018-07-25 20:29:51'),
(17, 142, 1, 3, 180000, '2018-07-25 20:30:36', '2018-07-25 20:30:36'),
(18, 143, 1, 3, 180000, '2018-07-25 20:31:04', '2018-07-25 20:31:04'),
(19, 144, 1, 3, 180000, '2018-07-25 20:32:05', '2018-07-25 20:32:05'),
(20, 145, 1, 3, 180000, '2018-07-25 20:33:09', '2018-07-25 20:33:09'),
(21, 146, 1, 3, 180000, '2018-07-25 20:35:22', '2018-07-25 20:35:22'),
(22, 147, 1, 3, 180000, '2018-07-25 20:42:19', '2018-07-25 20:42:19'),
(23, 148, 1, 3, 180000, '2018-07-25 20:43:18', '2018-07-25 20:43:18'),
(24, 149, 1, 3, 180000, '2018-07-25 20:47:47', '2018-07-25 20:47:47'),
(25, 150, 1, 3, 180000, '2018-07-25 20:51:10', '2018-07-25 20:51:10'),
(26, 151, 1, 3, 180000, '2018-07-25 21:03:06', '2018-07-25 21:03:06'),
(27, 152, 1, 3, 180000, '2018-07-25 21:04:06', '2018-07-25 21:04:06'),
(28, 153, 1, 3, 180000, '2018-07-25 21:07:49', '2018-07-25 21:07:49'),
(29, 154, 1, 3, 180000, '2018-07-25 21:08:57', '2018-07-25 21:08:57'),
(30, 155, 1, 3, 180000, '2018-07-25 21:10:58', '2018-07-25 21:10:58'),
(31, 156, 1, 3, 180000, '2018-07-25 21:11:21', '2018-07-25 21:11:21'),
(32, 157, 1, 3, 180000, '2018-07-25 21:34:13', '2018-07-25 21:34:13'),
(33, 158, 1, 3, 180000, '2018-07-25 21:34:29', '2018-07-25 21:34:29'),
(34, 159, 1, 3, 180000, '2018-07-25 21:35:54', '2018-07-25 21:35:54'),
(35, 160, 1, 3, 180000, '2018-07-25 21:36:23', '2018-07-25 21:36:23'),
(36, 161, 1, 3, 180000, '2018-07-25 21:36:49', '2018-07-25 21:36:49'),
(37, 162, 1, 3, 180000, '2018-07-25 21:39:35', '2018-07-25 21:39:35'),
(38, 163, 1, 3, 180000, '2018-07-25 21:40:18', '2018-07-25 21:40:18'),
(39, 164, 1, 3, 180000, '2018-07-25 21:42:15', '2018-07-25 21:42:15'),
(40, 165, 6, 1, 5000, '2018-07-26 14:54:27', '2018-07-26 14:54:27'),
(41, 166, 6, 1, 5000, '2018-07-26 15:02:55', '2018-07-26 15:02:55'),
(42, 167, 6, 1, 5000, '2018-07-26 15:03:20', '2018-07-26 15:03:20'),
(43, 168, 6, 1, 5000, '2018-07-26 15:05:34', '2018-07-26 15:05:34'),
(44, 169, 6, 1, 5000, '2018-07-26 15:06:24', '2018-07-26 15:06:24'),
(45, 170, 6, 1, 5000, '2018-07-26 15:48:55', '2018-07-26 15:48:55'),
(46, 171, 6, 1, 5000, '2018-07-26 15:58:49', '2018-07-26 15:58:49'),
(47, 172, 6, 1, 5000, '2018-07-26 16:10:35', '2018-07-26 16:10:35'),
(48, 172, 5, 1, 70000, '2018-07-26 16:10:35', '2018-07-26 16:10:35'),
(49, 173, 6, 1, 5000, '2018-07-26 16:10:53', '2018-07-26 16:10:53'),
(50, 173, 5, 1, 70000, '2018-07-26 16:10:53', '2018-07-26 16:10:53'),
(51, 174, 6, 1, 5000, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(52, 174, 5, 1, 70000, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(53, 175, 6, 1, 5000, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(54, 175, 5, 1, 70000, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(55, 176, 6, 1, 5000, '2018-07-26 16:16:04', '2018-07-26 16:16:04'),
(56, 176, 5, 1, 70000, '2018-07-26 16:16:04', '2018-07-26 16:16:04'),
(57, 177, 6, 1, 5000, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(58, 177, 5, 1, 70000, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(59, 178, 6, 1, 5000, '2018-07-26 17:34:41', '2018-07-26 17:34:41'),
(60, 178, 5, 1, 70000, '2018-07-26 17:34:41', '2018-07-26 17:34:41'),
(61, 179, 6, 1, 5000, '2018-07-26 17:50:26', '2018-07-26 17:50:26'),
(62, 179, 5, 1, 70000, '2018-07-26 17:50:26', '2018-07-26 17:50:26'),
(63, 180, 6, 1, 5000, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(64, 180, 5, 1, 70000, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(65, 181, 6, 1, 5000, '2018-07-26 17:56:41', '2018-07-26 17:56:41'),
(66, 181, 5, 1, 70000, '2018-07-26 17:56:41', '2018-07-26 17:56:41'),
(67, 182, 6, 1, 5000, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(68, 182, 5, 1, 70000, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(69, 183, 6, 1, 5000, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(70, 183, 5, 1, 70000, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(71, 184, 6, 1, 5000, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(72, 184, 5, 1, 70000, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(73, 185, 6, 1, 5000, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(74, 185, 5, 1, 70000, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(75, 186, 6, 1, 5000, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(76, 186, 5, 1, 70000, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(77, 187, 6, 1, 5000, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(78, 187, 5, 1, 70000, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(79, 188, 6, 1, 5000, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(80, 188, 5, 1, 70000, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(81, 189, 6, 1, 5000, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(82, 189, 5, 1, 70000, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(83, 190, 6, 1, 5000, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(84, 190, 5, 1, 70000, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(85, 191, 6, 1, 5000, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(86, 191, 5, 1, 70000, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(87, 192, 6, 1, 5000, '2018-07-26 20:47:16', '2018-07-26 20:47:16'),
(88, 192, 5, 1, 70000, '2018-07-26 20:47:16', '2018-07-26 20:47:16'),
(89, 193, 6, 1, 5000, '2018-07-26 20:48:50', '2018-07-26 20:48:50'),
(90, 193, 5, 1, 70000, '2018-07-26 20:48:50', '2018-07-26 20:48:50'),
(91, 194, 1, 1, 180000, '2018-07-30 15:05:56', '2018-07-30 15:05:56'),
(92, 195, 1, 1, 180000, '2018-07-30 15:08:43', '2018-07-30 15:08:43'),
(93, 196, 1, 1, 180000, '2018-07-30 15:11:33', '2018-07-30 15:11:33'),
(94, 197, 1, 1, 180000, '2018-07-30 15:15:31', '2018-07-30 15:15:31'),
(95, 198, 1, 1, 180000, '2018-07-30 17:44:31', '2018-07-30 17:44:31'),
(96, 199, 1, 1, 180000, '2018-07-30 17:45:30', '2018-07-30 17:45:30'),
(97, 200, 1, 1, 180000, '2018-07-30 17:52:29', '2018-07-30 17:52:29'),
(98, 201, 5, 1, 50500, '2018-07-30 18:03:17', '2018-07-30 18:03:17'),
(99, 202, 5, 1, 50500, '2018-07-30 19:47:28', '2018-07-30 19:47:28'),
(100, 203, 5, 1, 50500, '2018-07-30 19:48:55', '2018-07-30 19:48:55'),
(101, 204, 5, 1, 50500, '2018-07-30 19:49:07', '2018-07-30 19:49:07'),
(102, 205, 5, 1, 70500, '2018-07-30 21:34:01', '2018-07-30 21:34:01'),
(103, 206, 1, 4, 180350, '2018-08-03 17:48:32', '2018-08-03 17:48:32'),
(104, 207, 1, 4, 130000, '2018-08-03 17:56:07', '2018-08-03 17:56:07'),
(105, 208, 1, 1, 180000, '2018-08-13 19:42:57', '2018-08-13 19:42:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_product_items`
--

CREATE TABLE `order_product_items` (
  `id_product_item` int(10) UNSIGNED NOT NULL,
  `fk_order_product` int(10) UNSIGNED NOT NULL,
  `fk_service_item` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `order_product_items`
--

INSERT INTO `order_product_items` (`id_product_item`, `fk_order_product`, `fk_service_item`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-07-25 18:16:22', '2018-07-25 18:16:22'),
(2, 3, 1, '2018-07-25 18:17:26', '2018-07-25 18:17:26'),
(3, 5, 1, '2018-07-25 20:21:47', '2018-07-25 20:21:47'),
(4, 6, 1, '2018-07-25 20:22:19', '2018-07-25 20:22:19'),
(5, 7, 1, '2018-07-25 20:22:45', '2018-07-25 20:22:45'),
(6, 8, 1, '2018-07-25 20:23:13', '2018-07-25 20:23:13'),
(7, 9, 1, '2018-07-25 20:23:25', '2018-07-25 20:23:25'),
(8, 10, 1, '2018-07-25 20:23:52', '2018-07-25 20:23:52'),
(9, 11, 1, '2018-07-25 20:25:02', '2018-07-25 20:25:02'),
(10, 12, 1, '2018-07-25 20:26:55', '2018-07-25 20:26:55'),
(11, 13, 1, '2018-07-25 20:28:05', '2018-07-25 20:28:05'),
(12, 14, 1, '2018-07-25 20:28:14', '2018-07-25 20:28:14'),
(13, 15, 1, '2018-07-25 20:29:11', '2018-07-25 20:29:11'),
(14, 16, 1, '2018-07-25 20:29:51', '2018-07-25 20:29:51'),
(15, 17, 1, '2018-07-25 20:30:36', '2018-07-25 20:30:36'),
(16, 18, 1, '2018-07-25 20:31:04', '2018-07-25 20:31:04'),
(17, 19, 1, '2018-07-25 20:32:05', '2018-07-25 20:32:05'),
(18, 20, 1, '2018-07-25 20:33:09', '2018-07-25 20:33:09'),
(19, 21, 1, '2018-07-25 20:35:22', '2018-07-25 20:35:22'),
(20, 22, 1, '2018-07-25 20:42:19', '2018-07-25 20:42:19'),
(21, 23, 1, '2018-07-25 20:43:18', '2018-07-25 20:43:18'),
(22, 24, 1, '2018-07-25 20:47:47', '2018-07-25 20:47:47'),
(23, 25, 1, '2018-07-25 20:51:10', '2018-07-25 20:51:10'),
(24, 26, 1, '2018-07-25 21:03:06', '2018-07-25 21:03:06'),
(25, 27, 1, '2018-07-25 21:04:06', '2018-07-25 21:04:06'),
(26, 28, 1, '2018-07-25 21:07:49', '2018-07-25 21:07:49'),
(27, 29, 1, '2018-07-25 21:08:57', '2018-07-25 21:08:57'),
(28, 30, 1, '2018-07-25 21:10:58', '2018-07-25 21:10:58'),
(29, 31, 1, '2018-07-25 21:11:21', '2018-07-25 21:11:21'),
(30, 32, 1, '2018-07-25 21:34:13', '2018-07-25 21:34:13'),
(31, 33, 1, '2018-07-25 21:34:29', '2018-07-25 21:34:29'),
(32, 34, 1, '2018-07-25 21:35:54', '2018-07-25 21:35:54'),
(33, 35, 1, '2018-07-25 21:36:23', '2018-07-25 21:36:23'),
(34, 36, 1, '2018-07-25 21:36:49', '2018-07-25 21:36:49'),
(35, 37, 1, '2018-07-25 21:39:35', '2018-07-25 21:39:35'),
(36, 38, 1, '2018-07-25 21:40:18', '2018-07-25 21:40:18'),
(37, 39, 1, '2018-07-25 21:42:15', '2018-07-25 21:42:15'),
(38, 48, 4, '2018-07-26 16:10:35', '2018-07-26 16:10:35'),
(39, 50, 4, '2018-07-26 16:10:53', '2018-07-26 16:10:53'),
(40, 52, 4, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(41, 54, 4, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(42, 56, 4, '2018-07-26 16:16:04', '2018-07-26 16:16:04'),
(43, 58, 4, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(44, 60, 4, '2018-07-26 17:34:41', '2018-07-26 17:34:41'),
(45, 62, 4, '2018-07-26 17:50:26', '2018-07-26 17:50:26'),
(46, 64, 4, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(47, 66, 4, '2018-07-26 17:56:41', '2018-07-26 17:56:41'),
(48, 68, 4, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(49, 70, 4, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(50, 72, 4, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(51, 74, 4, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(52, 76, 4, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(53, 78, 4, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(54, 80, 4, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(55, 82, 4, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(56, 84, 4, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(57, 86, 4, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(58, 88, 4, '2018-07-26 20:47:16', '2018-07-26 20:47:16'),
(59, 90, 4, '2018-07-26 20:48:50', '2018-07-26 20:48:50'),
(60, 91, 1, '2018-07-30 15:05:56', '2018-07-30 15:05:56'),
(61, 92, 1, '2018-07-30 15:08:43', '2018-07-30 15:08:43'),
(62, 93, 1, '2018-07-30 15:11:33', '2018-07-30 15:11:33'),
(63, 94, 1, '2018-07-30 15:15:31', '2018-07-30 15:15:31'),
(64, 95, 1, '2018-07-30 17:44:31', '2018-07-30 17:44:31'),
(65, 96, 1, '2018-07-30 17:45:30', '2018-07-30 17:45:30'),
(66, 97, 1, '2018-07-30 17:52:29', '2018-07-30 17:52:29'),
(67, 102, 4, '2018-07-30 21:34:01', '2018-07-30 21:34:01'),
(68, 103, 1, '2018-08-03 17:48:32', '2018-08-03 17:48:32'),
(69, 105, 1, '2018-08-13 19:42:57', '2018-08-13 19:42:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pages`
--

INSERT INTO `pages` (`id_page`, `page_name`, `description`, `keywords`, `url`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Bienvenido', '{\"nomames\":\"sidhsdjfsd\",\"tel\":\"23423\"}', 'página de tu paseo', '/', 'index', '2018-04-12 16:13:54', '2018-07-26 17:56:03'),
(2, 'municipios', 'asda sdas das dasd asd asd asd a', 'asdas dasd asd,a s,da,sd,as,das,asd', '/municipalities', 'municipios', '2018-04-18 16:44:29', '2018-04-18 16:44:29'),
(3, 'Región Caribe', 'aksdbaksjdoajsdl aksdojab dlais ubasdn apsiodu absdnpasi ausbd nalskñdpoasio dubajsldk ñaspidoubasjda', 'pagina,del,caribe,tu,paseo,TuPaseo', '/region/caribe', 'region-caribe', '2018-05-09 18:08:19', '2018-05-09 18:08:19'),
(4, 'Municipio 2', 'alsjdnoasdpasod asdas dasd asd asdasd asd asdas dasd asd asd', 'no,pinches,putas,vergas,mames,a', '/municipality/mun2', 'municipio-2', '2018-05-18 18:53:44', '2018-05-18 18:53:44'),
(5, 'Restaurantes bogotá', 'asda jsdniasubd iasund iausndoa snoasndo aispdaosm dajsbdiu absiod', 'restaurantes,darkar,todo aspero,hary', '/restaurant/darkar', 'restaurantes-bogota', '2018-06-18 16:12:59', '2018-06-18 16:12:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_parts`
--

CREATE TABLE `page_parts` (
  `id_part` int(10) UNSIGNED NOT NULL,
  `part_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `page_parts`
--

INSERT INTO `page_parts` (`id_part`, `part_name`, `type`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Banner Principal', 'Carrusel', 'activo', 'banner-principal', '2018-04-12 16:13:11', '2018-04-17 21:05:16'),
(2, 'Vídeo', 'Estatica', 'activo', 'videos-home', '2018-04-17 21:05:57', '2018-04-17 21:59:30'),
(3, 'Panel 1', 'Estatica', 'activo', 'panel-1', '2018-04-18 16:42:09', '2018-04-27 21:47:20'),
(4, 'Panel 2', 'Estatica', 'activo', 'panel-2', '2018-04-18 16:42:20', '2018-04-18 16:44:42'),
(5, 'Footer página', 'Carrusel', 'activo', 'footer-pagina', '2018-04-27 21:47:54', '2018-04-27 21:47:54'),
(6, 'Entre Página', 'Carrusel', 'activo', 'entre-pagina', '2018-05-11 21:15:33', '2018-05-11 21:15:33'),
(7, 'Entre Página 2', 'Carrusel', 'activo', 'entre-pagina-2', '2018-06-13 17:30:13', '2018-06-13 17:30:13'),
(8, 'Entre Página 3', 'Carrusel', 'activo', 'entre-pagina-3', '2018-06-14 17:53:42', '2018-06-14 17:53:42'),
(9, 'Entre listado', 'Carrusel', 'activo', 'entre-listado', '2018-06-26 18:02:58', '2018-06-26 18:02:58'),
(10, 'Entre listado 2', 'Carrusel', 'activo', 'entre-listado-2', '2018-06-26 18:03:06', '2018-06-26 18:03:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_part_pages`
--

CREATE TABLE `page_part_pages` (
  `id_part_page` int(10) UNSIGNED NOT NULL,
  `fk_page` int(10) UNSIGNED NOT NULL,
  `fk_part` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `page_part_pages`
--

INSERT INTO `page_part_pages` (`id_part_page`, `fk_page`, `fk_part`, `created_at`, `updated_at`) VALUES
(2, 1, 2, NULL, NULL),
(3, 2, 1, NULL, NULL),
(4, 2, 3, NULL, NULL),
(5, 2, 4, NULL, NULL),
(6, 2, 2, NULL, NULL),
(7, 1, 3, NULL, NULL),
(8, 1, 4, NULL, NULL),
(9, 1, 5, NULL, NULL),
(10, 2, 5, NULL, NULL),
(11, 3, 3, NULL, NULL),
(14, 3, 6, NULL, NULL),
(15, 4, 3, NULL, NULL),
(16, 4, 5, NULL, NULL),
(17, 4, 6, NULL, NULL),
(18, 3, 7, NULL, NULL),
(19, 3, 2, NULL, NULL),
(20, 3, 8, NULL, NULL),
(21, 5, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('al4cdeveloper@gmail.com', '$2y$10$IHNfNjrNOFFj1VmNgk93x.xI530qjL35iweAFlIg0W06Bw/S7RMGK', '2018-08-14 21:53:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patterns`
--

CREATE TABLE `patterns` (
  `id_pattern` int(10) UNSIGNED NOT NULL,
  `fk_customer` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `clicks` int(11) DEFAULT NULL,
  `redirection` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publication_day` date NOT NULL,
  `publication_finish` date NOT NULL,
  `pattern` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `patterns`
--

INSERT INTO `patterns` (`id_pattern`, `fk_customer`, `type`, `multimedia_type`, `clicks`, `redirection`, `publication_day`, `publication_finish`, `pattern`, `state`, `created_at`, `updated_at`) VALUES
(1, 2, 'Estatica', 'Imagen', NULL, 'http://www.movistar.co', '2018-05-09', '2019-05-09', 'images/patterns/pattern20180503154329822471294.png', 'activo', '2018-04-27 21:51:40', '2018-05-09 18:09:03'),
(2, 3, 'Estatica', 'Imagen', NULL, 'https://www.redbull.com/co-es/', '2018-05-04', '2019-05-04', 'images/patterns/pattern20180430170248965036910.png', 'activo', '2018-04-30 22:02:48', '2018-05-04 19:17:22'),
(3, 4, 'Carrusel', 'Imagen', NULL, 'https://www.satena.com', '2018-05-04', '2019-05-04', 'images/patterns/pattern20180503124324345520534.png', 'activo', '2018-05-03 17:43:25', '2018-05-04 19:18:03'),
(4, 8, 'Estatica', 'Video', NULL, NULL, '2018-05-04', '2019-05-04', 'HqDz5U7U0Ak', 'activo', '2018-05-03 18:07:22', '2018-05-04 21:15:20'),
(5, 5, 'Carrusel', 'Imagen', NULL, 'https://www.envia.com', '2018-06-13', '2019-06-13', 'images/patterns/pattern201805041140541226640972.jpg', 'activo', '2018-05-03 18:44:13', '2018-06-13 17:32:10'),
(6, 6, 'Carrusel', 'Imagen', NULL, NULL, '2018-06-13', '2019-06-13', 'images/patterns/pattern201805031553102121942493.png', 'activo', '2018-05-03 20:53:10', '2018-06-13 17:33:26'),
(7, 7, 'Carrusel', 'Imagen', NULL, 'http://www.cafam.com.co', '2018-06-18', '2019-06-18', 'images/patterns/pattern201805071216241659849156.jpg', 'activo', '2018-05-07 17:16:24', '2018-06-18 16:31:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pattern_parts`
--

CREATE TABLE `pattern_parts` (
  `id_pattern_part` int(10) UNSIGNED NOT NULL,
  `fk_pattern` int(10) UNSIGNED NOT NULL,
  `fk_pagepart` int(10) UNSIGNED NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pattern_parts`
--

INSERT INTO `pattern_parts` (`id_pattern_part`, `fk_pattern`, `fk_pagepart`, `state`, `clicks`, `created_at`, `updated_at`) VALUES
(7, 2, 8, 'activo', NULL, NULL, '2018-05-04 19:17:22'),
(8, 3, 9, 'activo', NULL, NULL, '2018-05-04 19:18:03'),
(9, 4, 2, 'activo', NULL, NULL, '2018-05-04 21:16:21'),
(20, 1, 4, 'inactivo', NULL, NULL, '2018-05-04 17:46:58'),
(26, 6, 9, 'activo', NULL, NULL, '2018-06-13 17:33:26'),
(27, 5, 9, 'activo', 1, NULL, '2018-06-13 17:32:10'),
(29, 5, 3, 'inactivo', 0, NULL, '2018-05-04 18:12:13'),
(30, 5, 10, 'inactivo', 0, NULL, '2018-05-04 17:27:07'),
(31, 5, 7, 'inactivo', 0, NULL, '2018-05-04 16:40:54'),
(32, 1, 7, 'activo', 2, NULL, '2018-06-22 21:53:34'),
(33, 4, 7, 'inactivo', 0, NULL, '2018-05-04 21:16:21'),
(34, 7, 9, 'activo', 0, NULL, '2018-06-18 16:31:03'),
(35, 1, 11, 'activo', 0, NULL, NULL),
(36, 7, 14, 'activo', 0, NULL, '2018-06-18 16:31:03'),
(37, 5, 14, 'activo', 0, NULL, '2018-06-13 17:32:10'),
(38, 6, 14, 'activo', 0, NULL, '2018-06-13 17:33:26'),
(39, 7, 18, 'activo', 0, NULL, '2018-06-18 16:31:03'),
(40, 5, 18, 'activo', 0, NULL, NULL),
(41, 6, 18, 'activo', 0, NULL, NULL),
(42, 7, 21, 'activo', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pay_reports`
--

CREATE TABLE `pay_reports` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` date NOT NULL,
  `date_finish` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pay_reports`
--

INSERT INTO `pay_reports` (`id_report`, `state`, `date_start`, `date_finish`, `created_at`, `updated_at`) VALUES
(1, 'cancelado', '2018-08-01', '2018-08-31', '2018-08-06 15:45:06', '2018-08-10 21:27:24'),
(2, 'cancelado', '2018-08-01', '2018-08-31', '2018-08-06 16:05:24', '2018-08-10 21:27:20'),
(3, 'pagado', '2018-08-01', '2018-08-31', '2018-08-06 16:05:32', '2018-08-10 21:40:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plates`
--

CREATE TABLE `plates` (
  `id_plate` int(10) UNSIGNED NOT NULL,
  `plate` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `category` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `plates`
--

INSERT INTO `plates` (`id_plate`, `plate`, `cost`, `category`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(39, 'Pollo', 4000, 'Empanadas', 2, '2018-06-15 21:51:13', '2018-06-15 21:51:13'),
(40, 'Carne', 4000, 'Empanadas', 2, '2018-06-15 21:51:13', '2018-06-15 21:51:13'),
(41, 'Hawallana', 4000, 'Empanadas', 2, '2018-06-15 21:51:13', '2018-06-15 21:51:13'),
(42, 'Cocacola', 3000, 'Bebidas', 2, '2018-06-15 21:51:13', '2018-06-15 21:51:13'),
(43, 'Jugo', 3000, 'Bebidas', 2, '2018-06-15 21:51:13', '2018-06-15 21:51:13'),
(46, 'asdasd', 23423, 'Hola', 1, '2018-08-21 16:21:48', '2018-08-21 16:21:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `polls`
--

CREATE TABLE `polls` (
  `id_poll` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `city` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `activities` text COLLATE utf8_unicode_ci NOT NULL,
  `observations` text COLLATE utf8_unicode_ci,
  `state` varchar(131) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `polls`
--

INSERT INTO `polls` (`id_poll`, `name`, `email`, `phone`, `city`, `activities`, `observations`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Sergio Hernandez', 'sergio.hernandez.goyeneche@gmail.com', 2147483647, '', 'Rafting, Buceo , Canyoning , ', NULL, '', '2018-01-15 20:22:18', '2018-01-15 20:22:18'),
(2, 'Sergio Hernandez', 'sergio.hernandez.goyeneche@gmail.com', 2147483647, 'Bogotá', 'Torrentismo, ', NULL, 'new', '2018-01-15 20:44:39', '2018-01-15 20:44:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `id_region` int(10) UNSIGNED NOT NULL,
  `region_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `sentence` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `regions`
--

INSERT INTO `regions` (`id_region`, `region_name`, `sentence`, `link_image`, `description`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Caribe', 'Frase de prueba', 'images/regions/Region_20180508151847895918555.jpg', '<h3>Regi&oacute;n Caribe,</h3>\r\n\r\n<p>&nbsp;Est&aacute; ubicada en la parte norte de Colombia y Am&eacute;rica del Sur. Limita al norte con el mar caribe, al que debe su nombre, al este con Venezuela,&nbsp;al sur con la regi&oacute;n Andina&nbsp;y al oeste con la&nbsp;<a href=\"https://es.wikipedia.org/wiki/Regi%C3%B3n_del_Pac%C3%ADfico_(Colombia)\" title=\"Región del Pacífico (Colombia)\">r</a>egi&oacute;n del Pac&iacute;fico. Sus principales centros urbanos son:</p>\r\n\r\n<p>Recuerda visitar:</p>\r\n\r\n<ul>\r\n	<li>Barranquilla</li>\r\n	<li>Cartagena de Indias</li>\r\n	<li>Soldead.</li>\r\n	<li>Santa Marta</li>\r\n	<li>Valledupar</li>\r\n	<li>Monter&iacute;a</li>\r\n	<li>Sincelejo</li>\r\n	<li>Valledupar</li>\r\n</ul>', 'caribe', 'inactivo', '2018-04-09 18:37:52', '2018-08-15 16:53:02'),
(2, 'Amazonia', '', '', 'required', 'amazonia', 'activo', '2018-05-07 18:07:09', '2018-05-07 18:07:09'),
(3, 'Antioquia y eje cafetero', '', '', 'required', 'antioquia-y-eje-cafetero', 'activo', '2018-05-07 18:07:36', '2018-05-07 18:07:36'),
(4, 'Centro', '', '', 'required', 'centro', 'activo', '2018-05-07 18:07:57', '2018-05-07 18:07:57'),
(5, 'Costa Caribe', '', '', 'required', 'costa-caribe', 'activo', '2018-05-07 18:08:15', '2018-05-07 18:08:15'),
(6, 'Costa pacífica', '', '', 'required', 'costa-pacifica', 'activo', '2018-05-07 18:09:18', '2018-05-07 18:09:18'),
(7, 'Llanos orientales', '', '', 'required', 'llanos-orientales', 'activo', '2018-05-07 18:09:36', '2018-05-07 18:09:36'),
(8, 'Suroccidente', '', '', 'required', 'suroccidente', 'activo', '2018-05-07 18:09:54', '2018-05-07 18:09:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region_images`
--

CREATE TABLE `region_images` (
  `id_region_image` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_region` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `region_images`
--

INSERT INTO `region_images` (`id_region_image`, `link_image`, `fk_region`, `created_at`, `updated_at`) VALUES
(1, 'images/regions/Region_20180613110428738985031.jpg', 1, '2018-06-13 16:04:28', '2018-06-13 16:04:28'),
(2, 'images/regions/Region_201806131104291271849480.jpg', 1, '2018-06-13 16:04:29', '2018-06-13 16:04:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporters`
--

CREATE TABLE `reporters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporter_password_resets`
--

CREATE TABLE `reporter_password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `report_reservations`
--

CREATE TABLE `report_reservations` (
  `id_report_reservation` int(10) UNSIGNED NOT NULL,
  `fk_report` int(10) UNSIGNED NOT NULL,
  `fk_reservation` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `report_reservations`
--

INSERT INTO `report_reservations` (`id_report_reservation`, `fk_report`, `fk_reservation`, `created_at`, `updated_at`) VALUES
(1, 3, 21, NULL, NULL),
(2, 3, 22, NULL, NULL),
(3, 3, 97, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reprograms`
--

CREATE TABLE `reprograms` (
  `id_reprogram` int(10) UNSIGNED NOT NULL,
  `fk_reservation` int(10) UNSIGNED NOT NULL,
  `previous_date` date NOT NULL,
  `previous_time` time NOT NULL,
  `new_date` date NOT NULL,
  `new_time` time NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `reprograms`
--

INSERT INTO `reprograms` (`id_reprogram`, `fk_reservation`, `previous_date`, `previous_time`, `new_date`, `new_time`, `reason`, `created_at`, `updated_at`) VALUES
(1, 5, '2018-08-09', '11:00:00', '0000-00-00', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:07:22', '2018-08-23 16:07:22'),
(2, 5, '0000-00-00', '11:00:00', '0000-00-00', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:19:12', '2018-08-23 16:19:12'),
(3, 5, '0000-00-00', '11:00:00', '0000-00-00', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:23:13', '2018-08-23 16:23:13'),
(4, 5, '2018-08-25', '11:00:00', '0000-00-00', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:24:46', '2018-08-23 16:24:46'),
(5, 5, '0000-00-00', '11:00:00', '0000-00-00', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:26:39', '2018-08-23 16:26:39'),
(6, 5, '0000-00-00', '11:00:00', '2018-08-23', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:32:37', '2018-08-23 16:32:37'),
(7, 5, '2018-08-23', '11:00:00', '2018-08-23', '11:00:00', 'Se me da la gana! :v', '2018-08-23 16:34:00', '2018-08-23 16:34:00'),
(8, 5, '2018-08-23', '11:00:00', '2018-08-30', '14:00:00', '23423423423 423 423', '2018-08-23 17:18:06', '2018-08-23 17:18:06'),
(9, 5, '2018-08-30', '14:00:00', '2018-08-30', '14:00:00', '23423423423 423 423', '2018-08-23 17:18:22', '2018-08-23 17:18:22'),
(10, 5, '2018-08-30', '14:00:00', '2018-08-30', '14:00:00', '23423423423 423 423', '2018-08-23 17:20:16', '2018-08-23 17:20:16'),
(11, 96, '2018-08-30', '11:00:00', '2018-08-29', '11:00:00', 'Gosihfgsabidu hsja sd', '2018-08-23 17:43:32', '2018-08-23 17:43:32'),
(12, 96, '2018-08-29', '11:00:00', '2018-09-12', '08:00:00', 'Se reprograma por que Yolo', '2018-08-23 18:18:29', '2018-08-23 18:18:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id_reservation` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_user` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `fk_order_product` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `calification` double(8,2) DEFAULT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `reservations`
--

INSERT INTO `reservations` (`id_reservation`, `fk_service`, `fk_user`, `date`, `time`, `state`, `cuantity`, `fk_order_product`, `comment`, `calification`, `paid`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-08-09', '13:00:00', 'iniciado', 1, 3, NULL, NULL, 0, '2018-07-25 18:17:26', '2018-07-25 18:17:26'),
(2, 1, 3, '2018-08-23', '09:00:00', 'iniciado', 2, 4, NULL, NULL, 0, '2018-07-25 18:17:26', '2018-07-25 18:17:26'),
(3, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 5, NULL, NULL, 0, '2018-07-25 20:21:47', '2018-08-23 20:37:37'),
(4, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 6, NULL, NULL, 0, '2018-07-25 20:22:19', '2018-08-23 20:37:36'),
(5, 1, 3, '2018-08-30', '14:00:00', 'pagado', 3, 7, NULL, NULL, 0, '2018-07-25 20:22:45', '2018-08-23 17:18:06'),
(6, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 8, NULL, NULL, 0, '2018-07-25 20:23:13', '2018-08-23 20:37:34'),
(7, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 9, NULL, NULL, 0, '2018-07-25 20:23:25', '2018-08-23 20:37:33'),
(8, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 10, NULL, NULL, 0, '2018-07-25 20:23:52', '2018-08-23 20:37:31'),
(9, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 11, NULL, NULL, 0, '2018-07-25 20:25:02', '2018-08-23 20:37:02'),
(10, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 13, NULL, NULL, 0, '2018-07-25 20:28:05', '2018-07-25 20:28:05'),
(11, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 14, NULL, NULL, 0, '2018-07-25 20:28:14', '2018-07-25 20:28:14'),
(12, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 15, NULL, NULL, 0, '2018-07-25 20:29:11', '2018-07-25 20:29:11'),
(13, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 16, NULL, NULL, 0, '2018-07-25 20:29:51', '2018-08-23 20:37:00'),
(14, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 17, NULL, NULL, 0, '2018-07-25 20:30:36', '2018-08-23 20:36:58'),
(15, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 18, NULL, NULL, 0, '2018-07-25 20:31:04', '2018-08-23 20:36:57'),
(16, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 19, NULL, NULL, 0, '2018-07-25 20:32:05', '2018-08-23 20:36:55'),
(17, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 20, NULL, NULL, 0, '2018-07-25 20:33:09', '2018-08-23 20:36:54'),
(18, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 21, NULL, NULL, 0, '2018-07-25 20:35:22', '2018-08-23 20:36:52'),
(19, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 22, NULL, NULL, 0, '2018-07-25 20:42:19', '2018-08-23 20:36:50'),
(20, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 23, NULL, NULL, 0, '2018-07-25 20:43:18', '2018-08-23 20:36:49'),
(21, 1, 3, '2018-08-09', '11:00:00', 'Realizada', 3, 24, NULL, NULL, 1, '2018-07-25 20:47:47', '2018-08-10 21:40:24'),
(22, 1, 3, '2018-08-09', '11:00:00', 'Calificada', 3, 25, 'Coool', 5.00, 1, '2018-07-25 20:51:10', '2018-08-10 21:40:24'),
(25, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 28, NULL, NULL, 0, '2018-07-25 21:07:49', '2018-07-25 21:07:49'),
(26, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 29, NULL, NULL, 0, '2018-07-25 21:08:57', '2018-07-25 21:08:57'),
(27, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 30, NULL, NULL, 0, '2018-07-25 21:10:58', '2018-07-25 21:10:58'),
(32, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 35, NULL, NULL, 0, '2018-07-25 21:36:23', '2018-07-25 21:36:23'),
(33, 1, 3, '2018-08-09', '11:00:00', 'iniciado', 3, 36, NULL, NULL, 0, '2018-07-25 21:36:49', '2018-07-25 21:36:49'),
(34, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 40, NULL, NULL, 0, '2018-07-26 14:54:27', '2018-07-26 14:54:27'),
(35, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 41, NULL, NULL, 0, '2018-07-26 15:02:55', '2018-07-26 15:02:55'),
(36, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 42, NULL, NULL, 0, '2018-07-26 15:03:20', '2018-07-26 15:03:20'),
(37, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 43, NULL, NULL, 0, '2018-07-26 15:05:34', '2018-07-26 15:05:34'),
(38, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 44, NULL, NULL, 0, '2018-07-26 15:06:24', '2018-07-26 15:06:24'),
(39, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 45, NULL, NULL, 0, '2018-07-26 15:48:55', '2018-07-26 15:48:55'),
(40, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 46, NULL, NULL, 0, '2018-07-26 15:58:49', '2018-07-26 15:58:49'),
(45, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 51, NULL, NULL, 0, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(46, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 52, NULL, NULL, 0, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(47, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 53, NULL, NULL, 0, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(48, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 54, NULL, NULL, 0, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(51, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 57, NULL, NULL, 0, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(52, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 58, NULL, NULL, 0, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(57, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 63, NULL, NULL, 0, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(58, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 64, NULL, NULL, 0, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(61, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 67, NULL, NULL, 0, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(62, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 68, NULL, NULL, 0, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(63, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 69, NULL, NULL, 0, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(64, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 70, NULL, NULL, 0, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(65, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 71, NULL, NULL, 0, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(66, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 72, NULL, NULL, 0, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(67, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 73, NULL, NULL, 0, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(68, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 74, NULL, NULL, 0, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(69, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 75, NULL, NULL, 0, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(70, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 76, NULL, NULL, 0, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(71, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 77, NULL, NULL, 0, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(72, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 78, NULL, NULL, 0, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(73, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 79, NULL, NULL, 0, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(74, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 80, NULL, NULL, 0, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(75, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 81, NULL, NULL, 0, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(76, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 82, NULL, NULL, 0, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(77, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 83, NULL, NULL, 0, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(78, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 84, NULL, NULL, 0, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(79, 6, 3, '2018-07-31', '14:00:00', 'iniciado', 1, 85, NULL, NULL, 0, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(80, 5, 3, '2018-07-31', '11:00:00', 'iniciado', 1, 86, NULL, NULL, 0, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(81, 6, 3, '2018-07-31', '14:00:00', 'pendiente', 1, 87, NULL, NULL, 0, '2018-07-26 20:47:16', '2018-07-26 20:47:27'),
(82, 5, 3, '2018-07-31', '11:00:00', 'pendiente', 1, 88, NULL, NULL, 0, '2018-07-26 20:47:16', '2018-07-26 20:47:27'),
(83, 6, 3, '2018-07-31', '14:00:00', 'pendiente', 1, 89, NULL, NULL, 0, '2018-07-26 20:48:50', '2018-07-26 20:48:51'),
(84, 5, 3, '2018-07-31', '11:00:00', 'pendiente', 1, 90, NULL, NULL, 0, '2018-07-26 20:48:50', '2018-07-26 20:48:51'),
(85, 1, 3, '2018-08-02', '10:00:00', 'pendiente', 1, 91, NULL, NULL, 0, '2018-07-30 15:05:56', '2018-07-30 15:06:01'),
(86, 1, 3, '2018-08-02', '10:00:00', 'pendiente', 1, 92, NULL, NULL, 0, '2018-07-30 15:08:43', '2018-07-30 15:08:47'),
(87, 1, 3, '2018-08-02', '10:00:00', 'pendiente', 1, 93, NULL, NULL, 0, '2018-07-30 15:11:33', '2018-07-30 15:11:35'),
(88, 1, 3, '2018-08-30', '10:00:00', 'pendiente', 1, 94, NULL, NULL, 0, '2018-07-30 15:15:31', '2018-07-30 15:15:38'),
(90, 1, 3, '2018-08-29', '11:00:00', 'pendiente', 1, 96, NULL, NULL, 0, '2018-07-30 17:45:30', '2018-07-30 17:45:34'),
(91, 1, 3, '2018-08-29', '11:00:00', 'pendiente', 1, 97, NULL, NULL, 0, '2018-07-30 17:52:29', '2018-07-30 17:52:31'),
(92, 5, 3, '2018-08-21', '14:00:00', 'pendiente', 1, 98, NULL, NULL, 0, '2018-07-30 18:03:17', '2018-07-30 18:03:24'),
(93, 5, 3, '2018-08-21', '14:00:00', 'pendiente', 1, 99, NULL, NULL, 0, '2018-07-30 19:47:28', '2018-07-30 19:47:33'),
(94, 5, 3, '2018-08-21', '14:00:00', 'iniciado', 1, 100, NULL, NULL, 0, '2018-07-30 19:48:55', '2018-07-30 19:48:55'),
(95, 5, 3, '2018-08-21', '14:00:00', 'pendiente', 1, 101, NULL, NULL, 0, '2018-07-30 19:49:07', '2018-07-30 19:49:14'),
(96, 5, 3, '2018-09-12', '08:00:00', 'pagado', 1, 102, NULL, NULL, 0, '2018-07-30 21:34:01', '2018-08-23 18:18:29'),
(97, 1, 3, '2018-08-22', '09:00:00', 'Realizada', 4, 103, NULL, NULL, 1, '2018-08-03 17:48:32', '2018-08-10 21:40:24'),
(98, 1, 3, '2018-08-29', '09:00:00', 'pendiente', 1, 105, NULL, NULL, 0, '2018-08-13 19:42:57', '2018-08-13 19:43:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id_room` int(10) UNSIGNED NOT NULL,
  `room` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `capacity` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `bed` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `suite` tinyint(1) NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rooms`
--

INSERT INTO `rooms` (`id_room`, `room`, `cuantity`, `capacity`, `bed`, `suite`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(23, '123123', 123, '123', '123', 1, 4, '2018-08-21 20:15:16', '2018-08-21 20:15:16'),
(24, '123', 1231, '12312', '123', 1, 4, '2018-08-21 20:15:16', '2018-08-21 20:15:16'),
(25, 'hola', 1, 'ni', 'mames', 0, 4, '2018-08-21 20:15:16', '2018-08-21 20:15:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules`
--

CREATE TABLE `schedules` (
  `id_schedule` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_receso` time DEFAULT NULL,
  `tiempo_receso` int(11) DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `schedules`
--

INSERT INTO `schedules` (`id_schedule`, `fk_service`, `day`, `hora_inicio`, `hora_receso`, `tiempo_receso`, `hora_final`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 'Lunes', '08:00:00', '12:00:00', 4, '18:00:00', 'active', '2017-10-23 15:57:47', '2018-01-11 21:44:13'),
(2, 1, 'Miercoles', '08:00:00', '12:00:00', 2, '18:00:00', 'active', '2017-10-23 15:57:47', '2017-10-23 15:57:57'),
(3, 1, 'Jueves', '08:00:00', '12:00:00', 1, '18:00:00', 'active', '2017-10-23 15:57:47', '2017-12-04 16:54:27'),
(8, 1, 'Martes', NULL, NULL, NULL, NULL, 'active', '2017-12-04 19:42:32', '2017-12-04 19:42:32'),
(9, 1, 'Viernes', NULL, NULL, NULL, NULL, 'active', '2017-12-04 19:42:33', '2017-12-04 19:42:33'),
(10, 2, 'Miercoles', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-04-23 18:10:01', '2018-04-23 18:22:54'),
(11, 3, 'Lunes', NULL, NULL, NULL, NULL, 'active', '2018-04-23 19:56:09', '2018-04-23 19:56:09'),
(12, 3, 'Jueves', NULL, NULL, NULL, NULL, 'active', '2018-04-23 19:56:09', '2018-04-23 19:56:09'),
(13, 4, 'Lunes', NULL, NULL, NULL, NULL, 'active', '2018-05-16 21:43:01', '2018-05-16 21:43:01'),
(14, 4, 'Martes', NULL, NULL, NULL, NULL, 'active', '2018-05-16 21:43:01', '2018-05-16 21:43:01'),
(15, 4, 'Miercoles', NULL, NULL, NULL, NULL, 'active', '2018-05-16 21:43:01', '2018-05-16 21:43:01'),
(16, 4, 'Sábado', NULL, NULL, NULL, NULL, 'active', '2018-05-16 21:43:01', '2018-05-16 21:43:01'),
(17, 5, 'Lunes', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-05-24 21:50:57', '2018-06-14 21:41:00'),
(18, 5, 'Martes', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-05-24 21:50:57', '2018-06-14 21:41:00'),
(19, 5, 'Miercoles', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-05-24 21:50:57', '2018-06-14 21:41:00'),
(20, 5, 'Jueves', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-05-24 21:50:57', '2018-06-14 21:41:00'),
(21, 6, 'Martes', '08:00:00', '12:00:00', 2, '18:00:00', 'active', '2018-06-14 21:40:15', '2018-06-14 21:40:25'),
(22, 6, 'Miercoles', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-06-14 21:40:15', '2018-06-14 21:40:26'),
(23, 6, 'Jueves', '08:00:00', '12:00:00', 0, '18:00:00', 'active', '2018-06-14 21:40:15', '2018-06-14 21:40:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id_service` int(10) UNSIGNED NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_ico` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_class` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_url` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fk_service_category` int(10) UNSIGNED NOT NULL,
  `fk_ecosystem_category` int(10) UNSIGNED NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`service`, `type_service`, `description`, `image`, `map_ico`, `icon_class`, `video_url`, `fk_service_category`, `fk_ecosystem_category`, `slug`, `created_at`, `updated_at`) 
VALUES
('Parapente', 'Alto impacto', '<p>Su origen se remonta a la pr&aacute;ctica del paracaidismo en Francia hacia la d&eacute;cada de 1980. El aprovechamiento de las laderas para alzar vuelo y practicar el aterrizaje, dio forma a esta nueva modalidad de surcar el cielo, que no hace uso de maquinaria ni de combustible, sino de un planeador ultraligero impulsado por la fuerza del aire y dirigido manualmente.</p>', 'images/Service/Service_20180828163133821127548.jpg', '', 'icon_set_2_icon-117', '5kCAKZ2lRiM', 1, 3, 'parapente', '2017-10-23 15:55:27', '2018-06-22 19:59:07'),
( 'Ala Delta', 'Alto impacto', '<p>
Tomando provecho de las corrientes de aire, el Ala Delta es un deporte aéreo que reta al piloto a volar y a realizar acrobacias por un largo periodo. El ala delta, herramienta empleada para realizar estos vuelos sin motor, se rige con los mismos principios de un avión y se compone de una vela adherida a una estructura de aluminio en la que el piloto se ubica en el centro con la ayuda de un arnés.</p>
<h4>Elementos</h4>
<p>Para este deporte es necesario el planeador mencionado y el uso del casco, paracaídas de emergencia, doble cuelgue de seguridad y ropa adecuada. Hay dos tipos de ala delta: las flexibles, en las que el piloto ejerce su propio peso, y las rígidas, con superficies aerodinámicas móviles.</p>
<h4>Ten en cuenta</h4>
<p>La práctica del Ala Delta requiere de un espacio abierto y amplio, un sitio inclinado sin árboles ni otros obstáculos.</p>', 'images/Service/Service_20180827130344872664758.jpg', '', 'icon_set_2_icon-117', '4WhJhM1APY8', 1, 3, 'ala-delta', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Paracaidismo', 'Alto impacto', '<p>Este deporte extremo reconocido por brindar una experiencia llena de adrenalina, consiste en el salto desde un avi&oacute;n o helic&oacute;ptero y el uso de un paraca&iacute;das para suavizar el descenso. Son varias sus modalidades y, seg&uacute;n la que se practique, la cantidad de participantes puede cambiar.</p>

<h4>Elementos</h4>

<p>Cada saltador requiere un paraca&iacute;das personal y otro de reserva, un arn&eacute;s inm&oacute;vil, velcros y una cinta de piloto. El uso del casco es opcional. Una vez abierto el paraca&iacute;das, y en caso de que el practicante est&eacute; solo, debe controlar la direcci&oacute;n y la velocidad con las cuerdas de control.</p>

<h4>Ten en cuenta</h4>

<p>El salto t&aacute;ndem es la opci&oacute;n perfecta para los principiantes: el instructor salta unido al aprendiz y es quien dirige el descenso, abriendo el paraca&iacute;das en el momento justo para aterrizar.</p>', 'images/Service/Service_20180828165352266110687.jpg', '', 'icon_set_2_icon-117', 's0TNyyyDPHg', 1, 3, 'paracaidismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Vuelo en Globo ', 'Medio impacto', 'p>Si el objetivo es contemplar con calma el paisaje desde la altura, los paseos en globos aerost&aacute;ticos son la mejor opci&oacute;n. Esta actividad destinada a fines recreativos, permite el desplazamiento de un lugar a otro. A pesar de que algunos tipos de globo controlan su elevaci&oacute;n, la direcci&oacute;n depende de las corrientes de aire.</p>

<h4>Elementos</h4>

<p>Con el fin de elevar el globo, el aire que est&aacute; dentro de &eacute;l se calienta por medio de un quemador de gas propano que enciende el piloto. Al momento de descender, el piloto tira un cable para abrir un agujero en la parte superior del globo, lo que permite que el aire caliente escape.</p>

<h4>Ten en cuenta</h4>

<p>Se recomienda no llevar equipaje, ya que suma peso al globo. As&iacute; mismo, la indicaci&oacute;n principal es seguir cada una de las instrucciones y recomendaciones de seguridad dadas por el gu&iacute;a.</p>', 'images/Service/Service_201808281713481986053736.jpg', '', 'icon_set_2_icon-117', 'dxbKqyuBbD4', 1, 3, 'globo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Ultraliviano ', 'Alto impacto', '<p>Los vuelos en aviones ultralivianos son una buena alternativa para una aventura a&eacute;rea a bajo costo. Estos veh&iacute;culos se caracterizan por su vuelo bajo, su motor de 65 u 86 caballos de fuerza y por el consumo m&iacute;nimo de combustible. Se usan exclusivamente para recorridos cortos y tienen cupo para dos personas, el piloto y su acompa&ntilde;ante.</p>

<h4>Elementos</h4>

<p>El equipo b&aacute;sico consta de una aeronave ultraliviana. El piloto y el acompa&ntilde;ante deben usar gafas de protecci&oacute;n, guantes y casco.</p>

<h4>Ten en cuenta</h4>

<p>En caso de tener la licencia de vuelo otorgada por la Aeron&aacute;utica Civil de Colombia, es posible tomar un curso de m&aacute;ximo tres horas para ser piloto de estos aviones.</p>', 'images/Service/Service_201808281721342035602332.jpg', '', 'icon_set_2_icon-117', 'WwfJNP2VVno', 1, 3, 'ultraliviano', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Torrentismo/Barranquismo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'torrentismo-barranquismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Rafting', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'rafting', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Buceo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'buceo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Pesca deportiva', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'pesca-deportiva', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Canotaje/Balsaje/Bote/Navegación Naturalista', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'canotaje-balsaje-bote-navegacion-naturalista', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Kayaking', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'kayaking', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Jet Sky', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'jet-sky', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Surf', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'surf', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Tirolinea/Tirolesa/Canopy', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'tirolinea-tirolesa-canopy', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Tirolinea/Tirolesa/Canopy', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'tirolinea-tirolesa-canopy', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Rápel', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'Rapel', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Senderismo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'senderismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Alta montaña/Montañismo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'alta-montana-montanismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Bungee Jumping', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'bungee-jumping', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Ciclomontañismo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'ciclomontanismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Escalada', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'escalada', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Espeleísmo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'espeleismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Excursiones 4x4', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'excursiones4x4', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Cabalgata', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'cabalgata', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Ciclismo de ruta', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'ciclismo-de-ruta', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Enduro', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'enduro', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Bicicross', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'bicicross', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Acampar/Camping/Campismo', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'acampar-camping-campismo', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Avistamiento de aves', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'avistamiento-de-aves', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Avistamiento de ballenas', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 3, 'avistamiento-de-ballenas', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Excursiones educacionales y científicas', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'excursiones-educacionales-y-cientificas', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Observación de Flora y/o Fauna', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'observacion-de-flora-y-o-fauna', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Observación de fósiles', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'observacion-de-fosiles', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Observación sideral (estrellas)', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'observacion-sideral-estrellas', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Safari fotográfico', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 2, 'safari-fotografico', '2017-10-23 15:55:28', '2017-10-23 15:55:28'),
('Snorkeling', 'Alto impacto', '', NULL, '', 'icon_set_2_icon-117', NULL, 1, 1, 'snorkeling', '2017-10-23 15:55:28', '2017-10-23 15:55:28')
--
-- Estructura de tabla para la tabla `service_categories`
--

CREATE TABLE `service_categories` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `service_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `service_categories`
--

INSERT INTO `service_categories` (`id_category`, `service_category`, `created_at`, `updated_at`) VALUES
(1, 'Aventura', '2017-10-23 15:55:27', '2018-05-22 16:40:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishments`
--

CREATE TABLE `service_establishments` (
  `id_service_establishment` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_establishments`
--

INSERT INTO `service_establishments` (`id_service_establishment`, `service_name`, `created_at`, `updated_at`) VALUES
(2, 'Wifi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Plasma Tv', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Carta de vinos y licores', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Eventos especiales', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Cenas V.I.P.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Salón de espera', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Música en vivo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Música ambiental', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Acceso para sillas de ruedas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Opciones sin gluten', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Parqueadero', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Valet parking', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Traductor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Caja de seguridad', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Catering para eventos', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Pet Friendly', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Entrega a domicilio', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Acepta tarjetas de crédito', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Opciones veganas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Apto para vegetarianos', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'No se permiten mascotas   ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Zona de fumadores', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_establishment_pivots`
--

CREATE TABLE `service_establishment_pivots` (
  `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_establishment` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_establishment_pivots`
--

INSERT INTO `service_establishment_pivots` (`id_service_establishment_pivot`, `fk_service`, `fk_establishment`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, NULL),
(2, 7, 1, NULL, NULL),
(3, 14, 1, NULL, NULL),
(4, 18, 1, NULL, NULL),
(5, 20, 1, NULL, NULL),
(6, 2, 2, NULL, NULL),
(7, 3, 2, NULL, NULL),
(8, 4, 2, NULL, NULL),
(9, 21, 2, NULL, NULL),
(10, 22, 2, NULL, NULL),
(11, 23, 2, NULL, NULL),
(12, 3, 3, NULL, NULL),
(13, 11, 3, NULL, NULL),
(14, 14, 3, NULL, NULL),
(15, 19, 3, NULL, NULL),
(16, 3, 4, NULL, NULL),
(17, 11, 4, NULL, NULL),
(18, 14, 4, NULL, NULL),
(19, 19, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_items`
--

CREATE TABLE `service_items` (
  `id_service_item` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_items`
--

INSERT INTO `service_items` (`id_service_item`, `item_name`, `cost`, `description`, `fk_service`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Restaurante', 50000, '<p>aihsdbaisd ahsbd aiusibd iajsda jaosiud bajkslkd laonjs d asdas d</p>', 1, 'activo', '2018-04-20 16:45:16', '2018-04-20 16:45:16'),
(2, 'Restaurante', 50000, '<p>aihsdbaisd ahsbd aiusibd iajsda jaosiud bajkslkd laonjs d asdas d</p>', 1, 'inactivo', '2018-04-20 16:45:40', '2018-04-20 16:45:44'),
(3, 'BUSES', 50000, '<p>ASKDJBAKSDA SDAS DASD ASDA</p>', 2, 'inactivo', '2018-04-23 18:22:42', '2018-04-23 18:23:00'),
(4, 'Transporte', 20000, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.</p>', 5, 'activo', '2018-06-20 19:28:28', '2018-06-20 20:08:22'),
(5, 'Almuercito', 10000, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt placeat nihil odit libero mollitia vel dicta possimus accusantium facilis atque ut repellendus quisquam reprehenderit debitis at, recusandae maxime earum aspernatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis vel consequatur vitae, cum nobis quo voluptatum quam, amet eveniet quod perferendis, provident nostrum delectus dolorum sint commodi sit odio cupiditate?</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt placeat nihil odit libero mollitia vel dicta possimus accusantium facilis atque ut repellendus quisquam reprehenderit debitis at, recusandae maxime earum aspernatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis vel consequatur vitae, cum nobis quo voluptatum quam, amet eveniet quod perferendis, provident nostrum delectus dolorum sint commodi sit odio cupiditate?</p>', 5, 'activo', '2018-06-21 18:13:11', '2018-06-21 18:13:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_operators`
--

CREATE TABLE `service_operators` (
  `id_service_operator` int(10) UNSIGNED NOT NULL,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `fk_operator` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `video` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requisites` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `policies` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_municipality` int(11) NOT NULL,
  `fk_interest_site` int(11) DEFAULT NULL,
  `slug` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Inhabilitado',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `service_operators`
--

INSERT INTO `service_operators` (`id_service_operator`, `fk_service`, `fk_operator`, `service_name`, `cost`, `address`, `latitude`, `longitude`, `capacity`, `duration`, `video`, `requisites`, `description`, `policies`, `fk_municipality`, `fk_interest_site`, `slug`, `state`, `created_at`, `updated_at`) VALUES
(1, 13, 1, 'Mamalo! :vx2', '130000', 'cra 147 # 142 50, cra 142#134-33', '', '', 4, 1, '', '<p>&sect; Mapa: Las personas que van en carro particular siguiendo las indicaciones del mapa, una vez lleguen al letrero SENDERO PE&Ntilde;AS BLANCAS, parar en ese punto y dejar el carro al pie de la carretera; y subir caminando por la carretera donde se encuentra el letrero. &sect; ya que en el sitio de vuelo no contamos con servicio de restaurante, sugerimos llevar refrigerio y/o almuerzo, tambi&eacute;n se pueden pedir domicilios a Tocancipa. &sect; Muchos de nuestros clientes llevan carpa para resguardarse del frio o la lluvia &sect; Como recomendaci&oacute;n especial, el sitio de vuelo se encuentra a 3.115 metros de altura por lo tanto es importante estar muy abrigados. (Guantes al momento de volar son muy &uacute;tiles)</p>', '<p>Vive la AVENTURA con M&iacute;nimo Riesgo -Pilotos Certificados por la federaci&oacute;n Colombiana de Deportes A&eacute;reos para volar con pasajero. -Zona de Vuelo autorizada por la Aeronautica Civil para desarrollar la actividad. -Carretera hasta la zona de vuelo. -Sendero Ecol&oacute;gico demarcado con vegetaci&oacute;n nativa ideal para realizar una caminata. Nota importante: Nos permitimos aclarar que la v&iacute;a para acceder al sitio de vuelo desde el municipio de Tocancipa es destapada (trocha); por lo tanto se sugiere no utilizar autom&oacute;viles para llegar al sitio de vuelo. El valor del transporte desde Tocancipa hasta el sitio de vuelo tiene un costo de $60.000 ida y vuelta; valor que debe ser cancelado por el n&uacute;mero de personas que se transporten en el veh&iacute;culo particular, (Max. 4 personas). Este valor del transporte es para las personas que no van en carro particular, y opcional para las personas que no quieran llevar su autom&oacute;vil hasta el sitio de vuelo. PD: EL VALOR DE TRANSPORTE NO PERSONA</p>', '', 1, 1, 'nomamesquecabron', 'Activo', '2017-10-23 15:56:25', '2018-08-21 16:51:00'),
(2, 1, 1, '', '40000', 'cra 147 # 142 50, cra 142#134-33', '', '', 2, 3, '', '<p>&nbsp;asdasd asd asd&nbsp;</p>', '<p>asda sdas dasd&nbsp;</p>', '', 4, 0, 'cra-147-142-50-cra-142-134-33', 'inactivo', '2018-04-23 18:10:01', '2018-08-17 21:07:36'),
(3, 1, 1, '', '50000', 'cra 147 # 142 50, cra 142#134-33', '', '', 4, 2, '', '<p>as dasd asd asd asd as d</p>', '<p>asdas dasd asd asd asdasd&nbsp;</p>', '', 1, 0, 'cra-147-142-50-cra-142-134-33-1', 'inactivo', '2018-04-23 19:56:09', '2018-08-17 21:07:36'),
(4, 3, 1, 'Rafting para 2 personas así bien pinche mamón!', '40000', 'cra 147 # 142 50, cra 142#134-33', '', '', 4, 4, '', '<p>&nbsp;asda sdas dasd asd asd asd asd asd asd asd asd asd asdasd asd asd&nbsp;</p>', '<p>asd asda sdasd asd asd a</p>', '', 4, NULL, 'cra-147-142-50-cra-142-134-33-2', 'inactivo', '2018-05-16 21:43:01', '2018-08-17 21:07:36'),
(5, 2, 5, 'No mames!', '50500', 'cra 147 # 142 50, cra 142#134-33', '4.747999', '-74.11848680000003', 4, 3, '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi nostrum inventore architecto quibusdam? Magnam odio nobis, deleniti est et, quod ut iste eligendi ex id, laboriosam dolores, obcaecati molestias omnis.</p>', '<div class=\"col-md-6 col-sm-6\">\r\n<ul>\r\n	<li>Mapa: Las personas que van en carro particular siguiendo las indicaciones del mapa, una vez lleguen al letrero SENDERO PE&Ntilde;AS BLANCAS, parar en ese punto y dejar el carro al pie de la carretera; y subir caminando por la carretera donde se encuentra el letrero.</li>\r\n	<li>ya que en el sitio de vuelo no contamos con servicio de restaurante, sugerimos llevar refrigerio y/o almuerzo, tambi&eacute;n se pueden pedir domicilios a Tocancipa.</li>\r\n	<li>Muchos de nuestros clientes llevan carpa para resguardarse del frio o la lluvia.</li>\r\n	<li>Como recomendaci&oacute;n especial, el sitio de vuelo se encuentra a 3.115 metros de altura por lo tanto es importante estar muy abrigados. (Guantes al momento de volar son muy &uacute;tiles)</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<ul>\r\n	<li>La actividad est&aacute; sujeta a condiciones clim&aacute;ticas y puede ser modificada</li>\r\n	<li>Los menores de edad deben asistir acompa&ntilde;ados por un familiar (padre, madre o tutor).</li>\r\n	<li>Verificar disponibilidad antes de realizar la compra.</li>\r\n	<li>Los cambios y cancelaciones dependen de la pol&iacute;tica interna de cada actividad y proveedor</li>\r\n	<li>Una vez realizada la compra, recibir&aacute;s los datos del operador para que coordines tu actividad.</li>\r\n</ul>\r\n</div>', 1, NULL, 'cra-147-142-50-cra-142-134-33-3', 'Activo', '2018-05-24 21:50:57', '2018-06-21 20:43:58'),
(6, 10, 5, 'htyjgbh y kjbn .', '5000', 'cra 147 # 142 50, cra 142#134-33', '', '', 5, 4, '', '<p>utyrc i7tirtcvkuho86goy g7tf65f7uyu</p>', '<p>ftvbku txcvyicytvyiuvbo8y oilub pyg iybuyvukyutv buhvbkiuy buby uyb uibliug</p>', '', 1, NULL, 'cra-147-142-50-cra-142-134-33-4', 'Activo', '2018-06-14 21:40:15', '2018-06-14 21:40:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_pictures`
--

CREATE TABLE `service_pictures` (
  `id_picture` int(10) UNSIGNED NOT NULL,
  `link_image` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `fk_service` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `service_pictures`
--

INSERT INTO `service_pictures` (`id_picture`, `link_image`, `description`, `fk_service`, `created_at`, `updated_at`) VALUES
(4, 'images/services/Service_201710311216164272.jpg', NULL, 1, '2017-10-31 17:16:16', '2017-10-31 17:16:16'),
(6, 'images/services/Service_2017103112161720142.jpg', NULL, 1, '2017-10-31 17:16:17', '2017-10-31 17:16:17'),
(7, 'images/services/Service_2017103114133313391.jpg', NULL, 1, '2017-10-31 19:13:33', '2017-10-31 19:13:33'),
(9, 'images/services/Service_20180619152644823356470.jpg', 'No mames', 5, '2018-06-19 20:26:44', '2018-06-19 20:26:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping_orders`
--

CREATE TABLE `shipping_orders` (
  `orders_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `type_dni` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dni` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone_2` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `shipping_orders`
--

INSERT INTO `shipping_orders` (`orders_id`, `email`, `type_dni`, `dni`, `first_name`, `last_name`, `direccion`, `country`, `region`, `city`, `phone`, `phone_2`, `created_at`, `updated_at`) VALUES
(5, 'al4cdeveloper@gmail.com', '', '234234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:34:01', '2018-07-18 16:34:01'),
(6, 'al4cdeveloper@gmail.com', '', '234234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:34:30', '2018-07-18 16:34:30'),
(7, 'al4cdeveloper@gmail.com', '', '234234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:35:29', '2018-07-18 16:35:29'),
(8, 'al4cdeveloper@gmail.com', '', '234234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:36:50', '2018-07-18 16:36:50'),
(9, 'al4cdeveloper@gmail.com', '', '234234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:37:28', '2018-07-18 16:37:28'),
(10, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:40:35', '2018-07-18 16:40:35'),
(11, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:41:19', '2018-07-18 16:41:19'),
(12, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:42:33', '2018-07-18 16:42:33'),
(13, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:43:09', '2018-07-18 16:43:09'),
(14, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Antioquia', NULL, '3203233082', NULL, '2018-07-18 16:43:51', '2018-07-18 16:43:51'),
(15, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:44:26', '2018-07-18 16:44:26'),
(16, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:45:52', '2018-07-18 16:45:52'),
(17, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:47:12', '2018-07-18 16:47:12'),
(18, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:48:08', '2018-07-18 16:48:08'),
(19, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:52:13', '2018-07-18 16:52:13'),
(20, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 16:59:55', '2018-07-18 16:59:55'),
(21, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 17:00:07', '2018-07-18 17:00:07'),
(22, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 17:00:16', '2018-07-18 17:00:16'),
(23, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 17:00:26', '2018-07-18 17:00:26'),
(24, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Antioquia', NULL, '3203233082', NULL, '2018-07-18 17:00:54', '2018-07-18 17:00:54'),
(25, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Caldas', NULL, '3203233082', NULL, '2018-07-18 17:01:52', '2018-07-18 17:01:52'),
(26, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 17:54:43', '2018-07-18 17:54:43'),
(27, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 17:55:20', '2018-07-18 17:55:20'),
(28, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 17:56:03', '2018-07-18 17:56:03'),
(29, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 17:56:48', '2018-07-18 17:56:48'),
(30, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 17:58:11', '2018-07-18 17:58:11'),
(31, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 18:02:44', '2018-07-18 18:02:44'),
(32, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 18:08:31', '2018-07-18 18:08:31'),
(33, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Boyaca', NULL, '3203233082', NULL, '2018-07-18 18:09:55', '2018-07-18 18:09:55'),
(34, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'Colombia', 'Bogota', NULL, '3203233082', NULL, '2018-07-18 18:26:21', '2018-07-18 18:26:21'),
(37, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:01:18', '2018-07-23 19:01:18'),
(38, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:02:49', '2018-07-23 19:02:49'),
(39, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:04:37', '2018-07-23 19:04:37'),
(40, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:05:30', '2018-07-23 19:05:30'),
(41, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:05:43', '2018-07-23 19:05:43'),
(42, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:06:07', '2018-07-23 19:06:07'),
(43, 'al4cdeveloper@gmail.com', '', '5415668464654', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:06:47', '2018-07-23 19:06:47'),
(44, 'sergio.hernandez.goyeneche@gmail.com', '', '123123', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:08:12', '2018-07-23 19:08:12'),
(45, 'sergio.hernandez.goyeneche@gmail.com', '', '123123', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:13:06', '2018-07-23 19:13:06'),
(46, 'sergio.hernandez.goyeneche@gmail.com', '', '123123', 'Sergio', 'Hernandez', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 19:14:07', '2018-07-23 19:14:07'),
(47, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:15:55', '2018-07-23 20:15:55'),
(48, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:17:11', '2018-07-23 20:17:11'),
(49, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:37:43', '2018-07-23 20:37:43'),
(50, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:42:54', '2018-07-23 20:42:54'),
(51, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:43:04', '2018-07-23 20:43:04'),
(52, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:43:23', '2018-07-23 20:43:23'),
(53, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:44:26', '2018-07-23 20:44:26'),
(54, 'sergio.hernandez.goyenech2323423e@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:48:41', '2018-07-23 20:48:41'),
(55, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 20:50:10', '2018-07-23 20:50:10'),
(56, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 21:11:29', '2018-07-23 21:11:29'),
(57, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 21:11:49', '2018-07-23 21:11:49'),
(58, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 21:15:34', '2018-07-23 21:15:34'),
(59, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:12:36', '2018-07-23 22:12:36'),
(60, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:12:52', '2018-07-23 22:12:52'),
(61, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:14:56', '2018-07-23 22:14:56'),
(62, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:16:02', '2018-07-23 22:16:02'),
(63, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:16:13', '2018-07-23 22:16:13'),
(64, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:16:14', '2018-07-23 22:16:14'),
(65, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:16:45', '2018-07-23 22:16:45'),
(66, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:17:55', '2018-07-23 22:17:55'),
(67, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:19:24', '2018-07-23 22:19:24'),
(68, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:19:59', '2018-07-23 22:19:59'),
(69, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:20:41', '2018-07-23 22:20:41'),
(70, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:20:59', '2018-07-23 22:20:59'),
(71, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:28:54', '2018-07-23 22:28:54'),
(72, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio', 'Hernandez', '123123 123 12 31', 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', '3203233082', NULL, '2018-07-23 22:29:08', '2018-07-23 22:29:08'),
(73, 'al4cdeveloper4567@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 16:17:09', '2018-07-24 16:17:09'),
(74, 'al4cdeveloper4567@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 16:26:57', '2018-07-24 16:26:57'),
(75, 'al4cdeveloper4567@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 16:27:29', '2018-07-24 16:27:29'),
(76, 'al4cdeveloper4567@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 16:27:55', '2018-07-24 16:27:55'),
(77, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 17:57:49', '2018-07-24 17:57:49'),
(78, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 17:58:56', '2018-07-24 17:58:56'),
(79, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 17:59:07', '2018-07-24 17:59:07'),
(80, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 17:59:20', '2018-07-24 17:59:20'),
(81, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 17:59:38', '2018-07-24 17:59:38'),
(82, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 18:00:07', '2018-07-24 18:00:07'),
(83, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 18:00:16', '2018-07-24 18:00:16'),
(84, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 18:00:36', '2018-07-24 18:00:36'),
(85, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 18:01:03', '2018-07-24 18:01:03'),
(86, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 18:05:58', '2018-07-24 18:05:58'),
(87, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 19:53:35', '2018-07-24 19:53:35'),
(88, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 19:54:24', '2018-07-24 19:54:24'),
(89, 'al4cdeveloper1@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 19:55:19', '2018-07-24 19:55:19'),
(90, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 19:56:53', '2018-07-24 19:56:53'),
(91, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 19:58:07', '2018-07-24 19:58:07'),
(92, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:00:19', '2018-07-24 20:00:19'),
(93, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:08:04', '2018-07-24 20:08:04'),
(94, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:13:41', '2018-07-24 20:13:41'),
(95, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:13:59', '2018-07-24 20:13:59'),
(96, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:14:20', '2018-07-24 20:14:20'),
(97, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:16:53', '2018-07-24 20:16:53'),
(98, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:17:59', '2018-07-24 20:17:59'),
(99, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:19:32', '2018-07-24 20:19:32'),
(100, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:19:52', '2018-07-24 20:19:52'),
(101, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:20:36', '2018-07-24 20:20:36'),
(102, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:21:54', '2018-07-24 20:21:54'),
(103, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:22:11', '2018-07-24 20:22:11'),
(104, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:24:08', '2018-07-24 20:24:08'),
(105, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:24:33', '2018-07-24 20:24:33'),
(106, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:25:42', '2018-07-24 20:25:42'),
(107, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:26:38', '2018-07-24 20:26:38'),
(108, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:46:46', '2018-07-24 20:46:46'),
(109, 'al4cdeveloper@gmail.com', '', 'qweqweqwe', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-24 20:47:24', '2018-07-24 20:47:24'),
(110, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 15:28:46', '2018-07-25 15:28:46'),
(111, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 15:51:40', '2018-07-25 15:51:40'),
(112, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 15:52:13', '2018-07-25 15:52:13'),
(113, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 15:54:29', '2018-07-25 15:54:29'),
(114, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 16:42:46', '2018-07-25 16:42:46'),
(115, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 16:43:43', '2018-07-25 16:43:43'),
(116, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 16:48:58', '2018-07-25 16:48:58'),
(117, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:01:05', '2018-07-25 17:01:05'),
(118, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:01:21', '2018-07-25 17:01:21'),
(119, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:01:33', '2018-07-25 17:01:33'),
(120, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:01:45', '2018-07-25 17:01:45'),
(121, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:01:57', '2018-07-25 17:01:57'),
(122, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:12:33', '2018-07-25 17:12:33'),
(123, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:13:51', '2018-07-25 17:13:51'),
(124, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 17:16:34', '2018-07-25 17:16:34'),
(125, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 18:15:48', '2018-07-25 18:15:48'),
(126, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 18:16:22', '2018-07-25 18:16:22'),
(127, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 18:16:52', '2018-07-25 18:16:52'),
(128, 'al4cdeveloper@gmail.com', '', '2434', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 18:17:26', '2018-07-25 18:17:26'),
(129, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:21:47', '2018-07-25 20:21:47'),
(130, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:22:19', '2018-07-25 20:22:19'),
(131, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:22:45', '2018-07-25 20:22:45'),
(132, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:23:13', '2018-07-25 20:23:13'),
(133, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:23:25', '2018-07-25 20:23:25'),
(134, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:23:52', '2018-07-25 20:23:52'),
(135, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:25:02', '2018-07-25 20:25:02'),
(136, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:26:55', '2018-07-25 20:26:55'),
(137, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:27:20', '2018-07-25 20:27:20'),
(138, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:28:05', '2018-07-25 20:28:05'),
(139, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:28:14', '2018-07-25 20:28:14'),
(140, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:29:11', '2018-07-25 20:29:11'),
(141, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:29:51', '2018-07-25 20:29:51'),
(142, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:30:36', '2018-07-25 20:30:36'),
(143, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:31:04', '2018-07-25 20:31:04'),
(144, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:32:05', '2018-07-25 20:32:05'),
(145, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:33:09', '2018-07-25 20:33:09'),
(146, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:35:22', '2018-07-25 20:35:22'),
(147, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:42:19', '2018-07-25 20:42:19'),
(148, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:43:18', '2018-07-25 20:43:18'),
(149, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:47:47', '2018-07-25 20:47:47'),
(150, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 20:51:10', '2018-07-25 20:51:10'),
(151, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:03:06', '2018-07-25 21:03:06'),
(152, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:04:06', '2018-07-25 21:04:06'),
(153, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:07:49', '2018-07-25 21:07:49'),
(154, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:08:57', '2018-07-25 21:08:57'),
(155, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:10:58', '2018-07-25 21:10:58'),
(156, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:11:21', '2018-07-25 21:11:21'),
(157, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:34:13', '2018-07-25 21:34:13'),
(158, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:34:29', '2018-07-25 21:34:29'),
(159, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:35:54', '2018-07-25 21:35:54'),
(160, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:36:23', '2018-07-25 21:36:23'),
(161, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:36:49', '2018-07-25 21:36:49'),
(162, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:39:35', '2018-07-25 21:39:35'),
(163, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:40:18', '2018-07-25 21:40:18'),
(164, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-25 21:42:15', '2018-07-25 21:42:15'),
(165, 'al4cdeveloper@gmail.com', '', '1019191911', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 14:54:27', '2018-07-26 14:54:27'),
(166, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 15:02:55', '2018-07-26 15:02:55'),
(167, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 15:03:20', '2018-07-26 15:03:20'),
(168, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 15:05:34', '2018-07-26 15:05:34'),
(169, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 15:06:24', '2018-07-26 15:06:24'),
(170, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 15:48:55', '2018-07-26 15:48:55'),
(171, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 15:58:49', '2018-07-26 15:58:49'),
(172, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 16:10:35', '2018-07-26 16:10:35'),
(173, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 16:10:53', '2018-07-26 16:10:53'),
(174, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 16:11:28', '2018-07-26 16:11:28'),
(175, 'al4cdeveloper@gmail.com', '', '12312312', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 16:12:22', '2018-07-26 16:12:22'),
(176, 'al4cdeveloper@gmail.com', '', '22423423', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 16:16:04', '2018-07-26 16:16:04'),
(177, 'al4cdeveloper@gmail.com', '', '22423423', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 16:48:28', '2018-07-26 16:48:28'),
(178, 'al4cdeveloper@gmail.com', '', '22423423', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 17:34:41', '2018-07-26 17:34:41'),
(179, 'al4cdeveloper@gmail.com', '', '22423423', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 17:50:26', '2018-07-26 17:50:26'),
(180, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 17:51:58', '2018-07-26 17:51:58'),
(181, 'al4cdeveloper@gmail.com', '', '123123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 17:56:41', '2018-07-26 17:56:41'),
(182, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 19:34:20', '2018-07-26 19:34:20'),
(183, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 19:36:41', '2018-07-26 19:36:41'),
(184, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 19:36:52', '2018-07-26 19:36:52'),
(185, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:16:02', '2018-07-26 20:16:02'),
(186, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:16:24', '2018-07-26 20:16:24'),
(187, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:16:35', '2018-07-26 20:16:35'),
(188, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:17:56', '2018-07-26 20:17:56'),
(189, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:18:21', '2018-07-26 20:18:21'),
(190, 'al4cdeveloper@gmail.com', '', '3456789', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:21:38', '2018-07-26 20:21:38'),
(191, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:46:47', '2018-07-26 20:46:47'),
(192, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:47:16', '2018-07-26 20:47:16'),
(193, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-26 20:48:50', '2018-07-26 20:48:50'),
(194, 'al4cdeveloper@gmail.com', '', '1011010101', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 15:05:56', '2018-07-30 15:05:56'),
(195, 'al4cdeveloper@gmail.com', '', '1011010101', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 15:08:43', '2018-07-30 15:08:43'),
(196, 'al4cdeveloper@gmail.com', '', '1011010101', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 15:11:33', '2018-07-30 15:11:33'),
(197, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 15:15:31', '2018-07-30 15:15:31'),
(198, 'al4cdeveloper@gmail.com', '', '2342', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 17:44:31', '2018-07-30 17:44:31'),
(199, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 17:45:30', '2018-07-30 17:45:30'),
(200, 'al4cdeveloper@gmail.com', '', '123123', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 17:52:29', '2018-07-30 17:52:29'),
(201, 'al4cdeveloper@gmail.com', '', '232342', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 18:03:17', '2018-07-30 18:03:17'),
(202, 'al4cdeveloper@gmail.com', '', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 19:47:28', '2018-07-30 19:47:28'),
(203, 'al4cdeveloper@gmail.com', '', '1111111', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 19:48:55', '2018-07-30 19:48:55'),
(204, 'al4cdeveloper@gmail.com', '', '1111111', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 19:49:07', '2018-07-30 19:49:07'),
(205, 'al4cdeveloper@gmail.com', 'CC', '12345', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-07-30 21:34:01', '2018-07-30 21:34:01'),
(206, 'al4cdeveloper@gmail.com', 'CC', '123456', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-08-03 17:48:32', '2018-08-03 17:48:32'),
(207, 'al4cdeveloper@gmail.com', 'CE', '1234567', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-08-03 17:56:07', '2018-08-03 17:56:07'),
(208, 'al4cdeveloper@gmail.com', 'CC', '234234', 'Sergio David', 'Hernandez Goyeneche', 'cra 147 # 142 50, cra 142#134-33', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', '3203233082', NULL, '2018-08-13 19:42:57', '2018-08-13 19:42:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `avatar` varchar(131) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotionals_mails` tinyint(1) NOT NULL DEFAULT '0',
  `password_change` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(131) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `confirmation_code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `address`, `phone`, `role`, `avatar`, `country`, `region`, `city`, `promotionals_mails`, `password_change`, `password`, `status`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'David', 'Hernandez', 'sergio.hernandez@gmail.com', 'cra 147 # 142 50, cra 142#134-33', '3203233082', 'user', '/images/profile/Profile_20180116134012323574128.wall.jpg', NULL, NULL, NULL, 0, 1, '$2y$10$9t1PMSkLuUGWRNQsWLUnUuxqNSkldBb2lfAaNmM2zINUxzosN41fG', 'pending', '', 's6hoPx3LLURFInbpIZ0cqw9sbzXk1PJNqHtUeXWgH2lIFNDKuEI46aWjmMQI', '2017-11-25 14:55:25', '2018-06-29 20:13:40'),
(2, 'Desarrollo Web Guía de Rutas por Colombia', '', 'desarrollo@rutascolombia.com', '', '', 'user', 'https://lh3.googleusercontent.com/-YQWVhiZJJoU/AAAAAAAAAAI/AAAAAAAAAAA/AAnnY7qpPZdOqC-LVqp99S6T60eRE-x7EQ/mo/photo.jpg?sz=50', NULL, NULL, NULL, 0, 0, '', 'active', '', 'vkhDRYyCJNn5XzwShXJQ6VFq2dhnQ4JMgeRIP5rgkTfOPxcBXc6Ql8VFmP3V', '2018-07-10 15:24:22', '2018-07-10 15:24:22'),
(3, 'Sergio David', 'Hernandez Goyeneche', 'al4cdeveloper@gmail.com', 'cra 147 # 142 50, cra 142#134-33', '3203233082', 'user', 'https://lh6.googleusercontent.com/-zB0LCBQB5TE/AAAAAAAAAAI/AAAAAAAAACA/JB3MSE0rttQ/photo.jpg?sz=50', 'co', 'Distrito Capital de Bogota', 'Bogota  D.C.', 1, 0, '$2y$10$oA7l8Gf6SfJWWLgtv13yNu3ob2tFKcdQ0DuSFvuaHOeLHY6D.cPfK', 'active', NULL, 'tSePOnYVyV4av71xdKW0Vmawro0dK3CKYAT2AvBpBv54wmxp7QDW5VDGJi51', '2018-07-11 16:08:39', '2018-08-17 17:55:25'),
(4, 'Sergio', 'Hernandez', 'sergio.hernandez.222goyeneche@gmail.com', 'cra 147 # 142 50, cra 142#134-33', '3203233082', 'user', NULL, NULL, NULL, NULL, 0, 1, '$2y$10$WT.DUO/3gnk6kUqOdkER.OlMwezyh0N/za7sqfTRWJEj4IKkjWeee', 'active', '', NULL, '2018-07-12 16:59:11', '2018-07-12 16:59:11'),
(5, 'Sergio', 'Hernandez', 'sergio.hernandez.goyenech2323423e@gmail.com', NULL, '3203233082', 'user', NULL, 'co', 'Providencia y Santa Catalina, Departamento de Archipielago de San Andres', 'San Andres', 1, 1, '$2y$10$b6aQpcgefmlZcs.UouOMX.I.8WDi5P2/KC1zHfWqIZQooqPeype7G', 'active', '', NULL, '2018-07-19 17:10:20', '2018-07-23 16:44:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id_video` int(10) UNSIGNED NOT NULL,
  `link_video` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id_video`, `link_video`, `type_relation`, `fk_relation`, `created_at`, `updated_at`) VALUES
(1, 'AoZ569Pu2PY', 'municipality', 1, '2018-05-18 18:18:07', '2018-05-18 18:18:07'),
(2, 'qExd-3oCTl4', 'municipality', 4, '2018-05-18 18:30:58', '2018-05-18 18:30:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallpapers`
--

CREATE TABLE `wallpapers` (
  `id_wallpaper` int(10) UNSIGNED NOT NULL,
  `link_image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `primary_text` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secundary_text` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `second_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_text_button` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_text_button` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `wallpapers`
--

INSERT INTO `wallpapers` (`id_wallpaper`, `link_image`, `primary_text`, `secundary_text`, `first_url`, `second_url`, `first_text_button`, `second_text_button`, `state`, `created_at`, `updated_at`) VALUES
(1, 'wallpapers/wall_201804261216191406070230.jpg', 'Darkar', 'vete a la versh!', '/', '/olol', 'Vete', 'A la versh!', 'activo', '2018-04-10 18:53:05', '2018-04-27 21:40:33'),
(2, 'wallpapers/wall_20180426114120737408976.png', 'Prueba', NULL, '/', NULL, 'boton 1', NULL, 'activo', '2018-04-26 16:41:20', '2018-04-26 17:15:04');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id_department`),
  ADD KEY `departments_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `ecosystem_categories`
--
ALTER TABLE `ecosystem_categories`
  ADD PRIMARY KEY (`id_ecosystem_category`);

--
-- Indices de la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD PRIMARY KEY (`id_establishment`),
  ADD KEY `establishments_fk_operator_foreign` (`fk_operator`),
  ADD KEY `establishments_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD PRIMARY KEY (`id_establishment_image`),
  ADD KEY `establishment_images_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD PRIMARY KEY (`id_establishment_schedule`),
  ADD KEY `establishment_schedules_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `establishmen_images`
--
ALTER TABLE `establishmen_images`
  ADD PRIMARY KEY (`id_establishment_image`),
  ADD KEY `establishmen_images_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD PRIMARY KEY (`id_site`),
  ADD KEY `interest_sites_fk_category_foreign` (`fk_category`),
  ADD KEY `interest_sites_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD PRIMARY KEY (`id_site_image`),
  ADD KEY `interest_site_images_fk_site_foreign` (`fk_site`);

--
-- Indices de la tabla `keydatas`
--
ALTER TABLE `keydatas`
  ADD PRIMARY KEY (`id_keydata`),
  ADD KEY `keydatas_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD PRIMARY KEY (`id_municipality`),
  ADD KEY `municipalities_fk_department_foreign` (`fk_department`);

--
-- Indices de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD PRIMARY KEY (`id_municipality_image`),
  ADD KEY `municipality_images_fk_municipality_foreign` (`fk_municipality`);

--
-- Indices de la tabla `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id_operator`),
  ADD UNIQUE KEY `operators_email_unique` (`email`);

--
-- Indices de la tabla `operator_password_resets`
--
ALTER TABLE `operator_password_resets`
  ADD KEY `operator_password_resets_email_index` (`email`),
  ADD KEY `operator_password_resets_token_index` (`token`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `orders_fk_user_foreign` (`fk_user`);

--
-- Indices de la tabla `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id_order_product`),
  ADD KEY `order_products_fk_order_foreign` (`fk_order`),
  ADD KEY `order_products_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `order_product_items`
--
ALTER TABLE `order_product_items`
  ADD PRIMARY KEY (`id_product_item`),
  ADD KEY `order_product_items_fk_order_product_foreign` (`fk_order_product`),
  ADD KEY `order_product_items_fk_service_item_foreign` (`fk_service_item`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id_page`);

--
-- Indices de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  ADD PRIMARY KEY (`id_part`);

--
-- Indices de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD PRIMARY KEY (`id_part_page`),
  ADD KEY `page_part_pages_fk_page_foreign` (`fk_page`),
  ADD KEY `page_part_pages_fk_part_foreign` (`fk_part`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD PRIMARY KEY (`id_pattern`),
  ADD KEY `patterns_fk_customer_foreign` (`fk_customer`);

--
-- Indices de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD PRIMARY KEY (`id_pattern_part`),
  ADD KEY `pattern_parts_fk_pattern_foreign` (`fk_pattern`),
  ADD KEY `pattern_parts_fk_pagepart_foreign` (`fk_pagepart`);

--
-- Indices de la tabla `pay_reports`
--
ALTER TABLE `pay_reports`
  ADD PRIMARY KEY (`id_report`);

--
-- Indices de la tabla `plates`
--
ALTER TABLE `plates`
  ADD PRIMARY KEY (`id_plate`),
  ADD KEY `plates_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id_poll`);

--
-- Indices de la tabla `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD PRIMARY KEY (`id_region_image`),
  ADD KEY `region_images_fk_region_foreign` (`fk_region`);

--
-- Indices de la tabla `reporters`
--
ALTER TABLE `reporters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reporters_email_unique` (`email`);

--
-- Indices de la tabla `reporter_password_resets`
--
ALTER TABLE `reporter_password_resets`
  ADD KEY `reporter_password_resets_email_index` (`email`),
  ADD KEY `reporter_password_resets_token_index` (`token`);

--
-- Indices de la tabla `report_reservations`
--
ALTER TABLE `report_reservations`
  ADD PRIMARY KEY (`id_report_reservation`),
  ADD KEY `report_reservations_fk_report_foreign` (`fk_report`),
  ADD KEY `report_reservations_fk_reservation_foreign` (`fk_reservation`);

--
-- Indices de la tabla `reprograms`
--
ALTER TABLE `reprograms`
  ADD PRIMARY KEY (`id_reprogram`),
  ADD KEY `reprograms_fk_reservation_foreign` (`fk_reservation`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id_reservation`),
  ADD KEY `reservations_fk_service_foreign` (`fk_service`),
  ADD KEY `reservations_fk_user_foreign` (`fk_user`),
  ADD KEY `reservations_fk_order_product_foreign` (`fk_order_product`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id_room`),
  ADD KEY `rooms_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id_schedule`),
  ADD KEY `schedules_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `services_fk_service_category_foreign` (`fk_service_category`),
  ADD KEY `services_fk_ecosystem_category_foreign` (`fk_ecosystem_category`);

--
-- Indices de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  ADD PRIMARY KEY (`id_service_establishment`);

--
-- Indices de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  ADD PRIMARY KEY (`id_service_establishment_pivot`),
  ADD KEY `service_establishment_pivots_fk_service_foreign` (`fk_service`),
  ADD KEY `service_establishment_pivots_fk_establishment_foreign` (`fk_establishment`);

--
-- Indices de la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD PRIMARY KEY (`id_service_item`),
  ADD KEY `service_items_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD PRIMARY KEY (`id_service_operator`),
  ADD KEY `service_operators_fk_service_foreign` (`fk_service`),
  ADD KEY `service_operators_fk_operator_foreign` (`fk_operator`);

--
-- Indices de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD PRIMARY KEY (`id_picture`),
  ADD KEY `service_pictures_fk_service_foreign` (`fk_service`);

--
-- Indices de la tabla `shipping_orders`
--
ALTER TABLE `shipping_orders`
  ADD PRIMARY KEY (`orders_id`),
  ADD KEY `shipping_orders_orders_id_index` (`orders_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id_video`);

--
-- Indices de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  ADD PRIMARY KEY (`id_wallpaper`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id_department` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ecosystem_categories`
--
ALTER TABLE `ecosystem_categories`
  MODIFY `id_ecosystem_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `establishments`
--
ALTER TABLE `establishments`
  MODIFY `id_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  MODIFY `id_establishment_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  MODIFY `id_establishment_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `establishmen_images`
--
ALTER TABLE `establishmen_images`
  MODIFY `id_establishment_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  MODIFY `id_site` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `interest_site_categories`
--
ALTER TABLE `interest_site_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  MODIFY `id_site_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `keydatas`
--
ALTER TABLE `keydatas`
  MODIFY `id_keydata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT de la tabla `municipalities`
--
ALTER TABLE `municipalities`
  MODIFY `id_municipality` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  MODIFY `id_municipality_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `operators`
--
ALTER TABLE `operators`
  MODIFY `id_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT de la tabla `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id_order_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `order_product_items`
--
ALTER TABLE `order_product_items`
  MODIFY `id_product_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `page_parts`
--
ALTER TABLE `page_parts`
  MODIFY `id_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  MODIFY `id_part_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `patterns`
--
ALTER TABLE `patterns`
  MODIFY `id_pattern` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  MODIFY `id_pattern_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `pay_reports`
--
ALTER TABLE `pay_reports`
  MODIFY `id_report` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `plates`
--
ALTER TABLE `plates`
  MODIFY `id_plate` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `polls`
--
ALTER TABLE `polls`
  MODIFY `id_poll` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `regions`
--
ALTER TABLE `regions`
  MODIFY `id_region` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `region_images`
--
ALTER TABLE `region_images`
  MODIFY `id_region_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reporters`
--
ALTER TABLE `reporters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `report_reservations`
--
ALTER TABLE `report_reservations`
  MODIFY `id_report_reservation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `reprograms`
--
ALTER TABLE `reprograms`
  MODIFY `id_reprogram` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id_reservation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT de la tabla `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id_room` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id_schedule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id_service` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `service_establishments`
--
ALTER TABLE `service_establishments`
  MODIFY `id_service_establishment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `service_establishment_pivots`
--
ALTER TABLE `service_establishment_pivots`
  MODIFY `id_service_establishment_pivot` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `service_items`
--
ALTER TABLE `service_items`
  MODIFY `id_service_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `service_operators`
--
ALTER TABLE `service_operators`
  MODIFY `id_service_operator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  MODIFY `id_picture` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id_video` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `wallpapers`
--
ALTER TABLE `wallpapers`
  MODIFY `id_wallpaper` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `establishments`
--
ALTER TABLE `establishments`
  ADD CONSTRAINT `establishments_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`),
  ADD CONSTRAINT `establishments_fk_operator_foreign` FOREIGN KEY (`fk_operator`) REFERENCES `operators` (`id_operator`);

--
-- Filtros para la tabla `establishment_images`
--
ALTER TABLE `establishment_images`
  ADD CONSTRAINT `establishment_images_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `establishment_schedules`
--
ALTER TABLE `establishment_schedules`
  ADD CONSTRAINT `establishment_schedules_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `establishmen_images`
--
ALTER TABLE `establishmen_images`
  ADD CONSTRAINT `establishmen_images_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD CONSTRAINT `interest_sites_fk_category_foreign` FOREIGN KEY (`fk_category`) REFERENCES `interest_site_categories` (`id_category`),
  ADD CONSTRAINT `interest_sites_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD CONSTRAINT `interest_site_images_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `keydatas`
--
ALTER TABLE `keydatas`
  ADD CONSTRAINT `keydatas_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `municipalities`
--
ALTER TABLE `municipalities`
  ADD CONSTRAINT `municipalities_fk_department_foreign` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`id_department`);

--
-- Filtros para la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD CONSTRAINT `municipality_images_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_fk_order_foreign` FOREIGN KEY (`fk_order`) REFERENCES `orders` (`id_order`),
  ADD CONSTRAINT `order_products_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `order_product_items`
--
ALTER TABLE `order_product_items`
  ADD CONSTRAINT `order_product_items_fk_order_product_foreign` FOREIGN KEY (`fk_order_product`) REFERENCES `order_products` (`id_order_product`),
  ADD CONSTRAINT `order_product_items_fk_service_item_foreign` FOREIGN KEY (`fk_service_item`) REFERENCES `service_items` (`id_service_item`);

--
-- Filtros para la tabla `page_part_pages`
--
ALTER TABLE `page_part_pages`
  ADD CONSTRAINT `page_part_pages_fk_page_foreign` FOREIGN KEY (`fk_page`) REFERENCES `pages` (`id_page`),
  ADD CONSTRAINT `page_part_pages_fk_part_foreign` FOREIGN KEY (`fk_part`) REFERENCES `page_parts` (`id_part`);

--
-- Filtros para la tabla `patterns`
--
ALTER TABLE `patterns`
  ADD CONSTRAINT `patterns_fk_customer_foreign` FOREIGN KEY (`fk_customer`) REFERENCES `customers` (`id_customer`);

--
-- Filtros para la tabla `pattern_parts`
--
ALTER TABLE `pattern_parts`
  ADD CONSTRAINT `pattern_parts_fk_pagepart_foreign` FOREIGN KEY (`fk_pagepart`) REFERENCES `page_part_pages` (`id_part_page`),
  ADD CONSTRAINT `pattern_parts_fk_pattern_foreign` FOREIGN KEY (`fk_pattern`) REFERENCES `patterns` (`id_pattern`);

--
-- Filtros para la tabla `plates`
--
ALTER TABLE `plates`
  ADD CONSTRAINT `plates_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `region_images`
--
ALTER TABLE `region_images`
  ADD CONSTRAINT `region_images_fk_region_foreign` FOREIGN KEY (`fk_region`) REFERENCES `regions` (`id_region`);

--
-- Filtros para la tabla `report_reservations`
--
ALTER TABLE `report_reservations`
  ADD CONSTRAINT `report_reservations_fk_report_foreign` FOREIGN KEY (`fk_report`) REFERENCES `pay_reports` (`id_report`),
  ADD CONSTRAINT `report_reservations_fk_reservation_foreign` FOREIGN KEY (`fk_reservation`) REFERENCES `reservations` (`id_reservation`);

--
-- Filtros para la tabla `reprograms`
--
ALTER TABLE `reprograms`
  ADD CONSTRAINT `reprograms_fk_reservation_foreign` FOREIGN KEY (`fk_reservation`) REFERENCES `reservations` (`id_reservation`);

--
-- Filtros para la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_fk_order_product_foreign` FOREIGN KEY (`fk_order_product`) REFERENCES `order_products` (`id_order_product`),
  ADD CONSTRAINT `reservations_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`),
  ADD CONSTRAINT `reservations_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_fk_establishment_foreign` FOREIGN KEY (`fk_establishment`) REFERENCES `establishments` (`id_establishment`);

--
-- Filtros para la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fk_ecosystem_category_foreign` FOREIGN KEY (`fk_ecosystem_category`) REFERENCES `ecosystem_categories` (`id_ecosystem_category`),
  ADD CONSTRAINT `services_fk_service_category_foreign` FOREIGN KEY (`fk_service_category`) REFERENCES `service_categories` (`id_category`);

--
-- Filtros para la tabla `service_items`
--
ALTER TABLE `service_items`
  ADD CONSTRAINT `service_items_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD CONSTRAINT `service_operators_ibfk_1` FOREIGN KEY (`fk_service`) REFERENCES `services` (`id_service`);

--
-- Filtros para la tabla `shipping_orders`
--
ALTER TABLE `shipping_orders`
  ADD CONSTRAINT `shipping_orders_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id_order`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
