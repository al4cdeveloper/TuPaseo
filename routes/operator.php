<?php
use Session as SessionF;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('operator')->user();


    $services = Auth::user()->services->count();
    $restaurants = Auth::user()->restaurants->count();
    $hotels = Auth::user()->hotels->count();

    return view('operator.home',compact('services','restaurants','hotels'));
})->name('home');



//Selección de servicio a crear
Route::get('newservice',function()
{
	$services = Auth::user()->services->count();
    $restaurants = Auth::user()->restaurants->count();
    $hotels = Auth::user()->hotels->count();

    if($services==3 && $restaurants == 3 && $hotels==3)
    {
    	SessionF::flash('message-error','No se permite la creación de más servicios');
    	return redirect()->back();
    }

	return view('operator.selectNewTypeService',compact('services','restaurants','hotels'));
});
Route::get('establishment/{type}','Operator\establishmentController@index');
Route::get('establishment/{type}/create','Operator\establishmentController@create');
Route::post('establishment/{type}/store','Operator\establishmentController@store');
Route::get('establishment/edit/{slug}','Operator\establishmentController@edit');
Route::put('establishment/update/{slug}','Operator\establishmentController@update');
Route::get('establishment/desactivate/{slug}','Operator\establishmentController@desactivate');
Route::get('establishment/activate/{slug}','Operator\establishmentController@activate');
Route::get('establishment/images/{slug}','Operator\establishmentController@images');
Route::put('establishment/upload_images/{id}','Operator\establishmentController@upload_images');
Route::get('establishment/delete_image/{id}','Operator\establishmentController@delete_image');
Route::post('establishment/update_images/','Operator\establishmentController@update_images');

Route::resource('services','Operator\serviceController');
Route::post('updateservice/{id}','Operator\serviceController@update');
Route::get('shedule/{slug}','Operator\serviceController@shedule');
Route::post('updateshedule','Operator\serviceController@updateshedule');
Route::get('reservas','Operator\serviceController@listService');
Route::get('calendar','Operator\serviceController@calendarReservation');
Route::post('reprogram','Operator\serviceController@reprogramReservation');

Route::get('serviceimages/{id}','Operator\serviceController@images');
Route::put('service/upload_images/{id}','Operator\serviceController@upload_images');
Route::get('service/delete_image/{id}','Operator\serviceController@delete_image');
Route::post('service/update_images/','Operator\serviceController@update_images');

Route::get('servicesuccess/{id}','Operator\serviceController@serviceSuccess');

Route::get('items/{id}','Operator\serviceController@indexitem');
Route::get('items/create/{id}','Operator\serviceController@createItem');
Route::post('items/store','Operator\serviceController@storeItem');
Route::get('items/edit/{id}','Operator\serviceController@editItem');
Route::put('items/update/{id}','Operator\serviceController@updateItem');
Route::get('items/desactivate/{id}','Operator\serviceController@desactivate');
Route::get('items/activate/{id}','Operator\serviceController@activate');

Route::get('profile','Operator\operatorController@show');
Route::post('update','Operator\operatorController@update');
Route::get('bankinformation','Operator\operatorController@bankInformation');
Route::post('updateinfo','Operator\operatorController@updateBankInformation');

Route::get('preferences','Operator\operatorController@preferences');
Route::post('updatepreferences','Operator\operatorController@updatepreferences');

Route::get('service/activate/{slug}','Operator\serviceController@activateService');
Route::get('service/desactivate/{slug}','Operator\serviceController@desactivateService');

Route::get('pays','Operator\EconomyController@index');
Route::get('generatepayreport/{id}','Operator\EconomyController@generatePayReport');