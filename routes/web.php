<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Service;
use App\Models\ServiceOperator;
use Illuminate\Http\Request;

/*Route::get('/', function () 
{
	$services = Service::all();
    $servicesO = ServiceOperator::where('state','Activo')->get();

    return view('welcome',compact('services','servicesO'));
});
*/
Route::get('/',function(){
  return redirect('/home');
});

//LEGALIDAD ;V

Route::get('terminos','User\userController@publicTerms');
Route::get('terminos_operador','User\userController@operatorTerms');
Route::get('politica_proteccion_datos','User\userController@policyDataProtection');
Route::get('politica_sostenibilidad','User\userController@policySostenibility');
Route::get('about','User\userController@about');


Route::group(['prefix' => 'vue'], function () 
{
    Route::get('/home','User\VueFrontController@home');
    Route::post('/gettypeskitchen','User\VueFrontController@getTypesKitchen');
    Route::get('/pattern/{page}','User\VueFrontController@pattern');
    Route::get('/sumclick/{id}','User\VueFrontController@sumClick');
    Route::get('/navcomponent','User\VueFrontController@navComponent');
    Route::get('/region/{slug}','User\VueFrontController@region');
    Route::get('/municipality/{slug}','User\VueFrontController@municipality');
    Route::get('/restaurant/{slug}','User\VueFrontController@restaurant');
    Route::get('/hotel/{slug}','User\VueFrontController@hotel');
    Route::get('/service/{slug}','User\VueFrontController@service');
    Route::get('/ecosystem/{slug}','User\VueFrontController@ecosystem');
    Route::any('/services','User\VueFrontController@services');
    Route::any('/hotels','User\VueFrontController@hotels');
    Route::any('/restaurants','User\VueFrontController@restaurants');
});


Route::group(['prefix' => 'home'], function () {
  Route::get('/{vue_capture?}', function () {
   return view('welcome');
  })->where('vue_capture', '[\/\w\.-]*');

});

Route::get('services/{slug}','User\ServiceController@index');

//Contacto
Route::get('contact','User\userController@contact');
Route::post('storeemail','User\userController@saveEmail');

Auth::routes();

//ruta de callback PAYU
Route::any('/callbackpayu/{id}','User\CartController@callbackpayuWithId');
Route::any('/callbackpayu','Api\ApiController@responsePayU');

Route::get('/viewcart','User\CartController@viewCart');
Route::post('/pay_cart','User\CartController@payCart');

Route::get('reservation','User\ReservationController@index');
Route::post('make-reservation','User\ReservationController@prereservation');
Route::get('serviceslist','User\ReservationController@returnServices');

Route::put('add_cart/{id}','User\CartController@add_cart');
Route::get('allcart','User\CartController@allcart');
Route::get('subtotal','User\CartController@subtotal');
Route::get('remove_cart/{rowId}','User\CartController@removeCart');
Route::get('remove_cart_pay/{rowId}','User\CartController@removeCartPay');
Route::get('delete_cart','User\CartController@deleteCart');
Route::post('logincart','User\userController@loginCart')->name('logincart');
Route::post('registercart','User\userController@registerCart')->name('registercart');

Route::get('freehours/{id}/{date}/{cuantity}','User\ReservationController@freeHours');
Route::post('/getprovince','User\CartController@getProvince');

//Verify code ACCOUNT
Route::get('operator/register/verify/{code}', 'Auth\ActivateAccountController@VerifyCodeOperator');
Route::get('user/register/verify/{code}', 'Auth\ActivateAccountController@VerifyCodeUser');
Route::get('verifycode/resend','Auth\ActivateAccountController@showResendForm');
Route::post('verifycode/resend','Auth\ActivateAccountController@validateEmail');


Route::get('/logout','Auth\LoginController@logout');
Route::group(['prefix' => 'operator'], function () 
{


  Route::get('/','Operator\operatorController@index');
  Route::get('/create','Operator\operatorController@index');

  Route::get('/login', 'OperatorAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'OperatorAuth\LoginController@login');
  Route::post('/logout', 'OperatorAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'OperatorAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'OperatorAuth\RegisterController@register');

  Route::post('/password/email', 'OperatorAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'OperatorAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'OperatorAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'OperatorAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin'], function () {
	Route::get('/',function()
{
  if(Auth::guard('admin')->user())
        {
            return redirect('/admin/home');
        }
        else
        {
            return redirect('/login');
        }
});
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::get('user/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');



Route::post('savecomment','User\ReservationController@saveComment');
Route::get('/user/home', 'HomeController@index')->name('home');
Route::post('encuesta','User\userController@sendQuestions');
Route::post('mailchimp_register', 'User\userController@mailchimpRegister');
Route::get('search','User\ServiceController@search');

Route::middleware('user')->group(function () {
    Route::post('updateprofile','User\userController@updateprofile');
    Route::get('logeruser/reservation','User\ReservationController@reservationLogerUser');
});
    Route::get('userprofile','User\userController@profile')->middleware('auth');



Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');


Route::group(['prefix' => 'user'], function () {
  Route::get('/login', function()
  {
    return redirect('/login');
  })->name('login');
  Route::post('/login', 'Auth\LoginController@login');
  Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

  Route::get('/register', function()
{
  return redirect('/register');

})->name('register');
  Route::post('/register', 'Auth\RegisterController@register');

  Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
});
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'Auth\LoginController@login');
  Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

  Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'Auth\RegisterController@register');

  Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');



Route::group(['prefix' => 'reporter'], function () {
  //Route::get('/login', 'ReporterAuth\LoginController@showLoginForm')->name('login');
  Route::get('/login', function()
    {
      return redirect('/login');
    });
  Route::get('/register', function()
    {
      return redirect('/login');
    });
  Route::post('/login', 'ReporterAuth\LoginController@login');
  Route::post('/logout', 'ReporterAuth\LoginController@logout')->name('logout');

  //Route::get('/register', 'ReporterAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'ReporterAuth\RegisterController@register');

  Route::post('/password/email', 'ReporterAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'ReporterAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'ReporterAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'ReporterAuth\ResetPasswordController@showResetForm');
});


