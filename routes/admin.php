<?php
use App\Operator;
use App\Models\Reservation;
use App\Models\ServiceOperator;
use App\Models\Poll;
use App\Models\Contact;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    $operators = count(Operator::all());
    $reservations = Reservation::where('state','Por realizar')->count();
    $services = ServiceOperator::where('state','Activo')->count();
    $encuestas = Poll::where('state','new')->count();
    $contacto = Contact::where('state','new')->count();

    return view('admin.home',compact('operators','reservations','services','encuestas','contacto'));
})->name('home');

Route::resource('admin','Admin\UserController');
Route::post('admin/update/{id}','Admin\UserController@update');
Route::get('admin/restorepassword/{id}','Admin\UserController@restartPassword');
Route::post('admin/store','Admin\UserController@store');
Route::get('admin/desactivate/{id}','Admin\UserController@desactivate');
Route::get('admin/activate/{id}','Admin\UserController@activate');

Route::get('operators','Admin\adminController@operators');
Route::get('services','Admin\adminController@services');
Route::get('detailservice/{slug}','Admin\adminController@detailService');
Route::get('complreservation/{nationalregister}','Admin\adminController@completeReservationSpecific');

//ACTIVITIES

    //ROUTE SERVICE CATEGORY
    Route::resource('activities/servicecategory','Admin\ServiceCategoryController');
    Route::post('activities/servicecategory/store','Admin\ServiceCategoryController@store');
    Route::post('activities/servicecategory/update/{id}','Admin\ServiceCategoryController@update');
    //ROUTE ECOSYSTEM
    Route::resource('activities/ecosystem','Admin\EcosystemController');
    Route::post('activities/ecosystem/store','Admin\EcosystemController@store');
    Route::get('activities/ecosystem/edit/{id}','Admin\EcosystemController@edit');
    Route::post('activities/ecosystem/update/{id}','Admin\EcosystemController@update');
    //ROUTE SERVICES
    Route::resource('activities/service','Admin\ServiceController');
    Route::post('activities/service/store','Admin\ServiceController@store');
    Route::get('activities/service/edit/{slug}','Admin\ServiceController@edit');
    Route::post('activities/service/update/{id}','Admin\ServiceController@update');

    

//ROUTE REGIONS
Route::resource('regions','Admin\RegionController');
Route::post('regions/store','Admin\RegionController@store');
Route::get('regions/edit/{slug}','Admin\RegionController@edit');
Route::post('regions/update/{id}','Admin\RegionController@update');
Route::get('regions/desactivate/{id}','Admin\RegionController@desactivate');
Route::get('regions/activate/{id}','Admin\RegionController@activate');
Route::get('regions/images/{slug}','Admin\RegionController@images');
Route::put('regions/upload_images/{id}','Admin\RegionController@upload_images');
Route::get('regions/delete_image/{id}','Admin\RegionController@delete_image');
Route::post('regions/update_image','Admin\RegionController@updateImage');
//ROUTE DEPARTMENT
Route::resource('departments','Admin\DepartmentController');
Route::post('departments/store','Admin\DepartmentController@store');
Route::get('departments/edit/{slug}','Admin\DepartmentController@edit');
Route::post('departments/update/{id}','Admin\DepartmentController@update');
Route::get('departments/desactivate/{id}','Admin\DepartmentController@desactivate');
Route::get('departments/activate/{id}','Admin\DepartmentController@activate');
//ROUTE MUNICIPALY
route::resource('municipalities','Admin\MunicipalityController');
Route::post('municipalities/store','Admin\MunicipalityController@store');
Route::get('municipalities/edit/{slug}','Admin\MunicipalityController@edit');
Route::post('municipalities/update/{id}','Admin\MunicipalityController@update');
Route::get('municipalities/desactivate/{id}','Admin\MunicipalityController@desactivate');
Route::get('municipalities/activate/{id}','Admin\MunicipalityController@activate');
Route::get('municipalities/images/{slug}','Admin\MunicipalityController@images');
Route::put('municipalities/upload_images/{id}','Admin\MunicipalityController@upload_images');
Route::get('municipalities/delete_image/{id}','Admin\MunicipalityController@delete_image');
Route::post('municipalities/upload_video/{id}','Admin\MunicipalityController@upload_video');
Route::delete('municipalities/delete_video/{id}','Admin\MunicipalityController@delete_video');
Route::post('municipalities/update_image','Admin\MunicipalityController@updateImage');
Route::get('municipalities/keydata/{slug}','Admin\MunicipalityController@keydata');
Route::post('municipalities/keydata/{id}','Admin\MunicipalityController@saveKeydata');
//INTEREST SITES
	//CATEGORY
	Route::get('interestsites/category','Admin\InterestSiteController@indexCategory');
	Route::get('interestsites/category/create','Admin\InterestSiteController@createCategory');
	Route::post('interestsites/category/store','Admin\InterestSiteController@storeCategory');
	Route::get('interestsites/category/edit/{slug}','Admin\InterestSiteController@editCategory');
	Route::post('interestsites/category/update/{id}','Admin\InterestSiteController@updateCategory');
	Route::get('interestsites/category/desactivate/{id}','Admin\InterestSiteController@desactivateCategory');
	Route::get('interestsites/category/activate/{id}','Admin\InterestSiteController@activateCategory');

Route::resource('interestsites','Admin\InterestSiteController');
Route::post('interestsites/store','Admin\InterestSiteController@store');
Route::get('interestsites/edit/{slug}','Admin\InterestSiteController@edit');
Route::post('interestsites/update/{id}','Admin\InterestSiteController@update');
Route::get('interestsites/desactivate/{id}','Admin\InterestSiteController@desactivate');
Route::get('interestsites/activate/{id}','Admin\InterestSiteController@activate');
Route::get('interestsites/images/{slug}','Admin\InterestSiteController@images');
Route::put('interestsites/upload_images/{id}','Admin\InterestSiteController@upload_images');
Route::delete('interestsites/delete_image/{id}','Admin\InterestSiteController@delete_image');

//WALLPAPERS
Route::post('wallpapers/store','Admin\WallpaperController@store');
Route::get('wallpapers/edit/{id}','Admin\WallpaperController@edit');
Route::put('wallpapers/update/{id}','Admin\WallpaperController@update');
Route::resource('wallpapers','Admin\WallpaperController');
Route::get('wall/desactivate/{id}','Admin\WallpaperController@desactivate');
Route::get('wall/activate/{id}','Admin\WallpaperController@activate');

//CUSTOMERS
Route::resource('customers','Admin\CustomerController');
Route::post('customers/store','Admin\CustomerController@store');
Route::get('customers/edit/{slug}','Admin\CustomerController@edit');
Route::post('customers/update/{id}','Admin\CustomerController@update');
Route::get('customers/desactivate/{id}','Admin\CustomerController@desactivate');
Route::get('customers/activate/{id}','Admin\CustomerController@activate');

Route::resource('pageparts','Admin\PagePartController');
Route::post('pageparts/store','Admin\PagePartController@store');
Route::get('pageparts/edit/{slug}','Admin\PagePartController@edit');
Route::post('pageparts/update/{id}','Admin\PagePartController@update');
Route::get('pageparts/desactivate/{id}','Admin\PagePartController@desactivate');
Route::get('pageparts/activate/{id}','Admin\PagePartController@activate');

Route::resource('pages','Admin\PageController');
Route::post('pages/store','Admin\PageController@store');
Route::get('pages/edit/{slug}','Admin\PageController@edit');
Route::post('pages/update/{id}','Admin\PageController@update');

//PATTERNS-> PUBLICIDAD! :D 
Route::resource('patterns','Admin\PatternController');
Route::post('patterns/store','Admin\PatternController@store');
Route::get('patterns/edit/{id}','Admin\PatternController@edit');
Route::post('patterns/update/{id}','Admin\PatternController@update');
Route::get('patterns/desactivate/{id}','Admin\PatternController@desactivate');
Route::get('patterns/activate/{id}','Admin\PatternController@activate');

//Operator
Route::get('operators','Admin\adminController@operators');
Route::post('operators/update/{id}','Admin\adminController@updateOperator');
Route::get('operators/services/{id}','Admin\adminController@operatorServices');
Route::get('operators/services/edit/{id}','Admin\adminController@editServiceOperator');
Route::put('operators/services/update/{id}','Admin\adminController@updateServiceOperator');
Route::delete('operators/service/delete_image/{id}','Admin\adminController@delete_image');
Route::get('operators/serviceimages/{id}','Admin\adminController@imagesServiceOperator');
Route::put('operators/service/upload_images/{id}','Admin\adminController@upload_images');
Route::get('operators/service/desactivate/{id}','Admin\adminController@desactivate');
Route::get('operators/service/activate/{id}','Admin\adminController@activate');
Route::get('operators/items/{id}','Admin\adminController@itemsOperator');
Route::get('operators/items/edit/{id}','Admin\adminController@editItemOperator');
Route::put('operators/items/update/{id}','Admin\adminController@updateItemOperator');
Route::get('operators/items/desactivate/{id}','Admin\adminController@desactivateItem');
Route::get('operators/items/activate/{id}','Admin\adminController@activateItem');
Route::get('operators/desactivate/{id}','Admin\adminController@desactivateOperator');
Route::get('operators/activate/{id}','Admin\adminController@activateOperator');
Route::get('operators/establishment/edit/{slug}','Admin\adminController@editEstablishment');
Route::put('operators/establishment/update/{slug}','Admin\adminController@updateEstablishment');
Route::get('operators/establishment/images/{slug}','Admin\adminController@imagesEstablishment');
Route::get('operators/establishment/desactivate/{slug}','Admin\adminController@desactivateEstablishment');
Route::get('operators/establishment/activate/{slug}','Admin\adminController@activateEstablishment');
Route::get('operators/establishment/delete_image/{id}','Admin\adminController@delete_imageEstablishment');
Route::get('operators/establishment/{type}/{id_operator}','Admin\adminController@establishment');
Route::put('operators/establishment/upload_images/{id}','Admin\adminController@upload_imagesEstablishment');
Route::post('operators/establishment/update_images/','Admin\adminController@update_imagesEstablishment');
//ECONOMY
Route::get('economy/listreport','Admin\EconomyController@index');
Route::post('economy/generatereport','Admin\EconomyController@generateReport');
Route::get('economy/history','Admin\EconomyController@historyPays');
Route::get('economy/report/{id}/payreportreservation','Admin\EconomyController@generatePDFReservation');
Route::get('economy/checkpay/{id}','Admin\EconomyController@checkPay');
Route::get('economy/restartpay/{id}','Admin\EconomyController@restartPay');
Route::get('economy/cancelpay/{id}','Admin\EconomyController@cancelPay');
Route::get('economy/reactivatepay/{id}','Admin\EconomyController@reactivatePay');

//CONTACT
Route::resource('contact','Admin\ContactController');
Route::get('contact/{id}/archive','Admin\ContactController@archive');
Route::get('contact/{id}/active','Admin\ContactController@active');